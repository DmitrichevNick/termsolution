﻿using Client.MakePracticeDocs.Controls;
using Client.UICore;
using DatabaseConnector;

namespace Client.MakePracticeDocs
{
    /// <summary>
    /// Процесс создания документов практик.
    /// </summary>
    public class MakePracticeDocsProcess : UserProcess
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public MakePracticeDocsProcess(AppEnvironment environment) : base(environment, "Генерация документов практик")
        {
            AddStage(new ProcessStage("Выбор данных для генерации", true, 1), new ChooseControl(_environment));
            StagePanelVisible = true;
        }
    }
}
