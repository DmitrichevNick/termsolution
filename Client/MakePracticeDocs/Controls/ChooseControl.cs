﻿using System;
using DatabaseConnector;
using DocumentGeneration;
using DocumentWorker.InsertList;
using Helper;
using Manager;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Client.UICore;
using Entity;
using Manager.Base;

namespace Client.MakePracticeDocs.Controls
{
    /// <summary>
    /// Форма выбора групп для создания документов практик.
    /// </summary>
    public partial class ChooseControl : ContentControl
    {
        private AppEnvironment _environment;
        private DocumentMaker _docMaker;

        /// <summary>
        /// Конструктор
        /// </summary>
        public ChooseControl()
        {
            InitializeComponent();
            StagesPanelVisible = false;
            StatusBar.SetText("Выберите данные для генерации");

            _docMaker = new DocumentMaker();
            SelectGroupBox_EnabledChanged(null, null);
        }

        /// <summary>
        /// Конструктор, принимающий окружение приложения.
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public ChooseControl(AppEnvironment environment) : this()
        {
            _environment = environment;
            FillLevels();
        }

        private void FillLevels()
        {
            SelectDirectionBox.DataSource = null;
            SelectProfileBox.DataSource = null;
            SelectGroupBox.DataSource = null;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;
            SelectLevelBox.DataSource = (new LevelManager(_environment)).ReadAll();
            SelectLevelBox.DisplayMember = "Name";
            SelectLevelBox.ValueMember = "Id";
            FillCourses();
            SelectCourseBox_SelectionChangeCommitted(null,null);
            SelectCourseBox.Enabled = (SelectCourseBox.DataSource as List<int>)?.Count > 0;
        }

        private void FillCourses()
        {
            if (SelectLevelBox.SelectedValue as int? == 2)
            {
                SelectCourseBox.DataSource = new List<int>
                {
                    1, 2
                };
            }
            else if (SelectLevelBox.SelectedValue as int? == 1)
            {
                SelectCourseBox.DataSource = new List<int>
                {
                    1, 2,3,4
                };
            }
            else
            {
                SelectCourseBox.DataSource = new List<int>
                {
                    1, 2,3,4,5
                };
            }
        }

        private void FillDirections()
        {
            SelectProfileBox.DataSource = null;
            SelectGroupBox.DataSource = null;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;
            SelectDirectionBox.DataSource = (new EduDirectionManager(_environment)).ReadByIdLevel(ConvertHelper.StringToInt(SelectLevelBox.SelectedValue.ToString()));
            SelectDirectionBox.DisplayMember = "Name";
            SelectDirectionBox.ValueMember = "Id";
            SelectDirectionBox.SelectedItem = null;
        }

        private void FillProfiles()
        {
            SelectGroupBox.DataSource = null;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;
            SelectProfileBox.DataSource = (new EduProfileManager(_environment)).ReadByIdLevel((int?)SelectDirectionBox.SelectedValue);
            SelectProfileBox.DisplayMember = "Name";
            SelectProfileBox.ValueMember = "Id";
            SelectProfileBox.SelectedItem = null;
        }

        private void cbLevel_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            SelectCourseBox.DataSource = null;
            SelectDirectionBox.DataSource = null;
            SelectProfileBox.DataSource = null;
            SelectProfileBox.Enabled = false;
            SelectGroupBox.DataSource = null;
            SelectGroupBox.Enabled = false;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;

            FillCourses();
            FillDirections();
            var list = SelectDirectionBox.DataSource as List<EduDirectionEntity>;
            if (list?.Count == 1)
            {
                SelectDirectionBox_SelectionChangeCommitted(null, null);
            }
        }

        private void chooseTemplateButton_Click(object sender, System.EventArgs e)
        {
			_docMaker.OpenTemplate();
			makeButton.Enabled = _docMaker.isTemplateOpen;
        }

        private void makeButton_Click(object sender, System.EventArgs e)
        {
            StatusBar.SetProgressBar();
            var idGroup = new List<int?>();
            var groupsCount = SelectGroupBox.CheckedItems.Count;
            var procPerGroup = (int)Math.Ceiling((double)100 / groupsCount);
            Func<int, bool> func = new Func<int, bool>(UpdateStatusBar);
            foreach (var checkedItem in SelectGroupBox.CheckedItems)
            {
                var currentIdGroup = ConvertHelper.StringToInt((checkedItem as GroupEntity)?.Id.ToString());
                idGroup.Add(currentIdGroup);
                var fileName = _environment.TmpDirectory + "\\" + currentIdGroup + ".txt";
                _docMaker.setInsertionList(fileName);
                if (!File.Exists(fileName))
                {
                    var studentManager = new StudentManager(_environment);

                    var studentList = studentManager.ReadAll().Where(item =>
                        item.IdGroup == currentIdGroup).ToList();

                    using (StreamWriter sw = new StreamWriter(fileName, false, System.Text.Encoding.Default))
                    {
                        var insertList = new InsertList(_environment, currentIdGroup);

                        sw.Write(insertList.InitGroupInfoString());
                        sw.WriteLine();
                        foreach (var studentEntity in studentList)
                        {
                            insertList.ReadStudent(studentEntity.Id);
                            sw.Write(insertList.MakeInsertList());
                            sw.WriteLine();
                        }
                    }
                }

				//_docGen.insertion_list_button_Click();
				//_docGen.document_creation_button_Click(procPerGroup, func);
				_docMaker.MakeDocuments(procPerGroup, func);
				_docMaker.EndWork();
            }
            StatusBar.UpdateProgressBarValue(100,true);
        }

        private bool UpdateStatusBar(int val)
        {
            StatusBar.UpdateProgressBarValue(val);
            return true;
        }


        private void SelectCourseBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            SelectDirectionBox.DataSource = null;
            SelectDirectionBox.Enabled = false;
            SelectProfileBox.DataSource = null;
            SelectProfileBox.Enabled = false;
            SelectGroupBox.DataSource = null;
            SelectGroupBox.Enabled = false;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;

            FillDirections();
            SelectDirectionBox.Enabled = (SelectDirectionBox.DataSource as List<EduDirectionEntity>)?.Count>0;
        }

        private void SelectDirectionBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            FillProfiles();
            SelectGroupBox.Enabled = false;
            SelectAllGroupBox.Enabled = false;
            SelectAllGroupBox.Checked = false;
            var list = SelectProfileBox.DataSource as List<EduProfileEntity>;
            if (list?.Count == 1)
            {
                SelectProfileBox.SelectedItem = SelectProfileBox.Items[0];
                SelectProfileBox_SelectionChangeCommitted(null, null);
            }

            SelectProfileBox.Enabled = list?.Count > 0;
        }

        private void SelectProfileBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            var uniManager = new UniManager(_environment);
            var eduDirectionMain = uniManager.Read<EduDirectionEntity>((int?) SelectDirectionBox.SelectedValue);
            var allGroups = uniManager.ReadAll<GroupEntity>().Where(item =>
                item.Course == (int) SelectCourseBox.SelectedValue &&
                item.IdEduProfile == (int?) SelectProfileBox.SelectedValue).ToList();
            var groupsForFill = new List<GroupEntity>();
            foreach (var groupEntity in allGroups)
            {
                var eduProfle = uniManager.Read<EduProfileEntity>(groupEntity.IdEduProfile);
                var eduDirection = uniManager.Read<EduDirectionEntity>(eduProfle.IdEduDirection);
                if (eduDirection.IdLevel == eduDirectionMain.IdLevel)
                {
                    groupsForFill.Add(groupEntity);
                }
            }
            allGroups.Clear();;

            SelectGroupBox.DataSource = groupsForFill;
            SelectGroupBox.DisplayMember = "Number";
            SelectGroupBox.ValueMember = "Id";
            SelectGroupBox.Enabled = (SelectGroupBox.DataSource as List<GroupEntity>)?.Count > 0;
            SelectAllGroupBox.Enabled = (SelectGroupBox.DataSource as List<GroupEntity>)?.Count > 0;
        }

        private void SelectGroupBox_EnabledChanged(object sender, System.EventArgs e)
        {
            SelectGroupBox.BackColor = SelectGroupBox.Enabled?SystemColors.ControlLight:SystemColors.ScrollBar;
        }

        private void SelectGroupBox_SelectedValueChanged(object sender, System.EventArgs e)
        {
            //makeButton.Enabled = SelectGroupBox.CheckedItems.Count > 0;
			chooseTemplateButton.Enabled = SelectGroupBox.CheckedItems.Count > 0;
			SelectAllGroupBox.CheckedChanged -= SelectAllGroupBox_CheckedChanged;
            SelectAllGroupBox.Checked = SelectGroupBox.CheckedItems.Count ==
                                        (SelectGroupBox.DataSource as List<GroupEntity>)?.Count;
            SelectAllGroupBox.CheckedChanged += SelectAllGroupBox_CheckedChanged;
        }

        private void SelectAllGroupBox_CheckedChanged(object sender, System.EventArgs e)
        {
            for (var i = 0; i < (SelectGroupBox.DataSource as List<GroupEntity>)?.Count; i++) 
                SelectGroupBox.SetItemChecked(i,SelectAllGroupBox.Checked);
            SelectGroupBox_SelectedValueChanged(null, null);
        }
    }
}
