﻿namespace Client.MakePracticeDocs.Controls
{
    partial class ChooseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.SelectLevelBox = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SelectCourseBox = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.SelectDirectionBox = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.SelectProfileBox = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.makeButton = new System.Windows.Forms.Button();
			this.chooseTemplateButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.SelectGroupBox = new System.Windows.Forms.CheckedListBox();
			this.SelectAllGroupBox = new System.Windows.Forms.CheckBox();
			this.contentPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// contentPanel
			// 
			this.contentPanel.Controls.Add(this.SelectAllGroupBox);
			this.contentPanel.Controls.Add(this.SelectGroupBox);
			this.contentPanel.Controls.Add(this.label3);
			this.contentPanel.Controls.Add(this.label1);
			this.contentPanel.Controls.Add(this.chooseTemplateButton);
			this.contentPanel.Controls.Add(this.makeButton);
			this.contentPanel.Controls.Add(this.label6);
			this.contentPanel.Controls.Add(this.SelectProfileBox);
			this.contentPanel.Controls.Add(this.label5);
			this.contentPanel.Controls.Add(this.SelectDirectionBox);
			this.contentPanel.Controls.Add(this.label4);
			this.contentPanel.Controls.Add(this.SelectCourseBox);
			this.contentPanel.Controls.Add(this.SelectLevelBox);
			this.contentPanel.Controls.SetChildIndex(this.SelectLevelBox, 0);
			this.contentPanel.Controls.SetChildIndex(this.SelectCourseBox, 0);
			this.contentPanel.Controls.SetChildIndex(this.label4, 0);
			this.contentPanel.Controls.SetChildIndex(this.SelectDirectionBox, 0);
			this.contentPanel.Controls.SetChildIndex(this.label5, 0);
			this.contentPanel.Controls.SetChildIndex(this.SelectProfileBox, 0);
			this.contentPanel.Controls.SetChildIndex(this.label6, 0);
			this.contentPanel.Controls.SetChildIndex(this.makeButton, 0);
			this.contentPanel.Controls.SetChildIndex(this.chooseTemplateButton, 0);
			this.contentPanel.Controls.SetChildIndex(this.label1, 0);
			this.contentPanel.Controls.SetChildIndex(this.label3, 0);
			this.contentPanel.Controls.SetChildIndex(this.SelectGroupBox, 0);
			this.contentPanel.Controls.SetChildIndex(this.SelectAllGroupBox, 0);
			// 
			// SelectLevelBox
			// 
			this.SelectLevelBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.SelectLevelBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SelectLevelBox.FormattingEnabled = true;
			this.SelectLevelBox.Location = new System.Drawing.Point(262, 70);
			this.SelectLevelBox.Name = "SelectLevelBox";
			this.SelectLevelBox.Size = new System.Drawing.Size(272, 24);
			this.SelectLevelBox.TabIndex = 3;
			this.SelectLevelBox.SelectionChangeCommitted += new System.EventHandler(this.cbLevel_SelectionChangeCommitted);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(45, 110);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(41, 18);
			this.label3.TabIndex = 4;
			this.label3.Text = "Курс";
			// 
			// SelectCourseBox
			// 
			this.SelectCourseBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.SelectCourseBox.Enabled = false;
			this.SelectCourseBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SelectCourseBox.FormattingEnabled = true;
			this.SelectCourseBox.Location = new System.Drawing.Point(262, 110);
			this.SelectCourseBox.Name = "SelectCourseBox";
			this.SelectCourseBox.Size = new System.Drawing.Size(272, 24);
			this.SelectCourseBox.TabIndex = 5;
			this.SelectCourseBox.SelectionChangeCommitted += new System.EventHandler(this.SelectCourseBox_SelectionChangeCommitted);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Location = new System.Drawing.Point(45, 153);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(185, 18);
			this.label4.TabIndex = 6;
			this.label4.Text = "Направление подготовки";
			// 
			// SelectDirectionBox
			// 
			this.SelectDirectionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.SelectDirectionBox.Enabled = false;
			this.SelectDirectionBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SelectDirectionBox.FormattingEnabled = true;
			this.SelectDirectionBox.Location = new System.Drawing.Point(262, 153);
			this.SelectDirectionBox.Name = "SelectDirectionBox";
			this.SelectDirectionBox.Size = new System.Drawing.Size(272, 24);
			this.SelectDirectionBox.TabIndex = 7;
			this.SelectDirectionBox.SelectionChangeCommitted += new System.EventHandler(this.SelectDirectionBox_SelectionChangeCommitted);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(45, 197);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(159, 18);
			this.label5.TabIndex = 8;
			this.label5.Text = "Профиль подготовки";
			// 
			// SelectProfileBox
			// 
			this.SelectProfileBox.BackColor = System.Drawing.SystemColors.Window;
			this.SelectProfileBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.SelectProfileBox.Enabled = false;
			this.SelectProfileBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.SelectProfileBox.FormattingEnabled = true;
			this.SelectProfileBox.Location = new System.Drawing.Point(262, 197);
			this.SelectProfileBox.Name = "SelectProfileBox";
			this.SelectProfileBox.Size = new System.Drawing.Size(272, 24);
			this.SelectProfileBox.TabIndex = 9;
			this.SelectProfileBox.SelectionChangeCommitted += new System.EventHandler(this.SelectProfileBox_SelectionChangeCommitted);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(45, 238);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(56, 18);
			this.label6.TabIndex = 10;
			this.label6.Text = "Группа";
			// 
			// makeButton
			// 
			this.makeButton.Enabled = false;
			this.makeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.makeButton.ForeColor = System.Drawing.Color.Black;
			this.makeButton.Location = new System.Drawing.Point(236, 406);
			this.makeButton.Name = "makeButton";
			this.makeButton.Size = new System.Drawing.Size(147, 35);
			this.makeButton.TabIndex = 12;
			this.makeButton.Text = "Сформировать";
			this.makeButton.UseVisualStyleBackColor = true;
			this.makeButton.Click += new System.EventHandler(this.makeButton_Click);
			// 
			// chooseTemplateButton
			// 
			this.chooseTemplateButton.Enabled = false;
			this.chooseTemplateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.chooseTemplateButton.ForeColor = System.Drawing.Color.Black;
			this.chooseTemplateButton.Location = new System.Drawing.Point(236, 357);
			this.chooseTemplateButton.Name = "chooseTemplateButton";
			this.chooseTemplateButton.Size = new System.Drawing.Size(147, 33);
			this.chooseTemplateButton.TabIndex = 13;
			this.chooseTemplateButton.Text = "Выбрать шаблон";
			this.chooseTemplateButton.UseVisualStyleBackColor = true;
			this.chooseTemplateButton.Click += new System.EventHandler(this.chooseTemplateButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.BackColor = System.Drawing.Color.Transparent;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(45, 70);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(152, 18);
			this.label1.TabIndex = 14;
			this.label1.Text = "Уровень подготовки";
			// 
			// SelectGroupBox
			// 
			this.SelectGroupBox.BackColor = System.Drawing.SystemColors.Control;
			this.SelectGroupBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.SelectGroupBox.CheckOnClick = true;
			this.SelectGroupBox.Enabled = false;
			this.SelectGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.SelectGroupBox.FormattingEnabled = true;
			this.SelectGroupBox.Location = new System.Drawing.Point(262, 255);
			this.SelectGroupBox.Name = "SelectGroupBox";
			this.SelectGroupBox.Size = new System.Drawing.Size(272, 56);
			this.SelectGroupBox.Sorted = true;
			this.SelectGroupBox.TabIndex = 16;
			this.SelectGroupBox.SelectedValueChanged += new System.EventHandler(this.SelectGroupBox_SelectedValueChanged);
			this.SelectGroupBox.EnabledChanged += new System.EventHandler(this.SelectGroupBox_EnabledChanged);
			// 
			// SelectAllGroupBox
			// 
			this.SelectAllGroupBox.AutoSize = true;
			this.SelectAllGroupBox.ForeColor = System.Drawing.Color.Black;
			this.SelectAllGroupBox.Location = new System.Drawing.Point(264, 238);
			this.SelectAllGroupBox.Name = "SelectAllGroupBox";
			this.SelectAllGroupBox.Size = new System.Drawing.Size(91, 17);
			this.SelectAllGroupBox.TabIndex = 18;
			this.SelectAllGroupBox.Text = "Выбрать все";
			this.SelectAllGroupBox.UseVisualStyleBackColor = true;
			this.SelectAllGroupBox.CheckedChanged += new System.EventHandler(this.SelectAllGroupBox_CheckedChanged);
			// 
			// ChooseControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.Name = "ChooseControl";
			this.StagesPanelVisible = true;
			this.contentPanel.ResumeLayout(false);
			this.contentPanel.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox SelectProfileBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox SelectDirectionBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox SelectCourseBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox SelectLevelBox;
        private System.Windows.Forms.Button makeButton;
        private System.Windows.Forms.Button chooseTemplateButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox SelectGroupBox;
        private System.Windows.Forms.CheckBox SelectAllGroupBox;
    }
}
