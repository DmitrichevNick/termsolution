﻿namespace Client.UICore
{
    /// <summary>
    /// Визуальное окружение приложения.
    /// </summary>
    public static class VisualEnvironment
    {
        /// <summary>
        /// Браузер.
        /// </summary>
        public static BrowserControl Browser { get; private set; }
        /// <summary>
        /// Панель с режимами.
        /// </summary>
        public static RibbonControl Ribbon { get; private set; }

        /// <summary>
        /// Инициализация.
        /// </summary>
        /// <param name="browser">Браузер.</param>
        /// <param name="ribbon">Панель с режимами.</param>
        public static void Set(ref BrowserControl browser,
            ref RibbonControl ribbon)
        {
            Browser = browser;
            Ribbon = ribbon;
        }
    }
}
