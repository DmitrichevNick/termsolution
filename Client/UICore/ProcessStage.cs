﻿namespace Client.UICore
{
    /// <summary>
    /// Стадия обработки
    /// </summary>
    public class ProcessStage
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ProcessStage()
        {
            Text = string.Empty;
            Active = false;
            Number = 0;
        }

        /// <summary>
        /// Конструктор с параметрами.
        /// </summary>
        /// <param name="text">Текст.</param>
        /// <param name="active">Состояние стадии.</param>
        /// <param name="number">Номер стадии.</param>
        public ProcessStage(string text, bool active, int number)
        {
            Text = text;
            Active = active;
            Number = number;
        }

        /// <summary>
        ///  Текст.
        /// </summary>
        public string Text { get; }

        /// <summary>
        /// Состояние стадии.
        /// </summary>
        public bool Active { set; get; }

        /// <summary>
        /// Номер стадии.
        /// </summary>
        public int Number { get; }
    }
}
