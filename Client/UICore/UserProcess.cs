﻿using DatabaseConnector;
using System.Collections.Generic;
using System.Linq;

namespace Client.UICore
{
    /// <summary>
    /// Пользовательский процесс.
    /// </summary>
    public class UserProcess
    {
        /// <summary>
        /// Название процесса.
        /// </summary>
        public string ProcessTitle { get; }

        /// <summary>
        /// Номер активной стадии.
        /// </summary>
        public int ActiveStageNumber { get; private set; }

        /// <summary>
        /// Видимость панели стадий.
        /// </summary>
        public bool StagePanelVisible { get; protected set; }

        /// <summary>
        /// Количество стадий.
        /// </summary>
        public int StageCount { get { return _userProcessDictionary.Count; } }

        /// <summary>
        /// Стадии процесса.
        /// </summary>
        public List<ProcessStage> Stages { get { return _userProcessDictionary.Keys.ToList(); } }

        /// <summary>
        /// Содержимое стадий.
        /// </summary>
        public List<ContentControl> Contents { get { return _userProcessDictionary.Values.ToList(); } }

        protected readonly AppEnvironment _environment;
        private readonly Dictionary<ProcessStage, ContentControl> _userProcessDictionary;

        private void NextStage()
        {
            var prevContext = _userProcessDictionary.ElementAt(ActiveStageNumber).Value.GetContext();
            _userProcessDictionary.ElementAt(ActiveStageNumber).Key.Active = false;

            ActiveStageNumber++;
            _userProcessDictionary.ElementAt(ActiveStageNumber).Value.SetContext(prevContext);
            _userProcessDictionary.ElementAt(ActiveStageNumber).Key.Active = true;
        }

        private void PrevStage()
        {
            _userProcessDictionary.ElementAt(ActiveStageNumber).Key.Active = false;
            _userProcessDictionary.ElementAt(--ActiveStageNumber).Key.Active = true;
        }

        /// <summary>
        /// Конструктор, принимающий окружение приложения.
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        /// <param name="processTitle">Название процесса.</param>
        protected UserProcess(AppEnvironment environment, string processTitle)
        {
            _environment = environment;
            _userProcessDictionary = new Dictionary<ProcessStage, ContentControl>();
            ActiveStageNumber = -1;
            ProcessTitle = processTitle;
        }

        /// <summary>
        /// Добавить стадию
        /// </summary>
        /// <param name="stage">Стадия.</param>
        /// <param name="content">Контент.</param>
        protected void AddStage(ProcessStage stage, ContentControl content)
        {
            _userProcessDictionary.Add(stage, content);

            if (ActiveStageNumber == -1)
            {
                ActiveStageNumber++;
                _userProcessDictionary.ElementAt(ActiveStageNumber).Key.Active = true;
            }
            else
            {
                _userProcessDictionary.ElementAt(stage.Number - 1).Value.StagesPanelVisible = true;
                _userProcessDictionary.ElementAt(stage.Number - 1).Value.NextStageButtonVisible = true;
                _userProcessDictionary.ElementAt(stage.Number - 1).Value.SetNextStageClick((sender, args) =>
                {
                    var context = _userProcessDictionary.ElementAt(stage.Number - 1).Value.GetContext();
                    _userProcessDictionary.ElementAt(stage.Number).Value.SetContext(context);
                    NextStage();
                    VisualEnvironment.Browser.RedrawTab(this);
                });

                _userProcessDictionary.ElementAt(stage.Number).Value.PrevStageButtonVisible = true;
                _userProcessDictionary.ElementAt(stage.Number).Value.SetPrevStageClick((sender, args) =>
                {
                    PrevStage();
                    VisualEnvironment.Browser.RedrawTab(this);
                });
            }
        }
    }
}
