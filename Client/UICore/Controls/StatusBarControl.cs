﻿using System.Windows.Forms;

namespace Client.UICore
{
    /// <summary>
    /// Панель состояния.
    /// </summary>
    public partial class StatusBarControl : UserControl
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        public StatusBarControl()
        {
            InitializeComponent();

            DefaultText();
        }

        /// <summary>
        /// Сообщение по умолчанию.
        /// </summary>
        public void DefaultText()
        {
            SetText("Приложение готово к работе. Выберите одну из доступных функций.");
        }

        /// <summary>
        /// Установить сообщение.
        /// </summary>
        /// <param name="text">Сообщение.</param>
        public void SetText(string text)
        {
            Controls.Clear();

            var textBox = new Label {Text = text};
            Controls.Add(textBox);
            textBox.Dock = DockStyle.Fill;
        }

        /// <summary>
        /// Установить полосу состояния.
        /// </summary>
        public void SetProgressBar()
        {
            Controls.Clear();
            var progressBar = new ProgressBar {Name = "progressBar", Minimum = 0, Maximum = 100};

            Controls.Add(progressBar);
            progressBar.Dock = DockStyle.Fill;
        }

        /// <summary>
        /// Установить значение полосы состояния.
        /// </summary>
        /// <param name="value"></param>
        public void UpdateProgressBarValue(int value, bool fromBegin = false)
        {
            if (Controls.ContainsKey("progressBar") && value >= 0 && value <= 100)
            {
                var addValue = value;
                if (!fromBegin)
                {
                    addValue += (Controls["progressBar"] as ProgressBar).Value;
                    if (addValue >= 100)
                        SetText("Завершено");
                }
                if (addValue >= 100)
                    SetText("Завершено");
                else
                    (Controls["progressBar"] as ProgressBar).Value = addValue;
            }
        }
    }
}
