﻿using System.Drawing;
using System.Windows.Forms;

namespace Client.UICore
{
    /// <summary>
    /// Стадия процесса.
    /// </summary>
    public partial class LeftMenuStage : UserControl
    {
        private ProcessStage _processStage;
        /// <summary>
        /// Конструктор
        /// </summary>
        public LeftMenuStage()
        {
            InitializeComponent();
        }

        public LeftMenuStage(ProcessStage processStage)
        {
            InitializeComponent();

            _processStage = processStage;
            labelStage.Text = processStage.Text;
            Active = _processStage.Active;
        }

        /// <summary>
        /// Состояние стадии.
        /// </summary>
        public bool Active
        {
            get { return _processStage.Active; }
            set {
                _processStage.Active = value;
                labelStage.BackColor = _processStage.Active ? Color.DeepSkyBlue : Color.Gray;
                Refresh();
            }
        }

        public ProcessStage StageInfo
        {
            get { return _processStage; }
        }
    }
}
