﻿using App;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Client.UICore
{
    public partial class RibbonControl : UserControl
    {
        private Dictionary<string, RibbonButton> _buttons;
        public RibbonControl()
        {
            InitializeComponent();
            _buttons= new Dictionary<string, RibbonButton>();
        }

        public void AddButton(RibbonButton button)
        {
            if (!_buttons.ContainsKey(button.ButtonName))
            {
                _buttons.Add(button.ButtonName, button);
                tableLayoutPanel1.Controls.Add(button);
            }
        }

        public void RemoveButton(string name)
        {
            if (_buttons.ContainsKey(name))
            {
                _buttons.Remove(name);
                tableLayoutPanel1.Controls.Remove(_buttons[name]);
            }
        }

        public void ClearButtons()
        {
            _buttons.Clear();
            tableLayoutPanel1.Controls.Clear();
        }

        public RibbonButton GetRibbonButton(string name)
        {
            return _buttons.ContainsKey(name) ? _buttons[name] : null;
        }

        public bool RibbonVisible
        {
            set
            {
                tableLayoutPanel1.Visible = value;
                this.Visible = value;
                if (value)
                    this.Show();
                else
                    this.Hide();
            }
            get
            {
                return this.Visible;
            }
        }
    }
}
