﻿using System;
using System.Windows.Forms;
using Helper;

namespace Client.UICore
{
    public partial class ContentControl : UserControl
    {
        private UserControl _mainControl;
        private EventHandler _nextStageExecutor;
        private EventHandler _prevStageExecutor;
        protected Context _prevContext;
        protected Context _nextContext;
        public ContentControl()
        {
            InitializeComponent();
            StagesPanelVisible = false;
            _prevContext = new Context();
            _nextContext = new Context();
        }

        public Context GetContext()
        {
            return _nextContext;
        }

        public void SetContext(Context context)
        {
            _prevContext = context;
        }

        public void SetNextStageClick(EventHandler eventHandler)
        {
            if (_nextStageExecutor != null)
                nextStageButton.Click -= _nextStageExecutor;
            _nextStageExecutor = eventHandler;
            nextStageButton.Click += eventHandler;
        }

        public void SetPrevStageClick(EventHandler eventHandler)
        {
            if (_prevStageExecutor != null)
                prevStageButton.Click -= _prevStageExecutor;
            _prevStageExecutor = eventHandler;
            prevStageButton.Click += eventHandler;
        }

        public bool StagesPanelVisible
        {
            set
            {
                stagesPanel.Visible = value;
                nextStageButton.Visible = value;
                if (value)
                    stagesPanel.Show();
                else
                    stagesPanel.Hide();
            }
            get
            {
                return stagesPanel.Visible;
            }
        }

        public StatusBarControl StatusBar
        {
            get { return statusBarControl; }
        }

        public bool NextStageButtonVisible
        {
            get { return nextStageButton.Visible; }
            set { nextStageButton.Visible = value; }
        }

        public bool PrevStageButtonVisible
        {
            get { return prevStageButton.Visible; }
            set { prevStageButton.Visible = value; }
        }
    }
}
