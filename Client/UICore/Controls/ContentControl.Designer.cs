﻿namespace Client.UICore
{
    partial class ContentControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contentPanel = new System.Windows.Forms.Panel();
            this.statusBarControl = new Client.UICore.StatusBarControl();
            this.stagesPanel = new System.Windows.Forms.Panel();
            this.nextStageButton = new System.Windows.Forms.Button();
            this.prevStageButton = new System.Windows.Forms.Button();
            this.contentPanel.SuspendLayout();
            this.stagesPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contentPanel
            // 
            this.contentPanel.BackColor = System.Drawing.Color.Transparent;
            this.contentPanel.Controls.Add(this.statusBarControl);
            this.contentPanel.Controls.Add(this.stagesPanel);
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(636, 494);
            this.contentPanel.TabIndex = 0;
            // 
            // statusBarControl
            // 
            this.statusBarControl.BackColor = System.Drawing.Color.Transparent;
            this.statusBarControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.statusBarControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.statusBarControl.Location = new System.Drawing.Point(0, 477);
            this.statusBarControl.Name = "statusBarControl";
            this.statusBarControl.Size = new System.Drawing.Size(636, 17);
            this.statusBarControl.TabIndex = 2;
            // 
            // stagesPanel
            // 
            this.stagesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.stagesPanel.Controls.Add(this.prevStageButton);
            this.stagesPanel.Controls.Add(this.nextStageButton);
            this.stagesPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.stagesPanel.Location = new System.Drawing.Point(0, 0);
            this.stagesPanel.Name = "stagesPanel";
            this.stagesPanel.Size = new System.Drawing.Size(636, 32);
            this.stagesPanel.TabIndex = 1;
            this.stagesPanel.Visible = false;
            // 
            // nextStageButton
            // 
            this.nextStageButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nextStageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextStageButton.ForeColor = System.Drawing.Color.Black;
            this.nextStageButton.Location = new System.Drawing.Point(486, 3);
            this.nextStageButton.Name = "nextStageButton";
            this.nextStageButton.Size = new System.Drawing.Size(147, 26);
            this.nextStageButton.TabIndex = 0;
            this.nextStageButton.Text = "Следующая стадия";
            this.nextStageButton.UseVisualStyleBackColor = true;
            this.nextStageButton.Visible = false;
            // 
            // prevStageButton
            // 
            this.prevStageButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.prevStageButton.ForeColor = System.Drawing.Color.Black;
            this.prevStageButton.Location = new System.Drawing.Point(3, 3);
            this.prevStageButton.Name = "prevStageButton";
            this.prevStageButton.Size = new System.Drawing.Size(147, 26);
            this.prevStageButton.TabIndex = 1;
            this.prevStageButton.Text = "Предыдущая стадия";
            this.prevStageButton.UseVisualStyleBackColor = true;
            this.prevStageButton.Visible = false;
            // 
            // ContentControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.contentPanel);
            this.DoubleBuffered = true;
            this.Name = "ContentControl";
            this.Size = new System.Drawing.Size(636, 494);
            this.contentPanel.ResumeLayout(false);
            this.stagesPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Panel contentPanel;
        protected System.Windows.Forms.Button nextStageButton;
        private System.Windows.Forms.Panel stagesPanel;
        private StatusBarControl statusBarControl;
        protected System.Windows.Forms.Button prevStageButton;
    }
}
