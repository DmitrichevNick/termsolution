﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Client.UICore
{
    public partial class BrowserControl : UserControl
    {
        public BrowserControl()
        {
            InitializeComponent();
            tabControl.MouseDown += tabControl_MouseDown;
        }

        public void AddTab(UserProcess process)
        {
            var tp = new TabPage(process.ProcessTitle+"    X");

            var content = process.Contents.ElementAt(process.ActiveStageNumber);
            content.Dock = DockStyle.Fill;
            tp.Controls.Add(content);

            if (process.StagePanelVisible)
            {
                var leftMenu = new LeftMenuControl();
                leftMenu.SetStages(process.Stages.ToArray());
                leftMenu.Visible = true;
                leftMenu.Dock = DockStyle.Left;
                tp.Controls.Add(leftMenu);
            }

            tp.Tag = process;
            tabControl.TabPages.Add(tp);
            tp.BackColor = Color.White;
            tp.BorderStyle = BorderStyle.None;
            tp.UseVisualStyleBackColor = true;
            tabControl.SelectedTab = tp;
        }

        public void RedrawTab(UserProcess process)
        {
            var page = tabControl.TabPages.Cast<TabPage>().FirstOrDefault(item => item.Tag == process);
            page.Controls.Clear();
            var content = process.Contents.ElementAt(process.ActiveStageNumber);
            content.Dock = DockStyle.Fill;
            page.Controls.Add(content);

            if (process.StagePanelVisible)
            {
                var leftMenu = new LeftMenuControl();
                leftMenu.SetStages(process.Stages.ToArray());
                leftMenu.Visible = true;
                leftMenu.Dock = DockStyle.Left;
                page.Controls.Add(leftMenu);
            }
        }

        private void tabControl_MouseDown(object sender, MouseEventArgs e)
        {
            var r = tabControl.GetTabRect(this.tabControl.SelectedIndex);
            var closeButton = new Rectangle(r.Right - 15, r.Top + 4, 9, 7);
            if (closeButton.Contains(e.Location))
            {
                tabControl.TabPages.Remove(tabControl.SelectedTab);
            }
        }

        public TabPage GetTab(UserProcess process)
        {
            return tabControl.TabPages.Cast<TabPage>().FirstOrDefault(page => page.Tag == process);
        }
    }
}
