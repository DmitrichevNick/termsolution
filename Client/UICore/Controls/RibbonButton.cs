﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace App
{
    public partial class RibbonButton : UserControl
    {
        private bool _active;
        public string ButtonName { get; }

        public bool Active
        {
            get { return _active; }
            set
            {
                _active = value;
                if (value)
                {
                    BackColor = Color.LightSteelBlue;
                    foreach (Control control in Controls)
                    {
                        control.BackColor = BackColor;
                        control.MouseEnter -= RibbonButton_MouseEnter;
                        control.MouseLeave -= RibbonButton_MouseLeave;
                    }

                    MouseEnter -= RibbonButton_MouseEnter;
                    MouseLeave -= RibbonButton_MouseLeave;
                }
                else
                {
                    BackColor = Color.AliceBlue;
                    foreach (Control control in Controls)
                    {
                        control.BackColor = Color.AliceBlue;
                        control.MouseEnter += RibbonButton_MouseEnter;
                        control.MouseLeave += RibbonButton_MouseLeave;
                    }

                    MouseEnter += RibbonButton_MouseEnter;
                    MouseLeave += RibbonButton_MouseLeave;
                }
            }
        }

        public EventHandler Execute
        {
            set
            {
                Click += value;
                foreach (Control control in this.Controls)
                {
                    control.Click += value;
                }
            }
        }
        public RibbonButton()
        {
            InitializeComponent();

            SetUp();
        }

        public RibbonButton(string name):this()
        {
            ButtonName = name;
        }

        private void SetUp()
        {
            Active = false;
        }
        private void RibbonButton_MouseEnter(object sender, EventArgs e)
        {
            BackColor = Color.LightSteelBlue;
            foreach (Control control in this.Controls)
            {
                control.BackColor = BackColor;
            }
        }

        private void RibbonButton_MouseLeave(object sender, EventArgs e)
        {
            BackColor = Color.AliceBlue;
            foreach (Control control in this.Controls)
            {
                control.BackColor = Color.AliceBlue;
            }
        }

        public void SetName(string name)
        {
            labelName.Text = name;
        }

        public void SetCaption(string caption)
        {
            var toolTip = new ToolTip();
            toolTip.InitialDelay = 500;
            toolTip.SetToolTip(this, caption);
            foreach (Control control in this.Controls)
            {
                toolTip.SetToolTip(control, caption);
            }            

        }

        public void SetImage(Image image)
        {
            this.buttonImage.BackgroundImage = image;
        }
    }
}
