﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Client.UICore
{
    public partial class LeftMenuControl : UserControl
    {
        public LeftMenuControl()
        {
            InitializeComponent();
        }

        public void SetStages(ProcessStage[] stages)
        {
            stagesList.Controls.Clear();
            foreach (var processStage in stages)
            {
                stagesList.Controls.Add(new LeftMenuStage(processStage));
            }
            Refresh();
        }

        public void ClearStages()
        {
            stagesList.Controls.Clear();
        }

        public bool LeftMenuVisible
        {
            set
            {
                Visible = value;
                stagesList.Visible = value;
                if (value)
                    Show();
                else
                    Hide();
            }
            get { return Visible; }
        }
    }
}
