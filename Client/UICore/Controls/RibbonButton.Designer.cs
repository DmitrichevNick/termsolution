﻿namespace App
{
    partial class RibbonButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.buttonImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.buttonImage)).BeginInit();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelName.Location = new System.Drawing.Point(0, 38);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(141, 53);
            this.labelName.TabIndex = 0;
            this.labelName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // buttonImage
            // 
            this.buttonImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.buttonImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonImage.Location = new System.Drawing.Point(0, 0);
            this.buttonImage.Name = "buttonImage";
            this.buttonImage.Size = new System.Drawing.Size(141, 38);
            this.buttonImage.TabIndex = 1;
            this.buttonImage.TabStop = false;
            // 
            // RibbonButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.buttonImage);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Name = "RibbonButton";
            this.Size = new System.Drawing.Size(141, 100);
            this.MouseEnter += new System.EventHandler(this.RibbonButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.RibbonButton_MouseLeave);
            ((System.ComponentModel.ISupportInitialize)(this.buttonImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.PictureBox buttonImage;
    }
}
