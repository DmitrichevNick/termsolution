﻿namespace Client.Settings
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
			this.settingsTab = new System.Windows.Forms.TabControl();
			this.databaseSettings = new System.Windows.Forms.TabPage();
			this.tbPassword = new System.Windows.Forms.TextBox();
			this.tbUsername = new System.Windows.Forms.TextBox();
			this.tbPort = new System.Windows.Forms.TextBox();
			this.tbHost = new System.Windows.Forms.TextBox();
			this.tbDatabaseName = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.saveButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.docgenSettings = new System.Windows.Forms.TabPage();
			this.insertListBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.insertListButton = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.docgenBox = new System.Windows.Forms.TextBox();
			this.docgenButton = new System.Windows.Forms.Button();
			this.settingsTab.SuspendLayout();
			this.databaseSettings.SuspendLayout();
			this.docgenSettings.SuspendLayout();
			this.SuspendLayout();
			// 
			// settingsTab
			// 
			this.settingsTab.Controls.Add(this.databaseSettings);
			this.settingsTab.Controls.Add(this.docgenSettings);
			this.settingsTab.Dock = System.Windows.Forms.DockStyle.Top;
			this.settingsTab.Location = new System.Drawing.Point(0, 0);
			this.settingsTab.Name = "settingsTab";
			this.settingsTab.SelectedIndex = 0;
			this.settingsTab.Size = new System.Drawing.Size(364, 225);
			this.settingsTab.TabIndex = 0;
			// 
			// databaseSettings
			// 
			this.databaseSettings.Controls.Add(this.tbPassword);
			this.databaseSettings.Controls.Add(this.tbUsername);
			this.databaseSettings.Controls.Add(this.tbPort);
			this.databaseSettings.Controls.Add(this.tbHost);
			this.databaseSettings.Controls.Add(this.tbDatabaseName);
			this.databaseSettings.Controls.Add(this.label5);
			this.databaseSettings.Controls.Add(this.label4);
			this.databaseSettings.Controls.Add(this.label3);
			this.databaseSettings.Controls.Add(this.label2);
			this.databaseSettings.Controls.Add(this.label1);
			this.databaseSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.databaseSettings.Location = new System.Drawing.Point(4, 22);
			this.databaseSettings.Name = "databaseSettings";
			this.databaseSettings.Padding = new System.Windows.Forms.Padding(3);
			this.databaseSettings.Size = new System.Drawing.Size(356, 199);
			this.databaseSettings.TabIndex = 0;
			this.databaseSettings.Text = "База данных";
			this.databaseSettings.UseVisualStyleBackColor = true;
			// 
			// tbPassword
			// 
			this.tbPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPassword.Location = new System.Drawing.Point(165, 165);
			this.tbPassword.Name = "tbPassword";
			this.tbPassword.Size = new System.Drawing.Size(165, 21);
			this.tbPassword.TabIndex = 9;
			this.tbPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbUsername
			// 
			this.tbUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbUsername.Location = new System.Drawing.Point(165, 126);
			this.tbUsername.Name = "tbUsername";
			this.tbUsername.Size = new System.Drawing.Size(165, 21);
			this.tbUsername.TabIndex = 8;
			this.tbUsername.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbPort
			// 
			this.tbPort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbPort.Location = new System.Drawing.Point(165, 91);
			this.tbPort.Name = "tbPort";
			this.tbPort.Size = new System.Drawing.Size(165, 21);
			this.tbPort.TabIndex = 7;
			this.tbPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbHost
			// 
			this.tbHost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbHost.Location = new System.Drawing.Point(165, 56);
			this.tbHost.Name = "tbHost";
			this.tbHost.Size = new System.Drawing.Size(165, 21);
			this.tbHost.TabIndex = 6;
			this.tbHost.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// tbDatabaseName
			// 
			this.tbDatabaseName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.tbDatabaseName.Location = new System.Drawing.Point(165, 20);
			this.tbDatabaseName.Name = "tbDatabaseName";
			this.tbDatabaseName.Size = new System.Drawing.Size(165, 21);
			this.tbDatabaseName.TabIndex = 5;
			this.tbDatabaseName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label5.Location = new System.Drawing.Point(20, 169);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(61, 17);
			this.label5.TabIndex = 4;
			this.label5.Text = "Пароль:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label4.Location = new System.Drawing.Point(20, 129);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(135, 17);
			this.label4.TabIndex = 3;
			this.label4.Text = "Имя пользователя:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label3.Location = new System.Drawing.Point(20, 91);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(45, 17);
			this.label3.TabIndex = 2;
			this.label3.Text = "Порт:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label2.Location = new System.Drawing.Point(20, 56);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Хост:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.label1.Location = new System.Drawing.Point(20, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Название:";
			// 
			// saveButton
			// 
			this.saveButton.BackColor = System.Drawing.Color.AliceBlue;
			this.saveButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
			this.saveButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.saveButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.saveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.saveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.saveButton.ForeColor = System.Drawing.Color.Black;
			this.saveButton.Location = new System.Drawing.Point(77, 234);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(82, 25);
			this.saveButton.TabIndex = 1;
			this.saveButton.Text = "Сохранить";
			this.saveButton.UseVisualStyleBackColor = false;
			this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.BackColor = System.Drawing.Color.AliceBlue;
			this.cancelButton.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
			this.cancelButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.cancelButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
			this.cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.cancelButton.Location = new System.Drawing.Point(187, 234);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(80, 25);
			this.cancelButton.TabIndex = 2;
			this.cancelButton.Text = "Отмена";
			this.cancelButton.UseVisualStyleBackColor = false;
			this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
			// 
			// docgenSettings
			// 
			this.docgenSettings.Controls.Add(this.docgenButton);
			this.docgenSettings.Controls.Add(this.docgenBox);
			this.docgenSettings.Controls.Add(this.label7);
			this.docgenSettings.Controls.Add(this.insertListButton);
			this.docgenSettings.Controls.Add(this.label6);
			this.docgenSettings.Controls.Add(this.insertListBox);
			this.docgenSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
			this.docgenSettings.Location = new System.Drawing.Point(4, 22);
			this.docgenSettings.Name = "docgenSettings";
			this.docgenSettings.Padding = new System.Windows.Forms.Padding(3);
			this.docgenSettings.Size = new System.Drawing.Size(356, 199);
			this.docgenSettings.TabIndex = 1;
			this.docgenSettings.Text = "Документы";
			this.docgenSettings.UseVisualStyleBackColor = true;
			// 
			// insertListBox
			// 
			this.insertListBox.Location = new System.Drawing.Point(12, 39);
			this.insertListBox.Name = "insertListBox";
			this.insertListBox.Size = new System.Drawing.Size(325, 21);
			this.insertListBox.TabIndex = 0;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label6.Location = new System.Drawing.Point(9, 12);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(211, 17);
			this.label6.TabIndex = 1;
			this.label6.Text = "Папка для временных файлов:";
            // 
			// insertListButton
			// 
			this.insertListButton.Location = new System.Drawing.Point(132, 66);
			this.insertListButton.Name = "insertListButton";
			this.insertListButton.Size = new System.Drawing.Size(82, 25);
			this.insertListButton.TabIndex = 2;
			this.insertListButton.Text = "Выбрать";
			this.insertListButton.UseVisualStyleBackColor = true;
			this.insertListButton.Click += new System.EventHandler(this.insertListButton_Click);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
			this.label7.Location = new System.Drawing.Point(9, 101);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(162, 17);
			this.label7.TabIndex = 3;
			this.label7.Text = "Папка для документов:";
			// 
			// docgenBox
			// 
			this.docgenBox.Location = new System.Drawing.Point(12, 131);
			this.docgenBox.Name = "docgenBox";
			this.docgenBox.Size = new System.Drawing.Size(325, 21);
			this.docgenBox.TabIndex = 4;
			// 
			// docgenButton
			// 
			this.docgenButton.Location = new System.Drawing.Point(132, 158);
			this.docgenButton.Name = "docgenButton";
			this.docgenButton.Size = new System.Drawing.Size(82, 25);
			this.docgenButton.TabIndex = 5;
			this.docgenButton.Text = "Выбрать";
			this.docgenButton.UseVisualStyleBackColor = true;
			this.docgenButton.Click += new System.EventHandler(this.docgenButton_Click);
			// 
			// SettingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(364, 271);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.settingsTab);
			this.Controls.Add(this.saveButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "SettingsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Настройки";
			this.settingsTab.ResumeLayout(false);
			this.databaseSettings.ResumeLayout(false);
			this.databaseSettings.PerformLayout();
			this.docgenSettings.ResumeLayout(false);
			this.docgenSettings.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl settingsTab;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TabPage databaseSettings;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.TextBox tbUsername;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.TextBox tbHost;
        private System.Windows.Forms.TextBox tbDatabaseName;
		private System.Windows.Forms.TabPage docgenSettings;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox insertListBox;
		private System.Windows.Forms.Button insertListButton;
		private System.Windows.Forms.Button docgenButton;
		private System.Windows.Forms.TextBox docgenBox;
		private System.Windows.Forms.Label label7;
	}
}