﻿using DatabaseConnector;
using System.Configuration;
using System.Windows.Forms;

namespace Client.Settings
{
    /// <summary>
    /// Форма настроек.
    /// </summary>
    public partial class SettingsForm : Form
    {
        private AppEnvironment _environment;
        private Configuration _config;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public SettingsForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Конструктор, принимающий окружение приложения.
        /// </summary>
        /// <param name="environment"></param>
        public SettingsForm(AppEnvironment environment)
        {
            InitializeComponent();
            _environment = environment;
            _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            SetDatabaseFields();
            SetDocGenFields();
        }

        private void SetDatabaseFields()
        {
            tbDatabaseName.Text = _config.AppSettings.Settings["DatabaseName"].Value;
            tbHost.Text = _config.AppSettings.Settings["Host"].Value;
            tbPassword.Text = _config.AppSettings.Settings["Port"].Value;
            tbPort.Text = _config.AppSettings.Settings["Username"].Value;
            tbUsername.Text = _config.AppSettings.Settings["Password"].Value;
        }

        private void SetDocGenFields()
        {
            insertListBox.Text = _config.AppSettings.Settings["InsertionList"].Value;
            docgenBox.Text = _config.AppSettings.Settings["DocGenFolder"].Value;
        }

        private void saveButton_Click(object sender, System.EventArgs e)
        {
            if (ValidateDatabaseSettings())
            {
                _config.AppSettings.Settings["DatabaseName"].Value = tbDatabaseName.Text;
                _config.AppSettings.Settings["Host"].Value = tbHost.Text;
                _config.AppSettings.Settings["Port"].Value = tbPassword.Text;
                _config.AppSettings.Settings["Username"].Value = tbPort.Text;
                _config.AppSettings.Settings["Password"].Value = tbUsername.Text;
                _config.AppSettings.Settings["InsertionList"].Value = insertListBox.Text;
                _config.AppSettings.Settings["DocGenFolder"].Value = docgenBox.Text;

                _config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                _environment = new AppEnvironment();
                DialogResult = DialogResult.OK;
            }
            else
            {
                MessageBox.Show(@"Одно из полей в закладке ""База данных"" не заполнено", "Ошибка", MessageBoxButtons.OK);
            }
        }

        private bool ValidateDatabaseSettings()
        {
            return !string.IsNullOrEmpty(tbDatabaseName.Text) &&
                   !string.IsNullOrEmpty(tbHost.Text) &&
                   !string.IsNullOrEmpty(tbPassword.Text) &&
                   !string.IsNullOrEmpty(tbPort.Text) &&
                   !string.IsNullOrEmpty(tbUsername.Text);
        }

        private void cancelButton_Click(object sender, System.EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void insertListButton_Click(object sender, System.EventArgs e)
        {
            string path = null;
            using (var dialog = new FolderBrowserDialog())
                if (dialog.ShowDialog() == DialogResult.OK)
                    path = dialog.SelectedPath;

            insertListBox.Text = path;
        }

        private void docgenButton_Click(object sender, System.EventArgs e)
        {
            string path = null;
            using (var dialog = new FolderBrowserDialog())
                if (dialog.ShowDialog() == DialogResult.OK)
                    path = dialog.SelectedPath;

            docgenBox.Text = path;
        }
    }
}
