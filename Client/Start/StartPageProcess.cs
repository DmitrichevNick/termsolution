﻿using Client.UICore;
using DatabaseConnector;

namespace Client.Start
{
    public class StartPageProcess : UserProcess
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public StartPageProcess(AppEnvironment environment) : base(environment, "Стартовая страница")
        {
            AddStage(new ProcessStage("",true,1), new StartWindow());
            StagePanelVisible = false;
        }
    }
}
