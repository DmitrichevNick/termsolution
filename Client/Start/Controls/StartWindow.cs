﻿
using Client.UICore;

namespace Client.Start
{
    public partial class StartWindow : ContentControl
    {
        public StartWindow()
        {
            InitializeComponent();
            StagesPanelVisible = false;
        }
    }
}
