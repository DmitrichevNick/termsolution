﻿using Client.ImportExcelToBase.Controls;
using Client.Start;
using Client.UICore;
using DatabaseConnector;

namespace Client.ImportExcelToBase
{
    /// <summary>
    /// Процесс загрузки данных в базу.
    /// </summary>
    public class ImportExcelToBaseProcess : UserProcess
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public ImportExcelToBaseProcess(AppEnvironment environment) : base(environment, "Импорт данных")
        {
            AddStage(new ProcessStage("Выбор документов для импорта", true, 0), new ChooseControl(environment));
            StagePanelVisible = false;
        }
    }
}
