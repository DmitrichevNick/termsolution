﻿using Client.UICore;
using DatabaseConnector;
using DocumentWorker;
using Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Helper;

namespace Client.ImportExcelToBase.Controls
{
    /// <summary>
    /// Выбор данных для импорта
    /// </summary>
    public partial class ChooseControl : ContentControl
    {
        private AppEnvironment _environment;
        public ChooseControl()
        {
            InitializeComponent();
            StagesPanelVisible = false;
            StatusBar.SetText("Выберите файлы для импорта");
        }

        public ChooseControl(AppEnvironment environment) : this()
        {
            _environment = environment;
        }

        private void chooseFilesButton_Click(object sender, System.EventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Multiselect = true,
                ReadOnlyChecked = true,
                Filter = @"Excel Files|*.xls;*.xlsx;*.xlsm",
                RestoreDirectory = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                var paths = openFileDialog.FileNames;
                foreach (var path in paths)
                {
                    if (!pathsTable.Rows.Cast<DataGridViewRow>().Any(row => row.Cells["Path"].Value.ToString().Equals(path)))
                    {
                        var identType = IdentifyType(path);
                        var tableNames = identType.ConvertAll(GetTableName);

                        var selectCell = new DataGridViewComboBoxCell();
                        selectCell.DataSource = tableNames;
                        selectCell.Style.BackColor = Color.White;
                        selectCell.FlatStyle = FlatStyle.Flat;
                        pathsTable.Rows.Add(path, selectCell, identType.Any() ? "Корректно" : "Не корректно");
                        pathsTable.Rows[pathsTable.RowCount - 1].Cells["Table"] = selectCell;
                        pathsTable.Rows[pathsTable.RowCount - 1].Cells["Table"].Value = tableNames.FirstOrDefault();
                        if (!identType.Any())
                        {
                            pathsTable.Rows[pathsTable.RowCount - 1].DefaultCellStyle.BackColor = Color.Gray;
                            (pathsTable.Rows[pathsTable.RowCount - 1].Cells["Table"] as DataGridViewComboBoxCell)
                                .ReadOnly = true;
                            (pathsTable.Rows[pathsTable.RowCount - 1].Cells["Table"] as DataGridViewComboBoxCell).Style.BackColor = Color.Gray;
                        }
                    }
                }
            };
        }

        private List<Type> IdentifyType(string path)
        {
            var hash = new Dictionary<Type, List<string>>();
            var result = new List<Type>();
            hash.Add(typeof(DegreeEntity), new List<string> { "ID", "NAME", "SHORT_NAME" });
            hash.Add(typeof(DepartmentEntity), new List<string> { "ID", "NAME", "CODE", "ID_LECTURER" });
            hash.Add(typeof(EduDirectionEntity), new List<string> { "ID", "NAME", "CODE", "SHORT_NAME", "ID_LEVEL" });
            hash.Add(typeof(EduFormEntity), new List<string> { "ID", "NAME" });
            hash.Add(typeof(EduProfileEntity), new List<string> { "ID", "ID_EDU_DIRECTION", "NAME", "SHORT_NAME" });
            hash.Add(typeof(GroupEntity), new List<string> { "ID", "NUMBER", "ID_EDU_PROFILE", "COURSE", "ID_EDU_FORM" });
            hash.Add(typeof(LecturerEntity), new List<string> { "ID", "NAME", "SHORT_NAME", "ID_DEPARTMENT", "ID_DEGREE", "ID_POSITION" });
            hash.Add(typeof(LevelEntity), new List<string> { "ID", "NAME", "SHORT_NAME" });
            hash.Add(typeof(PositionEntity), new List<string> { "ID", "NAME", "SHORT_NAME" });
            hash.Add(typeof(PracticeEntity), new List<string> { "ID", "ID_PRACTICE_TYPE", "DATE_BEGIN", "DATE_END", "ID_EDU_PROFILE", "SEMESTER", "COURSE" });
            hash.Add(typeof(PracticeTypeEntity), new List<string> { "ID", "NAME", "NAME_R", "NAME_V", "ADD_NAME", "SHORT_ADD_NAME" });
            hash.Add(typeof(PracticePlaceEntity), new List<string> { "ID", "NAME" });
            hash.Add(typeof(StudentEntity), new List<string> { "ID", "NAME", "SHORT_NAME", "ID_GROUP", "ID_DEPARTMENT", "ID_LECTURER", "ID_PLACE" });

            var excelWorker = new ExcelWorker(path, true);
            result = hash.Where(ent => excelWorker.FindColumns(ent.Value)).ToList().ConvertAll(ent => ent.Key);
            excelWorker.Quit();

            return result;
        }

        private string GetTableName(Type typeTable)
        {
            if (typeTable == typeof(DegreeEntity))
                return "Степень проподавателя";
            else if (typeTable == typeof(DepartmentEntity))
                return "Кафедра";
            else if (typeTable == typeof(EduDirectionEntity))
                return "Направление подготовки";
            else if (typeTable == typeof(EduFormEntity))
                return "Форма обучения";
            else if (typeTable == typeof(EduProfileEntity))
                return "Профиль подготовки";
            else if (typeTable == typeof(GroupEntity))
                return "Группа";
            else if (typeTable == typeof(LecturerEntity))
                return "Преподаватель";
            else if (typeTable == typeof(LevelEntity))
                return "Уровень подготовки";
            else if (typeTable == typeof(PositionEntity))
                return "Должность преподавателя";
            else if (typeTable == typeof(PracticeEntity))
                return "Практика";
            else if (typeTable == typeof(PracticeTypeEntity))
                return "Тип практики";
            else if (typeTable == typeof(PracticePlaceEntity))
                return "Место проведения практики";
            else if (typeTable == typeof(StudentEntity))
                return "Студент";

            return null;
        }

        private void pathsTable_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }
    }
}
