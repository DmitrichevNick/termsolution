﻿namespace Client.ImportExcelToBase.Controls
{
    partial class ChooseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.chooseFilesButton = new System.Windows.Forms.Button();
            this.pathsTable = new System.Windows.Forms.DataGridView();
            this.Path = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Table = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Valid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.contentPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pathsTable)).BeginInit();
            this.SuspendLayout();
            // 
            // contentPanel
            // 
            this.contentPanel.Controls.Add(this.label2);
            this.contentPanel.Controls.Add(this.button1);
            this.contentPanel.Controls.Add(this.pathsTable);
            this.contentPanel.Controls.Add(this.chooseFilesButton);
            this.contentPanel.Controls.Add(this.label1);
            this.contentPanel.ForeColor = System.Drawing.Color.Black;
            this.contentPanel.Controls.SetChildIndex(this.label1, 0);
            this.contentPanel.Controls.SetChildIndex(this.chooseFilesButton, 0);
            this.contentPanel.Controls.SetChildIndex(this.pathsTable, 0);
            this.contentPanel.Controls.SetChildIndex(this.button1, 0);
            this.contentPanel.Controls.SetChildIndex(this.label2, 0);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(15, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(309, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Выберите файлы для импортирования";
            // 
            // chooseFilesButton
            // 
            this.chooseFilesButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chooseFilesButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chooseFilesButton.ForeColor = System.Drawing.Color.Black;
            this.chooseFilesButton.Location = new System.Drawing.Point(343, 92);
            this.chooseFilesButton.Name = "chooseFilesButton";
            this.chooseFilesButton.Size = new System.Drawing.Size(75, 29);
            this.chooseFilesButton.TabIndex = 3;
            this.chooseFilesButton.Text = "Выбрать";
            this.chooseFilesButton.UseVisualStyleBackColor = true;
            this.chooseFilesButton.Click += new System.EventHandler(this.chooseFilesButton_Click);
            // 
            // pathsTable
            // 
            this.pathsTable.AllowUserToAddRows = false;
            this.pathsTable.AllowUserToResizeColumns = false;
            this.pathsTable.AllowUserToResizeRows = false;
            this.pathsTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathsTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pathsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.pathsTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Path,
            this.Table,
            this.Valid});
            this.pathsTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.pathsTable.Location = new System.Drawing.Point(19, 153);
            this.pathsTable.Name = "pathsTable";
            this.pathsTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.pathsTable.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.pathsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.pathsTable.Size = new System.Drawing.Size(585, 185);
            this.pathsTable.TabIndex = 4;
            this.pathsTable.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.pathsTable_DataError);
            // 
            // Path
            // 
            this.Path.HeaderText = "Путь";
            this.Path.Name = "Path";
            this.Path.ReadOnly = true;
            // 
            // Table
            // 
            this.Table.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Table.HeaderText = "Определенный тип";
            this.Table.Name = "Table";
            this.Table.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Table.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Valid
            // 
            this.Valid.HeaderText = "Корректность";
            this.Valid.Name = "Valid";
            this.Valid.ReadOnly = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(220, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 37);
            this.button1.TabIndex = 5;
            this.button1.Text = "Начать импорт";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 341);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(474, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "* Будут обработаны только те таблицы, которые имеют признак корректности \"Коррект" +
    "но\"";
            // 
            // ChooseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ChooseControl";
            this.NextStageButtonVisible = true;
            this.PrevStageButtonVisible = true;
            this.StagesPanelVisible = true;
            this.contentPanel.ResumeLayout(false);
            this.contentPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pathsTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView pathsTable;
        private System.Windows.Forms.Button chooseFilesButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Path;
        private System.Windows.Forms.DataGridViewComboBoxColumn Table;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valid;
    }
}
