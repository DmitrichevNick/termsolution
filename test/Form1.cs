﻿using DatabaseConnector;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DocumentWorker;
using DocumentWorker.ExcelToBase;
using DocumentWorker.InsertList;
using Entity;
using Helper;
using Manager;

namespace test
{
    public partial class Form1 : Form
    {
        private AppEnvironment _environment;
        private List<GroupEntity> _listGroupEntity;
        private int? _idGroup;
        public Form1()
        {
            InitializeComponent();
            _environment = new AppEnvironment();;
            //_listGroupEntity = (new GroupManager(_environment)).ReadAll();
            //var listPractice = (new PracticeManager(_environment)).ReadAll();
            //comboBox1.DataSource = _listGroupEntity.Where(item => listPractice.Any(pactice => pactice.IdEduProfile == item.IdEduProfile &&
            //                                                                                  pactice.IdEduDirection == item.IdEduDirection &&
            //                                                                                  pactice.Course == item.Course)).ToList();
            //comboBox1.DisplayMember = "Number";
            //comboBox1.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var masterStudentExcel = new MasterStudentExcel(_environment,fileName);
                masterStudentExcel.Execute();
                masterStudentExcel.Quit();       
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //var excelReader = new ExcelWorker(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"temp\MasterStudentEduDirection.xlsx"), true);
            //var table = excelReader.ReadTableWithColumns(new List<string>
            //    {"ID", "NAME", "CODE", "SHORT_NAME", "ID_LEVEL"});
            //excelReader.Quit();

            //var manager = new EduDirectionManager(_environment);

            //foreach (DataRow row in table.Rows)
            //{
            //    var ent = new EduDirectionEntity
            //    {
            //        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
            //        Name = row["NAME"].ToString(),
            //        ShortName = row["SHORT_NAME"].ToString(),
            //        Code = row["CODE"].ToString(),
            //        IdLevel = ConvertHelper.StringToInt(row["ID_LEVEL"].ToString()),
            //        //Qualification = row["QUALIFICATION"].ToString()
            //    };
            //    manager.Write(ent);
            //}

            //excelReader = new ExcelWorker(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"temp\MasterStudentEduProfile.xlsx"), true);
            //table = excelReader.ReadTableWithColumns(new List<string>
            //    {"ID", "ID_EDU_DIRECTION", "NAME", "SHORT_NAME"});
            //excelReader.Quit();

            //var managerEduPro = new EduProfileManager(_environment);

            //foreach (DataRow row in table.Rows)
            //{
            //    var ent = new EduProfileEntity()
            //    {
            //        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
            //        IdEduDirection = ConvertHelper.StringToInt(row["ID_EDU_DIRECTION"].ToString()),
            //        Name = row["NAME"].ToString(),
            //        ShortName = row["SHORT_NAME"].ToString(),                  
            //    };
            //    managerEduPro.Write(ent);
            //}

            //excelReader = new ExcelWorker(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"temp\MasterStudentGroup.xlsx"), true);
            //table = excelReader.ReadTableWithColumns(new List<string>
            //    {"ID", "NUMBER", "ID_EDU_DIRECTION", "ID_EDU_PROFILE","COURSE","EDU_FORM"});
            //excelReader.Quit();

            //var managerGroup = new GroupManager(_environment);

            //foreach (DataRow row in table.Rows)
            //{
            //    var ent = new GroupEntity()
            //    {
            //        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
            //        IdEduDirection = ConvertHelper.StringToInt(row["ID_EDU_DIRECTION"].ToString()),
            //        IdEduProfile = ConvertHelper.StringToInt(row["ID_EDU_PROFILE"].ToString()),
            //        Number = row["NUMBER"].ToString(),
            //        Course = ConvertHelper.StringToInt(row["COURSE"].ToString()),
            //        EduForm = row["EDU_FORM"].ToString()
            //    };
            //    managerGroup.Write(ent);
            //}
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var lectures = new LecturesExcel(_environment,fileName);
                lectures.Execute();
                lectures.Quit();          
            }
        }

        private void buttonWriteDegree_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var excelReader = new ExcelWorker.ExcelWorker(fileName, true);
                var table = excelReader.ReadTableWithColumns(new List<string>
                    {"ID", "NAME", "SHORT_NAME"});
                excelReader.Quit();

                var degreeManager = new DegreeManager(_environment);

                foreach (DataRow row in table.Rows)
                {
                    var ent = new DegreeEntity()
                    {
                        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                        Name = row["NAME"].ToString(),
                        ShortName = row["SHORT_NAME"].ToString()
                    };
                    degreeManager.Write(ent);
                }
            }
        }

        private void buttonWritePositions_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var excelReader = new ExcelWorker.ExcelWorker(fileName, true);
                var table = excelReader.ReadTableWithColumns(new List<string>
                    {"ID", "NAME", "SHORT_NAME"});
                excelReader.Quit();

                var positionManager = new PositionManager(_environment);

                foreach (DataRow row in table.Rows)
                {
                    var ent = new PositionEntity()
                    {
                        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                        Name = row["NAME"].ToString(),
                        ShortName = row["SHORT_NAME"].ToString()
                    };
                    positionManager.Write(ent);
                }
            }
        }

        private void buttonWriteDepartments_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var excelReader = new ExcelWorker.ExcelWorker(fileName, true);
                var table = excelReader.ReadTableWithColumns(new List<string>
                    {"ID", "NAME", "CODE", "ID_LECTURER"});
                excelReader.Quit();

                var departmentManager = new DepartmentManager(_environment);

                foreach (DataRow row in table.Rows)
                {
                    var ent = new DepartmentEntity()
                    {
                        Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                        Name = row["NAME"].ToString(),
                        Code = row["CODE"].ToString(),
                        IdLecturer = null
                    };
                    departmentManager.Write(ent);
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    string fileName = openFileDialog.FileName;
                    var studentLecturerWorker = new StudentLecturerExcel(_environment, fileName);
                    studentLecturerWorker.FillDegree();

                    studentLecturerWorker.Quit();
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var excelReader = new ExcelWorker.ExcelWorker(fileName, true);
                var table = excelReader.ReadTableWithColumns(new List<string>
                    {"ID", "NAME", "SHORT_NAME","ID_DEPARTMENT", "ID_DEGREE", "ID_POSITION"});
                excelReader.Quit();

                var positionManager = new LecturerManager(_environment);

                foreach (DataRow row in table.Rows)
                {
                    var ent = new LecturerEntity()
                    {
                        //Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                        Name = row["NAME"].ToString(),
                        ShortName = row["SHORT_NAME"].ToString(),
                        IdDepartment = ConvertHelper.StringToInt(row["ID_DEPARTMENT"].ToString()),
                        IdDegree = ConvertHelper.StringToInt(row["ID_DEGREE"].ToString()),
                        IdPosition = ConvertHelper.StringToInt(row["ID_POSITION"].ToString()),

                    };
                    positionManager.Write(ent);
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var studentLecturerExcel = new StudentLecturerExcel(_environment, fileName);
                studentLecturerExcel.Execute();
                studentLecturerExcel.Quit();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var studentExporter = new StudentExporter(fileName, _environment);
                studentExporter.Execute();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //Object missing = Type.Missing;
            //string fileName = "";

            //using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            //{
            //    saveFileDialog.InitialDirectory = "c:\\";
            //    // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //    saveFileDialog.FilterIndex = 2;
            //    saveFileDialog.RestoreDirectory = true;

            //    if (saveFileDialog.ShowDialog() == DialogResult.OK)
            //    {
            //        fileName = saveFileDialog.FileName;
            //    }
            //}

            //if (!string.IsNullOrEmpty(fileName))
            //{
            //    var studentManager = new StudentManager(_environment);

            //    var studentList = studentManager.ReadAll().Where(item =>
            //        item.IdGroup == ConvertHelper.StringToInt(comboBox1.SelectedValue.ToString()));

            //    using (StreamWriter sw = new StreamWriter(fileName, true, System.Text.Encoding.Default))
            //    {
            //        var insertList = new InsertList(_environment, ConvertHelper.StringToInt(comboBox1.SelectedValue.ToString()));

            //        sw.Write(insertList.InitGroupInfoString());
            //        sw.WriteLine();
            //        foreach (var studentEntity in studentList)
            //        {                       
            //            insertList.ReadStudent(studentEntity.Id);
            //            sw.Write(insertList.MakeInsertList());
            //            sw.WriteLine();
            //        }
            //    } 
            //}
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var practiceTypeExporter = new PracticeTypeExporter(fileName, _environment);
                practiceTypeExporter.Execute();
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var practiceExporter = new PracticeExporter(fileName, _environment);
                practiceExporter.Execute();
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var groupExporter = new GroupExporter(fileName, _environment);
                groupExporter.Execute();
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Object missing = Type.Missing;
            string fileName = "";

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                // openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    fileName = openFileDialog.FileName;
                }
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                var groupExcel = new StudentsExcel(_environment, fileName);
                groupExcel.Execute();
                groupExcel.Quit();
            }
        }
    }
}
