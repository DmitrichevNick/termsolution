﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace DocumentGeneration
{
	class TemplateHandler
	{
		private Word.Application _wordApplication;
		private Word.Document _templateDocument;
		private Word.Paragraphs _templateParagraphs;
		private bool _isOpen;
		private Object _missing;
		private Object _fileName;
		private MatchCollection _matches;
		private Regex _regexInserts;
		private List<KeyValuePair<string, string>> _matchesList;

		public List<KeyValuePair<string, string>> MatchesList
		{
			get { return _matchesList; }
		}

		public Object FileName
		{
			get { return _fileName; }
		}

		public bool isOpen
		{
			get { return _isOpen; }
		}

		public TemplateHandler()
		{
			//this._wordApplication = new Word.Application();
			this._missing = Type.Missing;
			this._fileName = Type.Missing;
			this._isOpen = true;
			this._regexInserts = new Regex(@"#(\w*)#([()\w]*)#");
			this._matchesList = new List<KeyValuePair<string, string>>();
		}

		private Word.Document OpenTemplate(string templateFilePath)
		{
			_wordApplication = new Word.Application();
			Word.Document templateFile;

			using (OpenFileDialog openFileDialog = new OpenFileDialog())
			{
				openFileDialog.InitialDirectory = templateFilePath;
				openFileDialog.Filter = "text files(*.doc;*.docx)|*.doc;*.docx|All files (*.*)|*.*";
				openFileDialog.FilterIndex = 2;
				openFileDialog.RestoreDirectory = true;

				if (openFileDialog.ShowDialog() == DialogResult.OK)
				{
					_fileName = openFileDialog.FileName;
				}
			}

			try
			{
				if (_fileName != Type.Missing)
				{
					templateFile = _wordApplication.Documents.Open(ref _fileName,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing);
				}
				else
				{
					throw new System.Runtime.InteropServices.COMException();
				}
				
			}
			catch (System.Runtime.InteropServices.COMException)
			{
				MessageBox.Show("Шаблон был открыт некорректно");
				_isOpen = false;
				return null;
			}

			return templateFile;
		}

		public void ExtractInserts(string templateFilePath)
		{
			_templateDocument = OpenTemplate(templateFilePath);

			if (_isOpen)
			{
				_templateParagraphs = _templateDocument.Paragraphs;

				for (int i = 1; i < _templateParagraphs.Count; i++)
				{
					_matches = _regexInserts.Matches(_templateParagraphs[i].Range.Text);

					if (_matches.Count > 0)
					{
						foreach (Match match in _matches)
						{
							_matchesList.Add(new KeyValuePair<string, string>(match.Groups[0].Value, match.Groups[2].Value));
						}
					}
				}
			}

			CloseTemplate();
		}

		private void CloseTemplate()
		{
	        if(_isOpen) { _templateDocument.Close(); }
			_wordApplication.Quit();
		}
	}
}
