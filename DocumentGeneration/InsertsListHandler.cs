﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace DocumentGeneration
{
	class InsertsListHandler
	{
		private Word.Application _wordApplication;
		private Word.Document _insertsList;
		private Word.Paragraphs _insertionParagraphs;
		private Object _missing;
		private Object _fileName;
		private bool _isOpen;
		private List<Dictionary<string, string>> _studentsData;
		private string _documentFilePath;
		private int _studentCount;

		public InsertsListHandler()
		{
			//this._wordApplication = new Word.Application();
			this._missing = Type.Missing;
			this._fileName = Type.Missing;
			this._isOpen = true;
			this._studentsData = new List<Dictionary<string, string>>();
			this._documentFilePath = "";
			this._studentCount = 0;
		}

		public List<Dictionary<string, string>> StudentsData
		{
			get { return _studentsData; }
		}

		public int StudentCount
		{
			get { return _studentCount; }
		}

		public string DocumentFilePath
		{
			get { return _documentFilePath; }
		}

		private Word.Document OpenInsertsList(string filePath)
		{
			_wordApplication = new Word.Application();
			_fileName = filePath;
			Object missing = Type.Missing;
			Word.Document tmpInsertsList;

			try
			{
				tmpInsertsList = _wordApplication.Documents.Open(ref _fileName,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing,
					ref _missing, ref _missing, ref _missing);

				_insertionParagraphs = tmpInsertsList.Paragraphs;
			}
			catch (System.Runtime.InteropServices.COMException)
			{
				MessageBox.Show("Лист вставок был открыт некорректно");
				_isOpen = false;
				return null;
			}

			return tmpInsertsList;
		}

		private int MakeFilePath()
		{
			int i = 1;
			while (_insertionParagraphs[i].Range.Text.StartsWith("$"))
			{
				_documentFilePath = _documentFilePath + "\\" + _insertionParagraphs[i].Range.Text.Substring(2).Replace(Environment.NewLine, "");
				i++;
			}

			return i+1;
		}

		public void ExtractData(string filePath)
		{
			_insertsList = OpenInsertsList(filePath);

			if (_isOpen)
			{
				int i = MakeFilePath();
				Dictionary<string, string> studentDict = new Dictionary<string, string>();
				string[] stringData;

				while (i < _insertionParagraphs.Count)
				{
					

					if (_insertionParagraphs[i].Range.Text.Contains('|'))
					{
						stringData = _insertionParagraphs[i].Range.Text.Split('|');
						studentDict.Add(stringData[0], stringData[1].Replace(Environment.NewLine, ""));
					}
					else
					{
						_studentsData.Add(new Dictionary<string, string>(studentDict));
						_studentCount++;
						studentDict.Clear();
					}

					i++;
				}
			}

			CloseInsertsList();
		}

		private void CloseInsertsList()
		{
			_insertsList.Close();
			_wordApplication.Quit();
		}
	}
}
