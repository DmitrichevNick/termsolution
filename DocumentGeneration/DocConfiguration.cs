﻿using System.Configuration;

namespace DocumentGeneration
{
    class DocConfiguration
    {
        public DocConfiguration()
        {
            DefaultSaveFolder = ConfigurationManager.AppSettings["DocGenFolder"];
            TemplatesFolder = ConfigurationManager.AppSettings["TemplatesFolder"];
            InsertionList = ConfigurationManager.AppSettings["InsertionList"];
        }

        public string DefaultSaveFolder { get; }

        public string TemplatesFolder { get; }

        public string InsertionList { get; set; }
    }
}
