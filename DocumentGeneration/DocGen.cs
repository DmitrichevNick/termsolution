﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace DocumentGeneration
{
    public class DocGen
    {
        readonly DocConfiguration _docConfig;

        private Word.Application wordApplication;
        private Word.Document templateDocument;
        private Word.Document insertionList;
        private Word.Document newDocument;
        private Word.Paragraphs templateParagraphs;
        private Word.Paragraphs insertionParagraphs;
        private Word.Range wordrange;

        private Dictionary<string, string> configurationFile;
        private Regex regexInserts;
        private MatchCollection matches;
        private List<KeyValuePair<string, string>> matchesList;
        private Dictionary<string, string> inserts;
        private string documentFilePath;
        private string[] filePathParts;
        private Object fileName;
        private Object templateFileName;

        public DocGen()
        {
            this._docConfig = new DocConfiguration();
            this.configurationFile = new Dictionary<string, string>();
            this.wordApplication = new Word.Application();
            this.regexInserts = new Regex(@"#(\w*)#([()\w]*)#");
            this.matchesList = new List<KeyValuePair<string, string>>();
            this.inserts = new Dictionary<string, string>();
            this.wordApplication.Visible = false;
            this.filePathParts = new string[3];
        }

        public string get_insertionList()
        {
            return _docConfig.InsertionList;
        }
        public void setInsertionList(string path)
        {
            _docConfig.InsertionList = path;
        }

        public void template_button_Click()
        {
            templateDocument = open_file(_docConfig.TemplatesFolder);
            templateParagraphs = templateDocument.Paragraphs;
            templateFileName = fileName;

            extracting_inserts();
            templateDocument.Close();
        }


        private Word.Document open_file(string inputStr)
        {
            fileName = Type.Missing;
            Object missing = Type.Missing;

            if (!(inputStr.EndsWith(".txt")))
            {
                using (OpenFileDialog openFileDialog = new OpenFileDialog())
                {
                    openFileDialog.InitialDirectory = inputStr;
                    openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                    openFileDialog.FilterIndex = 2;
                    openFileDialog.RestoreDirectory = true;

                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        fileName = openFileDialog.FileName;
                    }
                }
            }
            else
            {
                fileName = inputStr;
            }

            return wordApplication.Documents.Open(ref fileName,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing);
        }

        private Word.Document loadInsertionList(string inputStr)
        {
            fileName = Type.Missing;
            fileName = inputStr;
            Object missing = Type.Missing;
            return wordApplication.Documents.Open(ref fileName,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing,
                ref missing, ref missing, ref missing);
        }

        private void extracting_inserts()
        {
            for (int i = 1; i < templateParagraphs.Count; i++)
            {
                matches = regexInserts.Matches(templateParagraphs[i].Range.Text);

                if (matches.Count > 0)
                {
                    foreach (Match match in matches)
                    {
                        matchesList.Add(new KeyValuePair<string, string>(match.Groups[0].Value, match.Groups[2].Value));
                    }
                }
            }
        }

        public void insertion_list_button_Click()
        {
            insertionList = loadInsertionList(_docConfig.InsertionList);
            //insertionList = open_file1(inStr);
            insertionParagraphs = insertionList.Paragraphs;
        }

        public void document_creation_button_Click(int procPerGroup, Func<int,bool> updater)
        {
            make_iteration(procPerGroup, updater);
        }

        private void make_iteration(int procPerGroup, Func<int, bool> updater)
        {
            int i = 1;
            bool nextDocument = false;
            string[] insertion_str = new string[2];
            //Установка итератора на начало списка
            while (!insertionParagraphs[i].Range.Text.StartsWith("$")) { i++; }
            var students = 1;
            int j = i;
            while (j < insertionParagraphs.Count)
            {
                if (insertionParagraphs[j].Range.Text == "\r\n")
                    students++;
                j++;
            }
            //Итерация по списку вставок
            var procPerStud = (int) Math.Ceiling((double) procPerGroup / students);
            while (i < insertionParagraphs.Count)
            {
                if (insertionParagraphs[i].Range.Text.StartsWith("$"))
                {
                    i = make_file_path(i);
                }

                if (insertionParagraphs[i].Range.Text != "\r\n")
                {
                    insertion_str = insertionParagraphs[i].Range.Text.Split('|');
                    inserts[insertion_str[0]] = insertion_str[1].Substring(0, insertion_str[1].Length - 2);
                    nextDocument = true;
                }
                else if (nextDocument != false)
                {
                    nextDocument = false;
                    newDocument = create_new_document();
                    replace();
                    create_directories();
                    save_new_document();
                    updater(procPerStud);
                }

                i++;

            }

            end_work();
        }

        private int make_file_path(int iterator)
        {
            int i = iterator;
            int part;
            string str;

            documentFilePath = "";
            str = insertionParagraphs[i].Range.Text;

            while (str.StartsWith("$"))
            {
                part = Int32.Parse(str[1].ToString()) - 1;

                switch (part)
                {
                    case 0:
                        filePathParts[part] = str.Substring(2, str.Length - 4);
                        filePathParts[1] = "";
                        filePathParts[2] = "";
                        break;
                    case 1:
                        filePathParts[part] = str.Substring(2, str.Length - 4);
                        filePathParts[2] = "";
                        break;
                    case 2:
                        filePathParts[part] = str.Substring(2, str.Length - 4);
                        break;
                    default:
                        break;
                }

                i++;

                str = insertionParagraphs[i].Range.Text;
            }

            foreach (string s in filePathParts)
            {
                //documentFilePath = documentFilePath + "\\" + s;
                if (s != "")
                    //documentFilePath = documentFilePath + " " + s;
                    documentFilePath = documentFilePath + "\\" + s;
            }

            return i + 1;
        }

        private Word.Document create_new_document()
        {
            return wordApplication.Documents.Add(templateFileName);
        }

        private void replace()
        {
            Object missing = Type.Missing;
            Object findText;

            foreach (KeyValuePair<string, string> matchPair in matchesList)
            {
                wordrange = newDocument.Range(ref missing, ref missing);
                wordrange.Find.ClearFormatting();
                findText = matchPair.Key;

                wordrange.Find.Execute(ref findText,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing, ref missing, ref missing,
                    ref missing, ref missing);

                wordrange.Select();
                wordrange.Text = inserts[matchPair.Value];
            }
        }

        private void save_new_document()
        {
            Object fileName = _docConfig.DefaultSaveFolder + documentFilePath + "\\" + inserts["FullName(student)"];
            //Object fileName = configurationFile["Folder"] + "\\" + documentFilePath + inserts["FullName(student)"];
            Object missing = Type.Missing;

            newDocument.SaveAs2(fileName, missing, missing,
                missing, missing, missing, missing, missing, missing, missing,
                missing, missing, missing, missing, missing, missing, missing);

            newDocument.Close(missing, missing, missing);
            inserts.Clear();
        }

        private void create_directories()
        {
            string path = _docConfig.DefaultSaveFolder + documentFilePath;
            DirectoryInfo dirInfo = new DirectoryInfo(path);

            if (!dirInfo.Exists)
            {
                dirInfo.Create();
            }
        }

        private void end_work()
        {
            Object missing = Type.Missing;
            inserts.Clear();
            matchesList.Clear();
            //insertionList.Close();
			//wordApplication.Quit();
            //MessageBox.Show("Документы сформированы");
        }
    }
}
