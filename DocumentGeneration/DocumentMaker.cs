﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace DocumentGeneration
{
	public class DocumentMaker
	{
		private TemplateHandler _templateHandler;
		private InsertsListHandler _insertsListHandler;
		private Word.Application _wordApplication;
		private Word.Document _newDocument;
		private Object _fileName;
		private Word.Range wordrange;
		private bool _isTemplateOpen;
		readonly DocConfiguration _docConfig;

		public DocumentMaker()
		{
			this._wordApplication = new Word.Application();
			this._templateHandler = new TemplateHandler();
			this._insertsListHandler = new InsertsListHandler();
			this._docConfig = new DocConfiguration();
		}

		public bool isTemplateOpen
		{
			get { return _isTemplateOpen; }
		}

		public string get_insertionList()
		{
			return _docConfig.InsertionList;
		}
		public void setInsertionList(string path)
		{
			_docConfig.InsertionList = path;
		}

		public void OpenTemplate()
		{
			_templateHandler.ExtractInserts(_docConfig.TemplatesFolder);
			_isTemplateOpen = _templateHandler.isOpen;
		}

		private Word.Document OpenNewDocument()
		{
			_fileName = _templateHandler.FileName;
			return _wordApplication.Documents.Add(_fileName);
		}

		public void MakeDocuments(int procPerGroup, Func<int, bool> updater)
		{
			_insertsListHandler.ExtractData(_docConfig.InsertionList);
			var studentsNumber = _insertsListHandler.StudentsData.Count;
			Object missing = Type.Missing;
			Object findText;

			var procPerStud = (int)Math.Ceiling((double)procPerGroup / studentsNumber);
			foreach (Dictionary<string, string> student in _insertsListHandler.StudentsData)
			{
				_newDocument = OpenNewDocument();
				foreach (KeyValuePair<string, string> matchPair in _templateHandler.MatchesList)
				{
					wordrange = _newDocument.Range(ref missing, ref missing);
					wordrange.Find.ClearFormatting();
					findText = matchPair.Key;

					wordrange.Find.Execute(ref findText,
						ref missing, ref missing, ref missing, ref missing,
						ref missing, ref missing, ref missing, ref missing,
						ref missing, ref missing, ref missing, ref missing,
						ref missing, ref missing);

					wordrange.Select();
					wordrange.Text = student[matchPair.Value];
				}

				CreateDirectory();
				SaveNewDocument(student["FullName(student)"]);
				updater(procPerStud);
			}
		}

		private void CreateDirectory()
		{
			string path = _docConfig.DefaultSaveFolder + _insertsListHandler.DocumentFilePath;
			DirectoryInfo dirInfo = new DirectoryInfo(path);

			if (!dirInfo.Exists)
			{
				dirInfo.Create();
			}
		}

		private void SaveNewDocument(string studentName)
		{
			Object fileName = _docConfig.DefaultSaveFolder + _insertsListHandler.DocumentFilePath + "\\" + studentName;
			//Object fileName = configurationFile["Folder"] + "\\" + documentFilePath + inserts["FullName(student)"];
			Object missing = Type.Missing;

			_newDocument.SaveAs2(fileName, missing, missing,
				missing, missing, missing, missing, missing, missing, missing,
				missing, missing, missing, missing, missing, missing, missing);

			_newDocument.Close(missing, missing, missing);
		}

		public void EndWork()
		{
			_wordApplication.Quit();
		}
	}
}
