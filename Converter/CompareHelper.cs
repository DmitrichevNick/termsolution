﻿using System;

namespace Helper
{
    public static class CompareHelper
    {
        public static bool CompareStrings(string leftString, string rightString)
        {
            return string.Equals(leftString.Replace(" ",""), rightString.Replace(" ", ""), StringComparison.CurrentCultureIgnoreCase);
        }
    }
}
