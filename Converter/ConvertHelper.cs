﻿using System;
using System.Globalization;

namespace Helper
{
    public static class ConvertHelper
    {
        public static int? StringToInt(string inString)
        {
            int? result = null;
            if (!string.IsNullOrEmpty(inString))
            {
                int tmp;
                int.TryParse(inString, out tmp);
                result = tmp;
            }

            return result;
        }

        public static string DateToMySqlDateString(DateTime inDate)
        {
            return inDate.ToString("yyyy-MM-dd H:mm:ss");
        }
    }
}