﻿using System.Collections.Generic;

namespace Helper
{
    public class Context
    {
        private Dictionary<string, object> _context;

        public Context()
        {
            _context = new Dictionary<string, object>();
        }

        public void Clear()
        {
            _context.Clear();
        }

        public object GetParameter(string key)
        {
            return _context[key];
        }

        public bool AddParameter(string key, object value)
        {
            if (_context.ContainsKey(key)) return false;

            _context.Add(key,value);
            return true;
        }
    }
}
