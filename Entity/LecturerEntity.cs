﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class LecturerEntity : LecturerStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string ShortName
        {
            get { return GetProperty("IN_SHORT_NAME"); }
            set { SetProperty("IN_SHORT_NAME", value); }
        }

        public int? IdDepartment
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_DEPARTMENT")); }
            set { SetProperty("IN_ID_DEPARTMENT", value.ToString()); }
        }

        public int? IdDegree
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_DEGREE")); }
            set { SetProperty("IN_ID_DEGREE", value.ToString()); }
        }

        public int? IdPosition
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_POSITION")); }
            set { SetProperty("IN_ID_POSITION", value.ToString()); }
        }
    }
}
