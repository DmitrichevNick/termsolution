﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class EduProfileEntity : EduProfileStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public int? IdEduDirection
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_EDU_DIRECTION")); }
            set { SetProperty("IN_ID_EDU_DIRECTION", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string ShortName
        {
            get { return GetProperty("IN_SHORT_NAME"); }
            set { SetProperty("IN_SHORT_NAME", value); }
        }
    }
}
