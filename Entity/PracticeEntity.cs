﻿using System;
using Helper;
using StorageEntity;

namespace Entity
{
    public class PracticeEntity : PracticeStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public int? IdPracticeType
        {
            get { return ConvertHelper.StringToInt(GetProperty("ID_PRACTICE_TYPE")); }
            set { SetProperty("IN_ID_PRACTICE_TYPE", value.ToString()); }
        }

        public DateTime DateBegin
        {
            get { return Convert.ToDateTime(GetProperty("IN_DATE_BEGIN")); }
            set { SetProperty("IN_DATE_BEGIN", ConvertHelper.DateToMySqlDateString(value)); }
        }

        public DateTime DateEnd
        {
            get { return Convert.ToDateTime(GetProperty("IN_DATE_END")); }
            set { SetProperty("IN_DATE_END", ConvertHelper.DateToMySqlDateString(value)); }
        }

        public int? IdEduProfile
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_EDU_PROFILE")); }
            set { SetProperty("IN_ID_EDU_PROFILE", value.ToString()); }
        }

        public int? Semester
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_SEMESTER")); }
            set { SetProperty("IN_SEMESTER", value.ToString()); }
        }

        public int? Course
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_COURSE")); }
            set { SetProperty("IN_COURSE", value.ToString()); }
        }
    }
}
