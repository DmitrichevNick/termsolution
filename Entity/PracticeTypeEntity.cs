﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class PracticeTypeEntity : PracticeTypeStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string NameR
        {
            get { return GetProperty("IN_NAME_R"); }
            set { SetProperty("IN_NAME_R", value); }
        }

        public string NameV
        {
            get { return GetProperty("IN_NAME_V"); }
            set { SetProperty("IN_NAME_V", value); }
        }

        public string AddName
        {
            get { return GetProperty("IN_ADD_NAME"); }
            set { SetProperty("IN_ADD_NAME", value); }
        }

        public string ShortAddName
        {
            get { return GetProperty("IN_SHORT_ADD_NAME"); }
            set { SetProperty("IN_SHORT_ADD_NAME", value); }
        }
    }
}
