﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class EduDirectionEntity : EduDirectionStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string Code
        {
            get { return GetProperty("IN_CODE"); }
            set { SetProperty("IN_CODE", value); }
        }

        public string ShortName
        {
            get { return GetProperty("IN_SHORT_NAME"); }
            set { SetProperty("IN_SHORT_NAME", value); }
        }

        public int? IdLevel
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_LEVEL")); }
            set { SetProperty("IN_ID_LEVEL", value.ToString()); }
        }
    }
}
