﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class GroupEntity : GroupStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public int? IdEduForm
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_EDU_FORM")); }
            set { SetProperty("IN_ID_EDU_FORM", value.ToString()); }
        }

        public int? IdEduProfile
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_EDU_PROFILE")); }
            set { SetProperty("IN_ID_EDU_PROFILE", value.ToString()); }
        }

        public int? Course
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_COURSE")); }
            set { SetProperty("IN_COURSE", value.ToString()); }
        }

        public string Number
        {
            get { return GetProperty("IN_NUMBER"); }
            set { SetProperty("IN_NUMBER", value); }
        }
    }
}
