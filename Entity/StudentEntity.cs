﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class StudentEntity : StudentStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string ShortName
        {
            get { return GetProperty("IN_SHORT_NAME"); }
            set { SetProperty("IN_SHORT_NAME", value); }
        }

        public int? IdGroup
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_GROUP")); }
            set { SetProperty("IN_ID_GROUP", value.ToString()); }
        }

        public int? IdDepartment
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_DEPARTMENT")); }
            set { SetProperty("IN_ID_DEPARTMENT", value.ToString()); }
        }

        public int? IdLecturer
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_LECTURER")); }
            set { SetProperty("IN_ID_LECTURER", value.ToString()); }
        }

        public int? IdPlace
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_PLACE")); }
            set { SetProperty("IN_ID_PLACE", value.ToString()); }
        }
    }
}
