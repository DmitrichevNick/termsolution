﻿using Helper;
using StorageEntity;

namespace Entity
{
    public class DepartmentEntity : DepartmentStorageEntity
    {
        public int? Id
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID")); }
            set { SetProperty("IN_ID", value.ToString()); }
        }

        public string Name
        {
            get { return GetProperty("IN_NAME"); }
            set { SetProperty("IN_NAME", value); }
        }

        public string Code
        {
            get { return GetProperty("IN_CODE"); }
            set { SetProperty("IN_CODE", value); }
        }

        public int? IdDepartmentHead
        {
            get { return ConvertHelper.StringToInt(GetProperty("IN_ID_DEPARTMENT_HEAD")); }
            set { SetProperty("IN_ID_DEPARTMENT_HEAD", value.ToString()); }
        }
    }
}
