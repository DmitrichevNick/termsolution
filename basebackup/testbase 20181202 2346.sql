﻿--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 8.0.80.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 02.12.2018 23:46:26
-- Версия сервера: 5.6.41-log
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Удалить процедуру `PROC_READ_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PROC_READ_PRACTICE_PLACE;

--
-- Удалить процедуру `PROC_GET_STUDENT_BY_ID`
--
DROP PROCEDURE IF EXISTS PROC_GET_STUDENT_BY_ID;

--
-- Удалить процедуру `PROC_READ_STUDENT`
--
DROP PROCEDURE IF EXISTS PROC_READ_STUDENT;

--
-- Удалить процедуру `PROC_READ_STUDENT_BY_ID`
--
DROP PROCEDURE IF EXISTS PROC_READ_STUDENT_BY_ID;

--
-- Удалить процедуру `PROC_WRITE_STUDENT`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_STUDENT;

--
-- Удалить таблицу `tabl_student`
--
DROP TABLE IF EXISTS tabl_student;

--
-- Удалить процедуру `PROC_READ_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PROC_READ_DEPARTMENT;

--
-- Удалить процедуру `PROC_WRITE_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_DEPARTMENT;

--
-- Удалить таблицу `tabl_department`
--
DROP TABLE IF EXISTS tabl_department;

--
-- Удалить процедуру `PROC_READ_GROUP`
--
DROP PROCEDURE IF EXISTS PROC_READ_GROUP;

--
-- Удалить процедуру `PROC_WRITE_GROUP`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_GROUP;

--
-- Удалить таблицу `tabl_group`
--
DROP TABLE IF EXISTS tabl_group;

--
-- Удалить процедуру `PROC_READ_LECTURER`
--
DROP PROCEDURE IF EXISTS PROC_READ_LECTURER;

--
-- Удалить процедуру `PROC_WRITE_LECTURER`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_LECTURER;

--
-- Удалить таблицу `tabl_lecturer`
--
DROP TABLE IF EXISTS tabl_lecturer;

--
-- Удалить процедуру `PROC_READ_POSITION`
--
DROP PROCEDURE IF EXISTS PROC_READ_POSITION;

--
-- Удалить процедуру `PROC_WRITE_POSITION`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_POSITION;

--
-- Удалить таблицу `tabl_position`
--
DROP TABLE IF EXISTS tabl_position;

--
-- Удалить процедуру `PROC_READ_PRACTICE`
--
DROP PROCEDURE IF EXISTS PROC_READ_PRACTICE;

--
-- Удалить процедуру `PROC_WRITE_PRACTICE`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_PRACTICE;

--
-- Удалить таблицу `tabl_practice`
--
DROP TABLE IF EXISTS tabl_practice;

--
-- Удалить процедуру `PROC_READ_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PROC_READ_PRACTICE_TYPE;

--
-- Удалить процедуру `PROC_WRITE_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_PRACTICE_TYPE;

--
-- Удалить таблицу `tabl_practice_type`
--
DROP TABLE IF EXISTS tabl_practice_type;

--
-- Удалить процедуру `PROC_WRITE_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_PRACTICE_PLACE;

--
-- Удалить таблицу `tabl_practice_place`
--
DROP TABLE IF EXISTS tabl_practice_place;

--
-- Удалить таблицу `tabl_student_group`
--
DROP TABLE IF EXISTS tabl_student_group;

--
-- Удалить процедуру `PROC_READ_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PROC_READ_EDU_PROFILE;

--
-- Удалить процедуру `PROC_WRITE_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_EDU_PROFILE;

--
-- Удалить таблицу `tabl_edu_profile`
--
DROP TABLE IF EXISTS tabl_edu_profile;

--
-- Удалить процедуру `PROC_READ_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PROC_READ_EDU_DIRECTION;

--
-- Удалить процедуру `PROC_WRITE_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_EDU_DIRECTION;

--
-- Удалить таблицу `tabl_edu_direction`
--
DROP TABLE IF EXISTS tabl_edu_direction;

--
-- Удалить процедуру `PROC_READ_DEGREE`
--
DROP PROCEDURE IF EXISTS PROC_READ_DEGREE;

--
-- Удалить процедуру `PROC_WRITE_DEGREE`
--
DROP PROCEDURE IF EXISTS PROC_WRITE_DEGREE;

--
-- Удалить таблицу `tabl_degree`
--
DROP TABLE IF EXISTS tabl_degree;

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Создать таблицу `tabl_degree`
--
CREATE TABLE tabl_degree (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(50) DEFAULT NULL COMMENT 'Íàçâàíèå',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ñîêðàùåííîå íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Ñòåïåíü ïðåïîäàâàòåëÿ';

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_DEGREE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_degree td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_degree (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_degree td
    SET td.NAME = IN_NAME,
        td.SHORT_NAME = IN_SHORT_NAME
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_DEGREE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      td.ID,
      td.NAME,
      td.SHORT_NAME
    FROM tabl_degree td;
  ELSE
    SELECT
      td.ID,
      td.NAME,
      td.SHORT_NAME
    FROM tabl_degree td
    WHERE td.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_edu_direction`
--
CREATE TABLE tabl_edu_direction (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  CODE varchar(255) DEFAULT NULL COMMENT 'Êîä',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ñîêðàùåííîå íàçâàíèå',
  LEVEL char(1) DEFAULT NULL COMMENT 'Óðîâåíü ïîäãîòîâêè',
  QUALIFICATION varchar(255) DEFAULT NULL COMMENT 'Êâàëèôèêàöèÿ',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Íàïðàâëåíèå ïîäãîòîâêè';

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_EDU_DIRECTION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_LEVEL char(1), IN IN_QUALIFICATION varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_direction ted
      WHERE ted.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_direction (ID, NAME, CODE, SHORT_NAME, LEVEL, QUALIFICATION)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_SHORT_NAME, IN_LEVEL, IN_QUALIFICATION);
  ELSE
    UPDATE tabl_edu_direction ted
    SET ted.NAME = IN_NAME,
        ted.CODE = IN_CODE,
        ted.SHORT_NAME = IN_SHORT_NAME,
        ted.LEVEL = IN_LEVEL,
        ted.QUALIFICATION = IN_QUALIFICATION
    WHERE ted.ID = IN_ID;
  END IF;

END
$$

--
-- Создать процедуру `PROC_READ_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_EDU_DIRECTION (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      ted.ID,
      ted.NAME,
      ted.CODE,
      ted.SHORT_NAME,
      ted.LEVEL,
      ted.QUALIFICATION
    FROM tabl_edu_direction ted;
  ELSE
    SELECT
      ted.ID,
      ted.NAME,
      ted.CODE,
      ted.SHORT_NAME,
      ted.LEVEL,
      ted.QUALIFICATION
    FROM tabl_edu_direction ted
    WHERE ted.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_edu_profile`
--
CREATE TABLE tabl_edu_profile (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  ID_EDU_DIRETION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàïðàâëåíèÿ ïîäãîòîâêè',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ñîêðàùåííîå íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 16,
AVG_ROW_LENGTH = 1092,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Ïðîôèëü ïîäãîòîâêè';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_edu_profile
ADD CONSTRAINT FK_TEP_ID_EDU_DIRECTION FOREIGN KEY (ID_EDU_DIRETION)
REFERENCES tabl_edu_direction (ID) ON DELETE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_EDU_PROFILE (IN IN_ID int(11), IN IN_ID_EDU_DIRECTION int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_profile tep
      WHERE tep.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_profile (ID, ID_EDU_DIRETION, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_ID_EDU_DIRECTION, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_edu_profile tep
    SET tep.NAME = IN_NAME,
        tep.SHORT_NAME = IN_SHORT_NAME,
        tep.ID_EDU_DIRETION = IN_ID_EDU_DIRECTION
    WHERE tep.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_EDU_PROFILE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tep.ID,
      tep.ID_EDU_DIRETION,
      tep.NAME,
      tep.SHORT_NAME
    FROM tabl_edu_profile tep;
  ELSE
    SELECT
      tep.ID,
      tep.ID_EDU_DIRETION,
      tep.NAME,
      tep.SHORT_NAME
    FROM tabl_edu_profile tep
    WHERE tep.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_student_group`
--
CREATE TABLE tabl_student_group (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NUMBER varchar(255) DEFAULT NULL COMMENT 'Íîìåð',
  ID_EDU_DIRECTION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàïðàâëåíèÿ ïîäãîòîâêè',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ïðîôèëÿ ïîäãîòîâêè',
  COURSE int(1) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student_group
ADD CONSTRAINT FK_TSG_ID_EDU_DIRECTION FOREIGN KEY (ID_EDU_DIRECTION)
REFERENCES tabl_edu_direction (ID) ON DELETE SET NULL;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student_group
ADD CONSTRAINT FK_TSG_ID_EDU_PROFILE FOREIGN KEY (ID_EDU_PROFILE)
REFERENCES tabl_edu_profile (ID) ON DELETE SET NULL;

--
-- Создать таблицу `tabl_practice_place`
--
CREATE TABLE tabl_practice_place (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
AVG_ROW_LENGTH = 16384,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_PRACTICE_PLACE (IN IN_ID int(11), IN IN_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_place tpp
      WHERE tpp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_place (ID, NAME)
      VALUES (IN_ID, IN_NAME);
  ELSE
    UPDATE tabl_practice_place tpp
    SET tpp.NAME = IN_NAME
    WHERE tpp.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_practice_type`
--
CREATE TABLE tabl_practice_type (
  ID int(11) NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  NAME_R varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå â ðîäèòåëüíîì ïàäåæå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_PRACTICE_TYPE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_NAME_R varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_type tpt
      WHERE tpt.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_type (ID, NAME, NAME_R)
      VALUES (IN_ID, IN_NAME, IN_NAME_R);
  ELSE
    UPDATE tabl_practice_type tpt
    SET tpt.NAME = IN_NAME,
        tpt.NAME_R = IN_NAME_R
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_PRACTICE_TYPE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R
    FROM tabl_practice_type tpt;
  ELSE
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R
    FROM tabl_practice_type tpt
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_practice`
--
CREATE TABLE tabl_practice (
  ID int(9) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  ID_PRACTICE_TYPE int(11) UNSIGNED NOT NULL COMMENT 'Òèï ïðàêòèêè',
  DATE_BEGIN date DEFAULT NULL COMMENT 'Äàòà íà÷àëà',
  DATE_END date DEFAULT NULL COMMENT 'Äàòà îêîí÷àíèÿ',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ïðîôèëÿ ïîäãîòîâêè',
  SEMESTER int(1) UNSIGNED DEFAULT NULL COMMENT 'Ñåìåñòð',
  COURSE int(1) UNSIGNED DEFAULT NULL COMMENT 'Êóðñ',
  LEVEL char(1) DEFAULT NULL COMMENT 'Óðîâåíü ïîäãîòîâêè',
  ID_EDU_DIRECTION int(11) UNSIGNED DEFAULT NULL,
  ID_GROUP int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 32,
AVG_ROW_LENGTH = 528,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать индекс `FK_TPR_ID_EDU_DIRECTION` для объекта типа таблица `tabl_practice`
--
ALTER TABLE tabl_practice
ADD INDEX FK_TPR_ID_EDU_DIRECTION (ID_EDU_DIRECTION);

--
-- Создать индекс `FK_TPR_ID_EDU_PROFILE` для объекта типа таблица `tabl_practice`
--
ALTER TABLE tabl_practice
ADD INDEX FK_TPR_ID_EDU_PROFILE (ID_EDU_PROFILE);

--
-- Создать индекс `FK_TPR_ID_PRACTICE_TYPE` для объекта типа таблица `tabl_practice`
--
ALTER TABLE tabl_practice
ADD INDEX FK_TPR_ID_PRACTICE_TYPE (ID_PRACTICE_TYPE);

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_PRACTICE (IN IN_ID int(11), IN IN_ID_PRACTICE_TYPE int(1), IN IN_DATE_BEGIN date, IN IN_DATE_END date, IN IN_ID_EDU_PROFILE int(11), IN IN_SEMESTER int(1), IN IN_COURSE int(1), IN IN_LEVEL char(1), IN IN_ID_EDU_DIRECTION int(11), IN IN_ID_GROUP int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice (ID, ID_PRACTICE_TYPE, DATE_BEGIN, DATE_END, ID_EDU_PROFILE, SEMESTER, COURSE, LEVEL, ID_EDU_DIRECTION, ID_GROUP)
      VALUES (IN_ID, IN_ID_PRACTICE_TYPE, IN_DATE_BEGIN, IN_DATE_END, IN_ID_EDU_PROFILE, IN_SEMESTER, IN_COURSE, IN_LEVEL, IN_ID_EDU_DIRECTION, IN_ID_GROUP);
  ELSE
    UPDATE tabl_practice tp
    SET tp.ID_PRACTICE_TYPE = IN_ID_PRACTICE_TYPE,
        tp.DATE_BEGIN = IN_DATE_BEGIN,
        tp.DATE_END = IN_DATE_END,
        tp.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tp.SEMESTER = IN_SEMESTER,
        tp.COURSE = IN_COURSE,
        tp.LEVEL = IN_LEVEL,
        tp.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION,
        tp.ID_GROUP = IN_ID_GROUP
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_PRACTICE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tp.ID,
      tp.ID_PRACTICE_TYPE,
      tp.DATE_BEGIN,
      tp.DATE_END,
      tp.ID_EDU_PROFILE,
      tp.SEMESTER,
      tp.COURSE,
      tp.LEVEL,
      tp.ID_EDU_DIRECTION,
      tp.ID_GROUP
    FROM tabl_practice tp;
  ELSE
    SELECT
      tp.ID,
      tp.ID_PRACTICE_TYPE,
      tp.DATE_BEGIN,
      tp.DATE_END,
      tp.ID_EDU_PROFILE,
      tp.SEMESTER,
      tp.COURSE,
      tp.LEVEL,
      tp.ID_EDU_DIRECTION,
      tp.ID_GROUP
    FROM tabl_practice tp
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_position`
--
CREATE TABLE tabl_position (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(50) DEFAULT NULL COMMENT 'Íàçâàíèå',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ñîêðàùåííîå íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_POSITION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_position tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_position (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_position tp
    SET tp.NAME = IN_NAME,
        tp.SHORT_NAME = IN_SHORT_NAME
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_POSITION (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tp.ID,
      tp.NAME,
      tp.SHORT_NAME
    FROM tabl_position tp;
  ELSE
    SELECT
      tp.ID,
      tp.NAME,
      tp.SHORT_NAME
    FROM tabl_position tp
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_lecturer`
--
CREATE TABLE tabl_lecturer (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Ô.È.Î. ïîëíîñòüþ',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ôàìèëèÿ È.Î.',
  ID_DEPARTMENT int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ êàôåäðû',
  ID_DEGREE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ñòåïåíè',
  ID_POSITION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ äîëæíîñòè',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 192,
AVG_ROW_LENGTH = 257,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TL_ID_DEGREE FOREIGN KEY (ID_DEGREE)
REFERENCES tabl_degree (ID) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TL_ID_POSITION FOREIGN KEY (ID_POSITION)
REFERENCES tabl_position (ID) ON DELETE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_LECTURER (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_DEPARTMENT int(11), IN IN_ID_DEGREE int(11), IN IN_ID_POSITION int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_lecturer tl
      WHERE tl.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_lecturer (ID, NAME, SHORT_NAME, ID_DEPARTMENT, ID_DEGREE, ID_POSITION)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_DEPARTMENT, IN_ID_DEGREE, IN_ID_POSITION);
  ELSE
    UPDATE tabl_lecturer tl
    SET tl.NAME = IN_NAME,
        tl.SHORT_NAME = IN_SHORT_NAME,
        tl.ID_DEPARTMENT = IN_ID_LECTURER,
        tl.ID_DEGREE = IN_ID_DEGREE,
        tl.ID_POSITION = IN_ID_POSITION
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_LECTURER (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      *
    FROM tabl_lecturer tl;
  ELSE
    SELECT
      *
    FROM tabl_lecturer tl
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_group`
--
CREATE TABLE tabl_group (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NUMBER varchar(255) DEFAULT NULL COMMENT 'Íîìåð',
  ID_EDU_DIRECTION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàïðàâëåíèÿ ïîäãîòîâêè',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ïðîôèëÿ ïîäãîòîâêè',
  COURSE int(1) UNSIGNED DEFAULT NULL,
  EDU_FORM varchar(255) DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 18,
AVG_ROW_LENGTH = 963,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_GROUP (IN IN_ID int(11), IN IN_NUMBER varchar(255), IN IN_ID_EDU_DIRECTION int(11), IN IN_ID_EDU_PROFILE int(11), IN IN_COURSE int(1), IN IN_EDU_FORM varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_group tg
      WHERE tg.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_group (ID, NUMBER, ID_EDU_DIRECTION, ID_EDU_PROFILE, COURSE, EDU_FORM)
      VALUES (IN_ID, IN_NUMBER, IN_ID_EDU_DIRECTION, IN_ID_EDU_PROFILE, IN_COURSE, IN_EDU_FORM);
  ELSE
    UPDATE tabl_group tg
    SET tg.NUMBER = IN_NUMBER,
        tg.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION,
        tg.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tg.COURSE = IN_COURSE,
        tg.EDU_FORM = IN_EDU_FORM
    WHERE tg.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_GROUP (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tg.ID,
      tg.NUMBER,
      tg.ID_EDU_DIRECTION,
      tg.ID_EDU_PROFILE,
      tg.COURSE,
      tg.EDU_FORM
    FROM tabl_group tg;
  ELSE
    SELECT
      tg.ID,
      tg.NUMBER,
      tg.ID_EDU_DIRECTION,
      tg.ID_EDU_PROFILE,
      tg.COURSE,
      tg.EDU_FORM
    FROM tabl_group tg
    WHERE tg.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_department`
--
CREATE TABLE tabl_department (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  CODE varchar(255) DEFAULT NULL COMMENT 'Êîä',
  ID_LECTURER int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ðóêîâîäèòåëÿ',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Êàôåäðà';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_department
ADD CONSTRAINT FK_TDEP_ID_LECTURER FOREIGN KEY (ID_LECTURER)
REFERENCES tabl_lecturer (ID) ON DELETE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TL_ID_DEPARTMENT FOREIGN KEY (ID_DEPARTMENT)
REFERENCES tabl_department (ID) ON DELETE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_DEPARTMENT (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_ID_LECTURER int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_department td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_department (ID, NAME, CODE, ID_LECTURER)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_ID_LECTURER);
  ELSE
    UPDATE tabl_department td
    SET td.NAME = IN_NAME,
        td.CODE = IN_CODE,
        td.ID_LECTURER = IN_ID_LECTURER
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_DEPARTMENT (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      *
    FROM tabl_department td;
  ELSE
    SELECT
      *
    FROM tabl_department td
    WHERE td.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_student`
--
CREATE TABLE tabl_student (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Ô.È.Î. ïîëíîñòüþ',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ôàìèëèÿ È.Î.',
  EDU_FORM varchar(255) DEFAULT NULL COMMENT 'Ôîðìà îáó÷åíèÿ',
  ID_GROUP int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ãðóïïû',
  ID_DEPARTMENT int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ êàôåäðû',
  ID_LECTURER int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàó÷íîãî ðóêîâîäèòåëÿ',
  ID_PLACE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ìåñòà ïðîõîæäåíèÿ ïðàêòèêè',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 165,
AVG_ROW_LENGTH = 481,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_DEPARTMENT FOREIGN KEY (ID_DEPARTMENT)
REFERENCES tabl_department (ID) ON DELETE SET NULL;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_GROUP FOREIGN KEY (ID_GROUP)
REFERENCES tabl_group (ID) ON DELETE SET NULL;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_LECTURER FOREIGN KEY (ID_LECTURER)
REFERENCES tabl_lecturer (ID) ON DELETE SET NULL;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_PLACE FOREIGN KEY (ID_PLACE)
REFERENCES tabl_practice_place (ID) ON DELETE SET NULL;

DELIMITER $$

--
-- Создать процедуру `PROC_WRITE_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_WRITE_STUDENT (IN IN_ID int, IN IN_NAME varchar(50), IN IN_SHORT_NAME varchar(255), IN IN_ID_GROUP int, IN IN_ID_DEPARTMENT int, IN IN_ID_LECTURER int, IN IN_ID_PLACE int)
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_student ts
      WHERE ts.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_student (ID, NAME, SHORT_NAME, ID_GROUP, ID_DEPARTMENT, ID_LECTURER, ID_PLACE)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_GROUP, IN_ID_DEPARTMENT, IN_ID_LECTURER, IN_ID_PLACE);
  ELSE
    UPDATE tabl_student ts
    SET ts.NAME = IN_NAME,
        ts.SHORT_NAME = IN_SHORT_NAME,
        ts.ID_GROUP = IN_ID_GROUP,
        ts.ID_DEPARTMENT = IN_ID_DEPARTMENT,
        ts.ID_LECTURER = IN_ID_LECTURER,
        ts.ID_PLACE = IN_ID_PLACE
    WHERE ts.ID = IN_ID;
  END IF;


END
$$

--
-- Создать процедуру `PROC_READ_STUDENT_BY_ID`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_STUDENT_BY_ID (IN IN_ID int)
BEGIN
  SELECT
    ts.ID,
    ts.NAME,
    ts.SHORT_NAME,
    ts.EDU_FORM,
    ts.ID_GROUP,
    ts.ID_DEPARTMENT,
    ts.ID_LECTURER,
    ts.ID_PLACE
  FROM testbase.tabl_student ts
  WHERE ts.ID = IN_ID;
END
$$

--
-- Создать процедуру `PROC_READ_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_STUDENT (IN IN_ID int)
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      ts.ID,
      ts.NAME,
      ts.SHORT_NAME,
      ts.ID_GROUP,
      ts.ID_DEPARTMENT,
      ts.ID_LECTURER,
      ts.ID_PLACE
    FROM testbase.tabl_student ts;
  ELSE
    SELECT
      ts.ID,
      ts.NAME,
      ts.SHORT_NAME,
      ts.ID_GROUP,
      ts.ID_DEPARTMENT,
      ts.ID_LECTURER,
      ts.ID_PLACE
    FROM testbase.tabl_student ts
    WHERE ts.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_GET_STUDENT_BY_ID`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_GET_STUDENT_BY_ID (IN IN_ID int)
BEGIN
  SELECT
    *
  FROM testbase.tabl_student ts
  WHERE ts.ID = IN_ID;
END
$$

--
-- Создать процедуру `PROC_READ_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_PRACTICE_PLACE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tpp.ID,
      tpp.NAME
    FROM tabl_practice_place tpp;
  ELSE
    SELECT
      tpp.ID,
      tpp.NAME
    FROM tabl_practice_place tpp
    WHERE tpp.ID = IN_ID;
  END IF;
END
$$

DELIMITER ;

-- 
-- Вывод данных для таблицы tabl_edu_direction
--
INSERT INTO tabl_edu_direction VALUES
(1, 'Математика', '01.04.01', 'Кор. имя', 'М', 'Магистратура'),
(2, 'Прикладная математика и информатика', '01.04.02', 'Кор. имя', 'М', 'Магистратура'),
(3, 'Механика и математическое моделирование', '01.04.03', 'Кор. имя', 'М', 'Магистратура'),
(4, 'Математика и компьютерные науки', '02.04.01', 'Кор. имя', 'М', 'Магистратура'),
(5, 'Фундаментальная информатика и информационные технологии', '02.04.02', 'Кор. имя', 'М', 'Магистратура'),
(6, 'Прикладная информатика', '09.04.03', 'Кор. имя', 'М', 'Магистратура');

-- 
-- Вывод данных для таблицы tabl_position
--
INSERT INTO tabl_position VALUES
(1, 'ассистент', 'ассистент'),
(2, 'доцент', 'доц.'),
(3, 'профессор', 'проф.'),
(4, 'старший научный сотрудник', 'ст. науч. сотр.'),
(5, 'младший научный сотрудник', 'мл. науч. сотр.'),
(6, 'заведующий кафедрой', 'зав. каф.');

-- 
-- Вывод данных для таблицы tabl_degree
--
INSERT INTO tabl_degree VALUES
(1, 'доктор технических наук', 'д.т.н.'),
(2, 'доктор физико-математических наук', 'д.ф.-м.н.'),
(3, 'кандидат технических наук', 'к.т.н.'),
(4, 'кандидат физико-математических наук', 'к.ф.-м.н.'),
(5, 'кандидат филологических наук', 'к.ф.н.'),
(6, 'старший научный сотрудник', 'с.н.с.');

-- 
-- Вывод данных для таблицы tabl_edu_profile
--
INSERT INTO tabl_edu_profile VALUES
(1, 1, 'Фундаментальная математика и приложения', 'Кор. имя'),
(2, 2, 'Компьютерные науки и приложения', 'Кор. имя'),
(3, 2, NULL, 'Кор. имя'),
(4, 3, 'Информационное и программное обеспечение. Инженерия', 'Кор. имя'),
(5, 4, 'Математическое и компьютерное моделирование в естественных науках', 'Кор. имя'),
(6, 5, NULL, 'Кор. имя'),
(7, 6, 'Прикладная информатика в области принятия решений', 'Кор. имя'),
(8, 2, 'Вычислительные методы и суперкомпьютерные технологии', 'Кор. имя'),
(9, 2, 'Математическое моделирование динамики систем и процессов управления', 'Кор. имя'),
(10, 2, 'Математическое моделирование физико-механических процессов', 'Кор. имя'),
(11, 3, NULL, 'Кор. имя'),
(12, 4, NULL, 'Кор. имя'),
(13, 5, 'Инженерия программного обеспечения', 'Кор. имя'),
(14, 5, 'Компьютерная графика и моделирование живых и технических систем', 'Кор. имя'),
(15, 6, NULL, 'Кор. имя');

-- 
-- Вывод данных для таблицы tabl_practice_place
--
INSERT INTO tabl_practice_place VALUES
(1, 'какое-то место');

-- 
-- Вывод данных для таблицы tabl_lecturer
--
INSERT INTO tabl_lecturer VALUES
(1, 'Ефремова Людмила Сергеевна', 'Ефремова Л. С.', 5, 4, 5),
(2, 'Алексеев Владимир Евгеньевич', 'Алексеев В. Е.', 1, 2, 3),
(3, 'Баландин Александр Владимирович', 'Баландин А. В.', 1, NULL, 2),
(4, 'Бастраков Сергей Иванович', 'Бастраков С. И.', 1, NULL, 5),
(5, 'Веселов Сергей Иванович', 'Веселов С. И.', 1, 4, 2),
(6, 'Грибанов Дмитрий Владимирович', 'Грибанов Д. В.', 1, NULL, 5),
(7, 'Захарова Дарья Владимировна', 'Захарова Д. В.', 1, NULL, 1),
(8, 'Звонилов Виктор Иванович', 'Звонилов В. И.', 1, NULL, 2),
(9, 'Золотых Николай Юрьевич', 'Золотых Н. Ю.', 1, 2, 3),
(10, 'Кузнецов Михаил Иванович', 'Кузнецов М. И.', 1, 2, 6),
(11, 'Любимцев Олег Владимирович', 'Любимцев О. В.', 1, NULL, 2),
(12, 'Малышев Дмитрий Сергеевич', 'Малышев Д. С.', 1, 2, 3),
(13, 'Мокеев Дмитрий Борисович', 'Мокеев Д. Б.', 1, NULL, 1),
(14, 'Мокеев Дмитрий Борисович', 'Мокеев Д. Б.', 1, NULL, 5),
(15, 'Муляр Ольга Александровна', 'Муляр О. А.', 1, NULL, 1),
(16, 'Полотовский Григорий Михайлович', 'Полотовский Г. М.', 1, NULL, 2),
(17, 'Разуваев Алексей Григорьевич', 'Разуваев А. Г.', 1, NULL, 2),
(18, 'Сидоров Сергей Владимирович', 'Сидоров С. В.', 1, NULL, 1),
(19, 'Смирнова Татьяна Геннадьевна', 'Смирнова Т. Г.', 1, 4, 2),
(20, 'Сорочан Сергей Владимирович', 'Сорочан С. В.', 1, 4, 2),
(21, 'Титова Елена Борисовна', 'Титова Е. Б.', 1, NULL, 1),
(22, 'Чирков Александр Юрьевич', 'Чирков А. Ю.', 1, NULL, 2),
(23, 'Шевченко Валерий Николаевич', 'Шевченко В. Н.', 1, NULL, 3),
(24, 'Баландин Дмитрий Владимирович', 'Баландин Д. В.', 2, 2, 3),
(25, 'Гордеева Ольга Владимировна', 'Гордеева О. В.', 2, NULL, 1),
(26, 'Донцова Марина Владимировна', 'Донцова М. В.', 2, NULL, 1),
(27, 'Драгунов Тимофей Николаевич', 'Драгунов Т. Н.', 2, NULL, 2),
(28, 'Ефремова Людмила Сергеевна', 'Ефремова Л. С.', 2, 4, 2),
(29, 'Калашников Александр Львович', 'Калашников А. Л.', 2, NULL, 2),
(30, 'Калинин Алексей Вячеславович', 'Калинин А. В.', 2, 4, 2),
(31, 'Калинин Алексей Вячеславович', 'Калинин А. В.', 2, 4, 6),
(32, 'Костромина Ольга Сергеевна', 'Костромина О. С.', 2, NULL, 1),
(33, 'Кротов Николай Владимирович', 'Кротов Н. В.', 2, 4, 2),
(34, 'Кузенков Олег Анатольевич', 'Кузенков О. А.', 2, 4, 2),
(35, 'Лерман Лев Михайлович', 'Лерман Л. М.', 2, 2, 3),
(36, 'Лукьянов Валерий Иванович', 'Лукьянов В. И.', 2, NULL, 2),
(37, 'Малкин Михаил Иосифович', 'Малкин М. И.', 2, 4, 2),
(38, 'Махрова Елена Николаевна', 'Махрова Е. Н.', 2, NULL, 2),
(39, 'Морозов Альберт Дмитриевич', 'Морозов А. Д.', 2, 2, 3),
(40, 'Потёмин Геннадий Владимирович', 'Потёмин Г. В.', 2, NULL, 2),
(41, 'Рябова Елена Александровна', 'Рябова Е. А.', 2, NULL, 2),
(42, 'Стребуляев Сергей Николаевич', 'Стребуляев С. Н.', 2, NULL, 2),
(43, 'Стронгина Наталья Романовна', 'Стронгина Н. Р.', 2, NULL, 2),
(44, 'Терентьев Алексей Михайлович', 'Терентьев А. М.', 2, NULL, 2),
(45, 'Федоткин Андрей Михайлович', 'Федоткин А. М.', 2, NULL, 2),
(46, 'Федюков Александр Анатольевич', 'Федюков А. А.', 2, NULL, 1),
(47, 'Филиппов Викторий Николаевич', 'Филиппов В. Н.', 2, 4, 2),
(48, 'Чубаров Георгий Владимирович', 'Чубаров Г. В.', 2, NULL, 1),
(49, 'Афраймович Лев Григорьевич', 'Афраймович Л. Г.', 3, 1, 3),
(50, 'Банкрутенко Владимир Викторович', 'Банкрутенко В. В.', 3, NULL, 2),
(51, 'Басалин Павел Дмитриевич', 'Басалин П. Д.', 3, NULL, 2),
(52, 'Белокрылов Петр Юрьевич', 'Белокрылов П. Ю.', 3, NULL, 2),
(53, 'Быкова Маргарита Александровна', 'Быкова М. А.', 3, NULL, 1),
(54, 'Иорданский Михаил Анатольевич', 'Иорданский М. А.', 3, NULL, 3),
(55, 'Карпычев Владимир Юрьевич', 'Карпычев В. Ю.', 3, NULL, 3),
(56, 'Коротченко Анатолий Григорьевич', 'Коротченко А. Г.', 3, NULL, 2),
(57, 'Костюков Валентин Ефимович', 'Костюков В. Е.', 3, NULL, 3),
(58, 'Кумагина Елена Александровна', 'Кумагина Е. А.', 3, 3, 2),
(59, 'Неймарк Елена Александровна', 'Неймарк Е. А.', 3, 3, 2),
(60, 'Плехов Александр Сергеевич', 'Плехов А. С.', 3, NULL, 2),
(61, 'Прилуцкий Михаил Хаимович', 'Прилуцкий М. Х.', 3, NULL, 6),
(62, 'Старостин Николай Владимирович', 'Старостин Н. В.', 3, 1, 3),
(63, 'Филимонов Андрей Викторович', 'Филимонов А. В.', 3, 3, 2),
(64, 'Фомина Ирина Александровна', 'Фомина И. А.', 3, NULL, 2),
(65, 'Чернышова Наталья Николаевна', 'Чернышова Н. Н.', 3, 4, 2),
(66, 'Баркалов Константин Александрович', 'Баркалов К. А.', 4, 4, 2),
(67, 'Баркалов Константин Александрович', 'Баркалов К. А.', 4, 4, 4),
(68, 'Васильев Евгений Павлович', 'Васильев Е. П.', 4, NULL, 1),
(69, 'Гетманская Александра Александровна', 'Гетманская А. А.', 4, NULL, 1),
(70, 'Горячих Алексей Сергеевич', 'Горячих А. С.', 4, NULL, 5),
(71, 'Гришагин Владимир Александрович', 'Гришагин В. А.', 4, 4, 2),
(72, 'Исрафилов Руслан Алиярович', 'Исрафилов Р. А.', 4, NULL, 5),
(73, 'Козинов Евгений Александрович', 'Козинов Е. А.', 4, NULL, 1),
(74, 'Козинов Евгений Александрович', 'Козинов Е. А.', 4, NULL, 5),
(75, 'Кочеганова Мария Анатольевна', 'Кочеганова М. А.', 4, NULL, 5),
(76, 'Кустикова Валентина Дмитриевна', 'Кустикова В. Д.', 4, 3, 2),
(77, 'Мееров Иосиф Борисович', 'Мееров И. Б.', 4, 3, 2),
(78, 'Носова Светлана Александровна', 'Носова С. А.', 4, NULL, 1),
(79, 'Пирова Анна Юрьевна', 'Пирова А. Ю.', 4, NULL, 1),
(80, 'Свистунов Алексей Николаевич', 'Свистунов А. Н.', 4, NULL, 1),
(81, 'Сергеев Ярослав Дмитриевич', 'Сергеев Я. Д.', 4, 2, 3),
(82, 'Соврасов Владислав Валерьевич', 'Соврасов В. В.', 4, NULL, 5),
(83, 'Сысоев Александр Владимирович', 'Сысоев А. В.', 4, 3, 2),
(84, 'Турлапов Вадим Евгеньевич', 'Турлапов В. Е.', 4, 1, 3),
(85, 'Швецов Владимир Иванович', 'Швецов В. И.', 4, NULL, 3),
(86, 'Шестакова Наталья Валерьевна', 'Шестакова Н. В.', 4, NULL, 1),
(87, 'Андрианов Валерий Леонидович', 'Андрианов В. Л.', 6, NULL, 2),
(88, 'Гаврилов Владимир Сергеевич', 'Гаврилов В. С.', 6, NULL, 2),
(89, 'Галкин Олег Евгеньевич', 'Галкин О. Е.', 6, 4, 2),
(90, 'Галкина Светлана Юрьевна', 'Галкина С. Ю.', 6, 4, 2),
(91, 'Грезина Александра Викторовна', 'Грезина А. В.', 6, 4, 2),
(92, 'Денисов Сергей', 'Денисов С.', 6, NULL, 3),
(93, 'Денисова Наталья Андреевна', 'Денисова Н. А.', 6, 4, 2),
(94, 'Додунова Людмила Кузьминична', 'Додунова Л. К.', 6, 4, 2),
(95, 'Дубков Александр Александрович', 'Дубков А. А.', 6, NULL, 4),
(96, 'Забурдаев Василий Юрьевич', 'Забурдаев В. Ю.', 6, NULL, 4),
(97, 'Заикин Алексей Анатольевич', 'Заикин А. А.', 6, 4, 4),
(98, 'Заикин Алексей Анатольевич', 'Заикин А. А.', 6, 4, 3),
(99, 'Иванченко Михаил Васильевич', 'Иванченко М. В.', 6, 2, 3),
(100, 'Изосимова Ольга Алексеевна', 'Изосимова О. А.', 6, NULL, 1),
(101, 'Кустикова Валентина Дмитриевна', 'Кустикова В. Д.', 6, 3, 5),
(102, 'Лаптева Татьяна', 'Лаптева Т.', 6, NULL, 4),
(103, 'Метрикин Владимир Семенович', 'Метрикин В. С.', 6, 4, 2),
(104, 'Митрякова Татьяна Михайловна', 'Митрякова Т. М.', 6, 4, 2),
(105, 'Панасенко Адольф Григорьевич', 'Панасенко А. Г.', 6, 4, 2),
(106, 'Панкратов Андрей Леонидович', 'Панкратов А. Л.', 6, 2, 3),
(107, 'Панкратова Евгения Валерьевна', 'Панкратова Е. В.', 6, NULL, 2),
(108, 'Панкратова Евгения Валерьевна', 'Панкратова Е. В.', 6, NULL, 4),
(109, 'Харчева Анна Александровна', 'Харчева А. А.', 6, NULL, 5),
(110, 'Чернов Андрей Владимирович', 'Чернов А. В.', 6, NULL, 2),
(111, 'Ястребова Ирина Юрьевна', 'Ястребова И. Ю.', 6, NULL, 2),
(112, 'Борисов Николай Анатольевич', 'Борисов Н. А.', 7, 3, 2),
(113, 'Гергель Виктор Павлович', 'Гергель В. П.', 7, NULL, 6),
(114, 'Голышева Наталья Михайловна', 'Голышева Н. М.', 7, NULL, 2),
(115, 'Зорин Андрей Владимирович', 'Зорин А. В.', 7, 2, 3),
(116, 'Зорин Владимир Александрович', 'Зорин В. А.', 7, NULL, 2),
(117, 'Карпенко Сергей Николаевич', 'Карпенко С. Н.', 7, NULL, 2),
(118, 'Кочеганова Мария Анатольевна', 'Кочеганова М. А.', 7, NULL, 1),
(119, 'Кувыкина Елена Вадимовна', 'Кувыкина Е. В.', 7, 4, 2),
(120, 'Кудрявцев Евгений Владимирович', 'Кудрявцев Е. В.', 7, NULL, 1),
(121, 'Кузенкова Галина Владимировна', 'Кузенкова Г. В.', 7, NULL, 2),
(122, 'Малкина Елена Владиславовна', 'Малкина Е. В.', 7, NULL, 2),
(123, 'Пакшин Павел Владимирович', 'Пакшин П. В.', 7, NULL, 3),
(124, 'Пройдакова Екатерина Вадимовна', 'Пройдакова Е. В.', 7, 4, 2),
(125, 'Тихов Михаил Семенович', 'Тихов М. С.', 7, NULL, 3),
(126, 'Федоткин Михаил Андреевич', 'Федоткин М. А.', 7, NULL, 3),
(127, 'Шагбазян Давид Варданович', 'Шагбазян Д. В.', 7, NULL, 1),
(128, 'Шапошников Дмитрий Евгеньевич', 'Шапошников Д. Е.', 7, 4, 2),
(129, 'Штанюк Антон Александрович', 'Штанюк А. А.', 7, 3, 2),
(130, 'Ярощук Марина Владимировна', 'Ярощук М. В.', 7, NULL, 2),
(131, 'Абросимов Николай Анатольевич', 'Абросимов Н. А.', 8, NULL, 3),
(132, 'Баландин Владимир Васильевич', 'Баландин В. В.', 8, 6, 2),
(133, 'Горохов Василий Андреевич', 'Горохов В. А.', 8, NULL, 2),
(134, 'Жегалов Дмитрий Владимирович', 'Жегалов Д. В.', 8, NULL, 2),
(135, 'Жидков Александр Васильевич', 'Жидков А. В.', 8, 3, 2),
(136, 'Игумнов Леонид Александрович', 'Игумнов Л. А.', 8, NULL, 6),
(137, 'Ипатов Александр Александрович', 'Ипатов А. А.', 8, NULL, 1),
(138, 'Киселев Владимир Геннадьевич', 'Киселев В. Г.', 8, 3, 2),
(139, 'Константинов Александр Юрьевич', 'Константинов А. Ю.', 8, NULL, 2),
(140, 'Котов Василий Леонидович', 'Котов В. Л.', 8, NULL, 3),
(141, 'Кочетков Анатолий Васильевич', 'Кочетков А. В.', 8, NULL, 3),
(142, 'Леонтьев Николай Васильевич', 'Леонтьев Н. В.', 8, NULL, 2),
(143, 'Леонтьева Анна Викторовна', 'Леонтьева А. В.', 8, NULL, 2),
(144, 'Литвинчук Светлана Юрьевна', 'Литвинчук С. Ю.', 8, 4, 2),
(145, 'Любимов Александр Константинович', 'Любимов А. К.', 8, 2, 3),
(146, 'Ляхов Александр Федорович', 'Ляхов А. Ф.', 8, 4, 2),
(147, 'Маркина Марина Викторовна', 'Маркина М. В.', 8, 4, 2),
(148, 'Новиков Валерий Вячеславович', 'Новиков В. В.', 8, 2, 3),
(149, 'Панов Владимир Александрович', 'Панов В. А.', 8, NULL, 3),
(150, 'Сабаева Татьяна Анатольевна', 'Сабаева Т. А.', 8, NULL, 2),
(151, 'Савихин Олег Геннадьевич', 'Савихин О. Г.', 8, 4, 2),
(152, 'Сандалов Владимир Михайлович', 'Сандалов В. М.', 8, 4, 2),
(153, 'Сергеев Олег Анатольевич', 'Сергеев О. А.', 8, 3, 2),
(154, 'Февральских Любовь Николаевна', 'Февральских Л. Н.', 8, NULL, 1),
(155, 'Чекмарев Дмитрий Тимофеевич', 'Чекмарев Д. Т.', 8, 2, 3),
(156, 'Чурилов Юрий Анатольевич', 'Чурилов Ю. А.', 8, NULL, 2),
(157, 'Бирюков Руслан Сергеевич', 'Бирюков Р. С.', 9, NULL, 1),
(158, 'Болотов Максим Ильич', 'Болотов М. И.', 9, NULL, 5),
(159, 'Борисов Николай Анатольевич', 'Борисов Н. А.', 9, 3, 4),
(160, 'Волков Валентин Владимирович', 'Волков В. В.', 9, NULL, 5),
(161, 'Волкова Наталья Геннадьевна', 'Волкова Н. Г.', 9, NULL, 5),
(162, 'Городецкий Станислав Юрьевич', 'Городецкий С. Ю.', 9, NULL, 2),
(163, 'Губина Елена Васильевна', 'Губина Е. В.', 9, NULL, 2),
(164, 'Дерендяев Николай Васильевич', 'Дерендяев Н. В.', 9, NULL, 3),
(165, 'Другова Ольга Валентиновна', 'Другова О. В.', 9, NULL, 2),
(166, 'Евстропова Светлана Валериановна', 'Евстропова С. В.', 9, NULL, 5),
(167, 'Кадина Елена Юрьевна', 'Кадина Е. Ю.', 9, NULL, 1),
(168, 'Карпенко Сергей Николаевич', 'Карпенко С. Н.', 9, NULL, 4),
(169, 'Кацубо Елизавета Михайловна', 'Кацубо Е. М.', 9, NULL, 5),
(170, 'Киселева Наталья Владимировна', 'Киселева Н. В.', 9, NULL, 2),
(171, 'Лаптева Татьяна', 'Лаптева Т.', 9, NULL, 2),
(172, 'Леванов Владимир Михайлович', 'Леванов В. М.', 9, NULL, 4),
(173, 'Леванова Татьяна Александровна', 'Леванова Т. А.', 9, NULL, 1),
(174, 'Майорова Мария Викторовна', 'Майорова М. В.', 9, NULL, 5),
(175, 'Мееров Иосиф Борисович', 'Мееров И. Б.', 9, 3, 4),
(176, 'Осипов Григорий Владимирович', 'Осипов Г. В.', 9, NULL, 6),
(177, 'Петров Валентин Сергеевич', 'Петров В. С.', 9, NULL, 4),
(178, 'Рождественская Татьяна Александровна', 'Рождественская Т. А.', 9, NULL, 5),
(179, 'Свистунов Алексей Николаевич', 'Свистунов А. Н.', 9, NULL, 5),
(180, 'Чачхиани Татьяна Игоревна', 'Чачхиани Т. И.', 9, NULL, 2),
(181, 'Шапошников Дмитрий Евгеньевич', 'Шапошников Д. Е.', 9, 4, 4),
(182, 'Малкин Михаил Иосифович', 'Малкин М. И.', 4, 4, 4),
(183, 'Ремизов Иван Дмитриевич', 'Ремизов И. Д.', 5, NULL, 5),
(184, 'Зиновьев Андрей Юрьевич', 'Зиновьев А. Ю.', 4, NULL, 4),
(185, 'Кузенков Олег Анатольевич', 'Кузенков О. А.', 4, 4, 4),
(186, 'Макаров Валерий Анатольевич', 'Макаров В. А.', 4, NULL, 4),
(187, 'Миркес Евгений Моисеевич', 'Миркес Е. М.', 4, NULL, 4),
(188, 'Нуйдель Ирина Владимировна', 'Нуйдель И. В.', 4, NULL, 4),
(189, 'Борусяк Александр Владимирович', 'Борусяк А. В.', 5, NULL, 5),
(190, 'Пахомов Павел Александрович', 'Пахомов П. А.', 5, NULL, 5),
(191, 'Ясаков Юрий Васильевич', 'Ясаков Ю. В.', 4, NULL, 4);

-- 
-- Вывод данных для таблицы tabl_group
--
INSERT INTO tabl_group VALUES
(1, '381802м', 1, 1, 1, 'Очная'),
(2, '381703м4', 2, 2, 1, 'Очная'),
(3, '381803м1', 2, 3, 1, 'Очная'),
(4, '381804м', 3, 4, 1, 'Очная'),
(5, '381805м', 4, 5, 1, 'Очная'),
(6, '381806м1', 5, 6, 1, 'Очная'),
(7, '381807м', 6, 7, 1, 'Очная'),
(8, '381702м', 1, 1, 2, 'Очная'),
(9, '381703м3', 2, 8, 2, 'Очная'),
(10, '381603м1', 2, 9, 2, 'Очная'),
(11, '381703м1', 2, 9, 2, 'Очная'),
(12, '381703м2', 2, 10, 2, 'Очная'),
(13, '381704м', 3, 11, 2, 'Очная'),
(14, '381705м', 4, 12, 2, 'Очная'),
(15, '381706м1', 5, 13, 2, 'Очная'),
(16, '381706м2', 5, 14, 2, 'Очная'),
(17, '381707м', 6, 15, 2, 'Очная');

-- 
-- Вывод данных для таблицы tabl_department
--
INSERT INTO tabl_department VALUES
(1, 'кафедра алгебры, геометрии и дискретной математики', 'АГДМ', 10),
(2, 'кафедра дифференциальных уравнений, математического и численного анализа', 'ДУМЧА', 35),
(3, 'кафедра информатики и автоматизации научных исследований', 'ИАНИ', 61),
(4, 'кафедра математического обеспечения и суперкомпьютерных технологий', 'МОСТ', NULL),
(5, 'кафедра математической физики и оптимального управления', 'МФОУ', NULL),
(6, 'кафедра прикладной математики', 'ПМ', 99),
(7, 'кафедра программной инженерии', 'ПРИН', 113),
(8, 'кафедра теоретической, компьютерной и экспериментальной механики', 'ТКЭМ', 136),
(9, 'кафедра теории управления и динамики систем', 'ТУДС', 176);

-- 
-- Вывод данных для таблицы tabl_student_group
--
-- Таблица testbase.tabl_student_group не содержит данных

-- 
-- Вывод данных для таблицы tabl_student
--
INSERT INTO tabl_student VALUES
(1, 'Денисов Максим Владимирович', 'Денисов М. В.', NULL, NULL, 5, NULL, NULL),
(2, 'Калинина Евгения Александровна', 'Калинина Е. А.', 'Очная', 8, 1, 10, NULL),
(3, 'Касаткина Юлия Александровна', 'Касаткина Ю. А.', 'Очная', 8, 5, 104, NULL),
(4, 'Королёва Екатерина Евгеньевна', 'Королёва Е. Е.', 'Очная', 8, 5, 94, NULL),
(5, 'Милешин Иван Геннадьевич', 'Милешин И. Г.', 'Очная', 8, 5, 31, NULL),
(6, 'Милешина Валерия Эдуардовна', 'Милешина В. Э.', 'Очная', 8, 5, 90, NULL),
(7, 'Рейнага Тюрин Михаэль Рэнович', 'Рейнага Т. М. Р.', 'Очная', 8, 1, 10, NULL),
(8, 'Сафонов Клим Андреевич', 'Сафонов К. А.', 'Очная', 8, 2, 182, NULL),
(9, 'Таланова Мария Сергеевна', 'Таланова М. С.', NULL, NULL, 5, 93, NULL),
(10, 'Трифонов Константин Николаевич', 'Трифонов К. Н.', 'Очная', 8, 2, 35, NULL),
(11, 'Даллул Мариана', 'Даллул М.', 'Очная', 8, 2, 35, NULL),
(12, 'Баханова Юлия Викторовна', 'Баханова Ю. В.', 'Очная', 11, 9, NULL, NULL),
(13, 'Гребеньков Александр Петрович', 'Гребеньков А. П.', NULL, NULL, 6, 105, NULL),
(14, 'Исакова Екатерина Евгеньевна', 'Исакова Е. Е.', 'Очная', 11, 2, 24, NULL),
(15, 'Карпов Александр Андреевич', 'Карпов А. А.', 'Очная', 11, 2, 24, NULL),
(16, 'Кузенков Николай Евгеньевич', 'Кузенков Н. Е.', 'Очная', 11, 2, NULL, NULL),
(17, 'Курочкина Татьяна Юрьевна', 'Курочкина Т. Ю.', NULL, NULL, 2, 33, NULL),
(18, 'Лепилова Алёна Александровна', 'Лепилова А. А.', 'Очная', 11, 2, 33, NULL),
(19, 'Малышева Ирина Вадимовна', 'Малышева И. В.', 'Очная', 11, 6, 91, NULL),
(20, 'Мастюгина Юлия Юрьевна', 'Мастюгина Ю. Ю.', 'Очная', 11, 2, 24, NULL),
(21, 'Ревин Владислав Сергеевич', 'Ревин В. С.', 'Очная', 11, 2, 24, NULL),
(22, 'Самылина Евгения Александровна', 'Самылина Е. А.', 'Очная', 11, 9, NULL, NULL),
(23, 'Бояринов Константин Сергеевич', 'Бояринов К. С.', 'Очная', 12, 8, 151, NULL),
(24, 'Бусаров Дмитрий Андреевич', 'Бусаров Д. А.', 'Очная', 12, 8, NULL, NULL),
(25, 'Голякова Евгения Валерьевна', 'Голякова Е. В.', 'Очная', 12, 8, 155, NULL),
(26, 'Жуков Владислав Павлович', 'Жуков В. П.', 'Очная', 12, 8, 155, NULL),
(27, 'Зайцев Дмитрий Дмитревич', 'Зайцев Д. Д.', NULL, NULL, 8, NULL, NULL),
(28, 'Зузенкова Анна Александровна', 'Зузенкова А. А.', NULL, NULL, 8, 155, NULL),
(29, 'Казарова Екатерина Игоревна', 'Казарова Е. И.', 'Очная', 12, 8, 144, NULL),
(30, 'Кондратьев Владислав Витальевич', 'Кондратьев В. В.', 'Очная', 12, 8, 148, NULL),
(31, 'Красный Владимир Сергеевич', 'Красный В. С.', 'Очная', 12, 8, NULL, NULL),
(32, 'Марьин Илья Андреевич', 'Марьин И. А.', NULL, NULL, 8, 146, NULL),
(33, 'Москова Екатерина Сергеевна', 'Москова Е. С.', 'Очная', 12, 8, 152, NULL),
(34, 'Мудрак Наталья Владимировна', 'Мудрак Н. В.', NULL, NULL, 8, 152, NULL),
(35, 'Мясников Дмитрий Сергеевич', 'Мясников Д. С.', NULL, NULL, 8, 147, NULL),
(36, 'Павелкин Федор Иванович', 'Павелкин Ф. И.', 'Очная', 12, 8, 147, NULL),
(37, 'Пронин Сергей Николаевич', 'Пронин С. Н.', 'Очная', 12, 8, 151, NULL),
(38, 'Сунгатуллин Ринат Рафисович', 'Сунгатуллин Р. Р.', NULL, NULL, 8, 152, NULL),
(39, 'Суслова Мария Евгеньевна', 'Суслова М. Е.', 'Очная', 12, 8, NULL, NULL),
(40, 'Тананаева Татьяна Андреевна', 'Тананаева Т. А.', 'Очная', 12, 8, 152, NULL),
(41, 'Чнегов Егор Игоревич', 'Чнегов Е. И.', 'Очная', 12, 8, 146, NULL),
(42, 'Юскова Полина Викторовна', 'Юскова П. В.', NULL, NULL, 8, NULL, NULL),
(43, 'Береснева Юлия васильевна', 'Береснева Ю. В.', 'Очная', 9, 4, 67, NULL),
(44, 'Веретельников Сергей Александрович', 'Веретельников С. А.', 'Очная', 9, 4, 47, NULL),
(45, 'Гладкова Татьяна Алексеевна', 'Гладкова Т. А.', 'Очная', 9, 4, 83, NULL),
(46, 'Жариков Алексей Дмитриевич', 'Жариков А. Д.', 'Очная', 9, 4, 179, NULL),
(47, 'Крутобережская Ирина Сергеевна', 'Крутобережская И. С.', 'Очная', 9, 6, 103, NULL),
(48, 'Крюкова Полина Алексеевна', 'Крюкова П. А.', 'Очная', 9, 4, 2, NULL),
(49, 'Курбатовский Илья Владимирович', 'Курбатовский И. В.', NULL, NULL, 6, 99, NULL),
(50, 'Лапина Анна Сергеевна', 'Лапина А. С.', NULL, NULL, NULL, 124, NULL),
(51, 'Машаев Артём Вячеславович', 'Машаев А. В.', 'Очная', 9, 4, NULL, NULL),
(52, 'Митрохина Юлия Сергеевна', 'Митрохина Ю. С.', 'Очная', 9, 2, 24, NULL),
(53, 'Подчищаева Мария Викторовна', 'Подчищаева М. В.', 'Очная', 9, 4, 84, NULL),
(54, 'Провидохин Николай Андреевич', 'Провидохин Н. А.', 'Очная', 9, 4, 179, NULL),
(55, 'Рябов Владислав Игоревич', 'Рябов В. И.', 'Очная', 9, 4, 175, NULL),
(56, 'Сахорова Анна Игоревна', 'Сахорова А. И.', NULL, NULL, 1, 2, NULL),
(57, 'Юфин Илья Михайлович', 'Юфин И. М.', 'Очная', 9, 6, 98, NULL),
(58, 'Асташов Дмитрий Александрович', 'Асташов Д. А.', NULL, NULL, 1, 81, NULL),
(59, 'Бабушкина валерия Евгеньевна', 'Бабушкина В. Е.', 'Очная', 2, 1, 9, NULL),
(60, 'Батанова Елизавета Владимировна', 'Батанова Е. В.', 'Очная', 2, 1, 9, NULL),
(61, 'Елизаров Александр Юрьевич', 'Елизаров А. Ю.', 'Очная', 2, 1, 6, NULL),
(62, 'Клищ Роман Валерьевич', 'Клищ Р. В.', NULL, NULL, 8, 151, NULL),
(63, 'Ларцев Максим Сергеевич', 'Ларцев М. С.', 'Очная', 2, 1, NULL, NULL),
(64, 'Львова Дарья Александровна', 'Львова Д. А.', 'Очная', 2, 1, NULL, NULL),
(65, 'Мавлютов Артем Маратович', 'Мавлютов А. М.', 'Очная', 2, 1, 6, NULL),
(66, 'Макаров Александр Андреевич', 'Макаров А. А.', 'Очная', 2, 1, 19, NULL),
(67, 'Пигалова Александра Александровна', 'Пигалова А. А.', 'Очная', 2, 1, 19, NULL),
(68, 'Сажин Максим Сергеевич', 'Сажин М. С.', 'Очная', 2, 1, 9, NULL),
(69, 'Свирежев Владислав Александрович', 'Свирежев В. А.', 'Очная', 2, 1, 6, NULL),
(70, 'Талецкий Дмитрий Сергеевич', 'Талецкий Д. С.', 'Очная', 2, 1, 12, NULL),
(71, 'Четвериков Антон Андреевич', 'Четвериков А. А.', 'Очная', 2, 1, 2, NULL),
(72, 'Шаров Федор Александрович', 'Шаров Ф. А.', NULL, NULL, 1, NULL, NULL),
(73, 'Акуленко Анастасия Сергеевна', 'Акуленко А. С.', 'Очная', 13, 8, 153, NULL),
(74, 'Белов Игорь Юрьевич', 'Белов И. Ю.', 'Очная', 13, 8, 132, NULL),
(75, 'Большаков Богдан Александрович', 'Большаков Б. А.', 'Очная', 13, 8, 135, NULL),
(76, 'Гречко Дина Алексеевна', 'Гречко Д. А.', 'Очная', 11, 8, NULL, NULL),
(77, 'Кашина Екатерина Анатольевна', 'Кашина Е. А.', 'Очная', 13, 8, 138, NULL),
(78, 'Корсаков Максим Игоревич', 'Корсаков М. И.', 'Очная', 13, 8, NULL, NULL),
(79, 'Рыбаков Евгений Леонидович', 'Рыбаков Е. Л.', 'Очная', 13, 8, 132, NULL),
(80, 'Чухманов Илья Геннадьевич', 'Чухманов И. Г.', 'Очная', 13, 8, 135, NULL),
(81, 'Шабетник Кристина Алексеевна', 'Шабетник К. А.', 'Очная', 13, 8, 153, NULL),
(82, 'Якимова Наталья Андреевна', 'Якимова Н. А.', NULL, NULL, 8, 145, NULL),
(83, 'Алешин Сергей Александрович', 'Алешин С. А.', 'Очная', 14, 5, NULL, NULL),
(84, 'Грачева Мария Андреевна', 'Грачева М. А.', 'Очная', 14, 5, NULL, NULL),
(85, 'Дороничев Александр Сергеевич', 'Дороничев А. С.', 'Очная', 14, 2, 39, NULL),
(86, 'Дубасова Анастасия Валерьевна', 'Дубасова А. В.', 'Очная', 14, 5, 89, NULL),
(87, 'Иванов Артём Сергеевич', 'Иванов А. С.', 'Очная', 14, 1, NULL, NULL),
(88, 'Иванова Юлия Анатольевна', 'Иванова Ю. А.', 'Очная', 14, 5, 89, NULL),
(89, 'Охатрина Дарья Дмитриевна', 'Охатрина Д. Д.', 'Очная', 14, 5, NULL, NULL),
(90, 'Сидорычев Александр Сергеевич', 'Сидорычев А. С.', 'Очная', 14, 1, NULL, NULL),
(91, 'Чередниченко Андрей Петрович', 'Чередниченко А. П.', 'Очная', 14, 2, 28, NULL),
(92, 'Шапаева Диана Олеговна', 'Шапаева Д. О.', 'Очная', 14, 2, 39, NULL),
(93, 'Андреев Владислав Станиславович', 'Андреев В. С.', 'Очная', 15, NULL, NULL, NULL),
(94, 'Аникина Ольга Михайловна', 'Аникина О. М.', 'Очная', 15, 1, 9, NULL),
(95, 'Анфимов Владислав Юрьевич', 'Анфимов В. Ю.', 'Очная', 15, 2, 24, NULL),
(96, 'Гладков Дмитрий Олегович', 'Гладков Д. О.', 'Очная', 15, 4, 83, NULL),
(97, 'Голованова Татьяна Александровна', 'Голованова Т. А.', 'Очная', 15, 1, 9, NULL),
(98, 'Жильцов Максим Сергеевич', 'Жильцов М. С.', 'Очная', 15, 4, 101, NULL),
(99, 'Иванова Ольга Алексеевна', 'Иванова О. А.', 'Очная', 15, NULL, 119, NULL),
(100, 'Коган Михаил Львович', 'Коган М. Л.', NULL, NULL, NULL, 159, NULL),
(101, 'Кочуев Андрей Игоревич', 'Кочуев А. И.', 'Очная', 15, 1, 5, NULL),
(102, 'Курсаков Евгения Сергеевич', 'Курсаков Е. С.', NULL, NULL, NULL, 159, NULL),
(103, 'Магазинник Иван Анатольевич', 'Магазинник И. А.', 'Очная', 15, 4, 179, NULL),
(104, 'Марченко Андрей Александрович', 'Марченко А. А.', 'Очная', 15, NULL, 159, NULL),
(105, 'Медведик Давид Валерьевич', 'Медведик Д. В.', NULL, NULL, 4, 74, NULL),
(106, 'Моисеев Никита Романович', 'Моисеев Н. Р.', 'Очная', 15, 1, NULL, NULL),
(107, 'Москаленко Виктор Алексеевич', 'Москаленко В. А.', 'Очная', 15, 1, 9, NULL),
(108, 'Оленева Мария Михайловна', 'Оленева М. М.', 'Очная', 15, NULL, NULL, NULL),
(109, 'Нихимович Дмитрий Михайлович', 'Нихимович Д. М.', NULL, NULL, 6, NULL, NULL),
(110, 'Осечкин Сергей Валерьевич', 'Осечкин С. В.', 'Очная', 15, 2, 185, NULL),
(111, 'Пипикин Олег Игоревич', 'Пипикин О. И.', 'Очная', 15, 6, 106, NULL),
(112, 'Подолов Кирилл Константинович', 'Подолов К. К.', 'Очная', 15, 1, 20, NULL),
(113, 'Поздяев Валерий Игоревич', 'Поздяев В. И.', 'Очная', 15, NULL, 181, NULL),
(114, 'Полканов Никита Сергеевич', 'Полканов Н. С.', NULL, NULL, 4, 159, NULL),
(115, 'Поляков Игорь Олегович', 'Поляков И. О.', 'Очная', 15, 4, 83, NULL),
(116, 'Пронина Мария Валерьевна', 'Пронина М. В.', 'Очная', 15, 4, 74, NULL),
(117, 'Румянцев Александр Валерьевич', 'Румянцев А. В.', 'Очная', 15, 4, 179, NULL),
(118, 'Румянцева Мария Алексеевна', 'Румянцева М. А.', 'Очная', 15, 6, 99, NULL),
(119, 'Сидоров Никита Владимирович', 'Сидоров Н. В.', 'Очная', 15, 1, 5, NULL),
(120, 'Скворцова Ольга Сергеевна', 'Скворцова О. С.', NULL, NULL, 4, 71, NULL),
(121, 'Степанов Николай Алексеевич', 'Степанов Н. А.', 'Очная', 15, 6, 91, NULL),
(122, 'Тарасов Евгений Александрович', 'Тарасов Е. А.', NULL, NULL, NULL, 115, NULL),
(123, 'Тишкин Константин Андреевич', 'Тишкин К. А.', 'Очная', 15, NULL, 159, NULL),
(124, 'Флегонтов Александр Игоревич', 'Флегонтов А. И.', 'Очная', 15, 4, NULL, NULL),
(125, 'Фытов Александр Михайлович', 'Фытов А. М.', 'Очная', 15, NULL, NULL, NULL),
(126, 'Чекодаев Олег Андреевич', 'Чекодаев О. А.', 'Очная', 15, NULL, NULL, NULL),
(127, 'Шептунов Владислав Олегович', 'Шептунов В. О.', 'Очная', 15, 2, 24, NULL),
(128, 'Ширяева Вера Сергеевна', 'Ширяева В. С.', 'Очная', 15, NULL, 181, NULL),
(129, 'Ширяева Екатерина Александровна', 'Ширяева Е. А.', 'Очная', 15, 2, NULL, NULL),
(130, 'Бабаев Иван Владимирович', 'Бабаев И. В.', 'Очная', 16, 2, NULL, NULL),
(131, 'Бебнев Виктор Владимирович', 'Бебнев В. В.', 'Очная', 16, 1, 9, NULL),
(132, 'Воеводин Андрей Михайлович', 'Воеводин А. М.', 'Очная', 16, 4, 84, NULL),
(133, 'Голякова Елена Александровна', 'Голякова Е. А.', 'Очная', 16, 2, 24, NULL),
(134, 'Гришин Сергей Алексеевич', 'Гришин С. А.', NULL, NULL, 1, 20, NULL),
(135, 'Демченко Никита Андреевич', 'Демченко Н. А.', 'Очная', 16, NULL, 129, NULL),
(136, 'Зубарева Екатерина Сергеевна', 'Зубарева Е. С.', 'Очная', 16, 4, 71, NULL),
(137, 'Реунова Ольга Алексеевна', 'Реунова О. А.', 'Очная', 16, 4, NULL, NULL),
(138, 'Курин Максим Евгеньевич', 'Курин М. Е.', 'Очная', 16, 4, 84, NULL),
(139, 'Марушкин Николай Алексеевич', 'Марушкин Н. А.', 'Очная', 16, NULL, 84, NULL),
(140, 'Морозов Алексей Викторович', 'Морозов А. В.', NULL, NULL, NULL, 129, NULL),
(141, 'Привалов Даниил Юрьевич', 'Привалов Д. Ю.', 'Очная', 16, 4, 159, NULL),
(142, 'Пухкий Константин Константинович', 'Пухкий К. К.', 'Очная', 16, 6, 84, NULL),
(143, 'Романенко Александр Александрович', 'Романенко А. А.', 'Очная', 16, 4, 84, NULL),
(144, 'Соснов Дмитрий Викторович', 'Соснов Д. В.', NULL, NULL, NULL, 159, NULL),
(145, 'Фадеев Алексей Андреевич', 'Фадеев А. А.', 'Очная', 16, 4, NULL, NULL),
(146, 'Хизбуллин Ренат Ахмирович', 'Хизбуллин Р. А.', 'Очная', 16, 1, 9, NULL),
(147, 'Храмов Илья Валерьевич', 'Храмов И. В.', 'Очная', 16, 4, 84, NULL),
(148, 'Бабушкина Лилия Александровна', 'Бабушкина Л. А.', 'Очная', 17, 3, 49, NULL),
(149, 'Буяков Дмитрий Владимирович', 'Буяков Д. В.', 'Очная', 17, 3, 58, NULL),
(150, 'Губарев Сергей Юрьевич', 'Губарев С. Ю.', 'Очная', 17, 3, 65, NULL),
(151, 'Кудимов Михаил Максимович', 'Кудимов М. М.', NULL, NULL, 3, 62, NULL),
(152, 'Кукушкина Дарья Михайловна', 'Кукушкина Д. М.', 'Очная', 17, 3, 58, NULL),
(153, 'Куликов Александр Павлович', 'Куликов А. П.', 'Очная', 3, 3, 59, NULL),
(154, 'Кулыгин Виктор Вячеславович', 'Кулыгин В. В.', 'Очная', 17, 3, 65, NULL),
(155, 'Макарин Михаил Сергеевич', 'Макарин М. С.', 'Очная', 17, 3, 62, NULL),
(156, 'Макаров Антон Сергеевич', 'Макаров А. С.', 'Очная', 17, 3, 129, NULL),
(157, 'Санин Сергей Алексеевич', 'Санин С. А.', 'Очная', 17, 3, 59, NULL),
(158, 'Ушакова Екатерина Александровна', 'Ушакова Е. А.', 'Очная', 17, 3, 56, NULL),
(159, 'Хапаев Илья Николаевич', 'Хапаев И. Н.', 'Очная', 17, 3, 62, NULL),
(160, 'Чапутина Яна Андреевна', 'Чапутина Я. А.', NULL, NULL, 3, 63, NULL),
(161, 'Шаталина Ксения Сергеевна', 'Шаталина К. С.', 'Очная', 17, 3, 63, NULL),
(162, 'Шульгинова Ирина Александровна', 'Шульгинова И. А.', 'Очная', 17, 3, NULL, NULL),
(163, 'Шумилов Иван Андреевич', 'Шумилов И. А.', 'Очная', 17, 3, 49, NULL),
(164, 'Регаладо Бучели Андрес Фернандо', 'Регаладо Б. А. Ф.', NULL, NULL, 3, 49, NULL);

-- 
-- Вывод данных для таблицы tabl_practice_type
--
INSERT INTO tabl_practice_type VALUES
(1, 'Учебная', 'Учебной'),
(2, 'Производственная', 'Производственной'),
(3, 'Практика по получению первичных профессиональных умений и навыков', 'Практики по получению первичных профессиональных умений и навыков');

-- 
-- Вывод данных для таблицы tabl_practice
--
INSERT INTO tabl_practice VALUES
(1, 3, '2018-09-01', '2018-12-31', 1, 9, 1, 'М', 1, NULL),
(2, 2, '2018-09-01', '2018-12-31', 1, 11, 2, 'М', 1, NULL),
(3, 2, '2018-09-01', '2018-12-31', 8, 9, 1, 'М', 2, NULL),
(4, 2, '2018-09-01', '2018-12-31', 8, 11, 2, 'М', 2, NULL),
(5, 2, '2018-09-01', '2018-12-31', 2, 9, 1, 'М', 2, NULL),
(6, 2, '2018-09-01', '2018-12-31', 2, 11, 2, 'М', 2, NULL),
(7, 2, '2018-09-01', '2018-12-31', 9, 9, 1, 'М', 2, NULL),
(8, 2, '2018-09-01', '2018-12-31', 9, 11, 2, 'М', 2, NULL),
(9, 2, '2018-09-01', '2018-12-31', 10, 9, 1, 'М', 2, NULL),
(10, 3, '2018-09-01', '2018-12-31', 10, 9, 1, 'М', 2, NULL),
(11, 3, '2018-09-01', '2018-12-31', 10, 11, 2, 'М', 2, NULL),
(12, 3, '2018-09-01', '2018-12-31', 10, 11, 2, 'М', 2, NULL),
(13, 2, '2018-09-01', '2018-12-31', 10, 11, 2, 'М', 2, NULL),
(14, 3, '2018-09-01', '2018-12-31', 4, 9, 1, 'М', 3, NULL),
(15, 2, '2018-09-01', '2018-12-31', 4, 9, 1, 'М', 3, NULL),
(16, 3, '2018-09-01', '2018-12-31', 4, 11, 2, 'М', 3, NULL),
(17, 3, '2018-09-01', '2018-12-31', 4, 11, 2, 'М', 3, NULL),
(18, 2, '2018-09-01', '2018-12-31', 4, 11, 2, 'М', 3, NULL),
(19, 3, '2018-09-01', '2018-12-31', 5, 9, 1, 'М', 4, NULL),
(20, 2, '2018-09-01', '2018-12-31', 5, 9, 1, 'М', 4, NULL),
(21, 3, '2018-09-01', '2018-12-31', 5, 11, 2, 'М', 4, NULL),
(22, 3, '2018-09-01', '2018-12-31', 5, 11, 2, 'М', 4, NULL),
(23, 3, '2018-09-01', '2018-12-31', 5, 11, 2, 'М', 4, NULL),
(24, 2, '2018-09-01', '2018-12-31', 13, 9, 1, 'М', 5, NULL),
(25, 2, '2018-09-01', '2018-12-31', 13, 11, 2, 'М', 5, NULL),
(26, 2, '2018-09-01', '2018-12-31', 14, 9, 1, 'М', 5, NULL),
(27, 2, '2018-09-01', '2018-12-31', 14, 11, 2, 'М', 5, NULL),
(28, 2, '2018-09-01', '2018-12-31', 0, 9, 1, 'М', 5, NULL),
(29, 2, '2018-09-01', '2018-12-31', 0, 9, 1, 'М', 5, NULL),
(30, 2, '2018-09-01', '2018-12-31', 7, 9, 1, 'М', 6, NULL),
(31, 2, '2018-09-01', '2018-12-31', 7, 11, 2, 'М', 6, NULL);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;