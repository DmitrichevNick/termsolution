﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.1.22.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 05.06.2019 19:20:21
-- Версия сервера: 5.6.41-log
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Удалить процедуру `PRA_DEGREE`
--
DROP PROCEDURE IF EXISTS PRA_DEGREE;

--
-- Удалить процедуру `PRA_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PRA_DEPARTMENT;

--
-- Удалить процедуру `PRA_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PRA_EDU_DIRECTION;

--
-- Удалить процедуру `PRA_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PRA_EDU_FORM;

--
-- Удалить процедуру `PRA_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PRA_EDU_PROFILE;

--
-- Удалить процедуру `PRA_GROUP`
--
DROP PROCEDURE IF EXISTS PRA_GROUP;

--
-- Удалить процедуру `PRA_LECTURER`
--
DROP PROCEDURE IF EXISTS PRA_LECTURER;

--
-- Удалить процедуру `PRA_LEVEL`
--
DROP PROCEDURE IF EXISTS PRA_LEVEL;

--
-- Удалить процедуру `PRA_POSITION`
--
DROP PROCEDURE IF EXISTS PRA_POSITION;

--
-- Удалить процедуру `PRA_PRACTICE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE;

--
-- Удалить процедуру `PRA_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE_PLACE;

--
-- Удалить процедуру `PRA_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE_TYPE;

--
-- Удалить процедуру `PRA_STUDENT`
--
DROP PROCEDURE IF EXISTS PRA_STUDENT;

--
-- Удалить процедуру `PROC_READ_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PROC_READ_PRACTICE_TYPE;

--
-- Удалить процедуру `PR_DEGREE`
--
DROP PROCEDURE IF EXISTS PR_DEGREE;

--
-- Удалить процедуру `PR_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PR_DEPARTMENT;

--
-- Удалить процедуру `PR_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PR_EDU_DIRECTION;

--
-- Удалить процедуру `PR_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PR_EDU_FORM;

--
-- Удалить процедуру `PR_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PR_EDU_PROFILE;

--
-- Удалить процедуру `PR_GROUP`
--
DROP PROCEDURE IF EXISTS PR_GROUP;

--
-- Удалить процедуру `PR_LECTURER`
--
DROP PROCEDURE IF EXISTS PR_LECTURER;

--
-- Удалить процедуру `PR_LEVEL`
--
DROP PROCEDURE IF EXISTS PR_LEVEL;

--
-- Удалить процедуру `PR_POSITION`
--
DROP PROCEDURE IF EXISTS PR_POSITION;

--
-- Удалить процедуру `PR_PRACTICE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE;

--
-- Удалить процедуру `PR_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE_PLACE;

--
-- Удалить процедуру `PR_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE_TYPE;

--
-- Удалить процедуру `PR_STUDENT`
--
DROP PROCEDURE IF EXISTS PR_STUDENT;

--
-- Удалить процедуру `PW_DEGREE`
--
DROP PROCEDURE IF EXISTS PW_DEGREE;

--
-- Удалить процедуру `PW_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PW_DEPARTMENT;

--
-- Удалить процедуру `PW_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PW_EDU_DIRECTION;

--
-- Удалить процедуру `PW_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PW_EDU_FORM;

--
-- Удалить процедуру `PW_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PW_EDU_PROFILE;

--
-- Удалить процедуру `PW_GROUP`
--
DROP PROCEDURE IF EXISTS PW_GROUP;

--
-- Удалить процедуру `PW_LECTURER`
--
DROP PROCEDURE IF EXISTS PW_LECTURER;

--
-- Удалить процедуру `PW_LEVEL`
--
DROP PROCEDURE IF EXISTS PW_LEVEL;

--
-- Удалить процедуру `PW_POSITION`
--
DROP PROCEDURE IF EXISTS PW_POSITION;

--
-- Удалить процедуру `PW_PRACTICE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE;

--
-- Удалить процедуру `PW_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE_PLACE;

--
-- Удалить процедуру `PW_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE_TYPE;

--
-- Удалить процедуру `PW_STUDENT`
--
DROP PROCEDURE IF EXISTS PW_STUDENT;

--
-- Удалить процедуру `R_EDU_DIRECTION_BY_ID_LEVEL`
--
DROP PROCEDURE IF EXISTS R_EDU_DIRECTION_BY_ID_LEVEL;

--
-- Удалить процедуру `R_EDU_PROFILE_BY_ID_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS R_EDU_PROFILE_BY_ID_EDU_DIRECTION;

--
-- Установка базы данных по умолчанию
--
USE testbase;

DELIMITER $$

--
-- Создать процедуру `R_EDU_PROFILE_BY_ID_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE R_EDU_PROFILE_BY_ID_EDU_DIRECTION (IN IN_ID_EDU_DIRECTION int(11))
BEGIN
  SELECT
    tep.ID,
    tep.ID_EDU_DIRECTION,
    tep.NAME,
    tep.SHORT_NAME
  FROM tabl_edu_profile tep
  WHERE tep.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION;
END
$$

--
-- Создать процедуру `R_EDU_DIRECTION_BY_ID_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE R_EDU_DIRECTION_BY_ID_LEVEL (IN IN_ID int(11))
BEGIN
  SELECT
    ted.ID,
    ted.NAME,
    ted.CODE,
    ted.SHORT_NAME,
    ted.ID_LEVEL
  FROM tabl_edu_direction ted
  WHERE ted.ID_LEVEL = IN_ID;
END
$$

--
-- Создать процедуру `PW_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_STUDENT (IN IN_ID int, IN IN_NAME varchar(50), IN IN_SHORT_NAME varchar(255), IN IN_ID_GROUP int, IN IN_ID_DEPARTMENT int, IN IN_ID_LECTURER int, IN IN_ID_PLACE int)
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_student ts
      WHERE ts.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_student (ID, NAME, SHORT_NAME, ID_GROUP, ID_DEPARTMENT, ID_LECTURER, ID_PLACE)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_GROUP, IN_ID_DEPARTMENT, IN_ID_LECTURER, IN_ID_PLACE);
  ELSE
    UPDATE tabl_student ts
    SET ts.NAME = IN_NAME,
        ts.SHORT_NAME = IN_SHORT_NAME,
        ts.ID_GROUP = IN_ID_GROUP,
        ts.ID_DEPARTMENT = IN_ID_DEPARTMENT,
        ts.ID_LECTURER = IN_ID_LECTURER,
        ts.ID_PLACE = IN_ID_PLACE
    WHERE ts.ID = IN_ID;
  END IF;


END
$$

--
-- Создать процедуру `PW_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE_TYPE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_NAME_R varchar(255), IN IN_NAME_V varchar(255), IN IN_ADD_NAME varchar(255), IN IN_SHORT_ADD_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_type tpt
      WHERE tpt.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_type (ID, NAME, NAME_R, NAME_V, ADD_NAME, SHORT_ADD_NAME)
      VALUES (IN_ID, IN_NAME, IN_NAME_R, IN_NAME_V, IN_ADD_NAME, IN_SHORT_ADD_NAME);
  ELSE
    UPDATE tabl_practice_type tpt
    SET tpt.NAME = IN_NAME,
        tpt.NAME_R = IN_NAME_R,
        tpt.NAME_V = IN_NAME_V,
        tpt.ADD_NAME = IN_ADD_NAME,
        tpt.SHORT_ADD_NAME = IN_SHORT_ADD_NAME
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE_PLACE (IN IN_ID int(11), IN IN_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_place tpp
      WHERE tpp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_place (ID, NAME)
      VALUES (IN_ID, IN_NAME);
  ELSE
    UPDATE tabl_practice_place tpp
    SET tpp.NAME = IN_NAME
    WHERE tpp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE (IN IN_ID int(11), IN IN_ID_PRACTICE_TYPE int(1), IN IN_DATE_BEGIN date, IN IN_DATE_END date, IN IN_ID_EDU_PROFILE int(11), IN IN_SEMESTER int(1), IN IN_COURSE int(1), IN IN_ID_LEVEL int(11), IN IN_ID_EDU_DIRECTION int(11), IN IN_ID_GROUP int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice (ID, ID_PRACTICE_TYPE, DATE_BEGIN, DATE_END, ID_EDU_PROFILE, SEMESTER, COURSE, ID_LEVEL, ID_EDU_DIRECTION, ID_GROUP)
      VALUES (IN_ID, IN_ID_PRACTICE_TYPE, IN_DATE_BEGIN, IN_DATE_END, IN_ID_EDU_PROFILE, IN_SEMESTER, IN_COURSE, IN_ID_LEVEL, IN_ID_EDU_DIRECTION, IN_ID_GROUP);
  ELSE
    UPDATE tabl_practice tp
    SET tp.ID_PRACTICE_TYPE = IN_ID_PRACTICE_TYPE,
        tp.DATE_BEGIN = IN_DATE_BEGIN,
        tp.DATE_END = IN_DATE_END,
        tp.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tp.SEMESTER = IN_SEMESTER,
        tp.COURSE = IN_COURSE,
        tp.ID_LEVEL = IN_ID_LEVEL,
        tp.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION,
        tp.ID_GROUP = IN_ID_GROUP
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_POSITION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_position tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_position (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_position tp
    SET tp.NAME = IN_NAME,
        tp.SHORT_NAME = IN_SHORT_NAME
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_LEVEL (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(10))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_level tl
      WHERE tl.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_level (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_level tl
    SET tl.NAME = IN_NAME,
        tl.SHORT_NAME = IN_SHORT_NAME
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_LECTURER (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_DEPARTMENT int(11), IN IN_ID_DEGREE int(11), IN IN_ID_POSITION int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_lecturer tl
      WHERE tl.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_lecturer (ID, NAME, SHORT_NAME, ID_DEPARTMENT, ID_DEGREE, ID_POSITION)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_DEPARTMENT, IN_ID_DEGREE, IN_ID_POSITION);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_lecturer (NAME, SHORT_NAME, ID_DEPARTMENT, ID_DEGREE, ID_POSITION)
      VALUES (IN_NAME, IN_SHORT_NAME, IN_ID_DEPARTMENT, IN_ID_DEGREE, IN_ID_POSITION);
  ELSE
    UPDATE tabl_lecturer tl
    SET tl.NAME = IN_NAME,
        tl.SHORT_NAME = IN_SHORT_NAME,
        tl.ID_DEPARTMENT = IN_ID_LECTURER,
        tl.ID_DEGREE = IN_ID_DEGREE,
        tl.ID_POSITION = IN_ID_POSITION
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_GROUP (IN IN_ID int(11), IN IN_NUMBER varchar(255), IN IN_ID_EDU_DIRECTION int(11), IN IN_ID_EDU_PROFILE int(11), IN IN_COURSE int(1), IN IN_ID_EDU_FORM int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_group tg
      WHERE tg.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_group (ID, NUMBER, ID_EDU_DIRECTION, ID_EDU_PROFILE, COURSE, ID_EDU_FORML)
      VALUES (IN_ID, IN_NUMBER, IN_ID_EDU_DIRECTION, IN_ID_EDU_PROFILE, IN_COURSE, IN_ID_EDU_FORM);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_group (NUMBER, ID_EDU_DIRECTION, ID_EDU_PROFILE, COURSE, ID_EDU_FORM)
      VALUES (IN_NUMBER, IN_ID_EDU_DIRECTION, IN_ID_EDU_PROFILE, IN_COURSE, IN_ID_EDU_FORM);
  ELSE
    UPDATE tabl_group tg
    SET tg.NUMBER = IN_NUMBER,
        tg.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION,
        tg.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tg.COURSE = IN_COURSE,
        tg.ID_EDU_FORM = IN_ID_EDU_FORM
    WHERE tg.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_PROFILE (IN IN_ID int(11), IN IN_ID_EDU_DIRECTION int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_profile tep
      WHERE tep.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_profile (ID, ID_EDU_DIRECTION, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_ID_EDU_DIRECTION, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_edu_profile tep
    SET tep.NAME = IN_NAME,
        tep.SHORT_NAME = IN_SHORT_NAME,
        tep.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION
    WHERE tep.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_FORM (IN IN_ID int(11), IN IN_NAME varchar(50))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_form tef
      WHERE tef.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_form (ID, NAMEE)
      VALUES (IN_ID, IN_NAME);
  ELSE
    UPDATE tabl_edu_form tef
    SET tef.NAME = IN_NAME
    WHERE tef.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_DIRECTION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_LEVEL char(1))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_direction ted
      WHERE ted.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_direction (ID, NAME, CODE, SHORT_NAME, ID_LEVEL)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_SHORT_NAME, IN_ID_LEVEL);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_edu_direction (NAME, CODE, SHORT_NAME, ID_LEVEL)
      VALUES (IN_NAME, IN_CODE, IN_SHORT_NAME, IN_ID_LEVEL);
  ELSE
    UPDATE tabl_edu_direction ted
    SET ted.NAME = IN_NAME,
        ted.CODE = IN_CODE,
        ted.SHORT_NAME = IN_SHORT_NAME,
        ted.ID_LEVEL = IN_ID_LEVEL
    WHERE ted.ID = IN_ID;
  END IF;

END
$$

--
-- Создать процедуру `PW_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_DEPARTMENT (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_ID_LECTURER int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_department td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_department (ID, NAME, CODE, ID_LECTURER)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_ID_LECTURER);
  ELSE
    UPDATE tabl_department td
    SET td.NAME = IN_NAME,
        td.CODE = IN_CODE,
        td.ID_LECTURER = IN_ID_LECTURER
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PW_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_DEGREE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_degree td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_degree (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_degree td
    SET td.NAME = IN_NAME,
        td.SHORT_NAME = IN_SHORT_NAME
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PR_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_STUDENT (IN IN_ID int)
BEGIN
  SELECT
    ts.ID,
    ts.NAME,
    ts.SHORT_NAME,
    ts.ID_GROUP,
    ts.ID_DEPARTMENT,
    ts.ID_LECTURER,
    ts.ID_PLACE
  FROM testbase.tabl_student ts
  WHERE ts.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE_TYPE (IN IN_ID int(11))
BEGIN
  SELECT
    tpt.ID,
    tpt.NAME,
    tpt.NAME_R,
    tpt.NAME_V,
    tpt.ADD_NAME,
    tpt.SHORT_ADD_NAME
  FROM tabl_practice_type tpt
  WHERE tpt.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE_PLACE (IN IN_ID int(11))
BEGIN
  SELECT
    tpp.ID,
    tpp.NAME
  FROM tabl_practice_place tpp
  WHERE tpp.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE (IN IN_ID int(11))
BEGIN
  SELECT
    tp.ID,
    tp.ID_PRACTICE_TYPE,
    tp.DATE_BEGIN,
    tp.DATE_END,
    tp.ID_EDU_PROFILE,
    tp.SEMESTER,
    tp.COURSE,
    tp.ID_LEVEL,
    tp.ID_EDU_DIRECTION,
    tp.ID_GROUP
  FROM tabl_practice tp
  WHERE tp.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_POSITION (IN IN_ID int(11))
BEGIN
  SELECT
    tp.ID,
    tp.NAME,
    tp.SHORT_NAME
  FROM tabl_position tp
  WHERE tp.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_LEVEL (IN IN_ID int(11))
BEGIN
  SELECT
    tl.ID,
    tl.NAME,
    tl.SHORT_NAME
  FROM tabl_level tl
  WHERE tl.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_LECTURER (IN IN_ID int(11))
BEGIN
  SELECT
    tl.ID,
    tl.NAME,
    tl.SHORT_NAME,
    tl.ID_DEPARTMENT,
    tl.ID_DEGREE,
    tl.ID_POSITION
  FROM tabl_lecturer tl
  WHERE tl.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_GROUP (IN IN_ID int(11))
BEGIN
  SELECT
    tg.ID,
    tg.NUMBER,
    tg.ID_EDU_DIRECTION,
    tg.ID_EDU_PROFILE,
    tg.COURSE,
    tg.ID_EDU_FORM
  FROM tabl_group tg
  WHERE tg.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_PROFILE (IN IN_ID int(11))
BEGIN
  SELECT
    tep.ID,
    tep.ID_EDU_DIRECTION,
    tep.NAME,
    tep.SHORT_NAME
  FROM tabl_edu_profile tep
  WHERE tep.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_FORM (IN IN_ID int(11))
BEGIN
  SELECT
    tef.ID,
    tef.NAME
  FROM tabl_edu_form tef
  WHERE tef.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_DIRECTION (IN IN_ID int(11))
BEGIN
  SELECT
    ted.ID,
    ted.NAME,
    ted.CODE,
    ted.SHORT_NAME,
    ted.ID_LEVEL
  FROM tabl_edu_direction ted
  WHERE ted.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_DEPARTMENT (IN IN_ID int(11))
BEGIN
  SELECT
    td.ID,
    td.NAME,
    td.CODE,
    td.ID_LECTURER
  FROM tabl_department td
  WHERE td.ID = IN_ID;
END
$$

--
-- Создать процедуру `PR_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_DEGREE (IN IN_ID int(11))
BEGIN
  SELECT
    td.ID,
    td.NAME,
    td.SHORT_NAME
  FROM tabl_degree td
  WHERE td.ID = IN_ID;
END
$$

--
-- Создать процедуру `PROC_READ_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_PRACTICE_TYPE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R,
      tpt.NAME_R2,
      tpt.ADD_NAME,
      tpt.SHORT_ADD_NAME
    FROM tabl_practice_type tpt;
  ELSE
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R,
      tpt.NAME_R2,
      tpt.ADD_NAME,
      tpt.SHORT_ADD_NAME
    FROM tabl_practice_type tpt
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_STUDENT ()
BEGIN
  SELECT
    ts.ID,
    ts.NAME,
    ts.SHORT_NAME,
    ts.ID_GROUP,
    ts.ID_DEPARTMENT,
    ts.ID_LECTURER,
    ts.ID_PLACE
  FROM testbase.tabl_student ts;
END
$$

--
-- Создать процедуру `PRA_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE_TYPE ()
BEGIN
  SELECT
    tpt.ID,
    tpt.NAME,
    tpt.NAME_R,
    tpt.NAME_V,
    tpt.ADD_NAME,
    tpt.SHORT_ADD_NAME
  FROM tabl_practice_type tpt;
END
$$

--
-- Создать процедуру `PRA_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE_PLACE ()
BEGIN
  SELECT
    tpp.ID,
    tpp.NAME
  FROM tabl_practice_place tpp;
END
$$

--
-- Создать процедуру `PRA_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE ()
BEGIN
  SELECT
    tp.ID,
    tp.ID_PRACTICE_TYPE,
    tp.DATE_BEGIN,
    tp.DATE_END,
    tp.ID_EDU_PROFILE,
    tp.SEMESTER,
    tp.COURSE,
    tp.ID_LEVEL,
    tp.ID_EDU_DIRECTION,
    tp.ID_GROUP
  FROM tabl_practice tp;
END
$$

--
-- Создать процедуру `PRA_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_POSITION ()
BEGIN
  SELECT
    tp.ID,
    tp.NAME,
    tp.SHORT_NAME
  FROM tabl_position tp;
END
$$

--
-- Создать процедуру `PRA_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_LEVEL ()
BEGIN
  SELECT
    tl.ID,
    tl.NAME,
    tl.SHORT_NAME
  FROM tabl_level tl;
END
$$

--
-- Создать процедуру `PRA_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_LECTURER ()
BEGIN
  SELECT
    tl.ID,
    tl.NAME,
    tl.SHORT_NAME,
    tl.ID_DEPARTMENT,
    tl.ID_DEGREE,
    tl.ID_POSITION
  FROM tabl_lecturer tl;
END
$$

--
-- Создать процедуру `PRA_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_GROUP ()
BEGIN
  SELECT
    tg.ID,
    tg.NUMBER,
    tg.ID_EDU_DIRECTION,
    tg.ID_EDU_PROFILE,
    tg.COURSE,
    tg.ID_EDU_FORM
  FROM tabl_group tg;
END
$$

--
-- Создать процедуру `PRA_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_PROFILE ()
BEGIN
  SELECT
    tep.ID,
    tep.ID_EDU_DIRECTION,
    tep.NAME,
    tep.SHORT_NAME
  FROM tabl_edu_profile tep;
END
$$

--
-- Создать процедуру `PRA_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_FORM ()
BEGIN
  SELECT
    tef.ID,
    tef.NAME
  FROM tabl_edu_form tef;
END
$$

--
-- Создать процедуру `PRA_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_DIRECTION ()
BEGIN
  SELECT
    ted.ID,
    ted.NAME,
    ted.CODE,
    ted.SHORT_NAME,
    ted.ID_LEVEL
  FROM tabl_edu_direction ted;
END
$$

--
-- Создать процедуру `PRA_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_DEPARTMENT ()
BEGIN
  SELECT
    td.ID,
    td.NAME,
    td.CODE,
    td.ID_LECTURER
  FROM tabl_department td;
END
$$

--
-- Создать процедуру `PRA_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_DEGREE ()
BEGIN
  SELECT
    td.ID,
    td.NAME,
    td.SHORT_NAME
  FROM tabl_degree td;
END
$$

DELIMITER ;

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;