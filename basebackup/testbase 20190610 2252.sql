﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.1.22.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 10.06.2019 22:52:48
-- Версия сервера: 5.6.41-log
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Удалить таблицу `tabl_group`
--
DROP TABLE IF EXISTS tabl_group;

--
-- Удалить таблицу `tabl_practice`
--
DROP TABLE IF EXISTS tabl_practice;

--
-- Удалить таблицу `tabl_department`
--
DROP TABLE IF EXISTS tabl_department;

--
-- Удалить таблицу `tabl_department_head`
--
DROP TABLE IF EXISTS tabl_department_head;

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Создать таблицу `tabl_department_head`
--
CREATE TABLE tabl_department_head (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Фамилия Имя Отчество',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Фамилия И.О.',
  ID_DEGREE int(11) UNSIGNED DEFAULT NULL COMMENT 'УН степень',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 8,
AVG_ROW_LENGTH = 2340,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Заведующий кафедрой';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_department_head
ADD CONSTRAINT FK_TDH_ID_DEGREE FOREIGN KEY (ID_DEGREE)
REFERENCES tabl_degree (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `tabl_department`
--
CREATE TABLE tabl_department (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Наименование',
  CODE varchar(255) DEFAULT NULL COMMENT 'Код',
  ID_DEPARTMENT_HEAD int(11) UNSIGNED DEFAULT NULL COMMENT 'УН заведующего',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Кафедра';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_department
ADD CONSTRAINT FK_TDEP_ID_DEPARTMENT_HEAD FOREIGN KEY (ID_DEPARTMENT_HEAD)
REFERENCES tabl_department_head (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `tabl_practice`
--
CREATE TABLE tabl_practice (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  ID_PRACTICE_TYPE int(11) UNSIGNED DEFAULT NULL COMMENT 'Òèï ïðàêòèêè',
  DATE_BEGIN date DEFAULT NULL COMMENT 'Äàòà íà÷àëà',
  DATE_END date DEFAULT NULL COMMENT 'Äàòà îêîí÷àíèÿ',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ïðîôèëÿ ïîäãîòîâêè',
  SEMESTER int(1) UNSIGNED DEFAULT NULL COMMENT 'Ñåìåñòð',
  COURSE int(1) UNSIGNED DEFAULT NULL COMMENT 'Êóðñ',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 32,
AVG_ROW_LENGTH = 528,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать индекс `FK_tabl_practice_ID_PRACTICE_TYPE` для объекта типа таблица `tabl_practice`
--
ALTER TABLE tabl_practice
ADD INDEX FK_tabl_practice_ID_PRACTICE_TYPE (ID_PRACTICE_TYPE);

--
-- Создать внешний ключ
--
ALTER TABLE tabl_practice
ADD CONSTRAINT FK_TPR_ID_EDU_PROFILE FOREIGN KEY (ID_EDU_PROFILE)
REFERENCES tabl_edu_profile (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_practice
ADD CONSTRAINT FK_TPR_ID_PR_TYPE FOREIGN KEY (ID_PRACTICE_TYPE)
REFERENCES tabl_practice_type (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать таблицу `tabl_group`
--
CREATE TABLE tabl_group (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NUMBER varchar(255) DEFAULT NULL COMMENT 'Номер',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'УН направленности',
  COURSE int(1) UNSIGNED DEFAULT NULL COMMENT 'Курс',
  ID_EDU_FORM int(11) UNSIGNED DEFAULT NULL COMMENT 'УН формы обучения',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 96,
AVG_ROW_LENGTH = 963,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_group
ADD CONSTRAINT FK_TGS_ID_EDU_FORM FOREIGN KEY (ID_EDU_FORM)
REFERENCES tabl_edu_form (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_group
ADD CONSTRAINT FK_TSG_ID_EDU_PROFILE FOREIGN KEY (ID_EDU_PROFILE)
REFERENCES tabl_edu_profile (ID) ON DELETE SET NULL;

-- 
-- Вывод данных для таблицы tabl_department_head
--
INSERT INTO tabl_department_head VALUES
(1, 'Кузнецов Михаил Иванович', 'Кузнецов М.И.', NULL),
(2, 'Калинин Алексей Вячеславович', 'Калинин А.В.', NULL),
(3, 'Прилуцкий Михаил Хаимович', 'Прилуцкий М.Х.', NULL),
(4, 'Иванченко Михаил Васильевич', 'Иванченко М.В.', NULL),
(5, 'Гергель Виктор Павлович', 'Гергель В.П.', NULL),
(6, 'Игумнов Леонид Александрович', 'Игумнов Л.А.', NULL),
(7, 'Осипов Григорий Владимирович', 'Осипов Г.В.', NULL);

-- 
-- Вывод данных для таблицы tabl_practice
--
INSERT INTO tabl_practice VALUES
(1, 3, '2018-09-01', '2018-12-31', 1, 9, 1),
(2, 2, '2018-09-01', '2018-12-31', 1, 11, 2),
(3, 2, '2018-09-01', '2018-12-31', 8, 9, 1),
(4, 2, '2018-09-01', '2018-12-31', 8, 11, 2),
(5, 2, '2018-09-01', '2018-12-31', 2, 9, 1),
(6, 2, '2018-09-01', '2018-12-31', 2, 11, 2),
(7, 2, '2018-09-01', '2018-12-31', 9, 9, 1),
(8, 2, '2018-09-01', '2018-12-31', 9, 11, 2),
(9, 2, '2018-09-01', '2018-12-31', 10, 9, 1),
(10, 3, '2018-09-01', '2018-12-31', 10, 9, 1),
(11, 3, '2018-09-01', '2018-12-31', 10, 11, 2),
(12, 3, '2018-09-01', '2018-12-31', 10, 11, 2),
(13, 2, '2018-09-01', '2018-12-31', 10, 11, 2),
(14, 3, '2018-09-01', '2018-12-31', 4, 9, 1),
(15, 2, '2018-09-01', '2018-12-31', 4, 9, 1),
(16, 3, '2018-09-01', '2018-12-31', 4, 11, 2),
(17, 3, '2018-09-01', '2018-12-31', 4, 11, 2),
(18, 2, '2018-09-01', '2018-12-31', 4, 11, 2),
(19, 3, '2018-09-01', '2018-12-31', 5, 9, 1),
(20, 2, '2018-09-01', '2018-12-31', 5, 9, 1),
(21, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(22, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(23, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(24, 2, '2018-09-01', '2018-12-31', 13, 9, 1),
(25, 2, '2018-09-01', '2018-12-31', 13, 11, 2),
(26, 2, '2018-09-01', '2018-12-31', 14, 9, 1),
(27, 2, '2018-09-01', '2018-12-31', 14, 11, 2),
(28, 2, '2018-09-01', '2018-12-31', NULL, 9, 1),
(29, 2, '2018-09-01', '2018-12-31', NULL, 9, 1),
(30, 2, '2018-09-01', '2018-12-31', 7, 9, 1),
(31, 2, '2018-09-01', '2018-12-31', 7, 11, 2);

-- 
-- Вывод данных для таблицы tabl_group
--
INSERT INTO tabl_group VALUES
(1, '381704м', 1, 2, 1),
(2, '381807м', 2, 1, 1),
(3, '381803м3', 3, 1, 1),
(4, '381705м', 4, 2, 1),
(5, '381706м1', 5, 2, 1),
(6, '381806м3', 6, 1, 1),
(7, '381703м3', 3, 2, 1),
(8, '381806м4', 7, 1, 1),
(9, '381706м2', 8, 2, 1),
(10, '381703м4', 9, 2, 1),
(11, '381805м', 4, 1, 1),
(12, '381703м1', 10, 2, 1),
(13, '381703м2', 11, 2, 1),
(14, '381803м1', 10, 1, 1),
(15, '381707м', 2, 2, 1),
(16, '381804м', 1, 1, 1),
(17, '381806м1', 5, 1, 1),
(18, '381803м2', 11, 1, 1),
(19, '381802м', 12, 1, 1),
(20, '381702м', 12, 2, 1),
(21, '381806м2', 8, 1, 1),
(22, '381803м4', 9, 1, 1),
(23, '381803м2-МО', 11, 1, 1),
(24, '381706м3', 5, 2, 1),
(25, '381806-ИА', 22, 1, 1),
(26, '381606-3', 5, 3, 1),
(27, '381807-3', 17, 1, 1),
(28, '381706-ИА', 18, 2, 1),
(29, '381808-2', 19, 1, 1),
(30, '381807-1', 2, 1, 1),
(31, '381607-1', 2, 3, 1),
(32, '381603-2', 21, 3, 1),
(33, '381803-2', 22, 1, 1),
(34, '381702', 23, 2, 1),
(35, '381507-1', 2, 4, 1),
(36, '381705', 22, 2, 1),
(37, '381608-1', 19, 3, 1),
(38, '381506-ИА', 22, 4, 1),
(39, '381606-ИА', 22, 3, 1),
(40, '381602', 23, 3, 1),
(41, '381707-3', 17, 2, 1),
(42, '381708-3', 19, 2, 1),
(43, '381806-2', 5, 1, 1),
(44, '381803-3', 22, 1, 1),
(45, '381804', 25, 1, 1),
(46, '381806-1', 5, 1, 1),
(47, '381603-3', 21, 3, 1),
(48, '381706-4', 5, 2, 1),
(49, '381708-1', 19, 2, 1),
(50, '381703-2', 21, 2, 1),
(51, '381507-3', 17, 4, 1),
(52, '381808-1', 19, 1, 1),
(53, '381706-2', 5, 2, 1),
(54, '381607и', 17, 3, 1),
(55, '381506-2', 5, 4, 1),
(56, '381606-2', 5, 3, 1),
(57, '381807-2', 2, 1, 1),
(58, '381703-1', 21, 2, 1),
(59, '381703-3', 21, 2, 1),
(60, '381802', 23, 1, 1),
(61, '381806-4', 5, 1, 1),
(62, '381603-1', 21, 3, 1),
(63, '381505', 26, 4, 1),
(64, '381607-2', 17, 3, 1),
(65, '381803-4', 22, 1, 1),
(66, '381708-2', 19, 2, 1),
(67, '381508', 19, 4, 1),
(68, '381808-3', 19, 1, 1),
(69, '381707-2', 2, 2, 1),
(70, '381706-3', 5, 2, 1),
(71, '381503-3', 22, 4, 1),
(72, '381603-5', 27, 3, 1),
(73, '381503-2', 22, 4, 1),
(74, '381604', 25, 3, 1),
(75, '381606-1', 5, 3, 1),
(76, '381506-3', 5, 4, 1),
(77, '381704', 25, 2, 1),
(78, '381504', 25, 4, 1),
(79, '381608-2', 19, 3, 1),
(80, '381706-1', 5, 2, 1),
(81, '381805', 22, 1, 1),
(82, '381502', 23, 4, 1),
(83, '381806-3', 5, 1, 1),
(84, '381503-4', 27, 4, 1),
(85, '381605', 22, 3, 1),
(86, '381803-1', 22, 1, 1),
(87, '381506и', 5, 4, 1),
(88, '381503-1', 22, 4, 1),
(89, '381507-2', 2, 4, 1),
(90, '381703-4', 21, 2, 1),
(91, '381506-1', 5, 4, 1),
(92, '381707-1', 2, 2, 1),
(93, '381603-2МО', 21, 3, 1),
(94, '381706-ИА МО', 22, 2, 1),
(95, '381603-4', 21, 3, 1);

-- 
-- Вывод данных для таблицы tabl_department
--
INSERT INTO tabl_department VALUES
(1, 'кафедра алгебры, геометрии и дискретной математики', 'АГДМ', 1),
(2, 'кафедра дифференциальных уравнений, математического и численного анализа', 'ДУМЧА', 2),
(3, 'кафедра информатики и автоматизации научных исследований', 'ИАНИ', 3),
(4, 'кафедра математического обеспечения и суперкомпьютерных технологий', 'МОСТ', NULL),
(5, 'кафедра математической физики и оптимального управления', 'МФОУ', NULL),
(6, 'кафедра прикладной математики', 'ПМ', 4),
(7, 'кафедра программной инженерии', 'ПРИН', 5),
(8, 'кафедра теоретической, компьютерной и экспериментальной механики', 'ТКЭМ', 6),
(9, 'кафедра теории управления и динамики систем', 'ТУДС', 7);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;