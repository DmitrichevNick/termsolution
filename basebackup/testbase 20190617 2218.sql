﻿--
-- Скрипт сгенерирован Devart dbForge Studio 2019 for MySQL, Версия 8.1.22.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 17.06.2019 22:18:43
-- Версия сервера: 5.6.41-log
-- Версия клиента: 4.1
--

-- 
-- Отключение внешних ключей
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Установить режим SQL (SQL mode)
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Удалить процедуру `PR_PRACTICE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE;

--
-- Удалить процедуру `PRA_PRACTICE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE;

--
-- Удалить процедуру `PW_PRACTICE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE;

--
-- Удалить таблицу `tabl_practice`
--
DROP TABLE IF EXISTS tabl_practice;

--
-- Удалить процедуру `PR_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE_TYPE;

--
-- Удалить процедуру `PRA_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE_TYPE;

--
-- Удалить процедуру `PROC_READ_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PROC_READ_PRACTICE_TYPE;

--
-- Удалить процедуру `PW_PRACTICE_TYPE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE_TYPE;

--
-- Удалить таблицу `tabl_practice_type`
--
DROP TABLE IF EXISTS tabl_practice_type;

--
-- Удалить процедуру `PR_STUDENT`
--
DROP PROCEDURE IF EXISTS PR_STUDENT;

--
-- Удалить процедуру `PRA_STUDENT`
--
DROP PROCEDURE IF EXISTS PRA_STUDENT;

--
-- Удалить процедуру `PW_STUDENT`
--
DROP PROCEDURE IF EXISTS PW_STUDENT;

--
-- Удалить таблицу `tabl_student`
--
DROP TABLE IF EXISTS tabl_student;

--
-- Удалить процедуру `PR_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PR_PRACTICE_PLACE;

--
-- Удалить процедуру `PRA_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PRA_PRACTICE_PLACE;

--
-- Удалить процедуру `PW_PRACTICE_PLACE`
--
DROP PROCEDURE IF EXISTS PW_PRACTICE_PLACE;

--
-- Удалить таблицу `tabl_practice_place`
--
DROP TABLE IF EXISTS tabl_practice_place;

--
-- Удалить процедуру `PR_GROUP`
--
DROP PROCEDURE IF EXISTS PR_GROUP;

--
-- Удалить процедуру `PRA_GROUP`
--
DROP PROCEDURE IF EXISTS PRA_GROUP;

--
-- Удалить процедуру `PW_GROUP`
--
DROP PROCEDURE IF EXISTS PW_GROUP;

--
-- Удалить таблицу `tabl_group`
--
DROP TABLE IF EXISTS tabl_group;

--
-- Удалить процедуру `PR_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PR_EDU_FORM;

--
-- Удалить процедуру `PRA_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PRA_EDU_FORM;

--
-- Удалить процедуру `PW_EDU_FORM`
--
DROP PROCEDURE IF EXISTS PW_EDU_FORM;

--
-- Удалить таблицу `tabl_edu_form`
--
DROP TABLE IF EXISTS tabl_edu_form;

--
-- Удалить процедуру `PR_LECTURER`
--
DROP PROCEDURE IF EXISTS PR_LECTURER;

--
-- Удалить процедуру `PRA_LECTURER`
--
DROP PROCEDURE IF EXISTS PRA_LECTURER;

--
-- Удалить процедуру `PW_LECTURER`
--
DROP PROCEDURE IF EXISTS PW_LECTURER;

--
-- Удалить таблицу `tabl_lecturer`
--
DROP TABLE IF EXISTS tabl_lecturer;

--
-- Удалить процедуру `PR_POSITION`
--
DROP PROCEDURE IF EXISTS PR_POSITION;

--
-- Удалить процедуру `PRA_POSITION`
--
DROP PROCEDURE IF EXISTS PRA_POSITION;

--
-- Удалить процедуру `PW_POSITION`
--
DROP PROCEDURE IF EXISTS PW_POSITION;

--
-- Удалить таблицу `tabl_position`
--
DROP TABLE IF EXISTS tabl_position;

--
-- Удалить процедуру `PR_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PR_EDU_PROFILE;

--
-- Удалить процедуру `PRA_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PRA_EDU_PROFILE;

--
-- Удалить процедуру `PW_EDU_PROFILE`
--
DROP PROCEDURE IF EXISTS PW_EDU_PROFILE;

--
-- Удалить процедуру `R_EDU_PROFILE_BY_ID_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS R_EDU_PROFILE_BY_ID_EDU_DIRECTION;

--
-- Удалить таблицу `tabl_edu_profile`
--
DROP TABLE IF EXISTS tabl_edu_profile;

--
-- Удалить процедуру `PR_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PR_EDU_DIRECTION;

--
-- Удалить процедуру `PRA_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PRA_EDU_DIRECTION;

--
-- Удалить процедуру `PW_EDU_DIRECTION`
--
DROP PROCEDURE IF EXISTS PW_EDU_DIRECTION;

--
-- Удалить процедуру `R_EDU_DIRECTION_BY_ID_LEVEL`
--
DROP PROCEDURE IF EXISTS R_EDU_DIRECTION_BY_ID_LEVEL;

--
-- Удалить таблицу `tabl_edu_direction`
--
DROP TABLE IF EXISTS tabl_edu_direction;

--
-- Удалить процедуру `PR_LEVEL`
--
DROP PROCEDURE IF EXISTS PR_LEVEL;

--
-- Удалить процедуру `PRA_LEVEL`
--
DROP PROCEDURE IF EXISTS PRA_LEVEL;

--
-- Удалить процедуру `PW_LEVEL`
--
DROP PROCEDURE IF EXISTS PW_LEVEL;

--
-- Удалить таблицу `tabl_level`
--
DROP TABLE IF EXISTS tabl_level;

--
-- Удалить процедуру `PR_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PR_DEPARTMENT;

--
-- Удалить процедуру `PRA_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PRA_DEPARTMENT;

--
-- Удалить процедуру `PW_DEPARTMENT`
--
DROP PROCEDURE IF EXISTS PW_DEPARTMENT;

--
-- Удалить таблицу `tabl_department`
--
DROP TABLE IF EXISTS tabl_department;

--
-- Удалить процедуру `PR_DEPARTMENT_HEAD`
--
DROP PROCEDURE IF EXISTS PR_DEPARTMENT_HEAD;

--
-- Удалить процедуру `PRA_DEPARTMENT_HEAD`
--
DROP PROCEDURE IF EXISTS PRA_DEPARTMENT_HEAD;

--
-- Удалить процедуру `PW_DEPARTMENT_HEAD`
--
DROP PROCEDURE IF EXISTS PW_DEPARTMENT_HEAD;

--
-- Удалить таблицу `tabl_department_head`
--
DROP TABLE IF EXISTS tabl_department_head;

--
-- Удалить процедуру `PR_DEGREE`
--
DROP PROCEDURE IF EXISTS PR_DEGREE;

--
-- Удалить процедуру `PRA_DEGREE`
--
DROP PROCEDURE IF EXISTS PRA_DEGREE;

--
-- Удалить процедуру `PW_DEGREE`
--
DROP PROCEDURE IF EXISTS PW_DEGREE;

--
-- Удалить таблицу `tabl_degree`
--
DROP TABLE IF EXISTS tabl_degree;

--
-- Установка базы данных по умолчанию
--
USE testbase;

--
-- Создать таблицу `tabl_degree`
--
CREATE TABLE tabl_degree (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Степень',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Сокращенная степень',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 41,
AVG_ROW_LENGTH = 420,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Степень преподавателя';

DELIMITER $$

--
-- Создать процедуру `PW_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_DEGREE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_degree td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_degree (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_degree td
    SET td.NAME = IN_NAME,
        td.SHORT_NAME = IN_SHORT_NAME
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_DEGREE ()
BEGIN
  SELECT
    td.ID,
    td.NAME,
    td.SHORT_NAME
  FROM tabl_degree td;
END
$$

--
-- Создать процедуру `PR_DEGREE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_DEGREE (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_degree td
  WHERE td.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_department_head`
--
CREATE TABLE tabl_department_head (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Фамилия Имя Отчество',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Фамилия И.О.',
  ID_DEGREE int(11) UNSIGNED DEFAULT NULL COMMENT 'УН степень',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 8,
AVG_ROW_LENGTH = 2340,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Заведующий кафедрой';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_department_head
ADD CONSTRAINT FK_TDH_ID_DEGREE FOREIGN KEY (ID_DEGREE)
REFERENCES tabl_degree (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PW_DEPARTMENT_HEAD`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_DEPARTMENT_HEAD (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_DEGREE int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_department_head tdh
      WHERE tdh.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_department_head (ID, NAME, SHORT_NAME, ID_DEGREE)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_DEGREE);
  ELSE
    UPDATE tabl_department_head tdh
    SET tdh.NAME = IN_NAME,
        tdh.SHORT_NAME = IN_SHORT_NAME,
        tdh.ID_DEGREE = IN_ID_DEGREE
    WHERE tdh.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_DEPARTMENT_HEAD`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_DEPARTMENT_HEAD ()
BEGIN
  SELECT
    *
  FROM tabl_department_head tdh;
END
$$

--
-- Создать процедуру `PR_DEPARTMENT_HEAD`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_DEPARTMENT_HEAD (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_department_head tdh
  WHERE tdh.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_department`
--
CREATE TABLE tabl_department (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Наименование',
  CODE varchar(255) DEFAULT NULL COMMENT 'Код',
  ID_DEPARTMENT_HEAD int(11) UNSIGNED DEFAULT NULL COMMENT 'УН заведующего',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 10,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Кафедра';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_department
ADD CONSTRAINT FK_TDEP_ID_DEPARTMENT_HEAD FOREIGN KEY (ID_DEPARTMENT_HEAD)
REFERENCES tabl_department_head (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PW_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_DEPARTMENT (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_ID_DEPARTMENT_HEAD int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_department td
      WHERE td.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_department (ID, NAME, CODE, ID_DEPARTMENT_HEAD)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_ID_DEPARTMENT_HEAD);
  ELSE
    UPDATE tabl_department td
    SET td.NAME = IN_NAME,
        td.CODE = IN_CODE,
        td.ID_DEPARTMENT_HEAD = IN_ID_DEPARTMENT_HEAD
    WHERE td.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_DEPARTMENT ()
BEGIN
  SELECT
    *
  FROM tabl_department td;
END
$$

--
-- Создать процедуру `PR_DEPARTMENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_DEPARTMENT (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_department td
  WHERE td.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_level`
--
CREATE TABLE tabl_level (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Наименование',
  SHORT_NAME varchar(10) DEFAULT NULL COMMENT 'Сокращенное наименование',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 8192,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Уровни подготовки';

DELIMITER $$

--
-- Создать процедуру `PW_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_LEVEL (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(10))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_level tl
      WHERE tl.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_level (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_level tl
    SET tl.NAME = IN_NAME,
        tl.SHORT_NAME = IN_SHORT_NAME
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_LEVEL ()
BEGIN
  SELECT
    *
  FROM tabl_level tl;
END
$$

--
-- Создать процедуру `PR_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_LEVEL (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_level tl
  WHERE tl.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_edu_direction`
--
CREATE TABLE tabl_edu_direction (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Наименование',
  CODE varchar(255) DEFAULT NULL COMMENT 'Код',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Сокращенное наименование',
  ID_LEVEL int(11) UNSIGNED DEFAULT NULL COMMENT 'УН уровня подготовки',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 15,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Направление подготовки';

--
-- Создать внешний ключ
--
ALTER TABLE tabl_edu_direction
ADD CONSTRAINT FK_T_EDIR_ID_LEVEL FOREIGN KEY (ID_LEVEL)
REFERENCES tabl_level (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `R_EDU_DIRECTION_BY_ID_LEVEL`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE R_EDU_DIRECTION_BY_ID_LEVEL (IN IN_ID int(11))
BEGIN
  SELECT
    ted.ID,
    ted.NAME,
    ted.CODE,
    ted.SHORT_NAME,
    ted.ID_LEVEL
  FROM tabl_edu_direction ted
  WHERE ted.ID_LEVEL = IN_ID;
END
$$

--
-- Создать процедуру `PW_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_DIRECTION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_CODE varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_LEVEL char(1))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_direction ted
      WHERE ted.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_direction (ID, NAME, CODE, SHORT_NAME, ID_LEVEL)
      VALUES (IN_ID, IN_NAME, IN_CODE, IN_SHORT_NAME, IN_ID_LEVEL);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_edu_direction (NAME, CODE, SHORT_NAME, ID_LEVEL)
      VALUES (IN_NAME, IN_CODE, IN_SHORT_NAME, IN_ID_LEVEL);
  ELSE
    UPDATE tabl_edu_direction ted
    SET ted.NAME = IN_NAME,
        ted.CODE = IN_CODE,
        ted.SHORT_NAME = IN_SHORT_NAME,
        ted.ID_LEVEL = IN_ID_LEVEL
    WHERE ted.ID = IN_ID;
  END IF;

END
$$

--
-- Создать процедуру `PRA_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_DIRECTION ()
BEGIN
  SELECT
    *
  FROM tabl_edu_direction ted;
END
$$

--
-- Создать процедуру `PR_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_DIRECTION (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_edu_direction ted
  WHERE ted.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_edu_profile`
--
CREATE TABLE tabl_edu_profile (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  ID_EDU_DIRECTION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàïðàâëåíèÿ ïîäãîòîâêè',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ñîêðàùåííîå íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 30,
AVG_ROW_LENGTH = 1092,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Ïðîôèëü ïîäãîòîâêè';

--
-- Создать индекс `FK_TEP_ID_EDU_DIRECTION` для объекта типа таблица `tabl_edu_profile`
--
ALTER TABLE tabl_edu_profile
ADD INDEX FK_TEP_ID_EDU_DIRECTION (ID_EDU_DIRECTION);

--
-- Создать внешний ключ
--
ALTER TABLE tabl_edu_profile
ADD CONSTRAINT FK_T_EPRO_ID_EDU_DIR FOREIGN KEY (ID_EDU_DIRECTION)
REFERENCES tabl_edu_direction (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `R_EDU_PROFILE_BY_ID_EDU_DIRECTION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE R_EDU_PROFILE_BY_ID_EDU_DIRECTION (IN IN_ID_EDU_DIRECTION int(11))
BEGIN
  SELECT
    tep.ID,
    tep.ID_EDU_DIRECTION,
    tep.NAME,
    tep.SHORT_NAME
  FROM tabl_edu_profile tep
  WHERE tep.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION;
END
$$

--
-- Создать процедуру `PW_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_PROFILE (IN IN_ID int(11), IN IN_ID_EDU_DIRECTION int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_profile tep
      WHERE tep.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_profile (ID, ID_EDU_DIRECTION, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_ID_EDU_DIRECTION, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_edu_profile tep
    SET tep.NAME = IN_NAME,
        tep.SHORT_NAME = IN_SHORT_NAME,
        tep.ID_EDU_DIRECTION = IN_ID_EDU_DIRECTION
    WHERE tep.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_PROFILE ()
BEGIN
  SELECT
    *
  FROM tabl_edu_profile tep;
END
$$

--
-- Создать процедуру `PR_EDU_PROFILE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_PROFILE (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_edu_profile tep
  WHERE tep.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_position`
--
CREATE TABLE tabl_position (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Должность',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Сокращенная должность',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 64,
AVG_ROW_LENGTH = 264,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Должность преподавателя';

DELIMITER $$

--
-- Создать процедуру `PW_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_POSITION (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_position tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_position (ID, NAME, SHORT_NAME)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME);
  ELSE
    UPDATE tabl_position tp
    SET tp.NAME = IN_NAME,
        tp.SHORT_NAME = IN_SHORT_NAME
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_POSITION ()
BEGIN
  SELECT
    *
  FROM tabl_position tp;
END
$$

--
-- Создать процедуру `PR_POSITION`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_POSITION (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_position tp
  WHERE tp.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_lecturer`
--
CREATE TABLE tabl_lecturer (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Ô.È.Î. ïîëíîñòüþ',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ôàìèëèÿ È.Î.',
  ID_DEPARTMENT int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ êàôåäðû',
  ID_DEGREE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ñòåïåíè',
  ID_POSITION int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ äîëæíîñòè',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 215,
AVG_ROW_LENGTH = 230,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TLEC_ID_DEGREE FOREIGN KEY (ID_DEGREE)
REFERENCES tabl_degree (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TLEC_ID_DEPARTMENT FOREIGN KEY (ID_DEPARTMENT)
REFERENCES tabl_department (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_lecturer
ADD CONSTRAINT FK_TLEC_ID_POS FOREIGN KEY (ID_POSITION)
REFERENCES tabl_position (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PW_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_LECTURER (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_SHORT_NAME varchar(255), IN IN_ID_DEPARTMENT int(11), IN IN_ID_DEGREE int(11), IN IN_ID_POSITION int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_lecturer tl
      WHERE tl.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_lecturer (ID, NAME, SHORT_NAME, ID_DEPARTMENT, ID_DEGREE, ID_POSITION)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_DEPARTMENT, IN_ID_DEGREE, IN_ID_POSITION);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_lecturer (NAME, SHORT_NAME, ID_DEPARTMENT, ID_DEGREE, ID_POSITION)
      VALUES (IN_NAME, IN_SHORT_NAME, IN_ID_DEPARTMENT, IN_ID_DEGREE, IN_ID_POSITION);
  ELSE
    UPDATE tabl_lecturer tl
    SET tl.NAME = IN_NAME,
        tl.SHORT_NAME = IN_SHORT_NAME,
        tl.ID_DEPARTMENT = IN_ID_LECTURER,
        tl.ID_DEGREE = IN_ID_DEGREE,
        tl.ID_POSITION = IN_ID_POSITION
    WHERE tl.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_LECTURER ()
BEGIN
  SELECT
    *
  FROM tabl_lecturer tl;
END
$$

--
-- Создать процедуру `PR_LECTURER`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_LECTURER (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_lecturer tl
  WHERE tl.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_edu_form`
--
CREATE TABLE tabl_edu_form (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Наименование',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 2,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci,
COMMENT = 'Форма обучения';

DELIMITER $$

--
-- Создать процедуру `PW_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_EDU_FORM (IN IN_ID int(11), IN IN_NAME varchar(50))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_edu_form tef
      WHERE tef.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_edu_form (ID, NAME)
      VALUES (IN_ID, IN_NAME);
  ELSE
    UPDATE tabl_edu_form tef
    SET tef.NAME = IN_NAME
    WHERE tef.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_EDU_FORM ()
BEGIN
  SELECT
    *
  FROM tabl_edu_form tef;
END
$$

--
-- Создать процедуру `PR_EDU_FORM`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_EDU_FORM (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_edu_form tef
  WHERE tef.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_group`
--
CREATE TABLE tabl_group (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NUMBER varchar(255) DEFAULT NULL COMMENT 'Номер',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'УН направленности',
  COURSE int(1) UNSIGNED DEFAULT NULL COMMENT 'Курс',
  ID_EDU_FORM int(11) UNSIGNED DEFAULT NULL COMMENT 'УН формы обучения',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 96,
AVG_ROW_LENGTH = 963,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_group
ADD CONSTRAINT FK_TGS_ID_EDU_FORM FOREIGN KEY (ID_EDU_FORM)
REFERENCES tabl_edu_form (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_group
ADD CONSTRAINT FK_TSG_ID_EDU_PROFILE FOREIGN KEY (ID_EDU_PROFILE)
REFERENCES tabl_edu_profile (ID) ON DELETE SET NULL;

DELIMITER $$

--
-- Создать процедуру `PW_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_GROUP (IN IN_ID int(11), IN IN_NUMBER varchar(255), IN IN_ID_EDU_PROFILE int(11), IN IN_COURSE int(1), IN IN_ID_EDU_FORM int(11))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_group tg
      WHERE tg.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_group (ID, NUMBER, ID_EDU_PROFILE, COURSE, ID_EDU_FORML)
      VALUES (IN_ID, IN_NUMBER, IN_ID_EDU_PROFILE, IN_COURSE, IN_ID_EDU_FORM);
  ELSEIF IN_ID IS NULL THEN
    INSERT INTO tabl_group (NUMBER, ID_EDU_PROFILE, COURSE, ID_EDU_FORM)
      VALUES (IN_NUMBER, IN_ID_EDU_PROFILE, IN_COURSE, IN_ID_EDU_FORM);
  ELSE
    UPDATE tabl_group tg
    SET tg.NUMBER = IN_NUMBER,
        tg.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tg.COURSE = IN_COURSE,
        tg.ID_EDU_FORM = IN_ID_EDU_FORM
    WHERE tg.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_GROUP ()
BEGIN
  SELECT
    *
  FROM tabl_group tg;
END
$$

--
-- Создать процедуру `PR_GROUP`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_GROUP (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_group tg
  WHERE tg.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_practice_place`
--
CREATE TABLE tabl_practice_place (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  NAME varchar(255) DEFAULT NULL COMMENT 'Íàçâàíèå',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PW_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE_PLACE (IN IN_ID int(11), IN IN_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_place tpp
      WHERE tpp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_place (ID, NAME)
      VALUES (IN_ID, IN_NAME);
  ELSE
    UPDATE tabl_practice_place tpp
    SET tpp.NAME = IN_NAME
    WHERE tpp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE_PLACE ()
BEGIN
  SELECT
    *
  FROM tabl_practice_place tpp;
END
$$

--
-- Создать процедуру `PR_PRACTICE_PLACE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE_PLACE (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_practice_place tpp
  WHERE tpp.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_student`
--
CREATE TABLE tabl_student (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(50) DEFAULT NULL COMMENT 'Ô.È.Î. ïîëíîñòüþ',
  SHORT_NAME varchar(255) DEFAULT NULL COMMENT 'Ôàìèëèÿ È.Î.',
  ID_GROUP int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ãðóïïû',
  ID_DEPARTMENT int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ êàôåäðû',
  ID_LECTURER int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ íàó÷íîãî ðóêîâîäèòåëÿ',
  ID_PLACE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ìåñòà ïðîõîæäåíèÿ ïðàêòèêè',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1569,
AVG_ROW_LENGTH = 481,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_DEPARTMENT FOREIGN KEY (ID_DEPARTMENT)
REFERENCES tabl_department (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_GROUP FOREIGN KEY (ID_GROUP)
REFERENCES tabl_group (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_LECTURER FOREIGN KEY (ID_LECTURER)
REFERENCES tabl_lecturer (ID) ON DELETE SET NULL;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_student
ADD CONSTRAINT FK_TS_ID_PR_PLACE FOREIGN KEY (ID_PLACE)
REFERENCES tabl_practice_place (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PW_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_STUDENT (IN IN_ID int, IN IN_NAME varchar(50), IN IN_SHORT_NAME varchar(255), IN IN_ID_GROUP int, IN IN_ID_DEPARTMENT int, IN IN_ID_LECTURER int, IN IN_ID_PLACE int)
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_student ts
      WHERE ts.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_student (ID, NAME, SHORT_NAME, ID_GROUP, ID_DEPARTMENT, ID_LECTURER, ID_PLACE)
      VALUES (IN_ID, IN_NAME, IN_SHORT_NAME, IN_ID_GROUP, IN_ID_DEPARTMENT, IN_ID_LECTURER, IN_ID_PLACE);
  ELSE
    UPDATE tabl_student ts
    SET ts.NAME = IN_NAME,
        ts.SHORT_NAME = IN_SHORT_NAME,
        ts.ID_GROUP = IN_ID_GROUP,
        ts.ID_DEPARTMENT = IN_ID_DEPARTMENT,
        ts.ID_LECTURER = IN_ID_LECTURER,
        ts.ID_PLACE = IN_ID_PLACE
    WHERE ts.ID = IN_ID;
  END IF;


END
$$

--
-- Создать процедуру `PRA_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_STUDENT ()
BEGIN
  SELECT
    *
  FROM testbase.tabl_student ts;
END
$$

--
-- Создать процедуру `PR_STUDENT`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_STUDENT (IN IN_ID int)
BEGIN
  SELECT
    *
  FROM testbase.tabl_student ts
  WHERE ts.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_practice_type`
--
CREATE TABLE tabl_practice_type (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'УН',
  NAME varchar(255) DEFAULT NULL COMMENT 'Наименование',
  NAME_R varchar(255) DEFAULT NULL COMMENT 'Наименование в родительном падеже',
  NAME_V varchar(255) DEFAULT NULL,
  ADD_NAME varchar(255) DEFAULT NULL,
  SHORT_ADD_NAME varchar(255) DEFAULT NULL,
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

DELIMITER $$

--
-- Создать процедуру `PW_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE_TYPE (IN IN_ID int(11), IN IN_NAME varchar(255), IN IN_NAME_R varchar(255), IN IN_NAME_V varchar(255), IN IN_ADD_NAME varchar(255), IN IN_SHORT_ADD_NAME varchar(255))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice_type tpt
      WHERE tpt.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice_type (ID, NAME, NAME_R, NAME_V, ADD_NAME, SHORT_ADD_NAME)
      VALUES (IN_ID, IN_NAME, IN_NAME_R, IN_NAME_V, IN_ADD_NAME, IN_SHORT_ADD_NAME);
  ELSE
    UPDATE tabl_practice_type tpt
    SET tpt.NAME = IN_NAME,
        tpt.NAME_R = IN_NAME_R,
        tpt.NAME_V = IN_NAME_V,
        tpt.ADD_NAME = IN_ADD_NAME,
        tpt.SHORT_ADD_NAME = IN_SHORT_ADD_NAME
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PROC_READ_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PROC_READ_PRACTICE_TYPE (IN IN_ID int(11))
BEGIN
  IF IN_ID IS NULL THEN
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R,
      tpt.NAME_R2,
      tpt.ADD_NAME,
      tpt.SHORT_ADD_NAME
    FROM tabl_practice_type tpt;
  ELSE
    SELECT
      tpt.ID,
      tpt.NAME,
      tpt.NAME_R,
      tpt.NAME_R2,
      tpt.ADD_NAME,
      tpt.SHORT_ADD_NAME
    FROM tabl_practice_type tpt
    WHERE tpt.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE_TYPE ()
BEGIN
  SELECT
    *
  FROM tabl_practice_type tpt;
END
$$

--
-- Создать процедуру `PR_PRACTICE_TYPE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE_TYPE (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_practice_type tpt
  WHERE tpt.ID = IN_ID;
END
$$

DELIMITER ;

--
-- Создать таблицу `tabl_practice`
--
CREATE TABLE tabl_practice (
  ID int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ÓÍ',
  ID_PRACTICE_TYPE int(11) UNSIGNED DEFAULT NULL COMMENT 'Òèï ïðàêòèêè',
  DATE_BEGIN date DEFAULT NULL COMMENT 'Äàòà íà÷àëà',
  DATE_END date DEFAULT NULL COMMENT 'Äàòà îêîí÷àíèÿ',
  ID_EDU_PROFILE int(11) UNSIGNED DEFAULT NULL COMMENT 'ÓÍ ïðîôèëÿ ïîäãîòîâêè',
  SEMESTER int(1) UNSIGNED DEFAULT NULL COMMENT 'Ñåìåñòð',
  COURSE int(1) UNSIGNED DEFAULT NULL COMMENT 'Êóðñ',
  PRIMARY KEY (ID)
)
ENGINE = INNODB,
AUTO_INCREMENT = 32,
AVG_ROW_LENGTH = 528,
CHARACTER SET utf8,
COLLATE utf8_unicode_ci;

--
-- Создать индекс `FK_tabl_practice_ID_PRACTICE_TYPE` для объекта типа таблица `tabl_practice`
--
ALTER TABLE tabl_practice
ADD INDEX FK_tabl_practice_ID_PRACTICE_TYPE (ID_PRACTICE_TYPE);

--
-- Создать внешний ключ
--
ALTER TABLE tabl_practice
ADD CONSTRAINT FK_TPR_ID_EDU_PROFILE FOREIGN KEY (ID_EDU_PROFILE)
REFERENCES tabl_edu_profile (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Создать внешний ключ
--
ALTER TABLE tabl_practice
ADD CONSTRAINT FK_TPR_ID_PR_TYPE FOREIGN KEY (ID_PRACTICE_TYPE)
REFERENCES tabl_practice_type (ID) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$

--
-- Создать процедуру `PW_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PW_PRACTICE (IN IN_ID int(11), IN IN_ID_PRACTICE_TYPE int(1), IN IN_DATE_BEGIN date, IN IN_DATE_END date, IN IN_ID_EDU_PROFILE int(11), IN IN_SEMESTER int(1), IN IN_COURSE int(1))
BEGIN
  IF (SELECT
        COUNT(*)
      FROM tabl_practice tp
      WHERE tp.ID = IN_ID) = 0 THEN
    INSERT INTO tabl_practice (ID, ID_PRACTICE_TYPE, DATE_BEGIN, DATE_END, ID_EDU_PROFILE, SEMESTER, COURSE)
      VALUES (IN_ID, IN_ID_PRACTICE_TYPE, IN_DATE_BEGIN, IN_DATE_END, IN_ID_EDU_PROFILE, IN_SEMESTER, IN_COURSE);
  ELSE
    UPDATE tabl_practice tp
    SET tp.ID_PRACTICE_TYPE = IN_ID_PRACTICE_TYPE,
        tp.DATE_BEGIN = IN_DATE_BEGIN,
        tp.DATE_END = IN_DATE_END,
        tp.ID_EDU_PROFILE = IN_ID_EDU_PROFILE,
        tp.SEMESTER = IN_SEMESTER,
        tp.COURSE = IN_COURSE
    WHERE tp.ID = IN_ID;
  END IF;
END
$$

--
-- Создать процедуру `PRA_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PRA_PRACTICE ()
BEGIN
  SELECT
    *
  FROM tabl_practice tp;
END
$$

--
-- Создать процедуру `PR_PRACTICE`
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE PR_PRACTICE (IN IN_ID int(11))
BEGIN
  SELECT
    *
  FROM tabl_practice tp
  WHERE tp.ID = IN_ID;
END
$$

DELIMITER ;

-- 
-- Вывод данных для таблицы tabl_degree
--
INSERT INTO tabl_degree VALUES
(1, 'академик', 'ак.'),
(2, 'доктор биологических наук', 'д.б.н.'),
(3, 'кандидат биологических наук', 'к.б.н.'),
(4, 'кандидат военных наук', 'к.воен.н.'),
(5, 'доктор географических наук', 'д.г.н.'),
(6, 'кандидат географических наук', 'к.г.н.'),
(7, 'доктор геолого-минералогических наук', 'д.г.-м.н.'),
(8, 'кандидат геолого-минералогических наук', 'к.г.-м.н.'),
(9, 'доктор исторических наук', 'д.и.н.'),
(10, 'кандидат исторических наук', 'к.и.н.'),
(11, 'доктор искусствоведения', 'д.иск.'),
(12, 'кандидат искусствоведения', 'к.иск.'),
(13, 'доктор медицинских наук', 'д.м.н.'),
(14, 'кандидат медицинских наук', 'к.м.н.'),
(15, 'доктор педагогических наук', 'д.пед.н.'),
(16, 'кандидат педагогических наук', 'к.пед.н.'),
(17, 'доктор политических наук', 'д.полит.н.'),
(18, 'кандидат политических наук', 'к.полит.н.'),
(19, 'доктор психологических наук', 'д.п.н.'),
(20, 'кандидат психологических наук\t', 'к.п.н.'),
(21, 'доктор сельскохозяйственных наук', 'д.с.-х.н.'),
(22, 'кандидат сельскохозяйственных наук', 'к.с.-х.н.'),
(23, 'доктор социологических наук', 'д.социол.н.'),
(24, 'кандидат социологических наук', 'к.социол.н.'),
(25, 'доктор технических наук', 'д.т.н.'),
(26, 'кандидат технических наук', 'к.т.н.'),
(27, 'кандидат фармакологических наук', 'к.фарм.н.'),
(28, 'доктор филологических наук', 'д.ф.н.'),
(29, 'кандидат филологических наук', 'к.ф.н.'),
(30, 'доктор физико-математических наук', 'д.ф.-м.н.'),
(31, 'кандидат физико-математических наук', 'к.ф.-м.н.'),
(32, 'доктор философских наук', 'д.филос.н.'),
(33, 'кандидат философских наук', 'к.филос.н.'),
(34, 'доктор химических наук', 'д.х.н.'),
(35, 'кандидат химических наук', 'к.х.н.'),
(36, 'доктор экономических наук', 'д.э.н.'),
(37, 'кандидат экономических наук', 'к.э.н.'),
(38, 'доктор юридических наук', 'д.ю.н.'),
(39, 'кандидат юридических наук', 'к.ю.н.'),
(40, 'без степени', 'б.с.');

-- 
-- Вывод данных для таблицы tabl_level
--
INSERT INTO tabl_level VALUES
(1, 'бакалавриат', 'б'),
(2, 'магистратура', 'м'),
(3, 'специалитет', 'с');

-- 
-- Вывод данных для таблицы tabl_department_head
--
INSERT INTO tabl_department_head VALUES
(1, 'Кузнецов Михаил Иванович', 'Кузнецов М.И.', NULL),
(2, 'Калинин Алексей Вячеславович', 'Калинин А.В.', NULL),
(3, 'Прилуцкий Михаил Хаимович', 'Прилуцкий М.Х.', NULL),
(4, 'Иванченко Михаил Васильевич', 'Иванченко М.В.', NULL),
(5, 'Гергель Виктор Павлович', 'Гергель В.П.', NULL),
(6, 'Игумнов Леонид Александрович', 'Игумнов Л.А.', NULL),
(7, 'Осипов Григорий Владимирович', 'Осипов Г.В.', NULL);

-- 
-- Вывод данных для таблицы tabl_edu_direction
--
INSERT INTO tabl_edu_direction VALUES
(1, 'механика и математическое моделирование', '01.04.03', 'к.и.', 2),
(2, 'прикладная информатика', '09.04.03', 'к.и.', 2),
(3, 'прикладная математика и информатика', '01.04.02', 'к.и.', 2),
(4, 'математика и компьютерные науки', '02.04.01', 'к.и.', 2),
(5, 'фундаментальная информатика и информационные технологии', '02.04.02', 'к.и.', 2),
(6, 'математика', '01.04.01', 'к.и.', 2),
(7, 'прикладная информатика', '09.03.03', 'к.и.', 1),
(8, 'программная инженерия', '09.03.04', 'к.и.', 1),
(9, 'прикладная математика и информатика', '01.03.02', 'к.и.', 1),
(10, 'математика', '01.03.01', 'к.и.', 1),
(11, 'математика и компьютерные науки', '02.03.01', 'к.и.', 1),
(12, 'механика и математическое моделирование', '01.04.03', 'к.и.', 1),
(13, 'механика и математическое моделирование', '01.03.03', 'к.и.', 1),
(14, 'фундаментальная информатика и информационные технологии', '02.03.02', 'к.и.', 1);

-- 
-- Вывод данных для таблицы tabl_position
--
INSERT INTO tabl_position VALUES
(1, 'академик-секретарь', 'акад.-секр.'),
(2, 'аспирант', 'асп.'),
(3, 'ассистент', 'асс.'),
(4, 'ведущий научный сотрудник', 'внс'),
(5, 'ведущий специалист', 'вед.спец.'),
(6, 'вице-президент', 'вице-през.'),
(7, 'генеральный директор', 'ген.дир.'),
(8, 'генеральный конструктор', 'ген.констр.'),
(9, 'главный научный сотрудник', 'гнс'),
(10, 'главный редактор', 'гл.ред.'),
(11, 'главный специалист', 'гл.спец.'),
(12, 'декан', 'декан'),
(13, 'директор', 'дир.'),
(14, 'докторант', 'докторант'),
(15, 'доцент', 'доц.'),
(16, 'заведующий кафедрой', 'зав.каф.'),
(17, 'заведующий станцией', 'зав.станц.'),
(18, 'зам. академика-секретаря', 'зам.акад.-секр.'),
(19, 'зам. генерального директора', 'зам.ген.дир.'),
(20, 'зам. главного редактора', 'зам.гл.ред.'),
(21, 'зам. декана', 'зам.дек.'),
(22, 'зам. директора', 'зам.дир.'),
(23, 'зам. председателя', 'зам.пред.'),
(24, 'зам. руководителя', 'зам.рук.'),
(25, 'зам. руководителя (заведующего, начальника) группы', 'зам.рук.гр.'),
(26, 'зам. руководителя (заведующего, начальника) лаборатории', 'зам.рук.лаб.'),
(27, 'зам. руководителя (заведующего, начальника) отдела', 'зам.рук.отдела'),
(28, 'зам. руководителя (заведующего, начальника, председателя) отделения', 'зам.рук.отд.'),
(29, 'зам. руководителя (заведующего, начальника) сектора', 'зам.рук.сект.'),
(30, 'зам. руководителя (заведующего, начальника, председателя) центра (научного, учебного и т.п.)', 'зам.рук.центра'),
(31, 'консультант', 'конс.'),
(32, 'лаборант', 'лаб.'),
(33, 'младший научный сотрудник', 'мнс'),
(34, 'научный консультант', 'науч.конс.'),
(35, 'научный сотрудник', 'нс'),
(36, 'начальник управления', 'нач.упр.'),
(37, 'начальник экспедиции', 'нач.экспед.'),
(38, 'председатель.', 'пред.'),
(39, 'президент', 'през.'),
(40, 'преподаватель', 'преп.'),
(41, 'проректор', 'проректор'),
(42, 'профессор', 'проф.'),
(43, 'редактор', 'ред.'),
(44, 'ректор', 'ректор'),
(45, 'руководитель (заведующий, начальник) группы', 'рук.гр.'),
(46, 'руководитель (заведующий, начальник) лаборатории', 'рук.лаб.'),
(47, 'руководитель (заведующий, начальник) отдела', 'рук.отдела'),
(48, 'руководитель (заведующий, начальник, председатель) отделения', 'рук.отд.'),
(49, 'руководитель (заведующий, начальник) сектора', 'рук.сект.'),
(50, 'руководитель (заведующий, начальник, председатель) центра (научного, учебного и т.п.)', 'рук.центра'),
(51, 'советник', 'советник'),
(52, 'специалист (зоолог, программист, геолог, инженер и т.п.)', 'спец.'),
(53, 'старший специалист (геолог, зоолог, инженер и т.п.)', 'ст.спец.'),
(54, 'старший лаборант', 'ст.лаб.'),
(55, 'старший преподаватель', 'ст.преп.'),
(56, 'старший техник', 'ст.техн.'),
(57, 'стажер', 'стажер'),
(58, 'старший научный сотрудник', 'снс'),
(59, 'студент', 'студ.'),
(60, 'техник', 'техн.'),
(61, 'ученый секpетаpь', 'уч.секp.'),
(62, 'другие должности', 'др.'),
(63, 'без должности', 'б.д.');

-- 
-- Вывод данных для таблицы tabl_department
--
INSERT INTO tabl_department VALUES
(1, 'кафедра алгебры, геометрии и дискретной математики', 'АГДМ', 1),
(2, 'кафедра дифференциальных уравнений, математического и численного анализа', 'ДУМЧА', 2),
(3, 'кафедра информатики и автоматизации научных исследований', 'ИАНИ', 3),
(4, 'кафедра математического обеспечения и суперкомпьютерных технологий', 'МОСТ', NULL),
(5, 'кафедра математической физики и оптимального управления', 'МФОУ', NULL),
(6, 'кафедра прикладной математики', 'ПМ', 4),
(7, 'кафедра программной инженерии', 'ПРИН', 5),
(8, 'кафедра теоретической, компьютерной и экспериментальной механики', 'ТКЭМ', 6),
(9, 'кафедра теории управления и динамики систем', 'ТУДС', 7);

-- 
-- Вывод данных для таблицы tabl_edu_profile
--
INSERT INTO tabl_edu_profile VALUES
(1, 1, 'информационное и программное обеспечение. Инженерия', 'к.и.'),
(2, 2, 'прикладная информатика в области принятия решений', 'к.и.'),
(3, 3, 'вычислительные методы и суперкомпьютерные технологии', 'к.и.'),
(4, 4, 'математическое и компьютерное моделирование в естественных науках', 'к.и.'),
(5, 5, 'инженерия программного обеспечения', 'к.и.'),
(6, 5, 'вероятностное моделирование и анализ данных', 'к.и.'),
(7, 5, 'когнитивные системы', 'к.и.'),
(8, 5, 'компьютерная графика и моделирование живых и технических систем', 'к.и.'),
(9, 3, 'компьютерные науки и приложения', 'к.и.'),
(10, 3, 'математическое моделирование динамики систем и процессов управления', 'к.и.'),
(11, 3, 'математическое моделирование физико-механических процессов', 'к.и.'),
(12, 6, 'фундаментальная математика и приложения', 'к.и.'),
(13, NULL, NULL, NULL),
(14, NULL, NULL, NULL),
(15, NULL, NULL, NULL),
(16, 14, 'инженерия программного обеспечения', 'к.и.'),
(17, 7, 'прикладная информатика в информационной сфере', 'к.и.'),
(18, 14, '', 'к.и.'),
(19, 8, 'разработка программно-информационных систем', 'к.и.'),
(20, 7, 'прикладная информатика в области принятия решений', 'к.и.'),
(21, 9, 'прикладная математика и информатика (общий)', 'к.и.'),
(22, 9, 'общий профиль', 'к.и.'),
(23, 10, 'общий', 'к.и.'),
(24, 11, 'общий профиль', 'к.и.'),
(25, 12, 'математическое моделирование и компьютерный инжиниринг', 'к.и.'),
(26, 11, 'математическое и компьютерное моделирование', 'к.и.'),
(27, 9, 'математическое моделирование и вычислительная математика', 'к.и.'),
(28, 13, 'математическое моделирование и компьютерный инжиниринг', 'к.и.'),
(29, 14, 'разработка программно-информационных систем', 'к.и.');

-- 
-- Вывод данных для таблицы tabl_edu_form
--
INSERT INTO tabl_edu_form VALUES
(1, 'очная');

-- 
-- Вывод данных для таблицы tabl_practice_place
--
INSERT INTO tabl_practice_place VALUES
(1, 'место проведения практики'),
(2, '"Объединенный центр компьютерных исследований" Нижегородский государственный университет им. Н.И. Лобачевского'),
(3, '"Лаборатория динамических и управляемых систем" Нижегородский государственный университет им. Н.И. Лобачевского'),
(4, '"Центр биоинформатики" Нижегородский государственный университет им. Н.И. Лобачевского'),
(5, '"Лаборатория прикладных информационных систем" Нижегородский государственный университет им. Н.И. Лобачевского'),
(6, '"Кафедра программной инженерии" Нижегородский государственный университет им. Н.И. Лобачевского');

-- 
-- Вывод данных для таблицы tabl_lecturer
--
INSERT INTO tabl_lecturer VALUES
(1, 'Абросимов Николай Анатольевич', 'Абросимов Н.А.', 8, NULL, 42),
(2, 'Алексеев Владимир Евгеньевич', 'Алексеев В.Е.', 1, NULL, 42),
(3, 'Алешин Кирилл Николаевич', 'Алешин К.Н.', 6, NULL, 3),
(4, 'Андрианов Валерий Леонидович', 'Андрианов В.Л.', 6, NULL, 15),
(5, 'Артемьева Нина Анатольевна', 'Артемьева Н.А.', 6, NULL, 63),
(6, 'Баландин Александр Владимирович', 'Баландин А.В.', 1, NULL, 15),
(7, 'Баландин Владимир Васильевич', 'Баландин В.В.', 8, NULL, 15),
(8, 'Баландин Владимир Владимирович', 'Баландин В.В.', 8, NULL, 63),
(9, 'Баландин Дмитрий Владимирович', 'Баландин Д.В.', 2, NULL, 42),
(10, 'Банкрутенко Владимир Викторович', 'Банкрутенко В.В.', 3, NULL, 15),
(11, 'Баркалов Александр Валентинович', 'Баркалов А.В.', 4, NULL, 40),
(12, 'Баркалов Константин Александрович', 'Баркалов К.А.', 4, NULL, 15),
(13, 'Бартенев Юрий Германович', 'Бартенев Ю.Г.', 3, NULL, 42),
(14, 'Барышева Ирина Викторовна', 'Барышева И.В.', 4, NULL, 40),
(15, 'Басалин Павел Дмитриевич', 'Басалин П.Д.', 3, NULL, 15),
(16, 'Белокрылов Петр Юрьевич', 'Белокрылов П.Ю.', 3, NULL, 15),
(17, 'Бирюков Руслан Сергеевич', 'Бирюков Р.С.', 9, NULL, 55),
(18, 'Борисов Николай Анатольевич', 'Борисов Н.А.', 7, NULL, 15),
(19, 'Бородина Татьяна Сергеевна', 'Бородина Т.С.', 7, NULL, 55),
(20, 'Быкова Маргарита Александровна', 'Быкова М.А.', 3, NULL, 3),
(21, 'Васильев Евгений Павлович', 'Васильев Е.П.', 4, NULL, 3),
(22, 'Веселов Сергей Иванович', 'Веселов С.И.', 1, NULL, 15),
(23, 'Власов Валентин Сергеевич', 'Власов В.С.', 3, NULL, 15),
(24, 'Власов Сергей Евгеньевич', 'Власов С.Е.', 3, NULL, 42),
(25, 'Волокитин Валентин Дмитриевич', 'Волокитин В.Д.', 4, NULL, 3),
(26, 'Гаврилов Владимир Сергеевич', 'Гаврилов В.С.', 6, NULL, 15),
(27, 'Галкин Олег Евгеньевич', 'Галкин О.Е.', 6, NULL, 15),
(28, 'Галкина Светлана Юрьевна', 'Галкина С.Ю.', 6, NULL, 15),
(29, 'Гергель Виктор Павлович', 'Гергель В.П.', 7, NULL, 16),
(30, 'Гетманская Александра Александровна', 'Гетманская А.А.', 4, NULL, 63),
(31, 'Гетманская Александра Александровна', 'Гетманская А.А.', 4, NULL, 3),
(32, 'Голдинова Екатерина Геннадьевна', 'Голдинова Е.Г.', 4, NULL, 63),
(33, 'Голышева Наталья Михайловна', 'Голышева Н.М.', 7, NULL, 15),
(34, 'Гонова Татьяна Васильевна', 'Гонова Т.В.', 8, NULL, 63),
(35, 'Гордеева Ольга Владимировна', 'Гордеева О.В.', 2, NULL, 3),
(36, 'Городецкий Станислав Юрьевич', 'Городецкий С.Ю.', 9, NULL, 15),
(37, 'Горохов Василий Андреевич', 'Горохов В.А.', 8, NULL, 15),
(38, 'Горшков Антон Валерьевич', 'Горшков А.В.', 4, NULL, 55),
(39, 'Грезина Александра Викторовна', 'Грезина А.В.', 6, NULL, 15),
(40, 'Грибанов Дмитрий Владимирович', 'Грибанов Д.В.', 1, NULL, 55),
(41, 'Грибанов Дмитрий Владимирович', 'Грибанов Д.В.', 1, NULL, 33),
(42, 'Гришагин Владимир Александрович', 'Гришагин В.А.', 4, NULL, 15),
(43, 'Губина Елена Васильевна', 'Губина Е.В.', 9, NULL, 15),
(44, 'Гуськова Юлия Андреевна', 'Гуськова Ю.А.', 7, NULL, 54),
(45, 'Денисов Вадим Владимирович', 'Денисов В.В.', 8, NULL, 63),
(46, 'Денисова Наталья Андреевна', 'Денисова Н.А.', 6, NULL, 15),
(47, 'Дерендяев Николай Васильевич', 'Дерендяев Н.В.', 9, NULL, 42),
(48, 'Додунова Людмила Кузьминична', 'Додунова Л.К.', 6, NULL, 15),
(49, 'Донцова Марина Владимировна', 'Донцова М.В.', 2, NULL, 3),
(50, 'Драгунов Тимофей Николаевич', 'Драгунов Т.Н.', 2, NULL, 15),
(51, 'Другова Ольга Валентиновна', 'Другова О.В.', 9, NULL, 15),
(52, 'Ефремова Людмила Сергеевна', 'Ефремова Л.С.', 2, NULL, 15),
(53, 'Жегалов Дмитрий Владимирович', 'Жегалов Д.В.', 8, NULL, 15),
(54, 'Жидков Александр Васильевич', 'Жидков А.В.', 8, NULL, 15),
(55, 'Заикин Алексей Анатольевич', 'Заикин А.А.', 6, NULL, 42),
(56, 'Захарова Дарья Владимировна', 'Захарова Д.В.', 1, NULL, 3),
(57, 'Звонилов Виктор Иванович', 'Звонилов В.И.', 1, NULL, 15),
(58, 'Золотых Николай Юрьевич', 'Золотых Н.Ю.', 1, NULL, 42),
(59, 'Золотых Николай Юрьевич', 'Золотых Н.Ю.', 1, NULL, 4),
(60, 'Зорин Андрей Владимирович', 'Зорин А.В.', 7, NULL, 42),
(61, 'Зорин Владимир Александрович', 'Зорин В.А.', 7, NULL, 15),
(62, 'Иванченко Михаил Васильевич', 'Иванченко М.В.', 6, NULL, 4),
(63, 'Иванченко Михаил Васильевич', 'Иванченко М.В.', 6, NULL, 42),
(64, 'Иванченко Михаил Васильевич', 'Иванченко М.В.', 6, NULL, 16),
(65, 'Игумнов Леонид Александрович', 'Игумнов Л.А.', 8, NULL, 16),
(66, 'Изосимова Ольга Алексеевна', 'Изосимова О.А.', 6, NULL, 3),
(67, 'Иорданский Михаил Анатольевич', 'Иорданский М.А.', 3, NULL, 42),
(68, 'Ипатов Александр Александрович', 'Ипатов А.А.', 8, NULL, 3),
(69, 'Кадина Елена Юрьевна', 'Кадина Е.Ю.', 9, NULL, 3),
(70, 'Казаков Алексей Олегович', 'Казаков А.О.', 9, NULL, 15),
(71, 'Калашников Александр Львович', 'Калашников А.Л.', 2, NULL, 15),
(72, 'Калинин Алексей Вячеславович', 'Калинин А.В.', 2, NULL, 16),
(74, 'Калякулина Алёна Игоревна', 'Калякулина А.И.', 6, NULL, 63),
(75, 'Капитанов Денис Владимирович', 'Капитанов Д.В.', 8, NULL, 55),
(76, 'Карпенко Сергей Николаевич', 'Карпенко С.Н.', 7, NULL, 15),
(77, 'Карпычев Владимир Юрьевич', 'Карпычев В.Ю.', 3, NULL, 42),
(78, 'Касаткин Дмитрий Владимирович', 'Касаткин Д.В.', 6, NULL, 55),
(79, 'Кириллин Михаил Юрьевич', 'Кириллин М.Ю.', 6, NULL, 15),
(80, 'Киселев Владимир Геннадьевич', 'Киселев В.Г.', 8, NULL, 15),
(81, 'Киселева Наталья Владимировна', 'Киселева Н.В.', 9, NULL, 15),
(82, 'Киселева Татьяна Петровна', 'Киселева Т.П.', 2, NULL, 40),
(83, 'Кожанов Дмитрий Александрович', 'Кожанов Д.А.', 8, NULL, 55),
(84, 'Козинов Евгений Александрович', 'Козинов Е.А.', 4, NULL, 3),
(85, 'Константинов Александр Юрьевич', 'Константинов А.Ю.', 8, NULL, 15),
(86, 'Константинов Владимир Николаевич', 'Константинов В.Н.', 8, NULL, 63),
(87, 'Коротченко Анатолий Григорьевич', 'Коротченко А.Г.', 3, NULL, 15),
(88, 'Костин Василий Александрович', 'Костин В.А.', 9, NULL, 15),
(89, 'Костромина Ольга Сергеевна', 'Костромина О.С.', 2, NULL, 3),
(90, 'Костюков Валентин Ефимович', 'Костюков В.Е.', 3, NULL, 42),
(91, 'Котов Василий Леонидович', 'Котов В.Л.', 8, NULL, 42),
(92, 'Котова Анна Владимировна', 'Котова А.В.', 8, NULL, 63),
(93, 'Кочеганова Мария Анатольевна', 'Кочеганова М.А.', 7, NULL, 3),
(94, 'Кочетков Анатолий Васильевич', 'Кочетков А.В.', 8, NULL, 42),
(95, 'Кротов Николай Владимирович', 'Кротов Н.В.', 2, NULL, 15),
(96, 'Крылова Любовь Львовна', 'Крылова Л.Л.', 9, NULL, 63),
(97, 'Кувыкина Елена Вадимовна', 'Кувыкина Е.В.', 7, NULL, 15),
(98, 'Кудрявцев Евгений Владимирович', 'Кудрявцев Е.В.', 7, NULL, 3),
(99, 'Кузенков Олег Анатольевич', 'Кузенков О.А.', 2, NULL, 15),
(100, 'Кузенкова Галина Владимировна', 'Кузенкова Г.В.', 7, NULL, 15),
(101, 'Кузнецов Михаил Иванович', 'Кузнецов М.И.', 1, NULL, 16),
(102, 'Кумагина Елена Александровна', 'Кумагина Е.А.', 3, NULL, 15),
(103, 'Кустикова Валентина Дмитриевна', 'Кустикова В.Д.', 4, NULL, 15),
(104, 'Ламзин Дмитрий Александрович', 'Ламзин Д.А.', 8, NULL, 15),
(105, 'Лаптева Татьяна', 'Лаптева Т.', 9, NULL, 15),
(106, 'Лебедев Илья Генадьевич', 'Лебедев И.Г.', 4, NULL, 3),
(107, 'Леванова Татьяна Александровна', 'Леванова Т.А.', 9, NULL, 3),
(108, 'Леванова Татьяна Александровна', 'Леванова Т.А.', 9, NULL, 35),
(109, 'Леонтьев Николай Васильевич', 'Леонтьев Н.В.', 8, NULL, 15),
(110, 'Леонтьева Анна Викторовна', 'Леонтьева А.В.', 8, NULL, 15),
(111, 'Лерман Лев Михайлович', 'Лерман Л.М.', 2, NULL, 42),
(112, 'Литвинчук Светлана Юрьевна', 'Литвинчук С.Ю.', 8, NULL, 15),
(113, 'Лозин Вадим Владиславович', 'Лозин В.В.', 1, NULL, 4),
(114, 'Лукьянов Валерий Иванович', 'Лукьянов В.И.', 2, NULL, 15),
(115, 'Любимов Александр Константинович', 'Любимов А.К.', 8, NULL, 42),
(116, 'Любимцев Олег Владимирович', 'Любимцев О.В.', 1, NULL, 15),
(117, 'Ляхов Александр Федорович', 'Ляхов А.Ф.', 8, NULL, 15),
(118, 'Макаров Евгений Маратович', 'Макаров Е.М.', 1, NULL, 55),
(119, 'Малкин Михаил Иосифович', 'Малкин М.И.', 2, NULL, 15),
(120, 'Малкина Елена Владиславовна', 'Малкина Е.В.', 7, NULL, 15),
(121, 'Малышев Дмитрий Сергеевич', 'Малышев Д.С.', 1, NULL, 42),
(122, 'Маркина Марина Викторовна', 'Маркина М.В.', 8, NULL, 15),
(123, 'Махрова Елена Николаевна', 'Махрова Е.Н.', 2, NULL, 15),
(124, 'Мацукова Ирина Макаровна', 'Мацукова И.М.', 1, NULL, 63),
(125, 'Мееров Иосиф Борисович', 'Мееров И.Б.', 4, NULL, 15),
(126, 'Метрикин Владимир Семенович', 'Метрикин В.С.', 6, NULL, 15),
(127, 'Митрякова Татьяна Михайловна', 'Митрякова Т.М.', 6, NULL, 15),
(128, 'Мокеев Дмитрий Борисович', 'Мокеев Д.Б.', 1, NULL, 3),
(129, 'Мокеев Дмитрий Борисович', 'Мокеев Д.Б.', 1, NULL, 33),
(130, 'Молчанова Нина Семеновна', 'Молчанова Н.С.', 3, NULL, 63),
(131, 'Морозов Альберт Дмитриевич', 'Морозов А.Д.', 2, NULL, 42),
(132, 'Муляр Ольга Александровна', 'Муляр О.А.', 1, NULL, 3),
(133, 'Муняев Вячеслав Олегович', 'Муняев В.О.', 9, NULL, 63),
(134, 'Неймарк Елена Александровна', 'Неймарк Е.А.', 3, NULL, 15),
(135, 'Никитина Ирина Владимировна', 'Никитина И.В.', 2, NULL, 63),
(136, 'Никифорова Ирина Владимировна', 'Никифорова И.В.', 6, NULL, 55),
(137, 'Новиков Валерий Вячеславович', 'Новиков В.В.', 8, NULL, 42),
(138, 'Носова Светлана Александровна', 'Носова С.А.', 4, NULL, 3),
(139, 'Олюнина Ирина Игоревна', 'Олюнина И.И.', 2, NULL, 40),
(140, 'Осипов Григорий Владимирович', 'Осипов Г.В.', 9, NULL, 16),
(141, 'Пакшин Павел Владимирович', 'Пакшин П.В.', 7, NULL, 42),
(142, 'Панкратов Андрей Леонидович', 'Панкратов А.Л.', 6, NULL, 42),
(143, 'Панкратова Евгения Валерьевна', 'Панкратова Е.В.', 6, NULL, 15),
(144, 'Панов Владимир Александрович', 'Панов В.А.', 8, NULL, 42),
(145, 'Пиковский Аркадий', 'Пиковский А.', 9, NULL, 4),
(146, 'Пирова Анна Юрьевна', 'Пирова А.Ю.', 4, NULL, 3),
(147, 'Плехов Александр Сергеевич', 'Плехов А.С.', 3, NULL, 15),
(148, 'Полотовский Григорий Михайлович', 'Полотовский Г.М.', 1, NULL, 15),
(149, 'Потёмин Геннадий Владимирович', 'Потёмин Г.В.', 2, NULL, 15),
(150, 'Прилуцкий Михаил Хаимович', 'Прилуцкий М.Х.', 3, NULL, 16),
(151, 'Пройдакова Екатерина Вадимовна', 'Пройдакова Е.В.', 7, NULL, 15),
(152, 'Прокофьев Юрий Васильевич', 'Прокофьев Ю.В.', 8, NULL, 63),
(153, 'Прокофьева Вера Александровна', 'Прокофьева В.А.', 8, NULL, 63),
(154, 'Разуваев Алексей Григорьевич', 'Разуваев А.Г.', 1, NULL, 15),
(155, 'Рябова Елена Александровна', 'Рябова Е.А.', 2, NULL, 15),
(156, 'Сабаева Татьяна Анатольевна', 'Сабаева Т.А.', 8, NULL, 15),
(157, 'Савельев Владимир Петрович', 'Савельев В.П.', 9, NULL, 15),
(158, 'Савихин Олег Геннадьевич', 'Савихин О.Г.', 8, NULL, 15),
(159, 'Сандалов Владимир Михайлович', 'Сандалов В.М.', 8, NULL, 15),
(160, 'Сафонов Юрий Александрович', 'Сафонов Ю.А.', 8, NULL, 63),
(161, 'Сергеев Олег Анатольевич', 'Сергеев О.А.', 8, NULL, 15),
(162, 'Сергеев Ярослав Дмитриевич', 'Сергеев Я.Д.', 4, NULL, 42),
(163, 'Сиднев Алексей Александрович', 'Сиднев А.А.', 7, NULL, 3),
(164, 'Сидоров Сергей Владимирович', 'Сидоров С.В.', 1, NULL, 3),
(165, 'Сидоров Сергей Владимирович', 'Сидоров С.В.', 1, NULL, 35),
(166, 'Сизова Наталья Алексеевна', 'Сизова Н.А.', 2, NULL, 40),
(167, 'Скрябина Елена Борисовна', 'Скрябина Е.Б.', 6, NULL, 63),
(168, 'Смирнов Лев Александрович', 'Смирнов Л.А.', 9, NULL, 55),
(169, 'Смирнова Татьяна Геннадьевна', 'Смирнова Т.Г.', 1, NULL, 15),
(170, 'Сорокина Мария Сергеевна', 'Сорокина М.С.', 2, NULL, 63),
(171, 'Сорочан Сергей Владимирович', 'Сорочан С.В.', 1, NULL, 15),
(172, 'Старостин Николай Владимирович', 'Старостин Н.В.', 3, NULL, 42),
(173, 'Старостина Надежда Геральдовна', 'Старостина Н.Г.', 3, NULL, 63),
(174, 'Стребуляев Сергей Николаевич', 'Стребуляев С.Н.', 2, NULL, 15),
(175, 'Стронгина Наталья Романовна', 'Стронгина Н.Р.', 2, NULL, 15),
(176, 'Сысоев Александр Владимирович', 'Сысоев А.В.', 4, NULL, 15),
(177, 'Терентьев Алексей Михайлович', 'Терентьев А.М.', 2, NULL, 15),
(178, 'Тимофеев Алексей Евгеньевич', 'Тимофеев А.Е.', 3, NULL, 63),
(179, 'Титова Елена Борисовна', 'Титова Е.Б.', 1, NULL, 3),
(180, 'Тихов Михаил Семенович', 'Тихов М.С.', 7, NULL, 42),
(181, 'Трухин Борис Владимирович', 'Трухин Б.В.', 8, NULL, 63),
(182, 'Турлапов Вадим Евгеньевич', 'Турлапов В.Е.', 4, NULL, 42),
(183, 'Февральских Любовь Николаевна', 'Февральских Л.Н.', 8, NULL, 3),
(184, 'Федоткин Андрей Михайлович', 'Федоткин А.М.', 2, NULL, 15),
(185, 'Федоткин Михаил Андреевич', 'Федоткин М.А.', 7, NULL, 42),
(186, 'Федотова Оксана Борисовна', 'Федотова О.Б.', 7, NULL, 63),
(187, 'Федюков Александр Анатольевич', 'Федюков А.А.', 2, NULL, 3),
(188, 'Филимонов Андрей Викторович', 'Филимонов А.В.', 3, NULL, 15),
(189, 'Филиппов Андрей Рудольфович', 'Филиппов А.Р.', 8, NULL, 63),
(190, 'Филиппов Викторий Николаевич', 'Филиппов В.Н.', 2, NULL, 15),
(191, 'Фокина Валентина Николаевна', 'Фокина В.Н.', 2, NULL, 55),
(192, 'Фомина Ирина Александровна', 'Фомина И.А.', 3, NULL, 15),
(193, 'Чачхиани Татьяна Игоревна', 'Чачхиани Т.И.', 9, NULL, 15),
(194, 'Чекмарев Дмитрий Тимофеевич', 'Чекмарев Д.Т.', 8, NULL, 42),
(195, 'Чернов Андрей Владимирович', 'Чернов А.В.', 6, NULL, 15),
(196, 'Чернышова Наталья Николаевна', 'Чернышова Н.Н.', 3, NULL, 15),
(197, 'Чирков Александр Юрьевич', 'Чирков А.Ю.', 1, NULL, 15),
(198, 'Чубаров Георгий Владимирович', 'Чубаров Г.В.', 2, NULL, 3),
(199, 'Чурилов Юрий Анатольевич', 'Чурилов Ю.А.', 8, NULL, 15),
(200, 'Шагбазян Давид Варданович', 'Шагбазян Д.В.', 7, NULL, 3),
(201, 'Шапошников Дмитрий Евгеньевич', 'Шапошников Д.Е.', 7, NULL, 15),
(202, 'Швецов Владимир Иванович', 'Швецов В.И.', 4, NULL, 42),
(203, 'Шевченко Валерий Николаевич', 'Шевченко В.Н.', 1, NULL, 42),
(204, 'Шевчук Елена Алексеевна', 'Шевчук Е.А.', 1, NULL, 63),
(205, 'Шевчук Елена Алексеевна', 'Шевчук Е.А.', 1, NULL, 63),
(206, 'Шестакова Наталья Валерьевна', 'Шестакова Н.В.', 4, NULL, 3),
(207, 'Штанюк Антон Александрович', 'Штанюк А.А.', 7, NULL, 15),
(208, 'Эгамов Альберт Исмаилович', 'Эгамов А.И.', 2, NULL, 55),
(209, 'Юсипов Игорь Ильясович', 'Юсипов И.И.', 6, NULL, 3),
(210, 'Якимова Ирина Васильевна', 'Якимова И.В.', 6, NULL, 63),
(211, 'Яковлев Евгений Иванович', 'Яковлев Е.И.', 1, NULL, 42),
(212, 'Ярощук Марина Владимировна', 'Ярощук М.В.', 7, NULL, 15),
(213, 'Ястребова Ирина Юрьевна', 'Ястребова И.Ю.', 6, NULL, 15),
(214, 'Яшунин Дмитрий Александрович', 'Яшунин Д.А.', 3, NULL, 55);

-- 
-- Вывод данных для таблицы tabl_group
--
INSERT INTO tabl_group VALUES
(1, '381704м', 1, 2, 1),
(2, '381807м', 2, 1, 1),
(3, '381803м3', 3, 1, 1),
(4, '381705м', 4, 2, 1),
(5, '381706м1', 5, 2, 1),
(6, '381806м3', 6, 1, 1),
(7, '381703м3', 3, 2, 1),
(8, '381806м4', 7, 1, 1),
(9, '381706м2', 8, 2, 1),
(10, '381703м4', 9, 2, 1),
(11, '381805м', 4, 1, 1),
(12, '381703м1', 10, 2, 1),
(13, '381703м2', 11, 2, 1),
(14, '381803м1', 10, 1, 1),
(15, '381707м', 2, 2, 1),
(16, '381804м', 1, 1, 1),
(17, '381806м1', 5, 1, 1),
(18, '381803м2', 11, 1, 1),
(19, '381802м', 12, 1, 1),
(20, '381702м', 12, 2, 1),
(21, '381806м2', 8, 1, 1),
(22, '381803м4', 9, 1, 1),
(23, '381803м2-МО', 11, 1, 1),
(24, '381706м3', 5, 2, 1),
(25, '381806-ИА', 22, 1, 1),
(26, '381606-3', 5, 3, 1),
(27, '381807-3', 17, 1, 1),
(28, '381706-ИА', 18, 2, 1),
(29, '381808-2', 19, 1, 1),
(30, '381807-1', 2, 1, 1),
(31, '381607-1', 2, 3, 1),
(32, '381603-2', 21, 3, 1),
(33, '381803-2', 22, 1, 1),
(34, '381702', 23, 2, 1),
(35, '381507-1', 2, 4, 1),
(36, '381705', 22, 2, 1),
(37, '381608-1', 19, 3, 1),
(38, '381506-ИА', 22, 4, 1),
(39, '381606-ИА', 22, 3, 1),
(40, '381602', 23, 3, 1),
(41, '381707-3', 17, 2, 1),
(42, '381708-3', 19, 2, 1),
(43, '381806-2', 5, 1, 1),
(44, '381803-3', 22, 1, 1),
(45, '381804', 25, 1, 1),
(46, '381806-1', 5, 1, 1),
(47, '381603-3', 21, 3, 1),
(48, '381706-4', 5, 2, 1),
(49, '381708-1', 19, 2, 1),
(50, '381703-2', 21, 2, 1),
(51, '381507-3', 17, 4, 1),
(52, '381808-1', 19, 1, 1),
(53, '381706-2', 5, 2, 1),
(54, '381607и', 17, 3, 1),
(55, '381506-2', 5, 4, 1),
(56, '381606-2', 5, 3, 1),
(57, '381807-2', 2, 1, 1),
(58, '381703-1', 21, 2, 1),
(59, '381703-3', 21, 2, 1),
(60, '381802', 23, 1, 1),
(61, '381806-4', 5, 1, 1),
(62, '381603-1', 21, 3, 1),
(63, '381505', 26, 4, 1),
(64, '381607-2', 17, 3, 1),
(65, '381803-4', 22, 1, 1),
(66, '381708-2', 19, 2, 1),
(67, '381508', 19, 4, 1),
(68, '381808-3', 19, 1, 1),
(69, '381707-2', 2, 2, 1),
(70, '381706-3', 5, 2, 1),
(71, '381503-3', 22, 4, 1),
(72, '381603-5', 27, 3, 1),
(73, '381503-2', 22, 4, 1),
(74, '381604', 25, 3, 1),
(75, '381606-1', 5, 3, 1),
(76, '381506-3', 5, 4, 1),
(77, '381704', 25, 2, 1),
(78, '381504', 25, 4, 1),
(79, '381608-2', 19, 3, 1),
(80, '381706-1', 5, 2, 1),
(81, '381805', 22, 1, 1),
(82, '381502', 23, 4, 1),
(83, '381806-3', 5, 1, 1),
(84, '381503-4', 27, 4, 1),
(85, '381605', 22, 3, 1),
(86, '381803-1', 22, 1, 1),
(87, '381506и', 5, 4, 1),
(88, '381503-1', 22, 4, 1),
(89, '381507-2', 2, 4, 1),
(90, '381703-4', 21, 2, 1),
(91, '381506-1', 5, 4, 1),
(92, '381707-1', 2, 2, 1),
(93, '381603-2МО', 21, 3, 1),
(94, '381706-ИА МО', 22, 2, 1),
(95, '381603-4', 21, 3, 1);

-- 
-- Вывод данных для таблицы tabl_practice_type
--
INSERT INTO tabl_practice_type VALUES
(1, 'Учебная', 'Учебной', 'УЧЕБНУЮ', 'ознакомительная', 'ознакомительная'),
(2, 'Производственная', 'Производственной', 'ПРОИЗВОДСТВЕННУЮ', 'Научно-исследовательская работа', 'НИР'),
(3, 'Учебная', 'Учебной', 'УЧЕБНУЮ', 'ППППУиН', 'ППППУиН');

-- 
-- Вывод данных для таблицы tabl_student
--
INSERT INTO tabl_student VALUES
(1, 'Акуленко Анастасия Сергеевна', 'Акуленко А.С.', 1, NULL, NULL, 1),
(2, 'Алабин Антон Андреевич', 'Алабин А.А.', 2, NULL, NULL, 1),
(3, 'Алексеев Сергей Васильевич', 'Алексеев С.В.', 3, NULL, NULL, 1),
(4, 'Алешин Сергей Александрович', 'Алешин С.А.', 4, NULL, NULL, 1),
(5, 'Аль Жарад Ваддах', 'Аль Ж.В.', 4, NULL, NULL, 1),
(6, 'Андреев Владислав Станиславович', 'Андреев В.С.', 5, NULL, NULL, 1),
(7, 'Аникина Ольга Михайловна', 'Аникина О.М.', 5, NULL, NULL, 1),
(8, 'Антипин Никита Андреевич', 'Антипин Н.А.', 6, NULL, NULL, 1),
(9, 'Анфимов Владислав Юрьевич', 'Анфимов В.Ю.', 5, NULL, NULL, 1),
(10, 'Ахмеджанов Дмитрий Ринатович', 'Ахмеджанов Д.Р.', 7, NULL, NULL, 1),
(11, 'Ахмедов Саркар Саркарович', 'Ахмедов С.С.', 8, NULL, NULL, 1),
(12, 'Бабаев Иван Владимирович', 'Бабаев И.В.', 9, NULL, NULL, 1),
(13, 'Бабушкина Валерия Евгеньевна', 'Бабушкина В.Е.', 10, NULL, NULL, 1),
(14, 'Бадейнов Артем Вадимович', 'Бадейнов А.В.', 11, NULL, NULL, 1),
(15, 'Баклай Кирилл Сергеевич', 'Баклай К.С.', 8, NULL, NULL, 1),
(16, 'Баландина Софья Валерьевна', 'Баландина С.В.', 2, NULL, NULL, 1),
(17, 'Баранов Евгений Васильевич', 'Баранов Е.В.', 3, NULL, NULL, 1),
(18, 'Баханова Юлия Викторовна', 'Баханова Ю.В.', 12, NULL, NULL, 1),
(19, 'Бебнев Виктор Владимирович', 'Бебнев В.В.', 9, NULL, NULL, 1),
(20, 'Бевзюк Семен Антонович', 'Бевзюк С.А.', 3, NULL, NULL, 1),
(21, 'Белов Игорь Юрьевич', 'Белов И.Ю.', 1, NULL, NULL, 1),
(22, 'Бельдюгина Наталья Владимировна', 'Бельдюгина Н.В.', 13, NULL, NULL, 1),
(23, 'Береснева Юлия Васильевна', 'Береснева Ю.В.', 7, NULL, NULL, 1),
(24, 'Богородская Валерия Сергеевна', 'Богородская В.С.', 11, NULL, NULL, 1),
(25, 'Большаков Богдан Александрович', 'Большаков Б.А.', 1, NULL, NULL, 1),
(26, 'Бояринов Константин Сергеевич', 'Бояринов К.С.', 13, NULL, NULL, 1),
(27, 'Бубнова Елена Сергеевна', 'Бубнова Е.С.', 14, NULL, NULL, 1),
(28, 'Буренина Дарья Дмитриевна', 'Буренина Д.Д.', 14, NULL, NULL, 1),
(29, 'Бусаров Дмитрий Андреевич', 'Бусаров Д.А.', 13, NULL, NULL, 1),
(30, 'Буяков Дмитрий Владимирович', 'Буяков Д.В.', 15, NULL, NULL, 1),
(31, 'Веретельников Сергей Александрович', 'Веретельников С.А.', 7, NULL, NULL, 1),
(32, 'Виноградов Николай Алексеевич', 'Виноградов Н.А.', 16, NULL, NULL, 1),
(33, 'Владимирова Елена Яковлевна', 'Владимирова Е.Я.', 11, NULL, NULL, 1),
(34, 'Власов Антон Сергеевич', 'Власов А.С.', 17, NULL, NULL, 1),
(35, 'Власов Дмитрий Дмитриевич', 'Власов Д.Д.', 13, NULL, NULL, 1),
(36, 'Воеводин Андрей Михайлович', 'Воеводин А.М.', 9, NULL, NULL, 1),
(37, 'Воробьев Никита Сергеевич', 'Воробьев Н.С.', 16, NULL, NULL, 1),
(38, 'Гаврилов Георгий Константинович', 'Гаврилов Г.К.', 6, NULL, NULL, 1),
(39, 'Гладков Дмитрий Олегович', 'Гладков Д.О.', 5, NULL, NULL, 1),
(40, 'Гладков Сергей Юрьевич', 'Гладков С.Ю.', 18, NULL, NULL, 1),
(41, 'Гладкова Татьяна Алексеевна', 'Гладкова Т.А.', 7, NULL, NULL, 1),
(42, 'Годовицын Максим Михайлович', 'Годовицын М.М.', 2, NULL, NULL, 1),
(43, 'Голованова Татьяна Александровна', 'Голованова Т.А.', 5, NULL, NULL, 1),
(44, 'Голубева Анастасия Сергеевна', 'Голубева А.С.', 14, NULL, NULL, 1),
(45, 'Голякова Евгения Валерьевна', 'Голякова Е.В.', 13, NULL, NULL, 1),
(46, 'Голякова Елена Александровна', 'Голякова Е.А.', 9, NULL, NULL, 1),
(47, 'Горская Виктория Александровна', 'Горская В.А.', 19, NULL, NULL, 1),
(48, 'Грачева Мария Андреевна', 'Грачева М.А.', 4, NULL, NULL, 1),
(49, 'Гречко Дина Алексеевна', 'Гречко Д.А.', 12, NULL, NULL, 1),
(50, 'Гречухин Никита Сергеевич', 'Гречухин Н.С.', 17, NULL, NULL, 1),
(51, 'Гришин Дмитрий Алексеевич', 'Гришин Д.А.', 17, NULL, NULL, 1),
(52, 'Губарев Сергей Юрьевич', 'Губарев С.Ю.', 15, NULL, NULL, 1),
(53, 'Гуськов Роман Андреевич', 'Гуськов Р.А.', 14, NULL, NULL, 1),
(54, 'Даллул Мариана', 'Даллул М.', 20, NULL, NULL, 1),
(55, 'Данилин Василий Витальевич', 'Данилин В.В.', 8, NULL, NULL, 1),
(56, 'Денисов Евгений Владимирович', 'Денисов Е.В.', 11, NULL, NULL, 1),
(57, 'Деркач Валерия Алексеевна', 'Деркач В.А.', 19, NULL, NULL, 1),
(58, 'Доронин Максим Алексеевич', 'Доронин М.А.', 17, NULL, NULL, 1),
(59, 'Доронин Роман Олегович', 'Доронин Р.О.', 21, NULL, NULL, 1),
(60, 'Дубасова Анастасия Валерьевна', 'Дубасова А.В.', 4, NULL, NULL, 1),
(61, 'Егорова Светлана Сергеевна', 'Егорова С.С.', 8, NULL, NULL, 1),
(62, 'Елизаров Александр Юрьевич', 'Елизаров А.Ю.', 10, NULL, NULL, 1),
(63, 'Жариков Алексей Дмитриевич', 'Жариков А.Д.', 7, NULL, NULL, 1),
(64, 'Живчикова Юлия Алексеевна', 'Живчикова Ю.А.', 2, NULL, NULL, 1),
(65, 'Жильцов Максим Сергеевич', 'Жильцов М.С.', 5, NULL, NULL, 1),
(66, 'Жорин Сергей Михайлович', 'Жорин С.М.', 14, NULL, NULL, 1),
(67, 'Жуков Владислав Павлович', 'Жуков В.П.', 13, NULL, NULL, 1),
(68, 'Забегалова Наталия Александровна', 'Забегалова Н.А.', 19, NULL, NULL, 1),
(69, 'Заводчиков Николай Александрович', 'Заводчиков Н.А.', 16, NULL, NULL, 1),
(70, 'Земцов Артем Дмитриевич', 'Земцов А.Д.', 22, NULL, NULL, 1),
(71, 'Зубарева Екатерина Сергеевна', 'Зубарева Е.С.', 9, NULL, NULL, 1),
(72, 'Зырина Анна Евгеньевна', 'Зырина А.Е.', 17, NULL, NULL, 1),
(73, 'Иванов Артём Сергеевич', 'Иванов А.С.', 4, NULL, NULL, 1),
(74, 'Иванова Ольга Алексеевна', 'Иванова О.А.', 5, NULL, NULL, 1),
(75, 'Иванова Юлия Анатольевна', 'Иванова Ю.А.', 4, NULL, NULL, 1),
(76, 'Иванович Марко', 'Иванович М.', 21, NULL, NULL, 1),
(77, 'Исакова Екатерина Евгеньевна', 'Исакова Е.Е.', 12, NULL, NULL, 1),
(78, 'Казарова Екатерина Игоревна', 'Казарова Е.И.', 13, NULL, NULL, 1),
(79, 'Калинин Михаил Сергеевич', 'Калинин М.С.', 18, NULL, NULL, 1),
(80, 'Калинина Евгения Александровна', 'Калинина Е.А.', 20, NULL, NULL, 1),
(81, 'Кальянов Дмитрий Олегович', 'Кальянов Д.О.', 17, NULL, NULL, 1),
(82, 'Карев Борис Юрьевич', 'Карев Б.Ю.', 17, NULL, NULL, 1),
(83, 'Карпов Александр Андреевич', 'Карпов А.А.', 12, NULL, NULL, 1),
(84, 'Касаткина Юлия Александровна', 'Касаткина Ю.А.', 20, NULL, NULL, 1),
(85, 'Кашина Екатерина Анатольевна', 'Кашина Е.А.', 1, NULL, NULL, 1),
(86, 'Кильдишев Максим Геннадьевич', 'Кильдишев М.Г.', 8, NULL, NULL, 1),
(87, 'Киселева Екатерина Вячеславовна', 'Киселева Е.В.', 11, NULL, NULL, 1),
(88, 'Киселева Ольга Вячеславовна', 'Киселева О.В.', 8, NULL, NULL, 1),
(89, 'Ковалева Ирина Александровна', 'Ковалева И.А.', 2, NULL, NULL, 1),
(90, 'Козырьков Владислав Евгеньевич', 'Козырьков В.Е.', 19, NULL, NULL, 1),
(91, 'Комаров Николай Александрович', 'Комаров Н.А.', 21, NULL, NULL, 1),
(92, 'Кондратьев Владислав Витальевич', 'Кондратьев В.В.', 13, NULL, NULL, 1),
(93, 'Конева Александра Дмитриевна', 'Конева А.Д.', 3, NULL, NULL, 1),
(94, 'Коневский Виталий Олегович', 'Коневский В.О.', 17, NULL, NULL, 1),
(95, 'Королёва Екатерина Евгеньевна', 'Королёва Е.Е.', 20, NULL, NULL, 1),
(96, 'Корсаков Максим Игоревич', 'Корсаков М.И.', 1, NULL, NULL, 1),
(97, 'Костерина Татьяна Александровна', 'Костерина Т.А.', 16, NULL, NULL, 1),
(98, 'Котова Ольга Александровна', 'Котова О.А.', 22, NULL, NULL, 1),
(99, 'Кочетова Дарья Александровна', 'Кочетова Д.А.', 6, NULL, NULL, 1),
(100, 'Кочуев Андрей Игоревич', 'Кочуев А.И.', 5, NULL, NULL, 1),
(101, 'Крапивин Александр Игоревич', 'Крапивин А.И.', 22, NULL, NULL, 1),
(102, 'Красильников Михаил Васильевич', 'Красильников М.В.', 8, NULL, NULL, 1),
(103, 'Краснов Александр Александрович', 'Краснов А.А.', 8, NULL, NULL, 1),
(104, 'Красный Владимир Сергеевич', 'Красный В.С.', 13, NULL, NULL, 1),
(105, 'Крутобережская Ирина Сергеевна', 'Крутобережская И.С.', 7, NULL, NULL, 1),
(106, 'Крюкова Полина Алексеевна', 'Крюкова П.А.', 7, NULL, NULL, 1),
(107, 'Кряжева Анастасия Александровна', 'Кряжева А.А.', 14, NULL, NULL, 1),
(108, 'Кузенков Николай Евгеньевич', 'Кузенков Н.Е.', 12, NULL, NULL, 1),
(109, 'Кукушкина Дарья Михайловна', 'Кукушкина Д.М.', 15, NULL, NULL, 1),
(110, 'Куликов Александр Павлович', 'Куликов А.П.', 3, NULL, NULL, 1),
(111, 'Куликова Анна Андреевна', 'Куликова А.А.', 14, NULL, NULL, 1),
(112, 'Кулиш Семён Андреевич', 'Кулиш С.А.', 22, NULL, NULL, 1),
(113, 'Кулыгин Виктор Вячеславович', 'Кулыгин В.В.', 15, NULL, NULL, 1),
(114, 'Кулыгина Юлия Андреевна', 'Кулыгина Ю.А.', 3, NULL, NULL, 1),
(115, 'Купцова Алеся Алексеевна', 'Купцова А.А.', 3, NULL, NULL, 1),
(116, 'Курин Максим Евгеньевич', 'Курин М.Е.', 9, NULL, NULL, 1),
(117, 'Курсаков Евгений Сергеевич', 'Курсаков Е.С.', 5, NULL, NULL, 1),
(118, 'Кучков Иван Дмитриевич', 'Кучков И.Д.', 8, NULL, NULL, 1),
(119, 'Ладейнов Дмитрий Александрович', 'Ладейнов Д.А.', 11, NULL, NULL, 1),
(120, 'Лапин Артем Андреевич', 'Лапин А.А.', 17, NULL, NULL, 1),
(121, 'Лахтачев Ярослав Юрьевич', 'Лахтачев Я.Ю.', 17, NULL, NULL, 1),
(122, 'Лепилова Алёна Александровна', 'Лепилова А.А.', 12, NULL, NULL, 1),
(123, 'Лицов Алексей Дмитриевич', 'Лицов А.Д.', 22, NULL, NULL, 1),
(124, 'Лобанкина Ксения Валерьевна', 'Лобанкина К.В.', 2, NULL, NULL, 1),
(125, 'Львова Дарья Александровна', 'Львова Д.А.', 10, NULL, NULL, 1),
(126, 'Магазинник Иван Анатольевич', 'Магазинник И.А.', 5, NULL, NULL, 1),
(127, 'Макарин Михаил Сергеевич', 'Макарин М.С.', 15, NULL, NULL, 1),
(128, 'Макаров Антон Сергеевич', 'Макаров А.С.', 15, NULL, NULL, 1),
(129, 'Малиновская Анастасия Сергеевна', 'Малиновская А.С.', 2, NULL, NULL, 1),
(130, 'Малова Анастасия Алексеевна', 'Малова А.А.', 22, NULL, NULL, 1),
(131, 'Малышева Ирина Вадимовна', 'Малышева И.В.', 12, NULL, NULL, 1),
(132, 'Маркова Валерия Викторовна', 'Маркова В.В.', 21, NULL, NULL, 1),
(133, 'Марушкин Николай Алексеевич', 'Марушкин Н.А.', 9, NULL, NULL, 1),
(134, 'Марченко Андрей Александрович', 'Марченко А.А.', 5, NULL, NULL, 1),
(135, 'Мастюгина Юлия Юрьевна', 'Мастюгина Ю.Ю.', 12, NULL, NULL, 1),
(136, 'Машаев Артём Вячеславович', 'Машаев А.В.', 7, NULL, NULL, 1),
(137, 'Медведев Егор Сергеевич', 'Медведев Е.С.', 19, NULL, NULL, 1),
(138, 'Метелев Евгений Борисович', 'Метелев Е.Б.', 5, NULL, NULL, 1),
(139, 'Милешин Иван Геннадьевич', 'Милешин И.Г.', 20, NULL, NULL, 1),
(140, 'Милешина Валерия Эдуардовна', 'Милешина В.Э.', 20, NULL, NULL, 1),
(141, 'Миронова Анна Михайловна', 'Миронова А.М.', 3, NULL, NULL, 1),
(142, 'Митрохин Никита Павлович', 'Митрохин Н.П.', 6, NULL, NULL, 1),
(143, 'Митрохина Юлия Сергеевна', 'Митрохина Ю.С.', 7, NULL, NULL, 1),
(144, 'Михайлова Светлана Сергеевна', 'Михайлова С.С.', 21, NULL, NULL, 1),
(145, 'Михалкина Елена Евгеньевна', 'Михалкина Е.Е.', 21, NULL, NULL, 1),
(146, 'Моисеев Никита Романович', 'Моисеев Н.Р.', 5, NULL, NULL, 1),
(147, 'Морозов Сергей Дмитриевич', 'Морозов С.Д.', 19, NULL, NULL, 1),
(148, 'Москаленко Виктор Алексеевич', 'Москаленко В.А.', 5, NULL, NULL, 1),
(149, 'Москова Екатерина Сергеевна', 'Москова Е.С.', 13, NULL, NULL, 1),
(150, 'Мурашов Михаил Валерьевич', 'Мурашов М.В.', 22, NULL, NULL, 1),
(151, 'Нахимович Дмитрий Михайлович', 'Нахимович Д.М.', 5, NULL, NULL, 1),
(152, 'Нестеров Александр Юрьевич', 'Нестеров А.Ю.', 3, NULL, NULL, 1),
(153, 'Нефедов Александр Дмитриевич', 'Нефедов А.Д.', 18, NULL, NULL, 1),
(154, 'Нечесанов Владимир Александрович', 'Нечесанов В.А.', 3, NULL, NULL, 1),
(155, 'Новожилова Анастасия Александровна', 'Новожилова А.А.', 22, NULL, NULL, 1),
(156, 'Овчарук Олег Русланович', 'Овчарук О.Р.', 8, NULL, NULL, 1),
(157, 'Оленева Мария Михайловна', 'Оленева М.М.', 5, NULL, NULL, 1),
(158, 'Ориничев Роман Юрьевич', 'Ориничев Р.Ю.', 23, NULL, NULL, 1),
(159, 'Осечкин Сергей Валерьевич', 'Осечкин С.В.', 5, NULL, NULL, 1),
(160, 'Охатрина Дарья Дмитриевна', 'Охатрина Д.Д.', 4, NULL, NULL, 1),
(161, 'Охотин Кирилл Александрович', 'Охотин К.А.', 16, NULL, NULL, 1),
(162, 'Павелкин Федор Иванович', 'Павелкин Ф.И.', 13, NULL, NULL, 1),
(163, 'Паненкова Дарья Алексеевна', 'Паненкова Д.А.', 22, NULL, NULL, 1),
(164, 'Парамонова Ольга Владиславовна', 'Парамонова О.В.', 14, NULL, NULL, 1),
(165, 'Петров Кирилл Петрович', 'Петров К.П.', 3, NULL, NULL, 1),
(166, 'Петрова Мария Сергеевна', 'Петрова М.С.', 14, NULL, NULL, 1),
(167, 'Пигалова Александра Александровна', 'Пигалова А.А.', 10, NULL, NULL, 1),
(168, 'Пипикин Олег Игоревич', 'Пипикин О.И.', 24, NULL, NULL, 1),
(169, 'Плетнев Никита Алексеевич', 'Плетнев Н.А.', 21, NULL, NULL, 1),
(170, 'Повеликин Ростислав Валерьевич', 'Повеликин Р.В.', 3, NULL, NULL, 1),
(171, 'Подолов Кирилл Константинович', 'Подолов К.К.', 24, NULL, NULL, 1),
(172, 'Подчищаева Мария Викторовна', 'Подчищаева М.В.', 7, NULL, NULL, 1),
(173, 'Поздяев Валерий Игоревич', 'Поздяев В.И.', 24, NULL, NULL, 1),
(174, 'Поляков Игорь Олегович', 'Поляков И.О.', 24, NULL, NULL, 1),
(175, 'Поляков Максим Олегович', 'Поляков М.О.', 2, NULL, NULL, 1),
(176, 'Попков Кирилл Сергеевич', 'Попков К.С.', 17, NULL, NULL, 1),
(177, 'Привалов Даниил Юрьевич', 'Привалов Д.Ю.', 9, NULL, NULL, 1),
(178, 'Провидохин Николай Андреевич', 'Провидохин Н.А.', 7, NULL, NULL, 1),
(179, 'Пронин Сергей Николаевич', 'Пронин С.Н.', 13, NULL, NULL, 1),
(180, 'Пронина Мария Валерьевна', 'Пронина М.В.', 24, NULL, NULL, 1),
(181, 'Прохоров Александр Александрович', 'Прохоров А.А.', 2, NULL, NULL, 1),
(182, 'Пухкий Константин Константинович', 'Пухкий К.К.', 9, NULL, NULL, 1),
(183, 'Разин Вячеслав Васильевич', 'Разин В.В.', 8, NULL, NULL, 1),
(184, 'Ревин Владислав Сергеевич', 'Ревин В.С.', 12, NULL, NULL, 1),
(185, 'Рейн Максим Федорович', 'Рейн М.Ф.', 12, NULL, NULL, 1),
(186, 'Рейнага Тюрин Михаэль Рэнович', 'Рейнага Т.М.Р.', 20, NULL, NULL, 1),
(187, 'Реунова Ольга Алексеевна', 'Реунова О.А.', 9, NULL, NULL, 1),
(188, 'Рожнова Майя Александровна', 'Рожнова М.А.', 14, NULL, NULL, 1),
(189, 'Романенко Александр Александрович', 'Романенко А.А.', 9, NULL, NULL, 1),
(190, 'Рукин Дмитрий Олегович', 'Рукин Д.О.', 14, NULL, NULL, 1),
(191, 'Румянцев Александр Валерьевич', 'Румянцев А.В.', 24, NULL, NULL, 1),
(192, 'Румянцева Мария Алексеевна', 'Румянцева М.А.', 24, NULL, NULL, 1),
(193, 'Руссу Арина Сергеевна', 'Руссу А.С.', 16, NULL, NULL, 1),
(194, 'Рыбаков Антон Александрович', 'Рыбаков А.А.', 9, NULL, NULL, 1),
(195, 'Рыбаков Евгений Леонидович', 'Рыбаков Е.Л.', 1, NULL, NULL, 1),
(196, 'Рябинин Кирилл Александрович', 'Рябинин К.А.', 17, NULL, NULL, 1),
(197, 'Рябов Владислав Игоревич', 'Рябов В.И.', 7, NULL, NULL, 1),
(198, 'Савкова Елизавета Владимировна', 'Савкова Е.В.', 22, NULL, NULL, 1),
(199, 'Сажин Максим Сергеевич', 'Сажин М.С.', 10, NULL, NULL, 1),
(200, 'Самылина Евгения Александровна', 'Самылина Е.А.', 12, NULL, NULL, 1),
(201, 'Санин Сергей Алексеевич', 'Санин С.А.', 15, NULL, NULL, 1),
(202, 'Сафонов Игорь Михайлович', 'Сафонов И.М.', 2, NULL, NULL, 1),
(203, 'Сафонов Клим Андреевич', 'Сафонов К.А.', 20, NULL, NULL, 1),
(204, 'Свирежев Владислав Александрович', 'Свирежев В.А.', 22, NULL, NULL, 1),
(205, 'Сидоров Никита Владимирович', 'Сидоров Н.В.', 24, NULL, NULL, 1),
(206, 'Сидорычев Александр Сергеевич', 'Сидорычев А.С.', 4, NULL, NULL, 1),
(207, 'Сизова Ксения Олеговна', 'Сизова К.О.', 6, NULL, NULL, 1),
(208, 'Симонян Вагинак Араикович', 'Симонян В.А.', 17, NULL, NULL, 1),
(209, 'Синицкая Олеся Вячеславовна', 'Синицкая О.В.', 22, NULL, NULL, 1),
(210, 'Смирнов Андрей Владимирович', 'Смирнов А.В.', 17, NULL, NULL, 1),
(211, 'Смирнов Егор Олегович', 'Смирнов Е.О.', 3, NULL, NULL, 1),
(212, 'Смирнов Иван Васильевич', 'Смирнов И.В.', 11, NULL, NULL, 1),
(213, 'Смирнова Анна Кирилловна', 'Смирнова А.К.', 14, NULL, NULL, 1),
(214, 'Смирнова Дарья Владимировна', 'Смирнова Д.В.', 2, NULL, NULL, 1),
(215, 'Смирнова Екатерина Андреевна', 'Смирнова Е.А.', 16, NULL, NULL, 1),
(216, 'Сорокина Мария Сергеевна', 'Сорокина М.С.', 14, NULL, NULL, 1),
(217, 'Софонова Алина Олеговна', 'Софонова А.О.', 14, NULL, NULL, 1),
(218, 'Степанов Артем Алексеевич', 'Степанов А.А.', 16, NULL, NULL, 1),
(219, 'Степанов Николай Алексеевич', 'Степанов Н.А.', 24, NULL, NULL, 1),
(220, 'Суслова Мария Евгеньевна', 'Суслова М.Е.', 13, NULL, NULL, 1),
(221, 'Талецкий Дмитрий Сергеевич', 'Талецкий Д.С.', 10, NULL, NULL, 1),
(222, 'Тамилина Дарья Валерьевна', 'Тамилина Д.В.', 6, NULL, NULL, 1),
(223, 'Тананаева Татьяна Андреевна', 'Тананаева Т.А.', 13, NULL, NULL, 1),
(224, 'Таранов Николай Сергеевич', 'Таранов Н.С.', 11, NULL, NULL, 1),
(225, 'Терехов Илья Дмитриевич', 'Терехов И.Д.', 2, NULL, NULL, 1),
(226, 'Тишкин Константин Андреевич', 'Тишкин К.А.', 24, NULL, NULL, 1),
(227, 'Толич Александр Олегович', 'Толич А.О.', 2, NULL, NULL, 1),
(228, 'Трифонов Константин Николаевич', 'Трифонов К.Н.', 20, NULL, NULL, 1),
(229, 'Трошина Екатерина Александровна', 'Трошина Е.А.', 15, NULL, NULL, 1),
(230, 'Тюнтяева Лилия Александровна', 'Тюнтяева Л.А.', 15, NULL, NULL, 1),
(231, 'Тюрин Дмитрий Евгеньевич', 'Тюрин Д.Е.', 2, NULL, NULL, 1),
(232, 'Тягунов Владимир Андреевич', 'Тягунов В.А.', 17, NULL, NULL, 1),
(233, 'Фадеев Алексей Андреевич', 'Фадеев А.А.', 9, NULL, NULL, 1),
(234, 'Федорова Дарья Алексеевна', 'Федорова Д.А.', 17, NULL, NULL, 1),
(235, 'Федотов Андрей Сергеевич', 'Федотов А.С.', 17, NULL, NULL, 1),
(236, 'Филатова Анастасия Сергеевна', 'Филатова А.С.', 2, NULL, NULL, 1),
(237, 'Флегонтов Александр Игоревич', 'Флегонтов А.И.', 24, NULL, NULL, 1),
(238, 'Фытов Александр Михайлович', 'Фытов А.М.', 24, NULL, NULL, 1),
(239, 'Хапаев Илья Николаевич', 'Хапаев И.Н.', 15, NULL, NULL, 1),
(240, 'Хачатрян Корюн Агаронович', 'Хачатрян К.А.', 21, NULL, NULL, 1),
(241, 'Хизбуллин Ренат Ахмирович', 'Хизбуллин Р.А.', 9, NULL, NULL, 1),
(242, 'Храмов Илья Валерьевич', 'Храмов И.В.', 9, NULL, NULL, 1),
(243, 'Хуссиен Бахадин Мухаммад Хуссиен', 'Хуссиен Б.М.Х.', 19, NULL, NULL, 1),
(244, 'Чекодаев Олег Андреевич', 'Чекодаев О.А.', 24, NULL, NULL, 1),
(245, 'Чередниченко Андрей Петрович', 'Чередниченко А.П.', 4, NULL, NULL, 1),
(246, 'Четвериков Антон Андреевич', 'Четвериков А.А.', 10, NULL, NULL, 1),
(247, 'Чнегов Егор Игоревич', 'Чнегов Е.И.', 13, NULL, NULL, 1),
(248, 'Чугунова Екатерина Евгеньевна', 'Чугунова Е.Е.', 18, NULL, NULL, 1),
(249, 'Чумаков Максим Валерьевич', 'Чумаков М.В.', 22, NULL, NULL, 1),
(250, 'Чуркин Андрей Валерьевич', 'Чуркин А.В.', 22, NULL, NULL, 1),
(251, 'Чухманов Илья Геннадьевич', 'Чухманов И.Г.', 1, NULL, NULL, 1),
(252, 'Шабетник Кристина Алексеевна', 'Шабетник К.А.', 1, NULL, NULL, 1),
(253, 'Шадрунов Артём Дмитриевич', 'Шадрунов А.Д.', 24, NULL, NULL, 1),
(254, 'Шапаева Диана Олеговна', 'Шапаева Д.О.', 4, NULL, NULL, 1),
(255, 'Шаталина Ксения Сергеевна', 'Шаталина К.С.', 15, NULL, NULL, 1),
(256, 'Шевчук Андрей Павлович', 'Шевчук А.П.', 22, NULL, NULL, 1),
(257, 'Шептунов Владислав Олегович', 'Шептунов В.О.', 24, NULL, NULL, 1),
(258, 'Шестаков Александр Владиславович', 'Шестаков А.В.', 12, NULL, NULL, 1),
(259, 'Широких Виолетта Дмитриевна', 'Широких В.Д.', 17, NULL, NULL, 1),
(260, 'Ширяева Екатерина Александровна', 'Ширяева Е.А.', 24, NULL, NULL, 1),
(261, 'Шульгинова Ирина Александровна', 'Шульгинова И.А.', 15, NULL, NULL, 1),
(262, 'Шульпин Степан Михайлович', 'Шульпин С.М.', 8, NULL, NULL, 1),
(263, 'Шумилов Иван Андреевич', 'Шумилов И.А.', 15, NULL, NULL, 1),
(264, 'Шумихин Сергей Николаевич', 'Шумихин С.Н.', 3, NULL, NULL, 1),
(265, 'Щербина Богуслав Владимирович', 'Щербина Б.В.', 16, NULL, NULL, 1),
(266, 'Юферев Кирилл Владимирович', 'Юферев К.В.', 6, NULL, NULL, 1),
(267, 'Юфин Илья Михайлович', 'Юфин И.М.', 7, NULL, NULL, 1),
(268, 'Ягудина Анастасия Ренатовна', 'Ягудина А.Р.', 22, NULL, NULL, 1),
(269, 'Абабио Исаак', 'Абабио И.', 25, NULL, NULL, 1),
(270, 'Абазов Руслан Габильевич', 'Абазов Р.Г.', 26, NULL, NULL, 1),
(271, 'Абалихин Никита Сергеевич', 'Абалихин Н.С.', 27, NULL, NULL, 1),
(272, 'Абесингхе Джаявардена Надика Лахиру', 'Абесингхе Д.Н.Л.', 28, NULL, NULL, 1),
(273, 'Авдеев Андрей Вадимович', 'Авдеев А.В.', 29, 7, 76, 6),
(274, 'Авдошин Данила Александрович', 'Авдошин Д.А.', 30, NULL, NULL, 1),
(275, 'Аверьянов Семен Игоревич', 'Аверьянов С.И.', 31, NULL, NULL, 1),
(276, 'Агапов Дмитрий Александрович', 'Агапов Д.А.', 32, NULL, NULL, 1),
(277, 'Агапов Олег Дмитриевич', 'Агапов О.Д.', 33, NULL, NULL, 1),
(278, 'Агапова Екатерина Ивановна', 'Агапова Е.И.', 34, NULL, NULL, 1),
(279, 'Агеева Анна Андреевна', 'Агеева А.А.', 35, NULL, NULL, 1),
(280, 'Агеева Ольга Павловна', 'Агеева О.П.', 36, NULL, NULL, 1),
(281, 'Агликов Илья Андреевич', 'Агликов И.А.', 37, NULL, NULL, 1),
(282, 'Азиз Локман', 'Азиз Л.', 38, NULL, NULL, 1),
(283, 'Азим Мд Аль-Амин', 'Азим М.А.', 39, NULL, NULL, 1),
(284, 'Аймалова Ульяна Михайловна', 'Аймалова У.М.', 40, NULL, NULL, 1),
(285, 'Айнетдинов Антон Рушанович', 'Айнетдинов А.Р.', 41, NULL, NULL, 1),
(286, 'Аксенов Никита Алексеевич', 'Аксенов Н.А.', 42, NULL, NULL, 1),
(287, 'Акуленок Артем Леонидович', 'Акуленок А.Л.', 27, NULL, NULL, 1),
(288, 'Ал Агеле Али Мохаммед Аббас', 'Ал А.А.М.А.', 25, NULL, NULL, 1),
(289, 'Александрычев Андрей Сергеевич', 'Александрычев А.С.', 43, NULL, NULL, 1),
(290, 'Алексеева Валерия Алексеевна', 'Алексеева В.А.', 44, NULL, NULL, 1),
(291, 'Алехин Денис Андреевич', 'Алехин Д.А.', 29, 7, 76, 6),
(292, 'Алещанов Максим Витальевич', 'Алещанов М.В.', 45, NULL, NULL, 1),
(293, 'Алибеков Мурад Рамазанович', 'Алибеков М.Р.', 46, NULL, NULL, 1),
(294, 'Алтавиль Мустафа Акил Мохаммед Али', 'Алтавиль М.А.М.А.', 25, NULL, NULL, 1),
(295, 'Аль-Алвани Каррар Хаидер Хамид', 'Аль-Алвани К.Х.Х.', 25, NULL, NULL, 1),
(296, 'Альхарасани Ясир Мохаммед Фалих', 'Альхарасани Я.М.Ф.', 28, NULL, NULL, 1),
(297, 'Ананченко Андрей Владимирович', 'Ананченко А.В.', 47, NULL, NULL, 1),
(298, 'Андреев Сергей Александрович', 'Андреев С.А.', 48, NULL, NULL, 1),
(299, 'Андронов Максим Николаевич', 'Андронов М.Н.', 49, NULL, NULL, 1),
(300, 'Аникин Сергей Алексеевич', 'Аникин С.А.', 50, NULL, NULL, 1),
(301, 'Аникина Анна Дмитриевна', 'Аникина А.Д.', 51, NULL, NULL, 1),
(302, 'Анисимов Михаил Игоревич', 'Анисимов М.И.', 27, NULL, NULL, 1),
(303, 'Анисимова Полина Викторовна', 'Анисимова П.В.', 52, 7, 76, 6),
(304, 'Антипин Александр Сергеевич', 'Антипин А.С.', 53, NULL, NULL, 1),
(305, 'Антипов Дмитрий Алексеевич', 'Антипов Д.А.', 36, NULL, NULL, 1),
(306, 'Антониу Гарсиа Мануэл Ширимбимби', 'Антониу Г.М.Ш.', 54, NULL, NULL, 1),
(307, 'Антонова Лилия Александровна', 'Антонова Л.А.', 55, NULL, NULL, 1),
(308, 'Апанович Андрей Михайлович', 'Апанович А.М.', 56, NULL, NULL, 1),
(309, 'Аппиа Нана Овусу', 'Аппиа Н.О.', 38, NULL, NULL, 1),
(310, 'Арбеков Сергей Михайлович', 'Арбеков С.М.', 57, NULL, NULL, 1),
(311, 'Ардашев Антон Андреевич', 'Ардашев А.А.', 45, NULL, NULL, 1),
(312, 'Арефьев Алексей Михайлович', 'Арефьев А.М.', 58, NULL, NULL, 1),
(313, 'Арефьев Дмитрий Алексеевич', 'Арефьев Д.А.', 59, NULL, NULL, 1),
(314, 'Арисова Анастасия Николаевна', 'Арисова А.Н.', 58, NULL, NULL, 1),
(315, 'Артемян Дарина Карапетовна', 'Артемян Д.К.', 60, NULL, NULL, 1),
(316, 'Аслезов Никита Андреевич', 'Аслезов Н.А.', 61, NULL, NULL, 1),
(317, 'Ассемгхор Реда', 'Ассемгхор Р.', 28, NULL, NULL, 1),
(318, 'Ассемиан Бение Брис Орфе', 'Ассемиан Б.Б.О.', 61, NULL, NULL, 1),
(319, 'Астафьева Ирина Алексеевна', 'Астафьева И.А.', 49, NULL, NULL, 1),
(320, 'Асянин Михаил Вадимович', 'Асянин М.В.', 42, NULL, NULL, 1),
(321, 'Атрощенко Валерия Викторовна', 'Атрощенко В.В.', 62, NULL, NULL, 1),
(322, 'Аушев Мустафа Мехтиевич', 'Аушев М.М.', 63, NULL, NULL, 1),
(323, 'Афанасьев Алексей Витальевич', 'Афанасьев А.В.', 64, NULL, NULL, 1),
(324, 'Афонин Алексей Михайлович', 'Афонин А.М.', 65, NULL, NULL, 1),
(325, 'Ахмед Ахмед Усама Халифа Сайед', 'Ахмед А.У.Х.С.', 28, NULL, NULL, 1),
(326, 'Бабатунде Вонуола Дэниэл', 'Бабатунде В.Д.', 28, NULL, NULL, 1),
(327, 'Бабичев Ярослав Сергеевич', 'Бабичев Я.С.', 59, NULL, NULL, 1),
(328, 'Бабушкин Алексей Михайлович', 'Бабушкин А.М.', 66, NULL, NULL, 1),
(329, 'Бакаева Мария Игоревна', 'Бакаева М.И.', 46, NULL, NULL, 1),
(330, 'Бакалин Андрей Евгеньевич', 'Бакалин А.Е.', 67, NULL, NULL, 1),
(331, 'Баканов Максим Павлович', 'Баканов М.П.', 41, NULL, NULL, 1),
(332, 'Бакаринов Алексей Павлович', 'Бакаринов А.П.', 36, NULL, NULL, 1),
(333, 'Балаев Александр Романович', 'Балаев А.Р.', 35, NULL, NULL, 1),
(334, 'Балакирева Надежда Витальевна', 'Балакирева Н.В.', 45, NULL, NULL, 1),
(335, 'Баландин Александр Андреевич', 'Баландин А.А.', 68, 7, 76, 6),
(336, 'Балашов Андрей Андреевич', 'Балашов А.А.', 69, NULL, NULL, 1),
(337, 'Балдин Алексей Александрович', 'Балдин А.А.', 70, NULL, NULL, 1),
(338, 'Балынин Алексей Вячеславович', 'Балынин А.В.', 42, NULL, NULL, 1),
(339, 'Банденков Даниил Викторович', 'Банденков Д.В.', 53, NULL, NULL, 1),
(340, 'Баранов Андрей Денисович', 'Баранов А.Д.', 33, NULL, NULL, 1),
(341, 'Баранов Артем Алексеевич', 'Баранов А.А.', 50, NULL, NULL, 1),
(342, 'Баранов Павел Дмитриевич', 'Баранов П.Д.', 71, NULL, NULL, 1),
(343, 'Бардасов Дмитрий Сергеевич', 'Бардасов Д.С.', 72, NULL, NULL, 1),
(344, 'Баринов Ярослав Олегович', 'Баринов Я.О.', 34, NULL, NULL, 1),
(345, 'Баринова Александра Михайловна', 'Баринова А.М.', 73, NULL, NULL, 1),
(346, 'Барсуков Артур Дмитриевич', 'Барсуков А.Д.', 72, NULL, NULL, 1),
(347, 'Барышникова Наталья Леонидовна', 'Барышникова Н.Л.', 74, NULL, NULL, 1),
(348, 'Басов Антон Сергеевич', 'Басов А.С.', 75, NULL, NULL, 1),
(349, 'Басыров Рустам Маратович', 'Басыров Р.М.', 70, NULL, NULL, 1),
(350, 'Батанина Любовь Константиновна', 'Батанина Л.К.', 76, NULL, NULL, 1),
(351, 'Баташев Александр Юрьевич', 'Баташев А.Ю.', 26, NULL, NULL, 1),
(352, 'Батова Дарья Александровна', 'Батова Д.А.', 75, NULL, NULL, 1),
(353, 'Баулин Михаил Алексеевич', 'Баулин М.А.', 29, 7, 76, 6),
(354, 'Бахар Мохаммад Мазбах Уддин', 'Бахар М.М.У.', 25, NULL, NULL, 1),
(355, 'Бахаревский Александр Александрович', 'Бахаревский А.А.', 30, NULL, NULL, 1),
(356, 'Бахвалов Александр Александрович', 'Бахвалов А.А.', 65, NULL, NULL, 1),
(357, 'Бачин Александр Сергеевич', 'Бачин А.С.', 77, NULL, NULL, 1),
(358, 'Бебихова Анастасия Сергеевна', 'Бебихова А.С.', 78, NULL, NULL, 1),
(359, 'Бедердинов Даниил Дмитриевич', 'Бедердинов Д.Д.', 79, NULL, NULL, 1),
(360, 'Безаев Роман Алексеевич', 'Безаев Р.А.', 58, NULL, NULL, 1),
(361, 'Безроднов Дмитрий Александрович', 'Безроднов Д.А.', 44, NULL, NULL, 1),
(362, 'Безруков Павел Дмитриевич', 'Безруков П.Д.', 50, NULL, NULL, 1),
(363, 'Безручко Ирина Юрьевна', 'Безручко И.Ю.', 80, NULL, NULL, 1),
(364, 'Безуиденхоут Александер Михаел', 'Безуиденхоут А.М.', 38, NULL, NULL, 1),
(365, 'Безыкорнов Дмитрий Вадимович', 'Безыкорнов Д.В.', 81, NULL, NULL, 1),
(366, 'Белик Юлия Андреевна', 'Белик Ю.А.', 46, NULL, NULL, 1),
(367, 'Белов Даниил Владимирович', 'Белов Д.В.', 61, NULL, NULL, 1),
(368, 'Белов Кирилл Евгеньевич', 'Белов К.Е.', 51, NULL, NULL, 1),
(369, 'Белова Ксения Дмитриевна', 'Белова К.Д.', 44, NULL, NULL, 1),
(370, 'Бель Хут Ахмед Зубаир', 'Бель Х.А.З.', 48, NULL, NULL, 1),
(371, 'Беляков Леонид Алексеевич', 'Беляков Л.А.', 56, NULL, NULL, 1),
(372, 'Белякова Елизавета Александровна', 'Белякова Е.А.', 32, NULL, NULL, 1),
(373, 'Бензауиа Анасс', 'Бензауиа А.', 48, NULL, NULL, 1),
(374, 'Беннур Мохамед Амин', 'Беннур М.А.', 39, NULL, NULL, 1),
(375, 'Бенсенуси Закария', 'Бенсенуси З.', 25, NULL, NULL, 1),
(376, 'Березина Ирина Юрьевна', 'Березина И.Ю.', 82, NULL, NULL, 1),
(377, 'Бессолицын Сергей Андреевич', 'Бессолицын С.А.', 83, NULL, NULL, 1),
(378, 'Бессонова Варвара Александровна', 'Бессонова В.А.', 33, NULL, NULL, 1),
(379, 'Беше Джем', 'Беше Д.', 38, NULL, NULL, 1),
(380, 'Бильковский Александр Алексеевич', 'Бильковский А.А.', 67, NULL, NULL, 1),
(381, 'Благин Иван Сергеевич', 'Благин И.С.', 42, NULL, NULL, 1),
(382, 'Блинцов Сергей Дмитриевич', 'Блинцов С.Д.', 37, NULL, NULL, 1),
(383, 'Блохина Елена Дмитриевна', 'Блохина Е.Д.', 36, NULL, NULL, 1),
(384, 'Боб Джубили Ободовхор', 'Боб Д.О.', 38, NULL, NULL, 1),
(385, 'Бобров Денис Васильевич', 'Бобров Д.В.', 41, NULL, NULL, 1),
(386, 'Бобык Владислав Павлович', 'Бобык В.П.', 59, NULL, NULL, 1),
(387, 'Бобыльков Андрей Геннадьевич', 'Бобыльков А.Г.', 84, NULL, NULL, 1),
(388, 'Боганов Сергей Вадимович', 'Боганов С.В.', 58, NULL, NULL, 1),
(389, 'Богатова Маргарита Дмитриевна', 'Богатова М.Д.', 37, NULL, NULL, 1),
(390, 'Боев Роман Владимирович', 'Боев Р.В.', 78, NULL, NULL, 1),
(391, 'Бойцов Владислав Александрович', 'Бойцов В.А.', 52, 7, 76, 6),
(392, 'Болотов Дмитрий Ильич', 'Болотов Д.И.', 32, NULL, NULL, 1),
(393, 'Большаков Константин', 'Большаков К.', 79, NULL, NULL, 1),
(394, 'Большакова Анастасия Алексеевна', 'Большакова А.А.', 82, NULL, NULL, 1),
(395, 'Бондаренко Артём Дмитриевич', 'Бондаренко А.Д.', 85, NULL, NULL, 1),
(396, 'Бондаренко Илья Вячеславович', 'Бондаренко И.В.', 86, NULL, NULL, 1),
(397, 'Борисов Виктор Германович', 'Борисов В.Г.', 73, NULL, NULL, 1),
(398, 'Борисова Алена Александровна', 'Борисова А.А.', 60, NULL, NULL, 1),
(399, 'Боченков Павел Сергеевич', 'Боченков П.С.', 62, NULL, NULL, 1),
(400, 'Бочкарев Константин Андреевич', 'Бочкарев К.А.', 44, NULL, NULL, 1),
(401, 'Бочкова Дарья Андреевна', 'Бочкова Д.А.', 48, NULL, NULL, 1),
(402, 'Бражников Евгений Александрович', 'Бражников Е.А.', 48, NULL, NULL, 1),
(403, 'Бревнов Федор Сергеевич', 'Бревнов Ф.С.', 66, NULL, NULL, 1),
(404, 'Бржезинская Полина Константиновна', 'Бржезинская П.К.', 57, NULL, NULL, 1),
(405, 'Бритова Юлия Евгеньевна', 'Бритова Ю.Е.', 41, NULL, NULL, 1),
(406, 'Брызгалова Анастасия Сергеевна', 'Брызгалова А.С.', 34, NULL, NULL, 1),
(407, 'Бубнов Егор Дмитриевич', 'Бубнов Е.Д.', 48, NULL, NULL, 1),
(408, 'Буева Любовь Григорьевна', 'Буева Л.Г.', 40, NULL, NULL, 1),
(409, 'Буика Сесилия Ланду Тембу', 'Буика С.Л.Т.', 87, NULL, NULL, 1),
(410, 'Букарева Арина Романовна', 'Букарева А.Р.', 58, NULL, NULL, 1),
(411, 'Буккита Нгома Хемеранд Бессер', 'Буккита Н.Х.Б.', 83, NULL, NULL, 1),
(412, 'Булычев Андрей Сергеевич', 'Булычев А.С.', 29, 7, 76, 6),
(413, 'Булычев Владислав Дмитриевич', 'Булычев В.Д.', 83, NULL, NULL, 1),
(414, 'Бунтова Ксения Владимировна', 'Бунтова К.В.', 76, NULL, NULL, 1),
(415, 'Бурмистров Евгений Евгеньевич', 'Бурмистров Е.Е.', 62, NULL, NULL, 1),
(416, 'Бурмистрова Екатерина Олеговна', 'Бурмистрова Е.О.', 52, 7, 76, 6),
(417, 'Буторина Оксана Сергеевна', 'Буторина О.С.', 60, NULL, NULL, 1),
(418, 'Буфрах Аймен Туфик', 'Буфрах А.Т.', 39, NULL, NULL, 1),
(419, 'Бушуев Всеволод Вячеславович', 'Бушуев В.В.', 68, 7, 76, 6),
(420, 'Буянов Артём Дмитриевич', 'Буянов А.Д.', 31, NULL, NULL, 1),
(421, 'Бхуийа Мд Нахид Хасан', 'Бхуийа М.Н.Х.', 25, NULL, NULL, 1),
(422, 'Быковская Алина Павловна', 'Быковская А.П.', 49, NULL, NULL, 1),
(423, 'Быстрова Александра Николаевна', 'Быстрова А.Н.', 74, NULL, NULL, 1),
(424, 'Бычкова Анастасия Сергеевна', 'Бычкова А.С.', 41, NULL, NULL, 1),
(425, 'Ваганов Алексей Сергеевич', 'Ваганов А.С.', 44, NULL, NULL, 1),
(426, 'Ваганов Дмитрий Николаевич', 'Ваганов Д.Н.', 51, NULL, NULL, 1),
(427, 'Вагина Валерия Дмитриевна', 'Вагина В.Д.', 46, NULL, NULL, 1),
(428, 'Валиуллин Дамир Дамирович', 'Валиуллин Д.Д.', 56, NULL, NULL, 1),
(429, 'Ванеев Никита Юрьевич', 'Ванеев Н.Ю.', 88, NULL, NULL, 1),
(430, 'Ванчин Антон Александрович', 'Ванчин А.А.', 57, NULL, NULL, 1),
(431, 'Варамбва Шантел Номатамусакса Няша', 'Варамбва Ш.Н.Н.', 38, NULL, NULL, 1),
(432, 'Васильев Александр Сергеевич', 'Васильев А.С.', 27, NULL, NULL, 1),
(433, 'Васильев Илья Евгеньевич', 'Васильев И.Е.', 44, NULL, NULL, 1),
(434, 'Васильева Анастасия Вячеславовна', 'Васильева А.В.', 30, NULL, NULL, 1),
(435, 'Васютин Сергей Сергеевич', 'Васютин С.С.', 59, NULL, NULL, 1),
(436, 'Ващук Никита Анатольевич', 'Ващук Н.А.', 35, NULL, NULL, 1),
(437, 'Вдовин Евгений Александрович', 'Вдовин Е.А.', 47, NULL, NULL, 1),
(438, 'Вдовина Мария Алексеевна', 'Вдовина М.А.', 62, NULL, NULL, 1),
(439, 'Ведруков Павел Евгеньевич', 'Ведруков П.Е.', 58, NULL, NULL, 1),
(440, 'Великанова Марина Александровна', 'Великанова М.А.', 59, NULL, NULL, 1),
(441, 'Венкова Мария Андреевна', 'Венкова М.А.', 58, NULL, NULL, 1),
(442, 'Вершинин Дмитрий Валерьевич', 'Вершинин Д.В.', 41, NULL, NULL, 1),
(443, 'Веселов Виктор Сергеевич', 'Веселов В.С.', 61, NULL, NULL, 1),
(444, 'Веселов Никита Дмитриевич', 'Веселов Н.Д.', 82, NULL, NULL, 1),
(445, 'Вестерли Александр Александрович', 'Вестерли А.А.', 40, NULL, NULL, 1),
(446, 'Визгалов Антон Игоревич', 'Визгалов А.И.', 52, 7, 76, 6),
(447, 'Викторов Андрей Алексеевич', 'Викторов А.А.', 69, NULL, NULL, 1),
(448, 'Виноградова Екатерина Владимировна', 'Виноградова Е.В.', 67, NULL, NULL, 1),
(449, 'Винокурова Мария Сергеевна', 'Винокурова М.С.', 33, NULL, NULL, 1),
(450, 'Вирясов Михаил Сергеевич', 'Вирясов М.С.', 89, NULL, NULL, 1),
(451, 'Витальский Артём Валерьевич', 'Витальский А.В.', 57, NULL, NULL, 1),
(452, 'Вихарев Максим Викторович', 'Вихарев М.В.', 43, NULL, NULL, 1),
(453, 'Вихрев Иван Борисович', 'Вихрев И.Б.', 47, NULL, NULL, 1),
(454, 'Владимиров Александр Романович', 'Владимиров А.Р.', 27, NULL, NULL, 1),
(455, 'Власов Андрей Сергеевич', 'Власов А.С.', 80, NULL, NULL, 1),
(456, 'Власов Дмитрий Сергеевич', 'Власов Д.С.', 90, NULL, NULL, 1),
(457, 'Власов Леонид Александрович', 'Власов Л.А.', 52, 7, 76, 6),
(458, 'Власов Максим Сергеевич', 'Власов М.С.', 46, NULL, NULL, 1),
(459, 'Власова Виктория Павловна', 'Власова В.П.', 77, NULL, NULL, 1),
(460, 'Воденеев Михаил Владимирович', 'Воденеев М.В.', 86, NULL, NULL, 1),
(461, 'Вознесенская Анна Андреевна', 'Вознесенская А.А.', 73, NULL, NULL, 1),
(462, 'Волжанкин Данил Алексеевич', 'Волжанкин Д.А.', 51, NULL, NULL, 1),
(463, 'Волков Артем Алексеевич', 'Волков А.А.', 75, NULL, NULL, 1),
(464, 'Волков Владислав Сергеевич', 'Волков В.С.', 30, NULL, NULL, 1),
(465, 'Волков Даниил Дмитриевич', 'Волков Д.Д.', 37, NULL, NULL, 1),
(466, 'Волков Максим Александрович', 'Волков М.А.', 41, NULL, NULL, 1),
(467, 'Волков Павел Сергеевич', 'Волков П.С.', 56, NULL, NULL, 1),
(468, 'Волков Сергей Александрович', 'Волков С.А.', 27, NULL, NULL, 1),
(469, 'Волков Сергей Александрович', 'Волков С.А.', 60, NULL, NULL, 1),
(470, 'Волкова Анастасия Дмитриевна', 'Волкова А.Д.', 43, NULL, NULL, 1),
(471, 'Воробьев Леонид Александрович', 'Воробьев Л.А.', 68, 7, 76, 6),
(472, 'Воробьев Павел Олегович', 'Воробьев П.О.', 75, NULL, NULL, 1),
(473, 'Воронин Алексей Андреевич', 'Воронин А.А.', 52, 7, 76, 6),
(474, 'Воронов Антон Алексеевич', 'Воронов А.А.', 72, NULL, NULL, 1),
(475, 'Воронов Дмитрий Михайлович', 'Воронов Д.М.', 64, NULL, NULL, 1),
(476, 'Воронцов Дмитрий Сергеевич', 'Воронцов Д.С.', 27, NULL, NULL, 1),
(477, 'Вострилов Алексей Сергеевич', 'Вострилов А.С.', 41, NULL, NULL, 1),
(478, 'Вунже Алсину Мануэл', 'Вунже А.М.', 61, NULL, NULL, 1),
(479, 'Выломов Вячеслав Александрович', 'Выломов В.А.', 83, NULL, NULL, 1),
(480, 'Вьюнов Роман Васильевич', 'Вьюнов Р.В.', 37, NULL, NULL, 1),
(481, 'Вятчанин Роман Олегович', 'Вятчанин Р.О.', 32, NULL, NULL, 1),
(482, 'Габра Михаэль Адель Ламье', 'Габра М.А.Л.', 39, NULL, NULL, 1),
(483, 'Гаврилов Никита Ильич', 'Гаврилов Н.И.', 52, 7, 76, 6),
(484, 'Гаврюшова Варвара Андреевна', 'Гаврюшова В.А.', 53, NULL, NULL, 1),
(485, 'Газизова Софья Александровна', 'Газизова С.А.', 88, NULL, NULL, 1),
(486, 'Гайдайчук Юрий Валериевич', 'Гайдайчук Ю.В.', 47, NULL, NULL, 1),
(487, 'Галиндо Буитраго Хулио Сесар', 'Галиндо Б.Х.С.', 61, NULL, NULL, 1),
(488, 'Галкин Владислав Александрович', 'Галкин В.А.', 43, NULL, NULL, 1),
(489, 'Галкина Мария Владимировна', 'Галкина М.В.', 35, NULL, NULL, 1),
(490, 'Галочкин Алексей Александрович', 'Галочкин А.А.', 89, NULL, NULL, 1),
(491, 'Галочкин Борис Владимирович', 'Галочкин Б.В.', 76, NULL, NULL, 1),
(492, 'Галузина Анна Александровна', 'Галузина А.А.', 66, NULL, NULL, 1),
(493, 'Гаммель Юлия Владимировна', 'Гаммель Ю.В.', 70, NULL, NULL, 1),
(494, 'Ганьяна Эммануэль', 'Ганьяна Э.', 28, NULL, NULL, 1),
(495, 'Ганюшин Илья Алексеевич', 'Ганюшин И.А.', 41, NULL, NULL, 1),
(496, 'Гапон Андрей Кириллович', 'Гапон А.К.', 83, NULL, NULL, 1),
(497, 'Гарифуллин Тимур Талгатович', 'Гарифуллин Т.Т.', 27, NULL, NULL, 1),
(498, 'Гейа Элизанжела Майа З Карвалью', 'Гейа Э.М.З.К.', 87, NULL, NULL, 1),
(499, 'Генералов Александр Вячеславович', 'Генералов А.В.', 91, NULL, NULL, 1),
(500, 'Герасимов Дмитрий Геннадьевич', 'Герасимов Д.Г.', 91, NULL, NULL, 1),
(501, 'Герасимов Михаил Алексеевич', 'Герасимов М.А.', 86, NULL, NULL, 1),
(502, 'Герасимчук Максим Николаевич', 'Герасимчук М.Н.', 51, NULL, NULL, 1),
(503, 'Гераськин Иван Сергеевич', 'Гераськин И.С.', 72, NULL, NULL, 1),
(504, 'Гиленков Александр Михайлович', 'Гиленков А.М.', 37, NULL, NULL, 1),
(505, 'Гитлина Зоя Михайловна', 'Гитлина З.М.', 58, NULL, NULL, 1),
(506, 'Гладкая Милена Ростиславовна', 'Гладкая М.Р.', 46, NULL, NULL, 1),
(507, 'Гладышев Алексей Александрович', 'Гладышев А.А.', 71, NULL, NULL, 1),
(508, 'Глазатов Владимир Андреевич', 'Глазатов В.А.', 32, NULL, NULL, 1),
(509, 'Глотов Никита Сергеевич', 'Глотов Н.С.', 48, NULL, NULL, 1),
(510, 'Глуздов Андрей Дмитриевич', 'Глуздов А.Д.', 71, NULL, NULL, 1),
(511, 'Гогов Владислав Игоревич', 'Гогов В.И.', 46, NULL, NULL, 1),
(512, 'Голендеева Ирина Олеговна', 'Голендеева И.О.', 61, NULL, NULL, 1),
(513, 'Голицын Дмитрий Алексеевич', 'Голицын Д.А.', 27, NULL, NULL, 1),
(514, 'Голованов Дмитрий Андреевич', 'Голованов Д.А.', 64, NULL, NULL, 1),
(515, 'Голованова Елена Александровна', 'Голованова Е.А.', 80, NULL, NULL, 1),
(516, 'Головин Дмитрий Александрович', 'Головин Д.А.', 64, NULL, NULL, 1),
(517, 'Голубев Владислав Сергеевич', 'Голубев В.С.', 80, NULL, NULL, 1),
(518, 'Голубева Анна Сергеевна', 'Голубева А.С.', 80, NULL, NULL, 1),
(519, 'Голунов Алексей Сергеевич', 'Голунов А.С.', 60, NULL, NULL, 1),
(520, 'Гомеш Эваришту Манаса', 'Гомеш Э.М.', 87, NULL, NULL, 1),
(521, 'Гондо Артур', 'Гондо А.', 36, NULL, NULL, 1),
(522, 'Гондурова Дарья Владимировна', 'Гондурова Д.В.', 86, NULL, NULL, 1),
(523, 'Гонзова Анастасия Алексеевна', 'Гонзова А.А.', 92, NULL, NULL, 1),
(524, 'Гончаренко Анастасия Владиславовна', 'Гончаренко А.В.', 29, 7, 76, 6),
(525, 'Горб Николай Николаевич', 'Горб Н.Н.', 73, NULL, NULL, 1),
(526, 'Горбанев Иван Юрьевич', 'Горбанев И.Ю.', 59, NULL, NULL, 1),
(527, 'Горбачев Дмитрий Юрьевич', 'Горбачев Д.Ю.', 47, NULL, NULL, 1),
(528, 'Горбашов Дмитрий Александрович', 'Горбашов Д.А.', 70, NULL, NULL, 1),
(529, 'Горбунов Владислав Михайлович', 'Горбунов В.М.', 79, NULL, NULL, 1),
(530, 'Горбунова Валерия Игоревна', 'Горбунова В.И.', 52, 7, 76, 6),
(531, 'Горелова Ксения Александровна', 'Горелова К.А.', 37, NULL, NULL, 1),
(532, 'Горожанин Михаил Юрьевич', 'Горожанин М.Ю.', 67, NULL, NULL, 1),
(533, 'Горохова Кристина Михайловна', 'Горохова К.М.', 74, NULL, NULL, 1),
(534, 'Горячкин Егор Александрович', 'Горячкин Е.А.', 71, NULL, NULL, 1),
(535, 'Госал Шанкадип', 'Госал Ш.', 38, NULL, NULL, 1),
(536, 'Готовцев Сергей Олегович', 'Готовцев С.О.', 89, NULL, NULL, 1),
(537, 'Грачев Владислав Вадимович', 'Грачев В.В.', 67, NULL, NULL, 1),
(538, 'Гребенников Андрей Дмитриевич', 'Гребенников А.Д.', 78, NULL, NULL, 1),
(539, 'Грибанов Михаил Михайлович', 'Грибанов М.М.', 49, NULL, NULL, 1),
(540, 'Грибов Михаил Николаевич', 'Грибов М.Н.', 91, NULL, NULL, 1),
(541, 'Грибов Павел Николаевич', 'Грибов П.Н.', 76, NULL, NULL, 1),
(542, 'Григорян Гарри Артурович', 'Григорян Г.А.', 48, NULL, NULL, 1),
(543, 'Грицаенко Светлана Алексеевна', 'Грицаенко С.А.', 32, NULL, NULL, 1),
(544, 'Гришин Антон Сергеевич', 'Гришин А.С.', 71, NULL, NULL, 1),
(545, 'Громов Игорь Олегович', 'Громов И.О.', 72, NULL, NULL, 1),
(546, 'Громов Николай Валерьевич', 'Громов Н.В.', 58, NULL, NULL, 1),
(547, 'Груздев Артур Евгеньевич', 'Груздев А.Е.', 35, NULL, NULL, 1),
(548, 'Груздева Диана Максимовна', 'Груздева Д.М.', 43, NULL, NULL, 1),
(549, 'Губская Александра Викторовна', 'Губская А.В.', 88, NULL, NULL, 1),
(550, 'Гугин Александр Сергеевич', 'Гугин А.С.', 83, NULL, NULL, 1),
(551, 'Гудков Кирилл Романович', 'Гудков К.Р.', 59, NULL, NULL, 1),
(552, 'Гуляев Иван Сергеевич', 'Гуляев И.С.', 57, NULL, NULL, 1),
(553, 'Гуляева Дарья Игоревна', 'Гуляева Д.И.', 58, NULL, NULL, 1),
(554, 'Гуляк Артур Евгеньевич', 'Гуляк А.Е.', 61, NULL, NULL, 1),
(555, 'Гунаев Егор Андреевич', 'Гунаев Е.А.', 45, NULL, NULL, 1),
(556, 'Гурджи Александра Михайловна', 'Гурджи А.М.', 90, NULL, NULL, 1),
(557, 'Гуров Максим Александрович', 'Гуров М.А.', 50, NULL, NULL, 1),
(558, 'Гурылев Никита Сергеевич', 'Гурылев Н.С.', 83, NULL, NULL, 1),
(559, 'Гурьянов Владислав Владимирович', 'Гурьянов В.В.', 43, NULL, NULL, 1),
(560, 'Гурьянова Ксения Юрьевна', 'Гурьянова К.Ю.', 65, NULL, NULL, 1),
(561, 'Гусарова Дарья Алексеевна', 'Гусарова Д.А.', 43, NULL, NULL, 1),
(562, 'Гусарова Надежда Алексеевна', 'Гусарова Н.А.', 90, NULL, NULL, 1),
(563, 'Гусев Александр Владимирович', 'Гусев А.В.', 91, NULL, NULL, 1),
(564, 'Гусев Илья Юрьевич', 'Гусев И.Ю.', 30, NULL, NULL, 1),
(565, 'Гусева Екатерина Дмитриевна', 'Гусева Е.Д.', 66, NULL, NULL, 1),
(566, 'Гусейнов Анар Авез оглы', 'Гусейнов А.А.О.', 27, NULL, NULL, 1),
(567, 'Гуськова Ирина Андреевна', 'Гуськова И.А.', 85, NULL, NULL, 1),
(568, 'Гущин Александр Владимирович', 'Гущин А.В.', 53, NULL, NULL, 1),
(569, 'Гущин Артем Александрович', 'Гущин А.А.', 83, NULL, NULL, 1),
(570, 'Гэн Юйцзе', 'Гэн Ю.', 38, NULL, NULL, 1),
(571, 'Дабагян Норайр Артакович', 'Дабагян Н.А.', 27, NULL, NULL, 1),
(572, 'Давыдов Антон Максимович', 'Давыдов А.М.', 33, NULL, NULL, 1),
(573, 'Давыдов Данил Валерьевич', 'Давыдов Д.В.', 72, NULL, NULL, 1),
(574, 'Дан Цимин', 'Дан Ц.', 39, NULL, NULL, 1),
(575, 'Даниелян Арменак Самвелович', 'Даниелян А.С.', 31, NULL, NULL, 1),
(576, 'Данилов Кирилл Андреевич', 'Данилов К.А.', 36, NULL, NULL, 1),
(577, 'Данилов Никита Андреевич', 'Данилов Н.А.', 47, NULL, NULL, 1),
(578, 'Данилова Екатерина Сергеевна', 'Данилова Е.С.', 45, NULL, NULL, 1),
(579, 'Данилова Светлана Андреевна', 'Данилова С.А.', 36, NULL, NULL, 1),
(580, 'Дворянчиков Евгений Алексеевич', 'Дворянчиков Е.А.', 91, NULL, NULL, 1),
(581, 'Девликамов Владислав Олегович', 'Девликамов В.О.', 58, NULL, NULL, 1),
(582, 'Девятайкин Анатолий Михайлович', 'Девятайкин А.М.', 60, NULL, NULL, 1),
(583, 'Девятов Роман Александрович', 'Девятов Р.А.', 62, NULL, NULL, 1),
(584, 'Дегтярев Антон Юрьевич', 'Дегтярев А.Ю.', 91, NULL, NULL, 1),
(585, 'Дедова Екатерина Евгеньевна', 'Дедова Е.Е.', 36, NULL, NULL, 1),
(586, 'Демаков Данила Андреевич', 'Демаков Д.А.', 59, NULL, NULL, 1),
(587, 'Денисов Владислав Львович', 'Денисов В.Л.', 80, NULL, NULL, 1),
(588, 'Десоки Хешам Абделвахаб Елиан', 'Десоки Х.А.Е.', 28, NULL, NULL, 1),
(589, 'Джарди Мессауд', 'Джарди М.', 39, NULL, NULL, 1),
(590, 'Дженба Абделхак', 'Дженба А.', 48, NULL, NULL, 1),
(591, 'Диену Лузала Пэдру', 'Диену Л.П.', 87, NULL, NULL, 1),
(592, 'Диженин Владислав Евгеньевич', 'Диженин В.Е.', 90, NULL, NULL, 1),
(593, 'Димаков Максим Ильич', 'Димаков М.И.', 69, NULL, NULL, 1),
(594, 'Дмитричев Никита Анатольевич', 'Дмитричев Н.А.', 67, NULL, NULL, 1),
(595, 'Добров Павел Сергеевич', 'Добров П.С.', 83, NULL, NULL, 1),
(596, 'Доброхотов Виталий Николаевич', 'Доброхотов В.Н.', 48, NULL, NULL, 1),
(597, 'Долгополов Даниил Александрович', 'Долгополов Д.А.', 50, NULL, NULL, 1),
(598, 'Домингуш Мелкиадеш Абел Де Керлам', 'Домингуш М.А.Д.К.', 54, NULL, NULL, 1),
(599, 'Дружинин Алексей Сергеевич', 'Дружинин А.С.', 47, NULL, NULL, 1),
(600, 'Дубов Алексей Леонидович', 'Дубов А.Л.', 91, NULL, NULL, 1),
(601, 'Дубовской Андрей Владимирович', 'Дубовской А.В.', 75, NULL, NULL, 1),
(602, 'Дудченко Антон Викторович', 'Дудченко А.В.', 49, NULL, NULL, 1),
(603, 'Дукова Екатерина Евгеньевна', 'Дукова Е.Е.', 64, NULL, NULL, 1),
(604, 'Дунаев Даниил Владимирович', 'Дунаев Д.В.', 57, NULL, NULL, 1),
(605, 'Душева Екатерина Сергеевна', 'Душева Е.С.', 35, NULL, NULL, 1),
(606, 'Дядькин Олег Юрьевич', 'Дядькин О.Ю.', 84, NULL, NULL, 1),
(607, 'Дятел Диана Размиковна', 'Дятел Д.Р.', 74, NULL, NULL, 1),
(608, 'Евдокимов Артем Андреевич', 'Евдокимов А.А.', 66, NULL, NULL, 1),
(609, 'Евдокимова Юлия Вячеславовна', 'Евдокимова Ю.В.', 75, NULL, NULL, 1),
(610, 'Евсеев Александр Дмитриевич', 'Евсеев А.Д.', 29, 7, 76, 6),
(611, 'Евстигнеева Дария Всеволодовна', 'Евстигнеева Д.В.', 33, NULL, NULL, 1),
(612, 'Евстифеев Михаил Александрович', 'Евстифеев М.А.', 69, NULL, NULL, 1),
(613, 'Евсякова Екатерина Сергеевна', 'Евсякова Е.С.', 79, NULL, NULL, 1),
(614, 'Егоров Владислав Евгеньевич', 'Егоров В.Е.', 42, NULL, NULL, 1),
(615, 'Егоров Данил Геннадьевич', 'Егоров Д.Г.', 70, NULL, NULL, 1),
(616, 'Егоров Кирилл Сергеевич', 'Егоров К.С.', 68, 7, 76, 6),
(617, 'Ежонков Илья Александрович', 'Ежонков И.А.', 90, NULL, NULL, 1),
(618, 'Елагина Дарья Михайловна', 'Елагина Д.М.', 35, NULL, NULL, 1),
(619, 'Еландаев Павел Евгеньевич', 'Еландаев П.Е.', 29, 7, 76, 6),
(620, 'Емелин Артем Евгеньевич', 'Емелин А.Е.', 90, NULL, NULL, 1),
(621, 'Емелин Максим Денисович', 'Емелин М.Д.', 31, NULL, NULL, 1),
(622, 'Емельховская Екатерина Евгеньевна', 'Емельховская Е.Е.', 46, NULL, NULL, 1),
(623, 'Емшанов Андрей Иванович', 'Емшанов А.И.', 66, NULL, NULL, 1),
(624, 'Еремина Алёна Сергеевна', 'Еремина А.С.', 52, 7, 76, 6),
(625, 'Ермаков Алексей Александрович', 'Ермаков А.А.', 91, NULL, NULL, 1),
(626, 'Ермакова Екатерина Алексеевна', 'Ермакова Е.А.', 88, NULL, NULL, 1),
(627, 'Ермаченко Борис Андреевич', 'Ермаченко Б.А.', 37, NULL, NULL, 1),
(628, 'Ермилин Елисей Александрович', 'Ермилин Е.А.', 56, NULL, NULL, 1),
(629, 'Ермолаев Илья Александрович', 'Ермолаев И.А.', 86, NULL, NULL, 1),
(630, 'Ермолаев Максим Александрович', 'Ермолаев М.А.', 88, NULL, NULL, 1),
(631, 'Ермолин Дмитрий Алексеевич', 'Ермолин Д.А.', 52, 7, 76, 6),
(632, 'Ермолин Евгений Александрович', 'Ермолин Е.А.', 41, NULL, NULL, 1),
(633, 'Ерофеев Александр Сергеевич', 'Ерофеев А.С.', 31, NULL, NULL, 1),
(634, 'Ерушкин Владислав Алексеевич', 'Ерушкин В.А.', 84, NULL, NULL, 1),
(635, 'Есина Валерия Геннадьевна', 'Есина В.Г.', 72, NULL, NULL, 1),
(636, 'Ефремов Всеволод Вячеславович', 'Ефремов В.В.', 60, NULL, NULL, 1),
(637, 'Жамков Андрей Олегович', 'Жамков А.О.', 43, NULL, NULL, 1),
(638, 'Жарков Сергей Николаевич', 'Жарков С.Н.', 65, NULL, NULL, 1),
(639, 'Жаркова Лолита Александровна', 'Жаркова Л.А.', 89, NULL, NULL, 1),
(640, 'Жаров Юлий Александрович', 'Жаров Ю.А.', 59, NULL, NULL, 1),
(641, 'Жаткин Артём Михайлович', 'Жаткин А.М.', 30, NULL, NULL, 1),
(642, 'Жафяров Олег Ильязович', 'Жафяров О.И.', 29, 7, 76, 6),
(643, 'Жбанова Надежда Сергеевна', 'Жбанова Н.С.', 53, NULL, NULL, 1),
(644, 'Жданович Андрей Сергеевич', 'Жданович А.С.', 65, NULL, NULL, 1),
(645, 'Желнов Илья Николаевич', 'Желнов И.Н.', 33, NULL, NULL, 1),
(646, 'Желтов Сергей Александрович', 'Желтов С.А.', 92, NULL, NULL, 1),
(647, 'Живаев Артем Евгеньевич', 'Живаев А.Е.', 49, NULL, NULL, 1),
(648, 'Живайкин Даниил Евгеньевич', 'Живайкин Д.Е.', 83, NULL, NULL, 1),
(649, 'Жилина Валерия Вадимовна', 'Жилина В.В.', 93, NULL, NULL, 1),
(650, 'Жирнов Владислав Геннадьевич', 'Жирнов В.Г.', 32, NULL, NULL, 1),
(651, 'Жоау Браулиу Да Консейсау Казуа', 'Жоау Б.Д.К.К.', 54, NULL, NULL, 1),
(652, 'Жорже Эмануэл Фернандеш', 'Жорже Э.Ф.', 54, NULL, NULL, 1),
(653, 'Жорин Максим Андреевич', 'Жорин М.А.', 36, NULL, NULL, 1),
(654, 'Жуниор Эуфразия Карина Домингуш', 'Жуниор Э.К.Д.', 54, NULL, NULL, 1),
(655, 'Журавлев Роман Александрович', 'Журавлев Р.А.', 61, NULL, NULL, 1),
(656, 'Журилова Анастасия Сергеевна', 'Журилова А.С.', 86, NULL, NULL, 1),
(657, 'Жучкова Алена Александровна', 'Жучкова А.А.', 74, NULL, NULL, 1),
(658, 'Заиграев Владислав Александрович', 'Заиграев В.А.', 89, NULL, NULL, 1),
(659, 'Заикин Илья Михайлович', 'Заикин И.М.', 70, NULL, NULL, 1),
(660, 'Зайцев Андрей Русланович', 'Зайцев А.Р.', 83, NULL, NULL, 1),
(661, 'Зайцев Даниил Маркович', 'Зайцев Д.М.', 42, NULL, NULL, 1),
(662, 'Захаров Михаил Дмитриевич', 'Захаров М.Д.', 70, NULL, NULL, 1),
(663, 'Захарова Дарья Дмитриевна', 'Захарова Д.Д.', 32, NULL, NULL, 1),
(664, 'Заятников Антон Алексеевич', 'Заятников А.А.', 86, NULL, NULL, 1),
(665, 'Зейналов Элчин Элшан оглы', 'Зейналов Э.Э.О.', 40, NULL, NULL, 1),
(666, 'Зеленевский Вадим Игоревич', 'Зеленевский В.И.', 30, NULL, NULL, 1),
(667, 'Зеленцов Никита Григорьевич', 'Зеленцов Н.Г.', 69, NULL, NULL, 1),
(668, 'Земляникин Евгений Станиславович', 'Земляникин Е.С.', 26, NULL, NULL, 1),
(669, 'Землянский Никита Алексеевич', 'Землянский Н.А.', 67, NULL, NULL, 1),
(670, 'Зибара Хассан', 'Зибара Х.', 28, NULL, NULL, 1),
(671, 'Зинин Антон Вячеславович', 'Зинин А.В.', 90, NULL, NULL, 1),
(672, 'Зинков Артем Сергеевич', 'Зинков А.С.', 53, NULL, NULL, 1),
(673, 'Зиновьев Владимир Евгеньевич', 'Зиновьев В.Е.', 71, NULL, NULL, 1),
(674, 'Зитха Сифо Джозеф', 'Зитха С.Д.', 28, NULL, NULL, 1),
(675, 'Злобин Георгий Максимович', 'Злобин Г.М.', 52, 7, 76, 6),
(676, 'Золотарёва Олеся Сергеевна', 'Золотарёва О.С.', 47, NULL, NULL, 1),
(677, 'Зореев Михаил Владимирович', 'Зореев М.В.', 46, NULL, NULL, 1),
(678, 'Зорина Екатерина Владимировна', 'Зорина Е.В.', 67, NULL, NULL, 1),
(679, 'Зотина Полина Андреевна', 'Зотина П.А.', 72, NULL, NULL, 1),
(680, 'Зотова Анастасия Владимировна', 'Зотова А.В.', 62, NULL, NULL, 1),
(681, 'Зубов Никита Андреевич', 'Зубов Н.А.', 60, NULL, NULL, 1),
(682, 'Зуев Даниил Александрович', 'Зуев Д.А.', 72, NULL, NULL, 1),
(683, 'Иванов Александр Александрович', 'Иванов А.А.', 60, NULL, NULL, 1),
(684, 'Иванов Александр Николаевич', 'Иванов А.Н.', 31, NULL, NULL, 1),
(685, 'Иванов Андрей Сергеевич', 'Иванов А.С.', 60, NULL, NULL, 1),
(686, 'Иванов Антон Сергеевич', 'Иванов А.С.', 59, NULL, NULL, 1),
(687, 'Иванов Даниил Николаевич', 'Иванов Д.Н.', 57, NULL, NULL, 1),
(688, 'Иванов Максим Вениаминович', 'Иванов М.В.', 59, NULL, NULL, 1),
(689, 'Иванов Тимофей Владимирович', 'Иванов Т.В.', 77, NULL, NULL, 1),
(690, 'Иванов Ярослав Алексеевич', 'Иванов Я.А.', 52, 7, 76, 6),
(691, 'Ивлев Андрей Александрович', 'Ивлев А.А.', 56, NULL, NULL, 1),
(692, 'Игошева Надежда Павловна', 'Игошева Н.П.', 62, NULL, NULL, 1),
(693, 'Идика Эммануэль Ифеаньи', 'Идика Э.И.', 39, NULL, NULL, 1),
(694, 'Икромов Инъомжон Икромалиевич', 'Икромов И.И.', 61, NULL, NULL, 1),
(695, 'Ильина Марина Васильевна', 'Ильина М.В.', 74, NULL, NULL, 1),
(696, 'Ильичев Виктор Николаевич', 'Ильичев В.Н.', 45, NULL, NULL, 1),
(697, 'Илюхин Илья Игоревич', 'Илюхин И.И.', 50, NULL, NULL, 1),
(698, 'Иоану Даниел', 'Иоану Д.', 91, NULL, NULL, 1),
(699, 'Ионов Михаил Владимирович', 'Ионов М.В.', 57, NULL, NULL, 1),
(700, 'Ипатов Евгений Игоревич', 'Ипатов Е.И.', 51, NULL, NULL, 1),
(701, 'Исаев Илья Алексеевич', 'Исаев И.А.', 53, NULL, NULL, 1),
(702, 'Исияку Абдулмалик Бала', 'Исияку А.Б.', 28, NULL, NULL, 1),
(703, 'Ислам Мд Таухидул', 'Ислам М.Т.', 38, NULL, NULL, 1),
(704, 'Исрафилов Марат Шавкатович', 'Исрафилов М.Ш.', 67, NULL, NULL, 1),
(705, 'Ицыксон Даниил Романович', 'Ицыксон Д.Р.', 61, NULL, NULL, 1),
(706, 'Ищенко Иван Дмитриевич', 'Ищенко И.Д.', 43, NULL, NULL, 1),
(707, 'Йурдан Мухаммет', 'Йурдан М.', 28, NULL, NULL, 1),
(708, 'Кабанов Иван Алексеевич', 'Кабанов И.А.', 51, NULL, NULL, 1),
(709, 'Кавалеса Мария Валдени Эвора', 'Кавалеса М.В.Э.', 87, NULL, NULL, 1),
(710, 'Каверин Николай Сергеевич', 'Каверин Н.С.', 72, NULL, NULL, 1),
(711, 'Каганов Дмитрий Алексеевич', 'Каганов Д.А.', 75, NULL, NULL, 1),
(712, 'Кадников Денис Владимирович', 'Кадников Д.В.', 29, 7, 76, 6),
(713, 'Казаков Андрей Михайлович', 'Казаков А.М.', 67, NULL, NULL, 1),
(714, 'Кайдалова Юлия Романовна', 'Кайдалова Ю.Р.', 77, NULL, NULL, 1),
(715, 'Каленга Даниель Мпойи', 'Каленга Д.М.', 41, NULL, NULL, 1),
(716, 'Калёнов Константин Юрьевич', 'Калёнов К.Ю.', 78, NULL, NULL, 1),
(717, 'Калинин Владимир Дмитриевич', 'Калинин В.Д.', 88, NULL, NULL, 1),
(718, 'Калистратов Андрей Сергеевич', 'Калистратов А.С.', 71, NULL, NULL, 1),
(719, 'Калмыков Егор Владимирович', 'Калмыков Е.В.', 59, NULL, NULL, 1),
(720, 'Калякулина Анастасия Игоревна', 'Калякулина А.И.', 50, NULL, NULL, 1),
(721, 'Камелина Юлия Дмитриевна', 'Камелина Ю.Д.', 71, NULL, NULL, 1),
(722, 'Камсков Евгений Маратович', 'Камсков Е.М.', 52, 7, 76, 6),
(723, 'Кандиейру Рожериа Де Энкарнасау Карлуш', 'Кандиейру Р.Д.Э.К.', 87, NULL, NULL, 1),
(724, 'Капкаев Никита Викторович', 'Капкаев Н.В.', 88, NULL, NULL, 1),
(725, 'Капустин Григорий Константинович', 'Капустин Г.К.', 50, NULL, NULL, 1),
(726, 'Капустин Дмитрий Олегович', 'Капустин Д.О.', 47, NULL, NULL, 1),
(727, 'Карагезов Александр Спартакович', 'Карагезов А.С.', 83, NULL, NULL, 1),
(728, 'Карасев Антон Андреевич', 'Карасев А.А.', 26, NULL, NULL, 1),
(729, 'Кардозу Ауреу Мартинш Да Кошта', 'Кардозу А.М.Д.К.', 25, NULL, NULL, 1),
(730, 'Карин Тимофей Андреевич', 'Карин Т.А.', 80, NULL, NULL, 1),
(731, 'Карташев Владислав Вячеславович', 'Карташев В.В.', 29, 7, 76, 6),
(732, 'Карташова Елена Игоревна', 'Карташова Е.И.', 69, NULL, NULL, 1),
(733, 'Картомин Никита Александрович', 'Картомин Н.А.', 32, NULL, NULL, 1),
(734, 'Касмазюк Никита Романович', 'Касмазюк Н.Р.', 75, NULL, NULL, 1),
(735, 'Касмон Нлиси Тамапо', 'Касмон Н.Т.', 38, NULL, NULL, 1),
(736, 'Касьянычев Михаил Петрович', 'Касьянычев М.П.', 46, NULL, NULL, 1),
(737, 'Катаев Роман Дмитриевич', 'Катаев Р.Д.', 55, NULL, NULL, 1),
(738, 'Каткова Дарья Алексеевна', 'Каткова Д.А.', 51, NULL, NULL, 1),
(739, 'Катранов Кирилл Александрович', 'Катранов К.А.', 58, NULL, NULL, 1),
(740, 'Катышев Павел Александрович', 'Катышев П.А.', 48, NULL, NULL, 1),
(741, 'Кац Евгений Михайлович', 'Кац Е.М.', 42, NULL, NULL, 1),
(742, 'Кацапов Егор Николаевич', 'Кацапов Е.Н.', 59, NULL, NULL, 1),
(743, 'Кашина Анастасия Александровна', 'Кашина А.А.', 70, NULL, NULL, 1),
(744, 'Кашневич Анастасия Андреевна', 'Кашневич А.А.', 85, NULL, NULL, 1),
(745, 'Каштелу Педру Мендеш', 'Каштелу П.М.', 87, NULL, NULL, 1),
(746, 'Кебе Элдер Муамад Перейра', 'Кебе Э.М.П.', 54, NULL, NULL, 1),
(747, 'Кизаев Дмитрий Дмитриевич', 'Кизаев Д.Д.', 46, NULL, NULL, 1),
(748, 'Кизембе Ошвалду Консейсау Канда', 'Кизембе О.К.К.', 87, NULL, NULL, 1),
(749, 'Кильмаева Юлия Сергеевна', 'Кильмаева Ю.С.', 59, NULL, NULL, 1),
(750, 'Кириллов Захар Алексеевич', 'Кириллов З.А.', 68, 7, 76, 6),
(751, 'Кириллов Константин Игоревич', 'Кириллов К.И.', 43, NULL, NULL, 1),
(752, 'Кириченко Никита Андреевич', 'Кириченко Н.А.', 83, NULL, NULL, 1),
(753, 'Киселев Денис Сергеевич', 'Киселев Д.С.', 37, NULL, NULL, 1),
(754, 'Киселёв Иван Сергеевич', 'Киселёв И.С.', 67, NULL, NULL, 1),
(755, 'Киселева Анастасия Сергеевна', 'Киселева А.С.', 43, NULL, NULL, 1),
(756, 'Киселева Ксения Григорьевна', 'Киселева К.Г.', 60, NULL, NULL, 1),
(757, 'Киселева Ольга Евгеньевна', 'Киселева О.Е.', 35, NULL, NULL, 1),
(758, 'Кислицын Михаил Владимирович', 'Кислицын М.В.', 60, NULL, NULL, 1),
(759, 'Климанова Анжела Александровна', 'Климанова А.А.', 34, NULL, NULL, 1),
(760, 'Клименко Ксения Евгеньевна', 'Клименко К.Е.', 44, NULL, NULL, 1),
(761, 'Клинцов Артем Сергеевич', 'Клинцов А.С.', 40, NULL, NULL, 1),
(762, 'Клоков Андрей Геннадьевич', 'Клоков А.Г.', 72, NULL, NULL, 1),
(763, 'Клюев Дмитрий Андреевич', 'Клюев Д.А.', 46, NULL, NULL, 1),
(764, 'Кобычкина Ольга Сергеевна', 'Кобычкина О.С.', 56, NULL, NULL, 1),
(765, 'Коврижных Мария Андреевна', 'Коврижных М.А.', 66, NULL, NULL, 1),
(766, 'Ковшов Андрей Николаевич', 'Ковшов А.Н.', 79, NULL, NULL, 1),
(767, 'Ковшов Степан Владимирович', 'Ковшов С.В.', 45, NULL, NULL, 1),
(768, 'Кожевников Владимир Владимирович', 'Кожевников В.В.', 48, NULL, NULL, 1),
(769, 'Козлов Игорь Александрович', 'Козлов И.А.', 30, NULL, NULL, 1),
(770, 'Козлов Илья Дмитриевич', 'Козлов И.Д.', 67, NULL, NULL, 1),
(771, 'Козорез Александр', 'Козорез А.', 49, NULL, NULL, 1),
(772, 'Колабин Юлиан Владимирович', 'Колабин Ю.В.', 45, NULL, NULL, 1),
(773, 'Колегов Илья Александрович', 'Колегов И.А.', 50, NULL, NULL, 1),
(774, 'Колесин Андрей Сергеевич', 'Колесин А.С.', 53, NULL, NULL, 1),
(775, 'Колесников Глеб Ростиславович', 'Колесников Г.Р.', 86, NULL, NULL, 1),
(776, 'Колесова Кристина Юрьевна', 'Колесова К.Ю.', 80, NULL, NULL, 1),
(777, 'Колмогоров Денис Сергеевич', 'Колмогоров Д.С.', 63, NULL, NULL, 1),
(778, 'Колодько Никита Александрович', 'Колодько Н.А.', 77, NULL, NULL, 1),
(779, 'Колосова Евгения Александровна', 'Колосова Е.А.', 56, NULL, NULL, 1),
(780, 'Кольтюшкина Янина Вадимовна', 'Кольтюшкина Я.В.', 80, NULL, NULL, 1),
(781, 'Комаров Никита Михайлович', 'Комаров Н.М.', 29, 7, 76, 6),
(782, 'Комин Владимир Евгеньевич', 'Комин В.Е.', 44, NULL, NULL, 1),
(783, 'Конаков Александр Владимирович', 'Конаков А.В.', 71, NULL, NULL, 1),
(784, 'Кондрина Татьяна Алексеевна', 'Кондрина Т.А.', 49, NULL, NULL, 1),
(785, 'Коннов Сергей Юрьевич', 'Коннов С.Ю.', 49, NULL, NULL, 1),
(786, 'Копейко Даниил Сергеевич', 'Копейко Д.С.', 60, NULL, NULL, 1),
(787, 'Копченов Кирилл Александрович', 'Копченов К.А.', 65, NULL, NULL, 1),
(788, 'Корнев Никита Алексеевич', 'Корнев Н.А.', 80, NULL, NULL, 1),
(789, 'Коробейников Алексей Петрович', 'Коробейников А.П.', 49, NULL, NULL, 1),
(790, 'Коробко Евгений Владимирович', 'Коробко Е.В.', 44, NULL, NULL, 1),
(791, 'Коробкова Екатерина Валентиновна', 'Коробкова Е.В.', 86, NULL, NULL, 1),
(792, 'Королев Александр Павлович', 'Королев А.П.', 62, NULL, NULL, 1),
(793, 'Королева Дарья Игоревна', 'Королева Д.И.', 49, NULL, NULL, 1),
(794, 'Коротков Денис Александрович', 'Коротков Д.А.', 35, NULL, NULL, 1),
(795, 'Косарев Роман Александрович', 'Косарев Р.А.', 73, NULL, NULL, 1),
(796, 'Косоруков Дмитрий Олегович', 'Косоруков Д.О.', 71, NULL, NULL, 1),
(797, 'Коссенге Элионе Кулека', 'Коссенге Э.К.', 54, NULL, NULL, 1),
(798, 'Костандян Тигран Каренович', 'Костандян Т.К.', 33, NULL, NULL, 1),
(799, 'Кострова Ирина Александровна', 'Кострова И.А.', 40, NULL, NULL, 1),
(800, 'Кострюков Андрей Александрович', 'Кострюков А.А.', 27, NULL, NULL, 1),
(801, 'Костюкова Ольга Сергеевна', 'Костюкова О.С.', 60, NULL, NULL, 1),
(802, 'Костюничев Никита Андреевич', 'Костюничев Н.А.', 51, NULL, NULL, 1),
(803, 'Котова Анастасия Евгеньевна', 'Котова А.Е.', 58, NULL, NULL, 1),
(804, 'Кох Владислав Альбертович', 'Кох В.А.', 29, 7, 76, 6),
(805, 'Коханов Виктор Романович', 'Коханов В.Р.', 68, 7, 76, 6),
(806, 'Кочанков Илья Дмитриевич', 'Кочанков И.Д.', 52, 7, 76, 6),
(807, 'Кочергин Кирилл Алексеевич', 'Кочергин К.А.', 92, NULL, NULL, 1),
(808, 'Кочетков Иван Александрович', 'Кочетков И.А.', 63, NULL, NULL, 1),
(809, 'Кочин Иван Васильевич', 'Кочин И.В.', 67, NULL, NULL, 1),
(810, 'Кочубаев Дмитрий Андреевич', 'Кочубаев Д.А.', 77, NULL, NULL, 1),
(811, 'Кошелев Дмитрий Игоревич', 'Кошелев Д.И.', 51, NULL, NULL, 1),
(812, 'Кошта Эриксон Фернанду П Родригеш Да', 'Кошта Э.Ф.П.Р.Д.', 54, NULL, NULL, 1),
(813, 'Краличкин Владимир Васильевич', 'Краличкин В.В.', 51, NULL, NULL, 1),
(814, 'Красикова Екатерина Андреевна', 'Красикова Е.А.', 71, NULL, NULL, 1),
(815, 'Красильников Александр Владимирович', 'Красильников А.В.', 42, NULL, NULL, 1),
(816, 'Красильников Алексей Владимирович', 'Красильников А.В.', 52, 7, 76, 6),
(817, 'Красков Михаил Алексеевич', 'Красков М.А.', 77, NULL, NULL, 1),
(818, 'Крень Полина Сергеевна', 'Крень П.С.', 29, 7, 76, 6),
(819, 'Кривошеев Сергей Александрович', 'Кривошеев С.А.', 73, NULL, NULL, 1),
(820, 'Крокулев Тимофей Сергеевич', 'Крокулев Т.С.', 35, NULL, NULL, 1),
(821, 'Круглов Егор Олегович', 'Круглов Е.О.', 56, NULL, NULL, 1),
(822, 'Круглов Михаил Сергеевич', 'Круглов М.С.', 74, NULL, NULL, 1),
(823, 'Крыков Даниил Ильич', 'Крыков Д.И.', 62, NULL, NULL, 1),
(824, 'Крылов Сергей Сергеевич', 'Крылов С.С.', 44, NULL, NULL, 1),
(825, 'Крюков Дмитрий Алексеевич', 'Крюков Д.А.', 53, NULL, NULL, 1),
(826, 'Крюков Сергей Алексеевич', 'Крюков С.А.', 52, 7, 76, 6),
(827, 'Кудалин Роман Михайлович', 'Кудалин Р.М.', 37, NULL, NULL, 1),
(828, 'Кудрин Матвей Александрович', 'Кудрин М.А.', 75, NULL, NULL, 1),
(829, 'Кудрявцев Александр Сергеевич', 'Кудрявцев А.С.', 86, NULL, NULL, 1),
(830, 'Кудряшов Никита Сергеевич', 'Кудряшов Н.С.', 58, NULL, NULL, 1),
(831, 'Кужелев Антон Александрович', 'Кужелев А.А.', 48, NULL, NULL, 1),
(832, 'Кузенкова Анастасия Евгеньевна', 'Кузенкова А.Е.', 86, NULL, NULL, 1),
(833, 'Кузминский Иван Николаевич', 'Кузминский И.Н.', 64, NULL, NULL, 1),
(834, 'Кузнецов Александр Алексеевич', 'Кузнецов А.А.', 72, NULL, NULL, 1),
(835, 'Кузнецов Виктор Леонидович', 'Кузнецов В.Л.', 73, NULL, NULL, 1),
(836, 'Кузнецов Владислав Валерьевич', 'Кузнецов В.В.', 88, NULL, NULL, 1),
(837, 'Кузнецов Дмитрий Андреевич', 'Кузнецов Д.А.', 44, NULL, NULL, 1),
(838, 'Кузнецов Константин Николаевич', 'Кузнецов К.Н.', 71, NULL, NULL, 1),
(839, 'Кузнецов Никита Сергеевич', 'Кузнецов Н.С.', 83, NULL, NULL, 1),
(840, 'Кузнецова Алина Игоревна', 'Кузнецова А.И.', 58, NULL, NULL, 1),
(841, 'Кузнецова Анастасия Олеговна', 'Кузнецова А.О.', 69, NULL, NULL, 1),
(842, 'Кузнецова Мария Валерьевна', 'Кузнецова М.В.', 33, NULL, NULL, 1),
(843, 'Кузьмин Александр Сергеевич', 'Кузьмин А.С.', 64, NULL, NULL, 1),
(844, 'Кузьмин Георгий Михайлович', 'Кузьмин Г.М.', 92, NULL, NULL, 1),
(845, 'Кузьмичев Николай Александрович', 'Кузьмичев Н.А.', 34, NULL, NULL, 1),
(846, 'Кузьмичева Анна Игоревна', 'Кузьмичева А.И.', 83, NULL, NULL, 1),
(847, 'Кукушкина Ксения Олеговна', 'Кукушкина К.О.', 80, NULL, NULL, 1),
(848, 'Кулаков Роман Анатольевич', 'Кулаков Р.А.', 64, NULL, NULL, 1),
(849, 'Куландин Денис Сергеевич', 'Куландин Д.С.', 43, NULL, NULL, 1),
(850, 'Куленков Сергей Александрович', 'Куленков С.А.', 41, NULL, NULL, 1),
(851, 'Куликов Владислав Александрович', 'Куликов В.А.', 62, NULL, NULL, 1),
(852, 'Куликов Дмитрий Александрович', 'Куликов Д.А.', 92, NULL, NULL, 1),
(853, 'Куликов Дмитрий Сергеевич', 'Куликов Д.С.', 51, NULL, NULL, 1),
(854, 'Куликова Анастасия Сергеевна', 'Куликова А.С.', 62, NULL, NULL, 1),
(855, 'Куликова Светлана Александровна', 'Куликова С.А.', 76, NULL, NULL, 1),
(856, 'Кумбрасьев Марк Эдуардович', 'Кумбрасьев М.Э.', 61, NULL, NULL, 1),
(857, 'Кумбрасьев Павел Эдуардович', 'Кумбрасьев П.Э.', 26, NULL, NULL, 1),
(858, 'Кумин Алексей Александрович', 'Кумин А.А.', 47, NULL, NULL, 1),
(859, 'Кунампарамбил Асфа Латиф', 'Кунампарамбил А.Л.', 38, NULL, NULL, 1),
(860, 'Куприянов Иван Анатольевич', 'Куприянов И.А.', 42, NULL, NULL, 1),
(861, 'Куракин Михаил Александрович', 'Куракин М.А.', 66, NULL, NULL, 1),
(862, 'Курникова Анастасия Александровна', 'Курникова А.А.', 86, NULL, NULL, 1),
(863, 'Курылев Илья Алексеевич', 'Курылев И.А.', 65, NULL, NULL, 1),
(864, 'Кустова Анастасия Игоревна', 'Кустова А.И.', 52, 7, 76, 6),
(865, 'Кутняков Андрей Игоревич', 'Кутняков А.И.', 60, NULL, NULL, 1),
(866, 'Кутовой Вадим Николаевич', 'Кутовой В.Н.', 75, NULL, NULL, 1),
(867, 'Кучерявых Ян Вячеславович', 'Кучерявых Я.В.', 29, 7, 76, 6),
(868, 'Кхоза Тхабанг Эбенезер', 'Кхоза Т.Э.', 28, NULL, NULL, 1),
(869, 'Кяшкина Майя Александровна', 'Кяшкина М.А.', 74, NULL, NULL, 1),
(870, 'Ладыгин Дмитрий Витальевич', 'Ладыгин Д.В.', 83, NULL, NULL, 1),
(871, 'Лакшина Александра Романовна', 'Лакшина А.Р.', 62, NULL, NULL, 1),
(872, 'Лалетин Алексей Михайлович', 'Лалетин А.М.', 74, NULL, NULL, 1),
(873, 'Лалыкин Олег Вадимович', 'Лалыкин О.В.', 71, NULL, NULL, 1),
(874, 'Ланской Григорий Владимирович', 'Ланской Г.В.', 48, NULL, NULL, 1),
(875, 'Лаптев Владислав Вячеславович', 'Лаптев В.В.', 58, NULL, NULL, 1),
(876, 'Лапшин Семен Михайлович', 'Лапшин С.М.', 92, NULL, NULL, 1),
(877, 'Ларин Захар Александрович', 'Ларин З.А.', 74, NULL, NULL, 1),
(878, 'Лахраш Уссама', 'Лахраш У.', 25, NULL, NULL, 1),
(879, 'Лахтуров Владимир Сергеевич', 'Лахтуров В.С.', 27, NULL, NULL, 1),
(880, 'Лебедев Александр Дмитриевич', 'Лебедев А.Д.', 62, NULL, NULL, 1),
(881, 'Лебедев Александр Сергеевич', 'Лебедев А.С.', 59, NULL, NULL, 1),
(882, 'Лебедев Андрей Васильевич', 'Лебедев А.В.', 33, NULL, NULL, 1),
(883, 'Лебедев Вячеслав Александрович', 'Лебедев В.А.', 91, NULL, NULL, 1),
(884, 'Лебедев Дмитрий Артемович', 'Лебедев Д.А.', 45, NULL, NULL, 1),
(885, 'Лебедев Дмитрий Константинович', 'Лебедев Д.К.', 60, NULL, NULL, 1),
(886, 'Лебедев Дмитрий Сергеевич', 'Лебедев Д.С.', 82, NULL, NULL, 1),
(887, 'Лебедева Ксения Михайловна', 'Лебедева К.М.', 78, NULL, NULL, 1),
(888, 'Лебединский Даниил Игоревич', 'Лебединский Д.И.', 73, NULL, NULL, 1),
(889, 'Леванов Никита Алексеевич', 'Леванов Н.А.', 45, NULL, NULL, 1),
(890, 'Левицкий Илья Олегович', 'Левицкий И.О.', 91, NULL, NULL, 1),
(891, 'Левкин Антон Андреевич', 'Левкин А.А.', 76, NULL, NULL, 1),
(892, 'Левшин Денис Дмитриевич', 'Левшин Д.Д.', 30, NULL, NULL, 1),
(893, 'Леденцов Егор Максимович', 'Леденцов Е.М.', 36, NULL, NULL, 1),
(894, 'Лембриков Степан Андреевич', 'Лембриков С.А.', 80, NULL, NULL, 1),
(895, 'Лемяскина Евгения Сергеевна', 'Лемяскина Е.С.', 55, NULL, NULL, 1),
(896, 'Ленин Алексей Олегович', 'Ленин А.О.', 74, NULL, NULL, 1),
(897, 'Ленькин Вячеслав Александрович', 'Ленькин В.А.', 79, NULL, NULL, 1),
(898, 'Леонтьев Вадим Евгеньевич', 'Леонтьев В.Е.', 32, NULL, NULL, 1),
(899, 'Лепешкин Максим Сергеевич', 'Лепешкин М.С.', 35, NULL, NULL, 1),
(900, 'Леушкин Антон Дмитриевич', 'Леушкин А.Д.', 69, NULL, NULL, 1),
(901, 'Либориу Педру Канисиу Онголу', 'Либориу П.К.О.', 87, NULL, NULL, 1),
(902, 'Линёва Анна Александровна', 'Линёва А.А.', 88, NULL, NULL, 1),
(903, 'Липатов Игорь Дмитриевич', 'Липатов И.Д.', 76, NULL, NULL, 1),
(904, 'Лихолат Анна Николаевна', 'Лихолат А.Н.', 56, NULL, NULL, 1),
(905, 'Лобанов Андрей Игоревич', 'Лобанов А.И.', 37, NULL, NULL, 1),
(906, 'Лобов Александр Александрович', 'Лобов А.А.', 61, NULL, NULL, 1),
(907, 'Лобода Максим Дмитриевич', 'Лобода М.Д.', 42, NULL, NULL, 1),
(908, 'Ловчиков Владислав Алексеевич', 'Ловчиков В.А.', 72, NULL, NULL, 1),
(909, 'Логанов Андрей Сергеевич', 'Логанов А.С.', 43, NULL, NULL, 1),
(910, 'Логвиненко Александра Викторовна', 'Логвиненко А.В.', 26, NULL, NULL, 1),
(911, 'Логинов Владимир Александрович', 'Логинов В.А.', 62, NULL, NULL, 1),
(912, 'Ложеницин Сергей Дмитриевич', 'Ложеницин С.Д.', 35, NULL, NULL, 1),
(913, 'Ломакин Александр Сергеевич', 'Ломакин А.С.', 36, NULL, NULL, 1),
(914, 'Лонгеза Сержиу Мануэл Маконда', 'Лонгеза С.М.М.', 54, NULL, NULL, 1),
(915, 'Лопаева Татьяна Андреевна', 'Лопаева Т.А.', 72, NULL, NULL, 1),
(916, 'Лопатин Дмитрий Алексеевич', 'Лопатин Д.А.', 61, NULL, NULL, 1),
(917, 'Лугин Михаил Дмитриевич', 'Лугин М.Д.', 50, NULL, NULL, 1),
(918, 'Лудина Дарья Александровна', 'Лудина Д.А.', 83, NULL, NULL, 1),
(919, 'Лукичев Александр Александрович', 'Лукичев А.А.', 70, NULL, NULL, 1),
(920, 'Лукьянченко Иван Алексеевич', 'Лукьянченко И.А.', 43, NULL, NULL, 1),
(921, 'Лундолока Нсимба Филипе', 'Лундолока Н.Ф.', 54, NULL, NULL, 1),
(922, 'Лутьянов Сергей Владимирович', 'Лутьянов С.В.', 40, NULL, NULL, 1),
(923, 'Лыков Александр Андреевич', 'Лыков А.А.', 73, NULL, NULL, 1),
(924, 'Лысов Максим Андреевич', 'Лысов М.А.', 32, NULL, NULL, 1),
(925, 'Львова Алина Дмитриевна', 'Львова А.Д.', 76, NULL, NULL, 1),
(926, 'Лю Юаньхун', 'Лю Ю.', 75, NULL, NULL, 1),
(927, 'Любимов Артём Юрьевич', 'Любимов А.Ю.', 60, NULL, NULL, 1),
(928, 'Лютова Татьяна Сергеевна', 'Лютова Т.С.', 46, NULL, NULL, 1),
(929, 'Лямин Роман Александрович', 'Лямин Р.А.', 69, NULL, NULL, 1),
(930, 'Ляо Синь', 'Ляо С.', 27, NULL, NULL, 1),
(931, 'Маганга Шадрек', 'Маганга Ш.', 39, NULL, NULL, 1),
(932, 'Мазур Даниил Андреевич', 'Мазур Д.А.', 80, NULL, NULL, 1),
(933, 'Майоров Николай Евгеньевич', 'Майоров Н.Е.', 64, NULL, NULL, 1),
(934, 'Макаревич Алексей Дмитриевич', 'Макаревич А.Д.', 62, NULL, NULL, 1),
(935, 'Макарин Кирилл Андреевич', 'Макарин К.А.', 37, NULL, NULL, 1),
(936, 'Макарихин Семен Андреевич', 'Макарихин С.А.', 53, NULL, NULL, 1),
(937, 'Макаров Александр Андреевич', 'Макаров А.А.', 46, NULL, NULL, 1),
(938, 'Макаров Алексей Александрович', 'Макаров А.А.', 82, NULL, NULL, 1),
(939, 'Макаров Алексей Сергеевич', 'Макаров А.С.', 45, NULL, NULL, 1),
(940, 'Макаров Артем Александрович', 'Макаров А.А.', 61, NULL, NULL, 1),
(941, 'Макаров Константин Андреевич', 'Макаров К.А.', 59, NULL, NULL, 1),
(942, 'Макарова Анастасия Алексеевна', 'Макарова А.А.', 31, NULL, NULL, 1),
(943, 'Макарова Виктория Эдуардовна', 'Макарова В.Э.', 42, NULL, NULL, 1),
(944, 'Макарычев Сергей Дмитриевич', 'Макарычев С.Д.', 83, NULL, NULL, 1),
(945, 'Макеев Марат Вадимович', 'Макеев М.В.', 42, NULL, NULL, 1),
(946, 'Максименко Алексей Николаевич', 'Максименко А.Н.', 67, NULL, NULL, 1),
(947, 'Максимов Андрей Александрович', 'Максимов А.А.', 52, 7, 76, 6),
(948, 'Максимова Ирина Игоревна', 'Максимова И.И.', 80, NULL, NULL, 1),
(949, 'Макхубеду Рабохлале Нтандокайисе', 'Макхубеду Р.Н.', 25, NULL, NULL, 1),
(950, 'Малаинин Мохамед Амин', 'Малаинин М.А.', 25, NULL, NULL, 1),
(951, 'Малахова Анастасия Сергеевна', 'Малахова А.С.', 90, NULL, NULL, 1),
(952, 'Малеков Азат Равильевич', 'Малеков А.Р.', 32, NULL, NULL, 1),
(953, 'Малкин Данил Олегович', 'Малкин Д.О.', 26, NULL, NULL, 1),
(954, 'Малов Степан Александрович', 'Малов С.А.', 86, NULL, NULL, 1),
(955, 'Малова Татьяна Валерьевна', 'Малова Т.В.', 92, NULL, NULL, 1),
(956, 'Малышев Артём Евгеньевич', 'Малышев А.Е.', 27, NULL, NULL, 1),
(957, 'Мамонов Александр Евгеньевич', 'Мамонов А.Е.', 65, NULL, NULL, 1),
(958, 'Мамонов Дмитрий Анатольевич', 'Мамонов Д.А.', 30, NULL, NULL, 1),
(959, 'Маниике Сан Теренсе', 'Маниике С.Т.', 28, NULL, NULL, 1),
(960, 'Манило Иван Алексеевич', 'Манило И.А.', 49, NULL, NULL, 1),
(961, 'Мануэл Жилсон Агостинью', 'Мануэл Ж.А.', 87, NULL, NULL, 1),
(962, 'Маремьянин Иван Алексеевич', 'Маремьянин И.А.', 70, NULL, NULL, 1),
(963, 'Маркелов Владислав Вадимович', 'Маркелов В.В.', 36, NULL, NULL, 1),
(964, 'Маркина Елена Геннадьевна', 'Маркина Е.Г.', 60, NULL, NULL, 1),
(965, 'Маркина Наталья Сергеевна', 'Маркина Н.С.', 26, NULL, NULL, 1),
(966, 'Марков Александр Николаевич', 'Марков А.Н.', 35, NULL, NULL, 1),
(967, 'Маскина Юлия Владимировна', 'Маскина Ю.В.', 92, NULL, NULL, 1),
(968, 'Масуд Омар Махмуд Омар', 'Масуд О.М.О.', 25, NULL, NULL, 1),
(969, 'Матвеев Иван Сергеевич', 'Матвеев И.С.', 82, NULL, NULL, 1),
(970, 'Матвеев Станислав Сергеевич', 'Матвеев С.С.', 64, NULL, NULL, 1),
(971, 'Махов Павел Евгеньевич', 'Махов П.Е.', 68, 7, 76, 6),
(972, 'Махонина Юлия Владимировна', 'Махонина Ю.В.', 92, NULL, NULL, 1),
(973, 'Мацокин Даниил Сергеевич', 'Мацокин Д.С.', 60, NULL, NULL, 1),
(974, 'Машков Илья Владимирович', 'Машков И.В.', 31, NULL, NULL, 1),
(975, 'Маячкин Арсений Станиславович', 'Маячкин А.С.', 76, NULL, NULL, 1),
(976, 'Мбамбара Бенжамин Кондвани', 'Мбамбара Б.К.', 25, NULL, NULL, 1),
(977, 'Мегалаа Бимен Басем Якуб', 'Мегалаа Б.Б.Я.', 28, NULL, NULL, 1),
(978, 'Медведева Екатерина Сергеевна', 'Медведева Е.С.', 60, NULL, NULL, 1),
(979, 'Межевов Кирилл Сергеевич', 'Межевов К.С.', 62, NULL, NULL, 1),
(980, 'Мезина Маргарита Дмитриевна', 'Мезина М.Д.', 37, NULL, NULL, 1),
(981, 'Мельник Василий Александрович', 'Мельник В.А.', 59, NULL, NULL, 1),
(982, 'Мендоса_Мартинес Лаура_Исабель Эрнестовна', 'Мендоса_Мартинес Л.Э.', 33, NULL, NULL, 1),
(983, 'Меркушев Сергей Сергеевич', 'Меркушев С.С.', 27, NULL, NULL, 1),
(984, 'Метелица Марина Олеговна', 'Метелица М.О.', 68, 7, 76, 6),
(985, 'Механи Амр Мустафа Мохамед', 'Механи А.М.М.', 94, NULL, NULL, 1),
(986, 'Мешалкин Егор Сергеевич', 'Мешалкин Е.С.', 41, NULL, NULL, 1),
(987, 'Мешалкин Никита Александрович', 'Мешалкин Н.А.', 44, NULL, NULL, 1),
(988, 'Миа Мохаммад Шоджиб', 'Миа М.Ш.', 39, NULL, NULL, 1),
(989, 'Мизгирев Александр Владимирович', 'Мизгирев А.В.', 64, NULL, NULL, 1),
(990, 'Микрюков Денис Евгеньевич', 'Микрюков Д.Е.', 61, NULL, NULL, 1),
(991, 'Милюков Юрий Викторович', 'Милюков Ю.В.', 92, NULL, NULL, 1),
(992, 'Минеев Илья Евгеньевич', 'Минеев И.Е.', 47, NULL, NULL, 1),
(993, 'Миняжетдинова Лилия Алиевна', 'Миняжетдинова Л.А.', 77, NULL, NULL, 1),
(994, 'Миранюк Роман Олегович', 'Миранюк Р.О.', 84, NULL, NULL, 1),
(995, 'Миронова Евгения Александровна', 'Миронова Е.А.', 82, NULL, NULL, 1),
(996, 'Митрохин Денис Алексеевич', 'Митрохин Д.А.', 88, NULL, NULL, 1),
(997, 'Митягина Дарья Сергеевна', 'Митягина Д.С.', 80, NULL, NULL, 1),
(998, 'Михайлов Олег Русланович', 'Михайлов О.Р.', 51, NULL, NULL, 1),
(999, 'Михайлова Екатерина Сергеевна', 'Михайлова Е.С.', 44, NULL, NULL, 1),
(1000, 'Михалева Маргарита Александровна', 'Михалева М.А.', 70, NULL, NULL, 1),
(1001, 'Михеев Егор Вадимович', 'Михеев Е.В.', 33, NULL, NULL, 1),
(1002, 'Мицкевич Всеволод Станиславович', 'Мицкевич В.С.', 89, NULL, NULL, 1),
(1003, 'Мишанина Полина Владимировна', 'Мишанина П.В.', 64, NULL, NULL, 1),
(1004, 'Мишин Илья Александрович', 'Мишин И.А.', 86, NULL, NULL, 1),
(1005, 'Мишина Надежда Валерьевна', 'Мишина Н.В.', 43, NULL, NULL, 1),
(1006, 'Мишутин Андрей Сергеевич', 'Мишутин А.С.', 91, NULL, NULL, 1),
(1007, 'Моисеев Андрей Олегович', 'Моисеев А.О.', 33, NULL, NULL, 1),
(1008, 'Мокоана Джозеф Кготлелело', 'Мокоана Д.К.', 25, NULL, NULL, 1),
(1009, 'Мокоене Кеагиле', 'Мокоене К.', 39, NULL, NULL, 1),
(1010, 'Молоднякова Екатерина Александровна', 'Молоднякова Е.А.', 36, NULL, NULL, 1),
(1011, 'Молоткова Светлана Сергеевна', 'Молоткова С.С.', 46, NULL, NULL, 1),
(1012, 'Монтасир Мд Рубиате', 'Монтасир М.Р.', 28, NULL, NULL, 1),
(1013, 'Моришенков Денис Владимирович', 'Моришенков Д.В.', 27, NULL, NULL, 1),
(1014, 'Морковкин Андрей Сергеевич', 'Морковкин А.С.', 26, NULL, NULL, 1),
(1015, 'Морнова Екатерина Михайловна', 'Морнова Е.М.', 40, NULL, NULL, 1),
(1016, 'Морозов Владислав Дмитриевич', 'Морозов В.Д.', 40, NULL, NULL, 1),
(1017, 'Морозова Алина Андреевна', 'Морозова А.А.', 40, NULL, NULL, 1),
(1018, 'Морозова Диана Антоновна', 'Морозова Д.А.', 89, NULL, NULL, 1),
(1019, 'Морозова Евгения Викторовна', 'Морозова Е.В.', 88, NULL, NULL, 1),
(1020, 'Москалева Дарья Дмитриевна', 'Москалева Д.Д.', 40, NULL, NULL, 1),
(1021, 'Мосягина Алёна Алексеевна', 'Мосягина А.А.', 40, NULL, NULL, 1),
(1022, 'Мотлоква Моропане Эдвин', 'Мотлоква М.Э.', 28, NULL, NULL, 1),
(1023, 'Моторин Иван Михайлович', 'Моторин И.М.', 27, NULL, NULL, 1),
(1024, 'Мотсуми Принц Лебоганг', 'Мотсуми П.Л.', 38, NULL, NULL, 1),
(1025, 'Мочалова Анастасия Романовна', 'Мочалова А.Р.', 58, NULL, NULL, 1),
(1026, 'Мтетва Мбали Линдокухле', 'Мтетва М.Л.', 39, NULL, NULL, 1),
(1027, 'Мтетва Фила Мнкоби', 'Мтетва Ф.М.', 25, NULL, NULL, 1),
(1028, 'Муанза Жозеф Белчиор', 'Муанза Ж.Б.', 87, NULL, NULL, 1),
(1029, 'Муафо Фомеконг Руди Франк', 'Муафо Ф.Р.Ф.', 25, NULL, NULL, 1),
(1030, 'Мукарири Сеан', 'Мукарири С.', 25, NULL, NULL, 1),
(1031, 'Муниарари Ниаша', 'Муниарари Н.', 38, NULL, NULL, 1),
(1032, 'Муравьев Денис Александрович', 'Муравьев Д.А.', 75, NULL, NULL, 1),
(1033, 'Муравьев Никита Алексеевич', 'Муравьев Н.А.', 45, NULL, NULL, 1),
(1034, 'Мурзин Дмитрий Александрович', 'Мурзин Д.А.', 27, NULL, NULL, 1),
(1035, 'Мурунгвени Лиза Рувараше', 'Мурунгвени Л.Р.', 39, NULL, NULL, 1),
(1036, 'Мусолина Анастасия Владимировна', 'Мусолина А.В.', 88, NULL, NULL, 1),
(1037, 'Муссунгу Маркуш Андре', 'Муссунгу М.А.', 87, NULL, NULL, 1),
(1038, 'Мухин Дмитрий Александрович', 'Мухин Д.А.', 77, NULL, NULL, 1),
(1039, 'Мхланга Джэйсон Мелулеки Михаел', 'Мхланга Д.М.М.', 38, NULL, NULL, 1),
(1040, 'Мхлонго Клифе', 'Мхлонго К.', 28, NULL, NULL, 1),
(1041, 'Мышкин Андрей Александрович', 'Мышкин А.А.', 53, NULL, NULL, 1),
(1042, 'Мячев Анатолий Сергеевич', 'Мячев А.С.', 89, NULL, NULL, 1),
(1043, 'Набил Фархан', 'Набил Ф.', 28, NULL, NULL, 1),
(1044, 'Надин Василий Андреевич', 'Надин В.А.', 77, NULL, NULL, 1),
(1045, 'Надир Мохаммед', 'Надир М.', 61, NULL, NULL, 1),
(1046, 'Назаров Александр Андреевич', 'Назаров А.А.', 67, NULL, NULL, 1),
(1047, 'Назаров Владислав Валерьевич', 'Назаров В.В.', 53, NULL, NULL, 1),
(1048, 'Назаров Иван Юрьевич', 'Назаров И.Ю.', 57, NULL, NULL, 1),
(1049, 'Назаров Максим Андреевич', 'Назаров М.А.', 30, NULL, NULL, 1),
(1050, 'Налчаджян Самвел Араевич', 'Налчаджян С.А.', 33, NULL, NULL, 1),
(1051, 'Напылов Евгений Игоревич', 'Напылов Е.И.', 43, NULL, NULL, 1),
(1052, 'Наседкин Никита Дмитриевич', 'Наседкин Н.Д.', 29, 7, 76, 6),
(1053, 'Наури Уаэл', 'Наури У.', 25, NULL, NULL, 1),
(1054, 'Нгомане Вернард Шарбни', 'Нгомане В.Ш.', 25, NULL, NULL, 1),
(1055, 'Ндах Био Эз Рони Амисьель Камаэль', 'Ндах Б.Э.Р.А.К.', 68, 7, 76, 6),
(1056, 'Небукин Леонид Олегович', 'Небукин Л.О.', 73, NULL, NULL, 1),
(1057, 'Небукина Виктория Александровна', 'Небукина В.А.', 86, NULL, NULL, 1),
(1058, 'Невретдинов Радмир Русланович', 'Невретдинов Р.Р.', 83, NULL, NULL, 1),
(1059, 'Некравцев Алексей Александрович', 'Некравцев А.А.', 29, 7, 76, 6),
(1060, 'Неробова Анастасия Сергеевна', 'Неробова А.С.', 50, NULL, NULL, 1),
(1061, 'Нестеров Дмитрий Романович', 'Нестеров Д.Р.', 59, NULL, NULL, 1),
(1062, 'Нечаева Екатерина Владимировна', 'Нечаева Е.В.', 80, NULL, NULL, 1),
(1063, 'Нжо Волло Арно Ростан', 'Нжо В.А.Р.', 39, NULL, NULL, 1),
(1064, 'Никитин Александр Владимирович', 'Никитин А.В.', 77, NULL, NULL, 1),
(1065, 'Никитин Дмитрий Сергеевич', 'Никитин Д.С.', 91, NULL, NULL, 1),
(1066, 'Никифорова Екатерина Александровна', 'Никифорова Е.А.', 91, NULL, NULL, 1),
(1067, 'Николаев Владислав Андреевич', 'Николаев В.А.', 30, NULL, NULL, 1),
(1068, 'Николаев Денис Владимирович', 'Николаев Д.В.', 43, NULL, NULL, 1),
(1069, 'Николаев Денис Эминович', 'Николаев Д.Э.', 50, NULL, NULL, 1),
(1070, 'Николаенко Дарья Александровна', 'Николаенко Д.А.', 57, NULL, NULL, 1),
(1071, 'Никольский Кирилл Сергеевич', 'Никольский К.С.', 68, 7, 76, 6),
(1072, 'Никоноров Илья Дмитриевич', 'Никоноров И.Д.', 48, NULL, NULL, 1),
(1073, 'Ниронго Осман Мтенде', 'Ниронго О.М.', 28, NULL, NULL, 1),
(1074, 'Нифадьев Вадим Сергеевич', 'Нифадьев В.С.', 37, NULL, NULL, 1),
(1075, 'Нкака Мума', 'Нкака М.', 38, NULL, NULL, 1),
(1076, 'Новиков Данил Максимович', 'Новиков Д.М.', 52, 7, 76, 6),
(1077, 'Новиков Евгений Назирович', 'Новиков Е.Н.', 31, NULL, NULL, 1),
(1078, 'Новиков Илья Станиславович', 'Новиков И.С.', 65, NULL, NULL, 1),
(1079, 'Новикова Алиса Александровна', 'Новикова А.А.', 79, NULL, NULL, 1),
(1080, 'Новикова Валерия Константиновна', 'Новикова В.К.', 64, NULL, NULL, 1),
(1081, 'Новикова Екатерина Андреевна', 'Новикова Е.А.', 32, NULL, NULL, 1),
(1082, 'Новичков Юрий Александрович', 'Новичков Ю.А.', 41, NULL, NULL, 1),
(1083, 'Новожилова Екатерина Михайловна', 'Новожилова Е.М.', 43, NULL, NULL, 1),
(1084, 'Новоселова Екатерина Андреевна', 'Новоселова Е.А.', 76, NULL, NULL, 1),
(1085, 'Новрузов Ильяс Азадович', 'Новрузов И.А.', 92, NULL, NULL, 1),
(1086, 'Номерова Мария Алексеевна', 'Номерова М.А.', 31, NULL, NULL, 1),
(1087, 'Оболенский Арсений Андреевич', 'Оболенский А.А.', 49, NULL, NULL, 1),
(1088, 'Оганян Роберт Владимирович', 'Оганян Р.В.', 52, 7, 76, 6),
(1089, 'Огнев Денис Вячеславович', 'Огнев Д.В.', 29, 7, 76, 6),
(1090, 'Окмянский Андрей Владимирович', 'Окмянский А.В.', 53, NULL, NULL, 1),
(1091, 'Окунев Борис Павлович', 'Окунев Б.П.', 76, NULL, NULL, 1),
(1092, 'Олейник Максим Алексеевич', 'Олейник М.А.', 73, NULL, NULL, 1),
(1093, 'Омар Исмаил Матук Мохамед Бакр', 'Омар И.М.М.Б.', 25, NULL, NULL, 1),
(1094, 'Орлов Андрей Эдуардович', 'Орлов А.Э.', 64, NULL, NULL, 1),
(1095, 'Орлов Егор Олегович', 'Орлов Е.О.', 65, NULL, NULL, 1),
(1096, 'Орлов Никита Алексеевич', 'Орлов Н.А.', 44, NULL, NULL, 1),
(1097, 'Орлова Александра Николаевна', 'Орлова А.Н.', 36, NULL, NULL, 1),
(1098, 'Осипов Николай Алексеевич', 'Осипов Н.А.', 29, 7, 76, 6),
(1099, 'Осипова Марина Анатольевна', 'Осипова М.А.', 73, NULL, NULL, 1),
(1100, 'Осокин Захар Анатольевич', 'Осокин З.А.', 40, NULL, NULL, 1),
(1101, 'Остапович Денис Евгеньевич', 'Остапович Д.Е.', 80, NULL, NULL, 1),
(1102, 'Островский Илья Сергеевич', 'Островский И.С.', 50, NULL, NULL, 1),
(1103, 'Осунсакин Томива Мартинс', 'Осунсакин Т.М.', 25, NULL, NULL, 1),
(1104, 'Осхилим Чукууди', 'Осхилим Ч.', 38, NULL, NULL, 1),
(1105, 'Осягин Максим Александрович', 'Осягин М.А.', 76, NULL, NULL, 1),
(1106, 'Охлопкова Наталья Александровна', 'Охлопкова Н.А.', 36, NULL, NULL, 1),
(1107, 'Павлов Андрей Аркадьевич', 'Павлов А.А.', 61, NULL, NULL, 1),
(1108, 'Павлова Евгения Алексеевна', 'Павлова Е.А.', 69, NULL, NULL, 1),
(1109, 'Палутин Иван Александрович', 'Палутин И.А.', 78, NULL, NULL, 1),
(1110, 'Пальгуев Игорь Олегович', 'Пальгуев И.О.', 66, NULL, NULL, 1),
(1111, 'Панов Александр Алексеевич', 'Панов А.А.', 76, NULL, NULL, 1),
(1112, 'Панова Елена Анатольевна', 'Панова Е.А.', 71, NULL, NULL, 1),
(1113, 'Панова Ольга Романовна', 'Панова О.Р.', 43, NULL, NULL, 1),
(1114, 'Панферов Владислав Михайлович', 'Панферов В.М.', 51, NULL, NULL, 1),
(1115, 'Паранин Валерий Александрович', 'Паранин В.А.', 44, NULL, NULL, 1),
(1116, 'Параничев Денис Александрович', 'Параничев Д.А.', 30, NULL, NULL, 1),
(1117, 'Параничева Алена Владиславовна', 'Параничева А.В.', 46, NULL, NULL, 1),
(1118, 'Паршакова Ирина Владимировна', 'Паршакова И.В.', 91, NULL, NULL, 1),
(1119, 'Паршутина Александра Денисовна', 'Паршутина А.Д.', 75, NULL, NULL, 1),
(1120, 'Пасечник Никита Алексеевич', 'Пасечник Н.А.', 49, NULL, NULL, 1),
(1121, 'Пасухин Дмитрий Андреевич', 'Пасухин Д.А.', 29, 7, 76, 6),
(1122, 'Паузин Леонид Павлович', 'Паузин Л.П.', 53, NULL, NULL, 1),
(1123, 'Пахомов Илья Андреевич', 'Пахомов И.А.', 77, NULL, NULL, 1),
(1124, 'Педру Эма Оссианина Де Араужу', 'Педру Э.О.Д.А.', 87, NULL, NULL, 1),
(1125, 'Первушкин Олег Евгеньевич', 'Первушкин О.Е.', 84, NULL, NULL, 1),
(1126, 'Перов Дмитрий Алексеевич', 'Перов Д.А.', 47, NULL, NULL, 1),
(1127, 'Пертовский Александр Владиславович', 'Пертовский А.В.', 69, NULL, NULL, 1),
(1128, 'Перфилова Оксана Александровна', 'Перфилова О.А.', 74, NULL, NULL, 1),
(1129, 'Першин Александр Дмитриевич', 'Першин А.Д.', 56, NULL, NULL, 1),
(1130, 'Першина Анна Михайловна', 'Першина А.М.', 36, NULL, NULL, 1),
(1131, 'Пестреев Даниил Сергеевич', 'Пестреев Д.С.', 43, NULL, NULL, 1),
(1132, 'Пестриков Илья Дмитриевич', 'Пестриков И.Д.', 52, 7, 76, 6),
(1133, 'Петров Александр Сергеевич', 'Петров А.С.', 32, NULL, NULL, 1),
(1134, 'Петров Антон Олегович', 'Петров А.О.', 70, NULL, NULL, 1),
(1135, 'Петров Павел Владимирович', 'Петров П.В.', 86, NULL, NULL, 1),
(1136, 'Петрова Ольга Вячеславовна', 'Петрова О.В.', 30, NULL, NULL, 1),
(1137, 'Петрякова Галина Константиновна', 'Петрякова Г.К.', 88, NULL, NULL, 1),
(1138, 'Петухов Алексей Владимирович', 'Петухов А.В.', 50, NULL, NULL, 1),
(1139, 'Пигин Андрей Павлович', 'Пигин А.П.', 56, NULL, NULL, 1),
(1140, 'Пиликин Андрей Вадимович', 'Пиликин А.В.', 88, NULL, NULL, 1),
(1141, 'Пинаев Данил Алексеевич', 'Пинаев Д.А.', 49, NULL, NULL, 1),
(1142, 'Пискунов Александр Евгеньевич', 'Пискунов А.Е.', 65, NULL, NULL, 1),
(1143, 'Плаксин Владислав Евгеньевич', 'Плаксин В.Е.', 40, NULL, NULL, 1),
(1144, 'Платонов Алексей Алексеевич', 'Платонов А.А.', 59, NULL, NULL, 1),
(1145, 'Плеханов Владислав Николаевич', 'Плеханов В.Н.', 44, NULL, NULL, 1),
(1146, 'Плотников Артем Алексеевич', 'Плотников А.А.', 36, NULL, NULL, 1),
(1147, 'Плохов Евгений Николаевич', 'Плохов Е.Н.', 59, NULL, NULL, 1),
(1148, 'Повшедный Евгений Александрович', 'Повшедный Е.А.', 69, NULL, NULL, 1),
(1149, 'Подлесов Антон Алексеевич', 'Подлесов А.А.', 78, NULL, NULL, 1),
(1150, 'Подоплелов Кирилл Дмитриевич', 'Подоплелов К.Д.', 57, NULL, NULL, 1),
(1151, 'Подрядчиков Максим Сергеевич', 'Подрядчиков М.С.', 68, 7, 76, 6),
(1152, 'Подскребко Никита', 'Подскребко Н.', 29, 7, 76, 6),
(1153, 'Поздеева Варвара Владимировна', 'Поздеева В.В.', 37, NULL, NULL, 1),
(1154, 'Полегайко Виктор Андреевич', 'Полегайко В.А.', 50, NULL, NULL, 1),
(1155, 'Полетуева Анастасия Николаевна', 'Полетуева А.Н.', 80, NULL, NULL, 1),
(1156, 'Поликсенов Иван Алексеевич', 'Поликсенов И.А.', 58, NULL, NULL, 1),
(1157, 'Полина Наталья Владимировна', 'Полина Н.В.', 84, NULL, NULL, 1),
(1158, 'Половинкин Антон Николаевич', 'Половинкин А.Н.', 70, NULL, NULL, 1),
(1159, 'Полусмак Сергей Владимирович', 'Полусмак С.В.', 77, NULL, NULL, 1),
(1160, 'Пономарев Алексей Сергеевич', 'Пономарев А.С.', 75, NULL, NULL, 1),
(1161, 'Попов Артём Андреевич', 'Попов А.А.', 76, NULL, NULL, 1),
(1162, 'Попов Даниил Денисович', 'Попов Д.Д.', 86, NULL, NULL, 1),
(1163, 'Попов Иван Сергеевич', 'Попов И.С.', 30, NULL, NULL, 1),
(1164, 'Попов Станислав Валерьевич', 'Попов С.В.', 75, NULL, NULL, 1),
(1165, 'Пота Кабело Мдимени Джудас', 'Пота К.М.Д.', 25, NULL, NULL, 1),
(1166, 'Потаничев Роман Дмитриевич', 'Потаничев Р.Д.', 90, NULL, NULL, 1),
(1167, 'Потапов Сергей Александрович', 'Потапов С.А.', 92, NULL, NULL, 1),
(1168, 'Прокофьева Елизавета Александровна', 'Прокофьева Е.А.', 43, NULL, NULL, 1),
(1169, 'Пронин Игорь Павлович', 'Пронин И.П.', 46, NULL, NULL, 1),
(1170, 'Пронькин Дмитрий Александрович', 'Пронькин Д.А.', 46, NULL, NULL, 1),
(1171, 'Просвирнин Павел Михайлович', 'Просвирнин П.М.', 48, NULL, NULL, 1),
(1172, 'Протас Мария Павловна', 'Протас М.П.', 73, NULL, NULL, 1),
(1173, 'Прохорова Ольга Дмитриевна', 'Прохорова О.Д.', 32, NULL, NULL, 1),
(1174, 'Прыгин Владислав Алексеевич', 'Прыгин В.А.', 76, NULL, NULL, 1),
(1175, 'Прытов Денис Дмитриевич', 'Прытов Д.Д.', 26, NULL, NULL, 1),
(1176, 'Птицын Павел Алексеевич', 'Птицын П.А.', 68, 7, 76, 6),
(1177, 'Пудовкин Артем Владиславович', 'Пудовкин А.В.', 61, NULL, NULL, 1),
(1178, 'Пуртова Ксения Андреевна', 'Пуртова К.А.', 44, NULL, NULL, 1),
(1179, 'Пучкова Наталья Дмитриевна', 'Пучкова Н.Д.', 63, NULL, NULL, 1),
(1180, 'Пушкарев Дмитрий Сергеевич', 'Пушкарев Д.С.', 48, NULL, NULL, 1),
(1181, 'Пыпин Кирилл Максимович', 'Пыпин К.М.', 35, NULL, NULL, 1),
(1182, 'Пырков Константин Олегович', 'Пырков К.О.', 78, NULL, NULL, 1),
(1183, 'Раевская Екатерина Олеговна', 'Раевская Е.О.', 83, NULL, NULL, 1),
(1184, 'Раевский Евгений Александрович', 'Раевский Е.А.', 60, NULL, NULL, 1),
(1185, 'Разживина Анастасия Александровна', 'Разживина А.А.', 86, NULL, NULL, 1),
(1186, 'Разумова Мария Дмитриевна', 'Разумова М.Д.', 37, NULL, NULL, 1),
(1187, 'Ракуть Таисия Дмитриевна', 'Ракуть Т.Д.', 31, NULL, NULL, 1),
(1188, 'Ратов Святослав Дмитриевич', 'Ратов С.Д.', 29, 7, 76, 6),
(1189, 'Рафикова Юлия Евгеньевна', 'Рафикова Ю.Е.', 60, NULL, NULL, 1),
(1190, 'Рачин Игорь Александрович', 'Рачин И.А.', 52, 7, 76, 6),
(1191, 'Ревенко Елена Витальевна', 'Ревенко Е.В.', 77, NULL, NULL, 1),
(1192, 'Резанцев Сергей Алексеевич', 'Резанцев С.А.', 80, NULL, NULL, 1),
(1193, 'Резвинский Артемий Александрович', 'Резвинский А.А.', 42, NULL, NULL, 1),
(1194, 'Реззуки Абделбассет', 'Реззуки А.', 38, NULL, NULL, 1),
(1195, 'Ремез Максим Александрович', 'Ремез М.А.', 86, NULL, NULL, 1),
(1196, 'Репин Владимир Игоревич', 'Репин В.И.', 47, NULL, NULL, 1),
(1197, 'Решетилов Роман Николаевич', 'Решетилов Р.Н.', 34, NULL, NULL, 1),
(1198, 'Родимков Юрий Александрович', 'Родимков Ю.А.', 26, NULL, NULL, 1),
(1199, 'Родионов Денис Михайлович', 'Родионов Д.М.', 50, NULL, NULL, 1),
(1200, 'Родионов Павел Александрович', 'Родионов П.А.', 33, NULL, NULL, 1),
(1201, 'Родионов Федор Андреевич', 'Родионов Ф.А.', 71, NULL, NULL, 1),
(1202, 'Родионова Дарья Алексеевна', 'Родионова Д.А.', 26, NULL, NULL, 1),
(1203, 'Роман Даниил Валерьевич', 'Роман Д.В.', 89, NULL, NULL, 1),
(1204, 'Романов Александр Анатольевич', 'Романов А.А.', 71, NULL, NULL, 1),
(1205, 'Романов Алексей Вадимович', 'Романов А.В.', 60, NULL, NULL, 1),
(1206, 'Романюк Сергей Сергеевич', 'Романюк С.С.', 43, NULL, NULL, 1),
(1207, 'Рубцова Мария Сергеевна', 'Рубцова М.С.', 63, NULL, NULL, 1),
(1208, 'Рубцова Татьяна Владимировна', 'Рубцова Т.В.', 36, NULL, NULL, 1),
(1209, 'Руденко Валерий Олегович', 'Руденко В.О.', 95, NULL, NULL, 1),
(1210, 'Рудой Владислав Русланович', 'Рудой В.Р.', 61, NULL, NULL, 1),
(1211, 'Румянцева Ольга Сергеевна', 'Румянцева О.С.', 61, NULL, NULL, 1),
(1212, 'Румянцева Полина Сергеевна', 'Румянцева П.С.', 56, NULL, NULL, 1),
(1213, 'Рустамов Азер Заур оглы', 'Рустамов А.З.О.', 46, NULL, NULL, 1),
(1214, 'Рухович Игорь Владимирович', 'Рухович И.В.', 52, 7, 76, 6),
(1215, 'Рыбакова Екатерина Михайловна', 'Рыбакова Е.М.', 51, NULL, NULL, 1),
(1216, 'Рыбкин Антон Васильевич', 'Рыбкин А.В.', 33, NULL, NULL, 1),
(1217, 'Рыбкин Роман Андреевич', 'Рыбкин Р.А.', 50, NULL, NULL, 1),
(1218, 'Рылов Александр Дмитриевич', 'Рылов А.Д.', 86, NULL, NULL, 1),
(1219, 'Рябинина Дарья Алексеевна', 'Рябинина Д.А.', 77, NULL, NULL, 1),
(1220, 'Рябова Алена Дмитриевна', 'Рябова А.Д.', 49, NULL, NULL, 1),
(1221, 'Рябушева Ирина Владимировна', 'Рябушева И.В.', 50, NULL, NULL, 1),
(1222, 'Рябчикова Валерия Георгиевна', 'Рябчикова В.Г.', 92, NULL, NULL, 1),
(1223, 'Рязанов Дмитрий Александрович', 'Рязанов Д.А.', 68, 7, 76, 6),
(1224, 'Савин Дмитрий Владимирович', 'Савин Д.В.', 70, NULL, NULL, 1),
(1225, 'Савкин Юрий Борисович', 'Савкин Ю.Б.', 49, NULL, NULL, 1),
(1226, 'Савосина Александра Дмитриевна', 'Савосина А.Д.', 53, NULL, NULL, 1),
(1227, 'Садиков Артем Сергеевич', 'Садиков А.С.', 66, NULL, NULL, 1),
(1228, 'Сазанов Дмитрий Евгеньевич', 'Сазанов Д.Е.', 66, NULL, NULL, 1),
(1229, 'Сазонова Елена Михайловна', 'Сазонова Е.М.', 45, NULL, NULL, 1),
(1230, 'Саиед Ахмед', 'Саиед А.', 87, NULL, NULL, 1),
(1231, 'Салех Ахмед Мохамед Галал Солиман', 'Салех А.М.Г.С.', 28, NULL, NULL, 1),
(1232, 'Салех Фалих Мохаммед Салех', 'Салех Ф.М.С.', 28, NULL, NULL, 1),
(1233, 'Самакембе Жорже Ваата Лоложи', 'Самакембе Ж.В.Л.', 54, NULL, NULL, 1),
(1234, 'Самарина Анастасия Сергеевна', 'Самарина А.С.', 73, NULL, NULL, 1),
(1235, 'Самаркина Валерия Вадимовна', 'Самаркина В.В.', 88, NULL, NULL, 1),
(1236, 'Сандалов Константин Юрьевич', 'Сандалов К.Ю.', 52, 7, 76, 6),
(1237, 'Санников Илья Александрович', 'Санников И.А.', 90, NULL, NULL, 1),
(1238, 'Санникова Виктория Олеговна', 'Санникова В.О.', 76, NULL, NULL, 1),
(1239, 'Саркхот Аваис Абрар', 'Саркхот А.А.', 25, NULL, NULL, 1),
(1240, 'Сарыкова Анастасия Александровна', 'Сарыкова А.А.', 85, NULL, NULL, 1),
(1241, 'Сарынин Сергей Валерьевич', 'Сарынин С.В.', 32, NULL, NULL, 1),
(1242, 'Сафи Мохамед', 'Сафи М.', 39, NULL, NULL, 1),
(1243, 'Сафонов Семен Евгеньевич', 'Сафонов С.Е.', 36, NULL, NULL, 1),
(1244, 'Сафронов Никита Сергеевич', 'Сафронов Н.С.', 68, 7, 76, 6),
(1245, 'Свинарёва Юлия Васильевна', 'Свинарёва Ю.В.', 73, NULL, NULL, 1),
(1246, 'Сегрушни Зухаир', 'Сегрушни З.', 42, NULL, NULL, 1),
(1247, 'Седова Александра Игоревна', 'Седова А.И.', 31, NULL, NULL, 1),
(1248, 'Селезнева Ольга Александровна', 'Селезнева О.А.', 59, NULL, NULL, 1),
(1249, 'Семаев Игнат Александрович', 'Семаев И.А.', 77, NULL, NULL, 1),
(1250, 'Семанда Джордан Квентин', 'Семанда Д.К.', 28, NULL, NULL, 1),
(1251, 'Семенов Илья Анатольевич', 'Семенов И.А.', 27, NULL, NULL, 1),
(1252, 'Семенова Екатерина Евгеньевна', 'Семенова Е.Е.', 84, NULL, NULL, 1),
(1253, 'Семенюта Егор Юрьевич', 'Семенюта Е.Ю.', 33, NULL, NULL, 1),
(1254, 'Сенина Анастасия Александровна', 'Сенина А.А.', 71, NULL, NULL, 1),
(1255, 'Сергеев Александр Александрович', 'Сергеев А.А.', 34, NULL, NULL, 1),
(1256, 'Сергеев Алексей Петрович', 'Сергеев А.П.', 76, NULL, NULL, 1),
(1257, 'Середнев Денис Михайлович', 'Середнев Д.М.', 83, NULL, NULL, 1),
(1258, 'Серков Владимир Максимович', 'Серков В.М.', 60, NULL, NULL, 1),
(1259, 'Сермяжко Елизавета Александровна', 'Сермяжко Е.А.', 86, NULL, NULL, 1),
(1260, 'Серобян Нарек Ширазович', 'Серобян Н.Ш.', 48, NULL, NULL, 1),
(1261, 'Серышев Александр Павлович', 'Серышев А.П.', 70, NULL, NULL, 1),
(1262, 'Серяков Андрей Сергеевич', 'Серяков А.С.', 33, NULL, NULL, 1),
(1263, 'Сетати Сепелонг', 'Сетати С.', 28, NULL, NULL, 1),
(1264, 'Сигачев Антон Владимирович', 'Сигачев А.В.', 83, NULL, NULL, 1),
(1265, 'Сигурова Анастасия Дмитриевна', 'Сигурова А.Д.', 59, NULL, NULL, 1),
(1266, 'Сидорова Александра Константиновна', 'Сидорова А.К.', 46, NULL, NULL, 1),
(1267, 'Сизов Илья Игоревич', 'Сизов И.И.', 50, NULL, NULL, 1),
(1268, 'Сизов Кирилл Дмитриевич', 'Сизов К.Д.', 58, NULL, NULL, 1),
(1269, 'Силенко Дмитрий Игоревич', 'Силенко Д.И.', 80, NULL, NULL, 1),
(1270, 'Силин Михаил Николаевич', 'Силин М.Н.', 91, NULL, NULL, 1),
(1271, 'Синицина Мария Сергеевна', 'Синицина М.С.', 53, NULL, NULL, 1),
(1272, 'Синицын Никита Максимович', 'Синицын Н.М.', 81, NULL, NULL, 1),
(1273, 'Сиротин Дмитрий Валерьевич', 'Сиротин Д.В.', 44, NULL, NULL, 1),
(1274, 'Сироткин Евгений Сергеевич', 'Сироткин Е.С.', 89, NULL, NULL, 1),
(1275, 'Сироткина Дарья Александровна', 'Сироткина Д.А.', 86, NULL, NULL, 1),
(1276, 'Ситкин Дмитрий Андреевич', 'Ситкин Д.А.', 75, NULL, NULL, 1),
(1277, 'Ситников Максим Дмитриевич', 'Ситников М.Д.', 74, NULL, NULL, 1),
(1278, 'Скороделов Михаил Сергеевич', 'Скороделов М.С.', 35, NULL, NULL, 1),
(1279, 'Скрипаль Андрей Дмитриевич', 'Скрипаль А.Д.', 43, NULL, NULL, 1),
(1280, 'Скулкина Надежда Сергеевна', 'Скулкина Н.С.', 31, NULL, NULL, 1),
(1281, 'Скуридин Юрий Алексеевич', 'Скуридин Ю.А.', 31, NULL, NULL, 1),
(1282, 'Слащева Яна Владиславовна', 'Слащева Я.В.', 64, NULL, NULL, 1),
(1283, 'Слепов Константин Дмитриевич', 'Слепов К.Д.', 68, 7, 76, 6),
(1284, 'Смертин Дмитрий Сергеевич', 'Смертин Д.С.', 55, NULL, NULL, 1),
(1285, 'Сметанин Данила Алексеевич', 'Сметанин Д.А.', 49, NULL, NULL, 1),
(1286, 'Смирнов Александр Иванович', 'Смирнов А.И.', 33, NULL, NULL, 1),
(1287, 'Смирнов Артём Александрович', 'Смирнов А.А.', 51, NULL, NULL, 1),
(1288, 'Смирнов Руслан Олегович', 'Смирнов Р.О.', 33, NULL, NULL, 1),
(1289, 'Смирнова Анастасия Евгеньевна', 'Смирнова А.Е.', 90, NULL, NULL, 1),
(1290, 'Смирнова Марина Сергеевна', 'Смирнова М.С.', 56, NULL, NULL, 1),
(1291, 'Снегирев Василий Владимирович', 'Снегирев В.В.', 77, NULL, NULL, 1),
(1292, 'Соболева Юлия Алексеевна', 'Соболева Ю.А.', 80, NULL, NULL, 1),
(1293, 'Созинов Алексей Павлович', 'Созинов А.П.', 52, 7, 76, 6),
(1294, 'Сокова Анастасия Алексеевна', 'Сокова А.А.', 53, NULL, NULL, 1),
(1295, 'Соколов Андрей Дмитриевич', 'Соколов А.Д.', 80, NULL, NULL, 1),
(1296, 'Соколов Григорий Вадимович', 'Соколов Г.В.', 65, NULL, NULL, 1),
(1297, 'Соколов Николай Александрович', 'Соколов Н.А.', 70, NULL, NULL, 1),
(1298, 'Соколова Екатерина Андреевна', 'Соколова Е.А.', 92, NULL, NULL, 1),
(1299, 'Соколова Мария Сергеевна', 'Соколова М.С.', 35, NULL, NULL, 1),
(1300, 'Соколова Татьяна Артёмовна', 'Соколова Т.А.', 85, NULL, NULL, 1),
(1301, 'Соколюк Никита Александрович', 'Соколюк Н.А.', 66, NULL, NULL, 1),
(1302, 'Солнцева Дарья Александровна', 'Солнцева Д.А.', 62, NULL, NULL, 1),
(1303, 'Соловьев Александр Юрьевич', 'Соловьев А.Ю.', 46, NULL, NULL, 1),
(1304, 'Соловьев Алексей Владимирович', 'Соловьев А.В.', 34, NULL, NULL, 1),
(1305, 'Соловьев Никита Александрович', 'Соловьев Н.А.', 73, NULL, NULL, 1),
(1306, 'Соловьева Ксения Максимовна', 'Соловьева К.М.', 91, NULL, NULL, 1),
(1307, 'Соловьева Татьяна Александровна', 'Соловьева Т.А.', 31, NULL, NULL, 1),
(1308, 'Соломахин Сергей Александрович', 'Соломахин С.А.', 68, 7, 76, 6),
(1309, 'Солуянов Алексей Александрович', 'Солуянов А.А.', 56, NULL, NULL, 1),
(1310, 'Сорокин Андрей Андреевич', 'Сорокин А.А.', 64, NULL, NULL, 1),
(1311, 'Сорокин Владислав Константинович', 'Сорокин В.К.', 89, NULL, NULL, 1),
(1312, 'Сорокин Максим Леонидович', 'Сорокин М.Л.', 61, NULL, NULL, 1),
(1313, 'Сорокина Мария Игоревна', 'Сорокина М.И.', 62, NULL, NULL, 1),
(1314, 'Сорокоумов Никита Леонидович', 'Сорокоумов Н.Л.', 90, NULL, NULL, 1),
(1315, 'Сорочинский Андрей Сергеевич', 'Сорочинский А.С.', 41, NULL, NULL, 1),
(1316, 'Софийский Алексей Владиславович', 'Софийский А.В.', 57, NULL, NULL, 1),
(1317, 'Спирин Георгий Александрович', 'Спирин Г.А.', 84, NULL, NULL, 1),
(1318, 'Ставров Данила Игоревич', 'Ставров Д.И.', 48, NULL, NULL, 1),
(1319, 'Старкин Михаил Дмитриевич', 'Старкин М.Д.', 44, NULL, NULL, 1),
(1320, 'Степанова Екатерина Сергеевна', 'Степанова Е.С.', 26, NULL, NULL, 1),
(1321, 'Стойичевич Душан', 'Стойичевич Д.', 65, NULL, NULL, 1),
(1322, 'Стойчева Дарья Дмитриевна', 'Стойчева Д.Д.', 68, 7, 76, 6),
(1323, 'Столярова Дарья Дмитриевна', 'Столярова Д.Д.', 74, NULL, NULL, 1),
(1324, 'Стояков Властимир', 'Стояков В.', 81, NULL, NULL, 1),
(1325, 'Стрельникова Надежда Олеговна', 'Стрельникова Н.О.', 62, NULL, NULL, 1),
(1326, 'Стрельцов Роман Александрович', 'Стрельцов Р.А.', 42, NULL, NULL, 1),
(1327, 'Стрельцова Яна Дмитриевна', 'Стрельцова Я.Д.', 46, NULL, NULL, 1),
(1328, 'Ступин Александр Алексеевич', 'Ступин А.А.', 42, NULL, NULL, 1),
(1329, 'Суареш Ошмар Паулу Порфириу', 'Суареш О.П.П.', 39, NULL, NULL, 1),
(1330, 'Суворов Кирилл Александрович', 'Суворов К.А.', 89, NULL, NULL, 1),
(1331, 'Суворова Ангелина Алексеевна', 'Суворова А.А.', 62, NULL, NULL, 1),
(1332, 'Сударский Евгений Сергеевич', 'Сударский Е.С.', 69, NULL, NULL, 1),
(1333, 'Султан Махмуд Ахмед Абдельхай Ибрагим', 'Султан М.А.А.И.', 68, 7, 76, 6),
(1334, 'Сумин Игорь Владимирович', 'Сумин И.В.', 44, NULL, NULL, 1),
(1335, 'Сунцов Сергей Игоревич', 'Сунцов С.И.', 91, NULL, NULL, 1),
(1336, 'Суровова Александра Николаевна', 'Суровова А.Н.', 60, NULL, NULL, 1),
(1337, 'Суслов Егор Игоревич', 'Суслов Е.И.', 80, NULL, NULL, 1),
(1338, 'Суслов Никита Алексеевич', 'Суслов Н.А.', 90, NULL, NULL, 1),
(1339, 'Суслов Никита Сергеевич', 'Суслов Н.С.', 30, NULL, NULL, 1),
(1340, 'Сухачев Сергей Андреевич', 'Сухачев С.А.', 31, NULL, NULL, 1),
(1341, 'Сучков Макар Андреевич', 'Сучков М.А.', 70, NULL, NULL, 1),
(1342, 'Сырцева Алина Валерьевна', 'Сырцева А.В.', 88, NULL, NULL, 1),
(1343, 'Сысоев Егор Алексеевич', 'Сысоев Е.А.', 41, NULL, NULL, 1),
(1344, 'Сычев Никита Маркович', 'Сычев Н.М.', 44, NULL, NULL, 1),
(1345, 'Табат Абделфеттах', 'Табат А.', 25, NULL, NULL, 1),
(1346, 'Тактаев Артем Андреевич', 'Тактаев А.А.', 46, NULL, NULL, 1),
(1347, 'Таламанов Анатолий Евгеньевич', 'Таламанов А.Е.', 76, NULL, NULL, 1),
(1348, 'Танский Юрий Игоревич', 'Танский Ю.И.', 53, NULL, NULL, 1),
(1349, 'Таппа Нукими Франк', 'Таппа Н.Ф.', 42, NULL, NULL, 1),
(1350, 'Тараканов Кирилл Сергеевич', 'Тараканов К.С.', 76, NULL, NULL, 1),
(1351, 'Таранов Николай Геннадьевич', 'Таранов Н.Г.', 74, NULL, NULL, 1),
(1352, 'Тарасенков Владислав Алексеевич', 'Тарасенков В.А.', 26, NULL, NULL, 1),
(1353, 'Тарасов Василий Дмитриевич', 'Тарасов В.Д.', 90, NULL, NULL, 1),
(1354, 'Тарасов Никита Олегович', 'Тарасов Н.О.', 70, NULL, NULL, 1),
(1355, 'Тарасов Олег Александрович', 'Тарасов О.А.', 67, NULL, NULL, 1),
(1356, 'Тарасова Татьяна Александровна', 'Тарасова Т.А.', 29, 7, 76, 6),
(1357, 'Тареев Даниил Андреевич', 'Тареев Д.А.', 61, NULL, NULL, 1),
(1358, 'Таширев Иван Эдуардович', 'Таширев И.Э.', 83, NULL, NULL, 1),
(1359, 'Темирбулатов Руслан Маратович', 'Темирбулатов Р.М.', 29, 7, 76, 6),
(1360, 'Терентьев Константин Альфредович', 'Терентьев К.А.', 36, NULL, NULL, 1),
(1361, 'Тимакин Никита Евгеньевич', 'Тимакин Н.Е.', 56, NULL, NULL, 1),
(1362, 'Тимофеев Евгений Викторович', 'Тимофеев Е.В.', 68, 7, 76, 6),
(1363, 'Тимуркаева Лилия Маратовна', 'Тимуркаева Л.М.', 63, NULL, NULL, 1),
(1364, 'Типанов Даниил Борисович', 'Типанов Д.Б.', 37, NULL, NULL, 1),
(1365, 'Тиханов Илья Николаевич', 'Тиханов И.Н.', 32, NULL, NULL, 1),
(1366, 'Тихомиров Сергей Николаевич', 'Тихомиров С.Н.', 36, NULL, NULL, 1),
(1367, 'Тихомирова Мария Андреевна', 'Тихомирова М.А.', 53, NULL, NULL, 1),
(1368, 'Тихонов Дмитрий Сергеевич', 'Тихонов Д.С.', 83, NULL, NULL, 1),
(1369, 'Ткачев Алексей Игоревич', 'Ткачев А.И.', 43, NULL, NULL, 1),
(1370, 'Ткаченко Виталий Алексеевич', 'Ткаченко В.А.', 61, NULL, NULL, 1),
(1371, 'Толоконцева Марина Валерьевна', 'Толоконцева М.В.', 45, NULL, NULL, 1),
(1372, 'Толоконцева Наталия Валерьевна', 'Толоконцева Н.В.', 76, NULL, NULL, 1),
(1373, 'Толстиков Максим Викторович', 'Толстиков М.В.', 75, NULL, NULL, 1),
(1374, 'Тонков Алексей Евгеньевич', 'Тонков А.Е.', 66, NULL, NULL, 1),
(1375, 'Торопов Дмитрий Николаевич', 'Торопов Д.Н.', 40, NULL, NULL, 1),
(1376, 'Тохтин Сергей Александрович', 'Тохтин С.А.', 66, NULL, NULL, 1),
(1377, 'Тренина Елизавета Романовна', 'Тренина Е.Р.', 49, NULL, NULL, 1),
(1378, 'Третьяков Алексей Андреевич', 'Третьяков А.А.', 49, NULL, NULL, 1),
(1379, 'Трифонов Александр Николаевич', 'Трифонов А.Н.', 91, NULL, NULL, 1),
(1380, 'Тронин Дмитрий Валерьевич', 'Тронин Д.В.', 52, 7, 76, 6),
(1381, 'Трофимов Владимир Александрович', 'Трофимов В.А.', 68, 7, 76, 6),
(1382, 'Трубина Анастасия Владимировна', 'Трубина А.В.', 26, NULL, NULL, 1),
(1383, 'Трыкин Александр Михайлович', 'Трыкин А.М.', 26, NULL, NULL, 1),
(1384, 'Тужилкина Анастасия Андреевна', 'Тужилкина А.А.', 75, NULL, NULL, 1),
(1385, 'Турков Дмитрий Игоревич', 'Турков Д.И.', 31, NULL, NULL, 1),
(1386, 'Туругульдинов Игорь Сергеевич', 'Туругульдинов И.С.', 77, NULL, NULL, 1),
(1387, 'Тюрин Олег Александрович', 'Тюрин О.А.', 31, NULL, NULL, 1),
(1388, 'Тюрина Дарья Дмитриевна', 'Тюрина Д.Д.', 33, NULL, NULL, 1),
(1389, 'Тюрмина Александра Николаевна', 'Тюрмина А.Н.', 83, NULL, NULL, 1),
(1390, 'Улитин Александр Владимирович', 'Улитин А.В.', 88, NULL, NULL, 1),
(1391, 'Усманова Валерия Александровна', 'Усманова В.А.', 30, NULL, NULL, 1),
(1392, 'Усова Анастасия Игоревна', 'Усова А.И.', 91, NULL, NULL, 1),
(1393, 'Усова Марина Андреевна', 'Усова М.А.', 71, NULL, NULL, 1),
(1394, 'Уткин Андрей Максимович', 'Уткин А.М.', 66, NULL, NULL, 1),
(1395, 'Уткин Константин Дмитриевич', 'Уткин К.Д.', 49, NULL, NULL, 1),
(1396, 'Уткин Сергей Константинович', 'Уткин С.К.', 35, NULL, NULL, 1),
(1397, 'Утов Роман Александрович', 'Утов Р.А.', 83, NULL, NULL, 1),
(1398, 'Утшо Мохибул Момен', 'Утшо М.М.', 42, NULL, NULL, 1),
(1399, 'Ухина Алла Александровна', 'Ухина А.А.', 55, NULL, NULL, 1),
(1400, 'Фадеева Александра Максимовна', 'Фадеева А.М.', 32, NULL, NULL, 1),
(1401, 'Фане Сюрпрайз Мафиа', 'Фане С.М.', 28, NULL, NULL, 1),
(1402, 'Фаури Абдулла', 'Фаури А.', 87, NULL, NULL, 1),
(1403, 'Федоренко Андрей Игоревич', 'Федоренко А.И.', 53, NULL, NULL, 1),
(1404, 'Фёдоров Валентин Александрович', 'Фёдоров В.А.', 51, NULL, NULL, 1),
(1405, 'Федоров Игорь Владимирович', 'Федоров И.В.', 67, NULL, NULL, 1),
(1406, 'Федоров Никита Михайлович', 'Федоров Н.М.', 27, NULL, NULL, 1),
(1407, 'Федоров Олег Андреевич', 'Федоров О.А.', 36, NULL, NULL, 1),
(1408, 'Федорова Ольга Павловна', 'Федорова О.П.', 76, NULL, NULL, 1),
(1409, 'Федосеев Алексей Геннадьевич', 'Федосеев А.Г.', 84, NULL, NULL, 1),
(1410, 'Федосеев Валерий Александрович', 'Федосеев В.А.', 83, NULL, NULL, 1),
(1411, 'Федотов Владислав Игоревич', 'Федотов В.И.', 49, NULL, NULL, 1),
(1412, 'Федотов Николай Алексеевич', 'Федотов Н.А.', 85, NULL, NULL, 1),
(1413, 'Федотова Екатерина Александровна', 'Федотова Е.А.', 32, NULL, NULL, 1),
(1414, 'Фирсов Дмитрий Сергеевич', 'Фирсов Д.С.', 85, NULL, NULL, 1),
(1415, 'Фомин Евгений Максимович', 'Фомин Е.М.', 35, NULL, NULL, 1),
(1416, 'Фонотов Владислав Сергеевич', 'Фонотов В.С.', 33, NULL, NULL, 1),
(1417, 'Фролов Андрей Сергеевич', 'Фролов А.С.', 70, NULL, NULL, 1),
(1418, 'Фролова Ольга Сергеевна', 'Фролова О.С.', 52, 7, 76, 6),
(1419, 'Футин Даниил Евгеньевич', 'Футин Д.Е.', 44, NULL, NULL, 1),
(1420, 'Хаифи Ашраф Зин Эддин', 'Хаифи А.З.Э.', 25, NULL, NULL, 1),
(1421, 'Хайрутдинов Эмиль Шамилевич', 'Хайрутдинов Э.Ш.', 50, NULL, NULL, 1),
(1422, 'Хана Мэтью Хану Эмил', 'Хана М.Х.Э.', 28, NULL, NULL, 1),
(1423, 'Харфауи Сами', 'Харфауи С.', 28, NULL, NULL, 1),
(1424, 'Харченко Иван Дмитриевич', 'Харченко И.Д.', 42, NULL, NULL, 1),
(1425, 'Харь Наталия Игоревна', 'Харь Н.И.', 31, NULL, NULL, 1),
(1426, 'Хасан Мд Мехеди', 'Хасан М.М.', 39, NULL, NULL, 1),
(1427, 'Хасан Мд Саид', 'Хасан М.С.', 39, NULL, NULL, 1),
(1428, 'Хассан Эззалдин Хешам Саеед', 'Хассан Э.Х.С.', 61, NULL, NULL, 1),
(1429, 'Хахалев Максим Владимирович', 'Хахалев М.В.', 90, NULL, NULL, 1),
(1430, 'Хватов Александр Евгеньевич', 'Хватов А.Е.', 59, NULL, NULL, 1),
(1431, 'Хисматулина Карина Фаритовна', 'Хисматулина К.Ф.', 29, 7, 76, 6),
(1432, 'Хлопин Илья Вячеславович', 'Хлопин И.В.', 65, NULL, NULL, 1),
(1433, 'Хлопцев Никита Алексеевич', 'Хлопцев Н.А.', 69, NULL, NULL, 1),
(1434, 'Ходалев Антон Павлович', 'Ходалев А.П.', 84, NULL, NULL, 1),
(1435, 'Ходалева Елизавета Владимировна', 'Ходалева Е.В.', 84, NULL, NULL, 1),
(1436, 'Холобко Владимир Алексеевич', 'Холобко В.А.', 42, NULL, NULL, 1),
(1437, 'Холькин Сергей Денисович', 'Холькин С.Д.', 30, NULL, NULL, 1),
(1438, 'Хоменко Алексей Игоревич', 'Хоменко А.И.', 71, NULL, NULL, 1),
(1439, 'Хорькин Алексей Сергеевич', 'Хорькин А.С.', 32, NULL, NULL, 1),
(1440, 'Хорькин Дмитрий Сергеевич', 'Хорькин Д.С.', 32, NULL, NULL, 1),
(1441, 'Хохидра Елизавета Васильевна', 'Хохидра Е.В.', 27, NULL, NULL, 1),
(1442, 'Хохлова Анна Станиславовна', 'Хохлова А.С.', 70, NULL, NULL, 1),
(1443, 'Хромов Никита Алексеевич', 'Хромов Н.А.', 35, NULL, NULL, 1),
(1444, 'Хуссейн Элияс Али', 'Хуссейн Э.А.', 38, NULL, NULL, 1),
(1445, 'Цаплина Алина Романовна', 'Цаплина А.Р.', 57, NULL, NULL, 1),
(1446, 'Царев Иван Сергеевич', 'Царев И.С.', 78, NULL, NULL, 1),
(1447, 'Цветков Максим Сергеевич', 'Цветков М.С.', 44, NULL, NULL, 1),
(1448, 'Цветов Артем Денисович', 'Цветов А.Д.', 33, NULL, NULL, 1),
(1449, 'Цепляева Анастасия Андреевна', 'Цепляева А.А.', 91, NULL, NULL, 1),
(1450, 'Цирулев Александр Сергеевич', 'Цирулев А.С.', 55, NULL, NULL, 1),
(1451, 'Цыбряев Роман Александрович', 'Цыбряев Р.А.', 51, NULL, NULL, 1),
(1452, 'Цыкунова Екатерина Александровна', 'Цыкунова Е.А.', 78, NULL, NULL, 1),
(1453, 'Цюпило Богдан Алексеевич', 'Цюпило Б.А.', 70, NULL, NULL, 1),
(1454, 'Чавкин Андрей Олегович', 'Чавкин А.О.', 34, NULL, NULL, 1),
(1455, 'Чандивана Мемори', 'Чандивана М.', 38, NULL, NULL, 1),
(1456, 'Чванов Леонид Леонидович', 'Чванов Л.Л.', 37, NULL, NULL, 1),
(1457, 'Чебыкина Александра Михайловна', 'Чебыкина А.М.', 47, NULL, NULL, 1),
(1458, 'Чекоданов Александр Витальевич', 'Чекоданов А.В.', 63, NULL, NULL, 1),
(1459, 'Чекушин Игорь Андреевич', 'Чекушин И.А.', 26, NULL, NULL, 1),
(1460, 'Челогузов Иван Андреевич', 'Челогузов И.А.', 66, NULL, NULL, 1),
(1461, 'Чемуру Панаше Бенджамин', 'Чемуру П.Б.', 25, NULL, NULL, 1),
(1462, 'Ченкова Анастасия Александровна', 'Ченкова А.А.', 58, NULL, NULL, 1),
(1463, 'Черненко Валерий Николаевич', 'Черненко В.Н.', 37, NULL, NULL, 1),
(1464, 'Черников Кирилл Владимирович', 'Черников К.В.', 62, NULL, NULL, 1),
(1465, 'Чернов Михаил Владимирович', 'Чернов М.В.', 31, NULL, NULL, 1),
(1466, 'Черноголовиков Владислав Дмитриевич', 'Черноголовиков В.Д.', 89, NULL, NULL, 1),
(1467, 'Черных Дарья Андреевна', 'Черных Д.А.', 33, NULL, NULL, 1),
(1468, 'Чернышов Денис Дмитриевич', 'Чернышов Д.Д.', 41, NULL, NULL, 1),
(1469, 'Чернышов Дмитрий Сергеевич', 'Чернышов Д.С.', 90, NULL, NULL, 1),
(1470, 'Черняев Ярослав Евгеньевич', 'Черняев Я.Е.', 34, NULL, NULL, 1),
(1471, 'Черняк Дарья Петровна', 'Черняк Д.П.', 26, NULL, NULL, 1),
(1472, 'Ческина Наталья Михайловна', 'Ческина Н.М.', 63, NULL, NULL, 1),
(1473, 'Чеснов Роман Юрьевич', 'Чеснов Р.Ю.', 52, 7, 76, 6),
(1474, 'Чесноков Артём Андреевич', 'Чесноков А.А.', 52, 7, 76, 6),
(1475, 'Чесноков Данил Андреевич', 'Чесноков Д.А.', 90, NULL, NULL, 1),
(1476, 'Чеченин Владислав Алексеевич', 'Чеченин В.А.', 60, NULL, NULL, 1),
(1477, 'Чигарев Дмитрий Владиславович', 'Чигарев Д.В.', 30, NULL, NULL, 1),
(1478, 'Чижов Кирилл Владимирович', 'Чижов К.В.', 59, NULL, NULL, 1),
(1479, 'Чикмарёв Илья Валерьевич', 'Чикмарёв И.В.', 63, NULL, NULL, 1),
(1480, 'Чиликин Денис Дмитриевич', 'Чиликин Д.Д.', 92, NULL, NULL, 1),
(1481, 'Чирков Роман Дмитриевич', 'Чирков Р.Д.', 53, NULL, NULL, 1),
(1482, 'Чиркова Ирина Викторовна', 'Чиркова И.В.', 89, NULL, NULL, 1),
(1483, 'Чистов Владимир Маркович', 'Чистов В.М.', 43, NULL, NULL, 1),
(1484, 'Чистяков Антон Максимович', 'Чистяков А.М.', 91, NULL, NULL, 1),
(1485, 'Чистяков Евгений Сергеевич', 'Чистяков Е.С.', 95, NULL, NULL, 1),
(1486, 'Чистякова Анна Владимировна', 'Чистякова А.В.', 77, NULL, NULL, 1),
(1487, 'Чиу Сок Вай', 'Чиу С.В.', 25, NULL, NULL, 1),
(1488, 'Чохан Веер Партап Сингх', 'Чохан В.П.С.', 39, NULL, NULL, 1),
(1489, 'Чувашов Артём Юрьевич', 'Чувашов А.Ю.', 29, 7, 76, 6),
(1490, 'Чугунов Никита Витальевич', 'Чугунов Н.В.', 40, NULL, NULL, 1),
(1491, 'Чураков Сергей Сергеевич', 'Чураков С.С.', 37, NULL, NULL, 1),
(1492, 'Чурдалев Артем Денисович', 'Чурдалев А.Д.', 84, NULL, NULL, 1),
(1493, 'Чурилов Егор Владимирович', 'Чурилов Е.В.', 60, NULL, NULL, 1),
(1494, 'Чучков Александр Сергеевич', 'Чучков А.С.', 45, NULL, NULL, 1),
(1495, 'Шагов Максим Алексеевич', 'Шагов М.А.', 46, NULL, NULL, 1),
(1496, 'Шайкх Насим Али', 'Шайкх Н.А.', 39, NULL, NULL, 1),
(1497, 'Шайхмагомедов Руслан Ибрагимович', 'Шайхмагомедов Р.И.', 41, NULL, NULL, 1),
(1498, 'Шакиров Илья Ренатович', 'Шакиров И.Р.', 42, NULL, NULL, 1),
(1499, 'Шамаева Елизавета Юрьевна', 'Шамаева Е.Ю.', 50, NULL, NULL, 1),
(1500, 'Шамина Мария Геннадьевна', 'Шамина М.Г.', 92, NULL, NULL, 1),
(1501, 'Шапиро Максим Дмитриевич', 'Шапиро М.Д.', 48, NULL, NULL, 1),
(1502, 'Шарипов Эмиль Маратович', 'Шарипов Э.М.', 86, NULL, NULL, 1),
(1503, 'Шаров Александр Дмитриевич', 'Шаров А.Д.', 68, 7, 76, 6),
(1504, 'Шаров Иван Александрович', 'Шаров И.А.', 71, NULL, NULL, 1),
(1505, 'Шаронов Михаил Михайлович', 'Шаронов М.М.', 36, NULL, NULL, 1),
(1506, 'Шаронова Ирина Игоревна', 'Шаронова И.И.', 85, NULL, NULL, 1),
(1507, 'Шахнов Андрей Владимирович', 'Шахнов А.В.', 62, NULL, NULL, 1),
(1508, 'Шашкин Евгений Вадимович', 'Шашкин Е.В.', 80, NULL, NULL, 1),
(1509, 'Шведов Кирилл Сергеевич', 'Шведов К.С.', 47, NULL, NULL, 1),
(1510, 'Швец Дмитрий Сергеевич', 'Швец Д.С.', 34, NULL, NULL, 1),
(1511, 'Швецова Анастасия Дмитриевна', 'Швецова А.Д.', 45, NULL, NULL, 1),
(1512, 'Шеин Илья Дмитриевич', 'Шеин И.Д.', 69, NULL, NULL, 1),
(1513, 'Шеметов Илья Михайлович', 'Шеметов И.М.', 35, NULL, NULL, 1),
(1514, 'Шеметов Филипп Алексеевич', 'Шеметов Ф.А.', 66, NULL, NULL, 1),
(1515, 'Шерстнев Дмитрий Денисович', 'Шерстнев Д.Д.', 42, NULL, NULL, 1),
(1516, 'Шерстнев Илья Игоревич', 'Шерстнев И.И.', 46, NULL, NULL, 1),
(1517, 'Шеховцова Наталья Владимировна', 'Шеховцова Н.В.', 27, NULL, NULL, 1),
(1518, 'Шибаева Ирина Вячеславовна', 'Шибаева И.В.', 58, NULL, NULL, 1),
(1519, 'Шикуло Алексей Антонович', 'Шикуло А.А.', 92, NULL, NULL, 1),
(1520, 'Шилов Антон Игоревич', 'Шилов А.И.', 60, NULL, NULL, 1),
(1521, 'Шимин Данила Александрович', 'Шимин Д.А.', 45, NULL, NULL, 1),
(1522, 'Шипулев Алексей Алексеевич', 'Шипулев А.А.', 68, 7, 76, 6),
(1523, 'Широков Дмитрий Сергеевич', 'Широков Д.С.', 41, NULL, NULL, 1),
(1524, 'Широков Сергей Александрович', 'Широков С.А.', 48, NULL, NULL, 1),
(1525, 'Ширяева Алена Александровна', 'Ширяева А.А.', 72, NULL, NULL, 1),
(1526, 'Шишкин Данила Сергеевич', 'Шишкин Д.С.', 74, NULL, NULL, 1),
(1527, 'Шкенев Петр Александрович', 'Шкенев П.А.', 76, NULL, NULL, 1),
(1528, 'Шкерин Игорь Владимирович', 'Шкерин И.В.', 56, NULL, NULL, 1),
(1529, 'Шкилева Ксения Николаевна', 'Шкилева К.Н.', 26, NULL, NULL, 1),
(1530, 'Шмиголь Кира Сергеевна', 'Шмиголь К.С.', 35, NULL, NULL, 1),
(1531, 'Шмуклер Валерия Ильинична', 'Шмуклер В.И.', 82, NULL, NULL, 1),
(1532, 'Шорина Евгения Михайловна', 'Шорина Е.М.', 73, NULL, NULL, 1),
(1533, 'Шпилев Андрей Алексеевич', 'Шпилев А.А.', 83, NULL, NULL, 1),
(1534, 'Шрамек Максим Александрович', 'Шрамек М.А.', 58, NULL, NULL, 1),
(1535, 'Шувалова Анастасия Игоревна', 'Шувалова А.И.', 36, NULL, NULL, 1),
(1536, 'Шулешов Павел Николаевич', 'Шулешов П.Н.', 52, 7, 76, 6),
(1537, 'Шульман Егор Алексеевич', 'Шульман Е.А.', 29, 7, 76, 6),
(1538, 'Шурыгин Дмитрий Владимирович', 'Шурыгин Д.В.', 92, NULL, NULL, 1),
(1539, 'Шурыгин Дмитрий Геннадьевич', 'Шурыгин Д.Г.', 71, NULL, NULL, 1),
(1540, 'Шурыгина Татьяна Сергеевна', 'Шурыгина Т.С.', 82, NULL, NULL, 1),
(1541, 'Шутина Светлана Юрьевна', 'Шутина С.Ю.', 91, NULL, NULL, 1),
(1542, 'Шушкевич Виталий Алексеевич', 'Шушкевич В.А.', 77, NULL, NULL, 1),
(1543, 'Щекотилова Юлия Алексеевна', 'Щекотилова Ю.А.', 83, NULL, NULL, 1),
(1544, 'Щелянскова Анастасия Ильинична', 'Щелянскова А.И.', 29, 7, 76, 6),
(1545, 'Щербаков Федор Юрьевич', 'Щербаков Ф.Ю.', 31, NULL, NULL, 1),
(1546, 'Щербакова Антонина Андреевна', 'Щербакова А.А.', 60, NULL, NULL, 1),
(1547, 'Щукина Анна Евгеньевна', 'Щукина А.Е.', 76, NULL, NULL, 1),
(1548, 'Щурупов Иван Владимирович', 'Щурупов И.В.', 63, NULL, NULL, 1),
(1549, 'Эисагирре Барреда Пауль Андре', 'Эисагирре Б.П.А.', 25, NULL, NULL, 1),
(1550, 'Эль Жанати Адам', 'Эль Ж.А.', 39, NULL, NULL, 1),
(1551, 'Эль Фахли Марием', 'Эль Ф.М.', 68, 7, 76, 6),
(1552, 'Эс-Себайи Уссама', 'Эс-Себайи У.', 25, NULL, NULL, 1),
(1553, 'Юдина Мария Владимировна', 'Юдина М.В.', 27, NULL, NULL, 1),
(1554, 'Юдина Олеся Евгеньевна', 'Юдина О.Е.', 65, NULL, NULL, 1),
(1555, 'Юдинцева Анна Игоревна', 'Юдинцева А.И.', 45, NULL, NULL, 1),
(1556, 'Юнин Иван Вячеславович', 'Юнин И.В.', 47, NULL, NULL, 1),
(1557, 'Юрин Станислав Иванович', 'Юрин С.И.', 53, NULL, NULL, 1),
(1558, 'Юрков Максим Евгеньевич', 'Юрков М.Е.', 45, NULL, NULL, 1),
(1559, 'Юрьева Валерия Юрьевна', 'Юрьева В.Ю.', 34, NULL, NULL, 1),
(1560, 'Юссеф Абделрахман Мохамед Халаф', 'Юссеф А.М.Х.', 28, NULL, NULL, 1),
(1561, 'Юсупов Абдулдиан Альбертович', 'Юсупов А.А.', 27, NULL, NULL, 1),
(1562, 'Яковлев Глеб Данилович', 'Яковлев Г.Д.', 90, NULL, NULL, 1),
(1563, 'Яковлев Денис Леонидович', 'Яковлев Д.Л.', 75, NULL, NULL, 1),
(1564, 'Яковлев Павел Сергеевич', 'Яковлев П.С.', 47, NULL, NULL, 1),
(1565, 'Ямщиков Иван Сергеевич', 'Ямщиков И.С.', 49, NULL, NULL, 1),
(1566, 'Ясакова Анастасия Евгеньевна', 'Ясакова А.Е.', 53, NULL, NULL, 1),
(1567, 'Яшин Егор Олегович', 'Яшин Е.О.', 83, NULL, NULL, 1),
(1568, 'Яшков Владислав Вадимович', 'Яшков В.В.', 67, NULL, NULL, 1);

-- 
-- Вывод данных для таблицы tabl_practice
--
INSERT INTO tabl_practice VALUES
(1, 3, '2018-09-01', '2018-12-31', 1, 9, 1),
(2, 2, '2018-09-01', '2018-12-31', 1, 11, 2),
(3, 2, '2018-09-01', '2018-12-31', 8, 9, 1),
(4, 2, '2018-09-01', '2018-12-31', 8, 11, 2),
(5, 2, '2018-09-01', '2018-12-31', 2, 9, 1),
(6, 2, '2018-09-01', '2018-12-31', 2, 11, 2),
(7, 2, '2018-09-01', '2018-12-31', 9, 9, 1),
(8, 2, '2018-09-01', '2018-12-31', 9, 11, 2),
(9, 2, '2018-09-01', '2018-12-31', 10, 9, 1),
(10, 3, '2018-09-01', '2018-12-31', 10, 9, 1),
(11, 3, '2018-09-01', '2018-12-31', 10, 11, 2),
(12, 3, '2018-09-01', '2018-12-31', 10, 11, 2),
(13, 2, '2018-09-01', '2018-12-31', 10, 11, 2),
(14, 3, '2018-09-01', '2018-12-31', 4, 9, 1),
(15, 2, '2018-09-01', '2018-12-31', 4, 9, 1),
(16, 3, '2018-09-01', '2018-12-31', 4, 11, 2),
(17, 3, '2018-09-01', '2018-12-31', 4, 11, 2),
(18, 2, '2018-09-01', '2018-12-31', 4, 11, 2),
(19, 3, '2018-09-01', '2018-12-31', 5, 9, 1),
(20, 2, '2018-09-01', '2018-12-31', 5, 9, 1),
(21, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(22, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(23, 3, '2018-09-01', '2018-12-31', 5, 11, 2),
(24, 2, '2018-09-01', '2018-12-31', 13, 9, 1),
(25, 2, '2018-09-01', '2018-12-31', 13, 11, 2),
(26, 2, '2018-09-01', '2018-12-31', 14, 9, 1),
(27, 2, '2018-09-01', '2018-12-31', 14, 11, 2),
(28, 2, '2018-09-01', '2018-12-31', NULL, 9, 1),
(29, 2, '2018-09-01', '2018-12-31', NULL, 9, 1),
(30, 2, '2018-09-01', '2018-12-31', 7, 9, 1),
(31, 2, '2018-09-01', '2018-12-31', 7, 11, 2);

-- 
-- Восстановить предыдущий режим SQL (SQL mode)
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Включение внешних ключей
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;