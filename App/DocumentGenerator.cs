﻿using App.Properties;
using Client.MakePracticeDocs;
using Client.Settings;
using Client.Start;
using Client.UICore;
using DatabaseConnector;
using System;
using System.Windows.Forms;
using Client.ImportExcelToBase;

namespace App
{
    /// <summary>
    /// Основная форма.
    /// </summary>
    public partial class DocumentGenerator : Form
    {
        private readonly AppEnvironment _environment;

        /// <summary>
        /// Конструктор.
        /// </summary>
        public DocumentGenerator()
        {
            InitializeComponent();
            VisualEnvironment.Set(ref browserControl, ref ribbonControl);
            VisualEnvironment.Browser.AddTab(new StartPageProcess(_environment));
            VisualEnvironment.Ribbon.AddButton(MakeInsertList());
            VisualEnvironment.Ribbon.AddButton(ImportToBase());
            AddCaptionToSettingButton();
        }

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public DocumentGenerator(AppEnvironment environment) : this()
        {
            _environment = environment;
        }

        private void AddCaptionToSettingButton()
        {
            var caption = "Настройки";
            var toolTip = new ToolTip {InitialDelay = 500};

            toolTip.SetToolTip(SettingButton, caption);
        }

        private RibbonButton MakeInsertList()
        {
            var makeInsertListButton = new RibbonButton("MakeDocs");

            makeInsertListButton.SetName("Сформировать документы практик");
            makeInsertListButton.SetCaption("Формирует документы практик");
            makeInsertListButton.SetImage(Resources.Word_x24);
            makeInsertListButton.Execute = MakeInsetListButtonExecute;

            return makeInsertListButton;
        }

        private RibbonButton ImportToBase()
        {
            var inmportToBaseButton = new RibbonButton("ImportToBase");

            inmportToBaseButton.SetName("Импортировать данные в базу");
            inmportToBaseButton.SetCaption("Импортирует Excel данные в базу");
            inmportToBaseButton.SetImage(Resources.Excel_To_Base_x24);
            inmportToBaseButton.Execute = ImportToBaseButtonExecute;

            return inmportToBaseButton;
        }

        private void MakeInsetListButtonExecute(object sender, EventArgs e)
        {
            if (!VisualEnvironment.Ribbon.GetRibbonButton("MakeDocs").Active)
            {
                var userProcess = new MakePracticeDocsProcess(_environment);
                VisualEnvironment.Browser.AddTab(userProcess);
            }
        }

        private void ImportToBaseButtonExecute(object sender, EventArgs e)
        {
            if (!VisualEnvironment.Ribbon.GetRibbonButton("ImportToBase").Active)
            {
                var userProcess = new ImportExcelToBaseProcess(_environment);
                VisualEnvironment.Browser.AddTab(userProcess);
            }
        }

        private void DocumentGenerator_FormClosing(object sender, FormClosingEventArgs e)
        {
            _environment.Destroy();
            VisualEnvironment.Browser.Controls.Clear();
        }

        private void SettingButton_MouseEnter(object sender, EventArgs e)
        {
            SettingButton.Image = Resources.SettingsOver_x32;
        }

        private void SettingButton_MouseLeave(object sender, EventArgs e)
        {
            SettingButton.Image = Resources.Settings_x32;
        }

        private void SettingButton_Click(object sender, EventArgs e)
        {
            SettingButton.Image = Resources.SettingsOver_x32;
            SettingButton.MouseLeave -= SettingButton_MouseLeave;
            var settingsForm = new SettingsForm(_environment);
            var dialogResult = settingsForm.ShowDialog();
            if (dialogResult == DialogResult.OK ||
                dialogResult == DialogResult.Cancel)
            {
                SettingButton.Image = Resources.Settings_x32;
            }
            SettingButton.MouseLeave += SettingButton_MouseLeave;
        }

        private void DocumentGenerator_FormClosed(object sender, FormClosedEventArgs e)
        {
            _environment.Destroy();
            VisualEnvironment.Browser.Controls.Clear();
        }
    }
}
