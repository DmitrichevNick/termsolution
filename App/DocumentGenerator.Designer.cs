﻿using Client.UICore;

namespace App
{
    partial class DocumentGenerator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DocumentGenerator));
            this.SettingButton = new System.Windows.Forms.Button();
            this.ribbonControl = new Client.UICore.RibbonControl();
            this.browserControl = new Client.UICore.BrowserControl();
            this.SuspendLayout();
            // 
            // SettingButton
            // 
            this.SettingButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingButton.BackColor = System.Drawing.Color.White;
            this.SettingButton.FlatAppearance.BorderSize = 0;
            this.SettingButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SettingButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SettingButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SettingButton.ForeColor = System.Drawing.Color.Transparent;
            this.SettingButton.Image = global::App.Properties.Resources.Settings_x32;
            this.SettingButton.Location = new System.Drawing.Point(748, 2);
            this.SettingButton.Name = "SettingButton";
            this.SettingButton.Size = new System.Drawing.Size(34, 37);
            this.SettingButton.TabIndex = 4;
            this.SettingButton.UseVisualStyleBackColor = false;
            this.SettingButton.Click += new System.EventHandler(this.SettingButton_Click);
            this.SettingButton.MouseEnter += new System.EventHandler(this.SettingButton_MouseEnter);
            this.SettingButton.MouseLeave += new System.EventHandler(this.SettingButton_MouseLeave);
            // 
            // ribbonControl
            // 
            this.ribbonControl.BackColor = System.Drawing.Color.White;
            this.ribbonControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ribbonControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.ribbonControl.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl.Name = "ribbonControl";
            this.ribbonControl.RibbonVisible = true;
            this.ribbonControl.Size = new System.Drawing.Size(784, 100);
            this.ribbonControl.TabIndex = 8;
            // 
            // browserControl
            // 
            this.browserControl.BackColor = System.Drawing.Color.White;
            this.browserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browserControl.ForeColor = System.Drawing.Color.Black;
            this.browserControl.Location = new System.Drawing.Point(0, 100);
            this.browserControl.Name = "browserControl";
            this.browserControl.Size = new System.Drawing.Size(784, 511);
            this.browserControl.TabIndex = 9;
            // 
            // DocumentGenerator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 611);
            this.Controls.Add(this.browserControl);
            this.Controls.Add(this.SettingButton);
            this.Controls.Add(this.ribbonControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 650);
            this.Name = "DocumentGenerator";
            this.Text = "Автоматизированная система генерации документации";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DocumentGenerator_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DocumentGenerator_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button SettingButton;
        private RibbonControl ribbonControl;
        private BrowserControl browserControl;
    }
}

