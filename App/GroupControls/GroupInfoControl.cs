﻿using Entity;
using Manager;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DatabaseConnector;

namespace App.GroupControls
{
    public partial class GroupInfoControl : UserControl
    {
        private bool _active;
        private GroupEntity _groupEntity;
        private EduProfileEntity _eduProfileEntity;
        private EduDirectionEntity _eduDirectionEntity;
        private List<StudentEntity> _studentEntities;
        private PracticeEntity _practiceEntity;
        private AppEnvironment _environment;
        public GroupInfoControl()
        {
            InitializeComponent();
            _active = false;
        }

        public bool IsActive
        {
            get { return _active; }
        }

        public void SetEnvironment(AppEnvironment environment)
        {
            _environment = environment;
        }
        public void SetGroup(decimal? id)
        {
            var groupManager = new GroupManager(_environment);
            _groupEntity = groupManager.Read(id);

            var eduProfileManager = new EduProfileManager(_environment);
            _eduProfileEntity = eduProfileManager.Read(_groupEntity?.IdEduProfile);

            var eduDirectionManager = new EduDirectionManager(_environment);
            _eduDirectionEntity = eduDirectionManager.Read(_groupEntity?.IdEduDirection);

            var studentManager = new StudentManager(_environment);
            _studentEntities = studentManager.ReadAll().Where(item => item.IdGroup == _groupEntity.Id).ToList();

            var practiceManager = new PracticeManager(_environment);
            _practiceEntity = practiceManager.ReadAll().First(item =>
                item.IdEduDirection == _groupEntity.IdEduDirection &&
                item.IdEduProfile == _groupEntity.IdEduProfile &&
                item.Course == _groupEntity.Course);

            UpdateInfo();
        }

        public void UpdateInfo()
        {
            groupName.Text = "Группа:  " + _groupEntity.Number;
            course.Text =  _groupEntity.Course + " курс";
            studentCount.Text = "Студентов:  " + _studentEntities.Count;
            level.Text = "Уровень подготовки:  " + (_practiceEntity.Level == "М" ? "магистратура" : "бакалавриат");
            direction.Text = "Направление:  " + _eduDirectionEntity.Name;
            profile.Text = "Профиль:  " + _eduProfileEntity.Name;
        }

        public void GroupInfoControl_Click(object sender, EventArgs e)
        {
            _active = !_active;
            if (_active)
            {
                BackColor = Color.Aquamarine;
            }
            else
            {
                BackColor = SystemColors.ControlLight;
            }
        }

        public decimal? GroupId
        {
            get { return _groupEntity.Id; }
        }
    }
}
