﻿namespace App.GroupControls
{
    partial class GroupInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupName = new System.Windows.Forms.Label();
            this.studentCount = new System.Windows.Forms.Label();
            this.course = new System.Windows.Forms.Label();
            this.level = new System.Windows.Forms.Label();
            this.direction = new System.Windows.Forms.Label();
            this.profile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // groupName
            // 
            this.groupName.AutoSize = true;
            this.groupName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupName.Location = new System.Drawing.Point(16, 10);
            this.groupName.Name = "groupName";
            this.groupName.Size = new System.Drawing.Size(148, 18);
            this.groupName.TabIndex = 0;
            this.groupName.Text = "Группа: 999999x9-99";
            // 
            // studentCount
            // 
            this.studentCount.AutoSize = true;
            this.studentCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.studentCount.Location = new System.Drawing.Point(444, 10);
            this.studentCount.Name = "studentCount";
            this.studentCount.Size = new System.Drawing.Size(106, 18);
            this.studentCount.TabIndex = 1;
            this.studentCount.Text = "Студентов: 99";
            // 
            // course
            // 
            this.course.AutoSize = true;
            this.course.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.course.Location = new System.Drawing.Point(270, 10);
            this.course.Name = "course";
            this.course.Size = new System.Drawing.Size(51, 18);
            this.course.TabIndex = 2;
            this.course.Text = "9 курс";
            // 
            // level
            // 
            this.level.AutoSize = true;
            this.level.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.level.Location = new System.Drawing.Point(16, 37);
            this.level.Name = "level";
            this.level.Size = new System.Drawing.Size(280, 18);
            this.level.TabIndex = 5;
            this.level.Text = "Уровень подготовки: XXXXXXXXXXXX";
            // 
            // direction
            // 
            this.direction.AutoSize = true;
            this.direction.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.direction.Location = new System.Drawing.Point(16, 64);
            this.direction.MaximumSize = new System.Drawing.Size(600, 0);
            this.direction.Name = "direction";
            this.direction.Size = new System.Drawing.Size(104, 18);
            this.direction.TabIndex = 3;
            this.direction.Text = "Направление:";
            // 
            // profile
            // 
            this.profile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.profile.AutoSize = true;
            this.profile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.profile.Location = new System.Drawing.Point(16, 92);
            this.profile.MaximumSize = new System.Drawing.Size(600, 0);
            this.profile.Name = "profile";
            this.profile.Size = new System.Drawing.Size(78, 18);
            this.profile.TabIndex = 4;
            this.profile.Text = "Профиль:";
            // 
            // GroupInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Controls.Add(this.level);
            this.Controls.Add(this.profile);
            this.Controls.Add(this.direction);
            this.Controls.Add(this.course);
            this.Controls.Add(this.studentCount);
            this.Controls.Add(this.groupName);
            this.MinimumSize = new System.Drawing.Size(638, 0);
            this.Name = "GroupInfoControl";
            this.Size = new System.Drawing.Size(638, 122);
            this.Click += new System.EventHandler(this.GroupInfoControl_Click);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label groupName;
        private System.Windows.Forms.Label studentCount;
        private System.Windows.Forms.Label course;
        private System.Windows.Forms.Label level;
        private System.Windows.Forms.Label direction;
        private System.Windows.Forms.Label profile;
    }
}
