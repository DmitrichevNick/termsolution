﻿using App.GroupControls;
using DatabaseConnector;
using Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace App
{
    public partial class GroupChooseControl : ContentControl
    {
        private AppEnvironment _environment;
        public GroupChooseControl()
        {
            InitializeComponent();
            base.nextStageButton.Visible = true;
        }

        public GroupChooseControl(AppEnvironment env) : this()
        {
            SetEnvironment(env);
        }

        public void SetEnvironment(AppEnvironment appEnvironment)
        {
            _environment = appEnvironment;
            SetUp();
        }

        public void SetUp()
        {
            var practiceManager = new PracticeManager(_environment);
            var practices = practiceManager.ReadAll();

            var groupManager = new GroupManager(_environment);
            var groups = groupManager.ReadAll().Where(item => practices.Any(practice =>
                practice.IdEduDirection == item.IdEduDirection && practice.IdEduProfile == item.IdEduProfile
                                                               && practice.Course == item.Course));

            var groupControls = new GroupInfoControl[groups.Count()];

            int i = 0;
            foreach (var group in groups)
            {
                var groupInfo = new GroupInfoControl();
                groupInfo.SetEnvironment(_environment);
                groupInfo.SetGroup(group.Id);
                groupControls[i] = groupInfo;
                Click += groupInfo.GroupInfoControl_Click;
                i++;
            }

            tableLayoutPanel1.Controls.Clear();
            tableLayoutPanel1.Controls.AddRange(groupControls);
            tableLayoutPanel1.AutoScroll = true;
            tableLayoutPanel1.HorizontalScroll.Enabled = false;
            tableLayoutPanel1.HorizontalScroll.Visible = false;
            tableLayoutPanel1.BorderStyle = BorderStyle.FixedSingle;
        }

        public List<decimal?> CollectGroups()
        {
            var result = new List<decimal?>();
            foreach (GroupInfoControl groupInfoControl in tableLayoutPanel1.Controls)
            {
                if (groupInfoControl.IsActive)
                {
                    result.Add(groupInfoControl.GroupId);
                }
            }

            return result;
        }

        public override List<object> GetContext()
        {
            return new List<object>()
            {
                CollectGroups()
            };
        }

        private void GroupChooseControl_Click(object sender, EventArgs e)
        {

        }
    }
}
