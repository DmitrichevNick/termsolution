﻿using System;
using System.Collections.Generic;
using DatabaseConnector;
using DatabaseConnector.Base;
using MySql.Data.MySqlClient;
using StorageEntity;

namespace Manager.Base
{
    /// <summary>
    /// Базовый менеджер. Для MySql
    /// </summary>
    public class Manager<TEntity> where TEntity : IStorageEntity<MySqlParameter>
    {
        protected readonly AppEnvironment _environment;

        public Manager(AppEnvironment environment)
        {
            _environment = environment;
        }

        /// <summary>
        /// Стандартная функция записи.
        /// </summary>
        /// <param name="entity">Сущность для записи.</param>
        public void Write(TEntity entity)
        {
            var executor = new Executor(_environment);
            executor.ExecuteWrite(entity.Write, entity);
        }

        /// <summary>
        /// Стандартная функция чтения по УН.
        /// </summary>
        /// <param name="inId">УН сущности.</param>
        /// <returns></returns>
        public TEntity Read(int? inId)
        {
            var entity = Activator.CreateInstance<TEntity>();
            if (inId == null) return default(TEntity);

            entity.SetProperty("IN_ID", inId.ToString());
            var executor = new Executor(_environment);

            return executor.ExecuteSingle(entity.Read, entity);
        }

        /// <summary>
        /// Стандартная функция чтения всех записей.
        /// </summary>
        /// <returns></returns>
        public List<TEntity> ReadAll()
        {
            var entity = Activator.CreateInstance<TEntity>();
            var executor = new Executor(_environment);

            return executor.ExecuteMulty(entity.ReadAll, entity);
        }
    }
}
