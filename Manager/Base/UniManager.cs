﻿using System.Collections.Generic;
using DatabaseConnector;
using MySql.Data.MySqlClient;
using StorageEntity;

namespace Manager.Base
{
    /// <summary>
    /// Универсальный менеджер.
    /// </summary>
    public class UniManager
    {
        private readonly AppEnvironment _environment;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="environment"></param>
        public UniManager(AppEnvironment environment)
        {
            _environment = environment;
        }

        /// <summary>
        /// Чтение по УН.
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности.</typeparam>
        /// <param name="id">УН сущности.</param>
        /// <returns></returns>
        public TEntity Read<TEntity>(int? id) where TEntity : IStorageEntity<MySqlParameter>
        {
            return new Manager<TEntity>(_environment).Read(id);
        }

        /// <summary>
        /// Чтение всех записей.
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности.</typeparam>
        /// <returns></returns>
        public List<TEntity> ReadAll<TEntity>() where TEntity : IStorageEntity<MySqlParameter>
        {
            return new Manager<TEntity>(_environment).ReadAll();
        }

        /// <summary>
        /// Запись в базу.
        /// </summary>
        /// <typeparam name="TEntity">Тип сущности.</typeparam>
        /// <param name="entity">Сущность.</param>
        public void Write<TEntity>(TEntity entity) where TEntity : IStorageEntity<MySqlParameter>
        {
            new Manager<TEntity>(_environment).Write(entity);
        }
    }
}
