﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Преподаватель"
    /// </summary>
    public class LecturerManager : Manager<LecturerEntity>
    {
        public LecturerManager(AppEnvironment environment) : base(environment) { }
    }
}
