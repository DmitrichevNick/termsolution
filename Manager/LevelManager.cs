﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Уровень"
    /// </summary>
    public class LevelManager : Manager<LevelEntity>
    {
        public LevelManager(AppEnvironment environment) : base(environment) { }
    }
}
