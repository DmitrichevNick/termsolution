﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    public class StudentManager : Manager<StudentEntity>
    {
        public StudentManager(AppEnvironment environment) : base(environment) { }
    }
}
