﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Тип практики"
    /// </summary>
    public class PracticeTypeManager : Manager<PracticeTypeEntity>
    {
        public PracticeTypeManager(AppEnvironment environment) : base(environment) { }
    }
}
