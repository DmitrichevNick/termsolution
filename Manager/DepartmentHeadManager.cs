﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Заведующий кафедрой"
    /// </summary>
    public class DepartmentHeadManager : Manager<LecturerEntity>
    {
        public DepartmentHeadManager(AppEnvironment environment) : base(environment) { }
    }
}
