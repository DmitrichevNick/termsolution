﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Практика"
    /// </summary>
    public class PracticeManager : Manager<PracticeEntity>
    {
        public PracticeManager(AppEnvironment environment) : base(environment) { }
    }
}
