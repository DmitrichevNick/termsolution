﻿using DatabaseConnector;
using Entity;
using StorageEntity;
using System.Collections.Generic;
using DatabaseConnector.Base;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности EduProfileEntity
    /// </summary>
    public class EduProfileManager : Manager<EduProfileEntity>
    {
        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="environment">Окружение приложения.</param>
        public EduProfileManager(AppEnvironment environment) : base(environment) { }

        /// <summary>
        /// Чтение по УН направления.
        /// </summary>
        /// <param name="idEduDirection">УН направления.</param>
        /// <returns></returns>
        public List<EduProfileEntity> ReadByIdLevel(int? idEduDirection)
        {
            var executor = new Executor(_environment);
            return executor.ExecuteMulty(EduProfileStorageEntity.ReadByIdEduDirection, new EduProfileEntity() { IdEduDirection = idEduDirection });
        }
    }
}
