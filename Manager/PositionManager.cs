﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Должность"
    /// </summary>
    public class PositionManager : Manager<PositionEntity>
    {
        public PositionManager(AppEnvironment environment) : base(environment) { }
    }
}
