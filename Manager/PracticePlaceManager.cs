﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Место проведения практики"
    /// </summary>
    public class PracticePlaceManager : Manager<PracticePlaceEntity>
    {
        public PracticePlaceManager(AppEnvironment environment) : base(environment) { }
    }
}
