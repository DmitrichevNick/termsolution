﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Кафедра"
    /// </summary>
    public class DepartmentManager : Manager<DepartmentEntity>
    {
        public DepartmentManager(AppEnvironment environment) : base(environment) { }
    }
}
