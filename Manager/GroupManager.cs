﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    public class GroupManager : Manager<GroupEntity>
    {
        public GroupManager(AppEnvironment environment) : base(environment) { }
    }
}
