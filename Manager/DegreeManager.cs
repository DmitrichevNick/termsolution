﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Степень"
    /// </summary>
    public class DegreeManager : Manager<DegreeEntity>
    {
        public DegreeManager(AppEnvironment environment) : base(environment) { }
    }
}
