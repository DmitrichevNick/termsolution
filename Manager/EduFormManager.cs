﻿using DatabaseConnector;
using Entity;
using Manager.Base;

namespace Manager
{
    /// <summary>
    /// Менеджер сущности "Форма обучения"
    /// </summary>
    public class EduFormManager : Manager<EduFormEntity>
    {
        public EduFormManager(AppEnvironment environment) : base(environment) { }
    }
}
