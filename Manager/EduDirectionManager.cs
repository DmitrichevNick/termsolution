﻿using DatabaseConnector;
using Entity;
using StorageEntity;
using System.Collections.Generic;
using DatabaseConnector.Base;
using Manager.Base;

namespace Manager
{
    public class EduDirectionManager : Manager<EduDirectionEntity>
    {
        public EduDirectionManager(AppEnvironment environment) : base(environment) { }

        public List<EduDirectionEntity> ReadByIdLevel(int? idLevel)
        {
            var executor = new Executor(_environment);
            return executor.ExecuteMulty(EduDirectionStorageEntity.ReadByIdLevel, new EduDirectionEntity() { Id = idLevel });
        }
    }
}
