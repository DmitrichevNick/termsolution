﻿using System;
using System.Linq;
using DatabaseConnector;
using Entity;
using Manager;
using Manager.Base;

namespace DocumentWorker.InsertList
{
    public class InsertList
    {
        private readonly AppEnvironment _environment;
        private StudentEntity _studentEntity;
        private GroupEntity _groupEntity;
        private DepartmentEntity _studentDepartmentEntity;
        private LecturerEntity _studentLecturerEntity;
        private DepartmentHeadEntity _departmentLecturerEntity;
        private EduProfileEntity _groupEduProfileEntity;
        private EduDirectionEntity _groupEduDirectionEntity;
        private DepartmentEntity _lecturerDepartmentEntity;
        private DegreeEntity _studentLecturerDegreeEntity;
        private PositionEntity _studentLecturerPositionEntity;
        private PracticeEntity _practiceEntity;
        private PracticeTypeEntity _practiceTypeEntity;
        private PracticePlaceEntity _practicePlaceEntity;
        private LevelEntity _levelEntity;
        private EduFormEntity _eduFormEntity;
        public InsertList(AppEnvironment environment, int? idGroup)
        {
            _environment = environment;
            InitGroupInfo(idGroup);
        }

        public void ReadStudent(int? idStudent)
        {
            var uniManager = new UniManager(_environment);

            _studentEntity = uniManager.Read<StudentEntity>(idStudent);
            _studentDepartmentEntity = uniManager.Read<DepartmentEntity>(_studentEntity?.IdDepartment);
            _studentLecturerEntity = uniManager.Read<LecturerEntity>(_studentEntity?.IdLecturer);
            _departmentLecturerEntity = uniManager.Read<DepartmentHeadEntity>(_studentDepartmentEntity?.IdDepartmentHead);
            _lecturerDepartmentEntity = uniManager.Read<DepartmentEntity>(_studentLecturerEntity?.IdDepartment);
            _studentLecturerDegreeEntity = uniManager.Read<DegreeEntity>(_studentLecturerEntity?.IdDegree);
            _studentLecturerPositionEntity = uniManager.Read<PositionEntity>(_studentLecturerEntity?.IdPosition);
            _practiceEntity = uniManager.ReadAll<PracticeEntity>().FirstOrDefault(item => item.IdEduProfile == _groupEntity?.IdEduProfile &&
                                                                                          item.Course == _groupEntity?.Course);
            _practiceTypeEntity = uniManager.Read<PracticeTypeEntity>(_practiceEntity?.IdPracticeType);
            _practicePlaceEntity = uniManager.Read<PracticePlaceEntity>(_studentEntity?.IdPlace);
        }

        private void InitGroupInfo(int? idGroup)
        {
            var uniManager = new UniManager(_environment);

            _groupEntity = uniManager.Read<GroupEntity>(idGroup);
            _groupEduProfileEntity = uniManager.Read<EduProfileEntity>(_groupEntity?.IdEduProfile);
            _groupEduDirectionEntity = uniManager.Read<EduDirectionEntity>(_groupEduProfileEntity?.IdEduDirection);
            _levelEntity = uniManager.Read<LevelEntity>(_groupEduDirectionEntity?.IdLevel);
            _eduFormEntity = uniManager.Read<EduFormEntity>(_groupEntity?.IdEduForm);
        }

        public string InitGroupInfoString()
        {
            var result = "";

            result = "$1" + _groupEduDirectionEntity?.Name + Environment.NewLine;
            result += "$2" + _groupEduProfileEntity?.Name + Environment.NewLine;
            result += "$3" + _groupEntity?.Number + Environment.NewLine;

            return result;
        }

        public string MakeInsertList()
        {
            var result = "";

            result += "Name1|" + CheckEmpty(_practiceTypeEntity?.Name) + Environment.NewLine;
            result += "FullName(student)|" + CheckEmpty(_studentEntity?.Name) + Environment.NewLine;
            result += "Qualification(EduDirection)|" + CheckEmpty(_levelEntity?.Name) + Environment.NewLine;
            result += "EduForm(student)|" + CheckEmpty(_eduFormEntity?.Name) + Environment.NewLine;
            result += "Name(EduDirection)|" + CheckEmpty(_groupEduDirectionEntity?.Name) + Environment.NewLine;
            result += "Name(EduProfile)|" + CheckEmpty(_groupEduProfileEntity?.Name) + Environment.NewLine;
            result += "Surname(Lecture)|" + CheckEmpty(_studentLecturerEntity?.ShortName) + Environment.NewLine;
            result += "Surname(student)|" + CheckEmpty(_studentEntity?.ShortName) + Environment.NewLine;
            result += "BeginData(Practice)|" + CheckEmpty(_practiceEntity?.DateBegin.ToShortDateString()) + Environment.NewLine;
            result += "EndData(Practice)|" + CheckEmpty(_practiceEntity?.DateEnd.ToShortDateString()) + Environment.NewLine;
            result += "Course(student)|" + CheckEmpty(_groupEntity?.Course.ToString()) + Environment.NewLine;
            result += "Place(Practice)|" + CheckEmpty(_practicePlaceEntity?.Name) + Environment.NewLine;
            result += "FullName(Lecturer)|" + CheckEmpty(_studentLecturerEntity?.Name) + Environment.NewLine;
            result += "Degree|" + CheckEmpty(_studentLecturerDegreeEntity?.ShortName) + Environment.NewLine;
            result += "Position|" + CheckEmpty(_studentLecturerPositionEntity?.ShortName) + Environment.NewLine;
            result += "Name(department)|" + CheckEmpty(_lecturerDepartmentEntity?.Name) + Environment.NewLine;
            result += "Name2|" + CheckEmpty(_practiceTypeEntity?.NameR) + Environment.NewLine;
            result += "dateBeginStr|" + CheckEmpty(_practiceEntity?.DateBegin.ToShortDateString()) + Environment.NewLine;
            result += "dateEndStr|" + CheckEmpty(_practiceEntity?.DateEnd.ToShortDateString()) + Environment.NewLine;
            result += "Group|" + CheckEmpty(_groupEntity?.Number) + Environment.NewLine;
            result += "Year|" + CheckEmpty(_practiceEntity?.DateBegin.Year.ToString()) + Environment.NewLine;
            result += "AddName|" + CheckEmpty(_practiceTypeEntity?.AddName) + Environment.NewLine;
            result += "ShortAddName|" + CheckEmpty(_practiceTypeEntity?.ShortAddName) + Environment.NewLine;
            result += "Name3|" + CheckEmpty(_practiceTypeEntity?.NameV) + Environment.NewLine;
            result += "FullPosition|" + CheckEmpty(_studentLecturerPositionEntity?.Name) + Environment.NewLine;


            return result;
        }

        private string CheckEmpty(string input)
        {
            return !string.IsNullOrEmpty(input) ? input : "НЕИЗВЕСТНО";
        }
    }
}
