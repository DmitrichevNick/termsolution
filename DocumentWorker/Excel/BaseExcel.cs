﻿using DatabaseConnector;

namespace DocumentWorker
{
    public class BaseExcel
    {
        protected ExcelWorker _excelWorker;
        protected readonly AppEnvironment _environment;

        protected BaseExcel(AppEnvironment environment)
        {
            _environment = environment;
        }

        public void Quit()
        {
            _excelWorker.Quit();
        }
    }
}
