﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using DataTable = System.Data.DataTable;


namespace DocumentWorker
{
    public class ExcelWorker
    {
        private Microsoft.Office.Interop.Excel.Application _application;
        private Microsoft.Office.Interop.Excel.Workbook _workbook;
        private Microsoft.Office.Interop.Excel.Worksheet _workSheet;
        private int _usedColumnRange;

        /// <summary>
        /// Открывает существующий
        /// </summary>
        /// <param name="path"></param>
        /// <param name="isReadonly"></param>
        public ExcelWorker(string path, bool isReadonly)
        {
            _application = new Microsoft.Office.Interop.Excel.Application();
            _workbook = _application.Workbooks.Open(path, 0, isReadonly, 5, "", "", false, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
            _workSheet = (Microsoft.Office.Interop.Excel.Worksheet)_workbook.Sheets[1];
            _usedColumnRange = 0;
            ColumnsCount = _workSheet.UsedRange.Columns.Count;
            RowsCount = _workSheet.UsedRange.Rows.Count;
        }

        public int RowsCount { get; }

        public int ColumnsCount { get; }

        public void Quit()
        {
            _workSheet = null;
            if (_workbook != null)
            {
                _workbook.Close(false, Type.Missing, Type.Missing);
                _workbook = null;
            }
            if (_application != null)
            {
                _application.Application.Quit();
                _application.Quit();
                _application = null;
            }
        }


        /// <summary>
        /// Создает новый
        /// </summary>
        public ExcelWorker()
        {
            _application = new Microsoft.Office.Interop.Excel.Application();
            _workbook = _application.Workbooks.Add();
            _workSheet = (Microsoft.Office.Interop.Excel.Worksheet)_workbook.Sheets.Add();
        }

        public void Save(string path)
        {
            FileInfo excelFile = new FileInfo(path);
            _workbook.SaveAs(excelFile);
        }

        public void SaveFullPath(string path)
        {
            FileInfo excelFile = new FileInfo(path);
            _workbook.SaveAs(excelFile);
        }

        public bool TryWriteColumns(List<Tuple<string, List<object>>> rows)
        {
            foreach (var pair in rows)
            {
                WriteColumn(pair);
            }

            return true;
        }

        public void WriteTable(DataTable table)
        {
            var colNumber = 1;
            var rowNumber = 2;
            foreach (DataColumn column in table.Columns)
            {
                _workSheet.Cells[1, colNumber] = column.ColumnName;
                colNumber++;
            }

            colNumber = 1;

            foreach (DataRow row in table.Rows)
            {
                foreach (var cell in row.ItemArray)
                {
                    _workSheet.Cells[rowNumber, colNumber] = cell.ToString();
                    colNumber++;
                }

                colNumber = 1;
                rowNumber++;
            }

        }

        public void WriteColumn(Tuple<string, List<object>> rows)
        {
            _usedColumnRange++;
            _workSheet.Cells[1, _usedColumnRange] = rows.Item1;
            var row = 2;

            foreach (var value in rows.Item2)
            {
                _workSheet.Cells[row, _usedColumnRange] = value;
                row++;
            }
        }

        public void SetWorkSheet(string sheetName)
        {
            _workSheet = (Microsoft.Office.Interop.Excel.Worksheet)_workbook.Sheets[sheetName];
        }

        public bool TryFindColumn(string columnName, out List<object> rows)
        {
            int colNo = _workSheet.UsedRange.Columns.Count;
            int rowNo = _workSheet.UsedRange.Rows.Count;
            rows = new List<object>();

            // read the value into an array.
            object[,] array = _workSheet.UsedRange.Value;
            for (int j = 1; j <= colNo; j++)
            {
                for (int i = 1; i <= rowNo; i++)
                {
                    if (array[i, j] != null)
                        if (array[i, j].ToString() == columnName)
                        {
                            for (int m = i + 1; m <= rowNo; m++)
                            {
                                rows.Add(array[m, j]);
                            }

                            _workSheet.UsedRange.Value = array;
                            return true;
                        }
                }
            }

            return false;
        }

        public DataTable ReadTableWithColumns(List<string> columns)
        {
            var resultTable = new DataTable();
            int colNo = _workSheet.UsedRange.Columns.Count;
            int rowNo = _workSheet.UsedRange.Rows.Count;
            var index = 0;
            foreach (var columnName in columns)
            {
                if (FindColumn(columnName, out index))
                {
                    resultTable.Columns.Add(columnName);
                }
            }

            for (var i = 2; i <= rowNo; i++)
            {
                var list = ReadRow(i, columns);
                var newRow = resultTable.NewRow();
                newRow.ItemArray = list;
                resultTable.Rows.Add(newRow);
            }

            return resultTable;
        }

        public object[] ReadRow(int rowNumber, List<string> columns)
        {
            int colNo = _workSheet.UsedRange.Columns.Count;
            int rowNo = _workSheet.UsedRange.Rows.Count;
            int column;
            var foundColumns = columns.Count(item => FindColumn(item, out column));

            var resultList = new object[foundColumns];

            object[,] array = _workSheet.UsedRange.Value;
            if (rowNumber <= rowNo)
            {
                var i = 0;
                foreach (var columnName in columns)
                {
                    if (FindColumn(columnName, out column))
                    {
                        resultList[i] = array[rowNumber, column] ?? "";
                        i++;
                    }
                }
            }

            return resultList;
        }

        public void WriteRow(int rowNumber, object[] values)
        {
            int rowNo = _workSheet.UsedRange.Rows.Count;

            if (rowNumber <= rowNo)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    _workSheet.Cells[rowNumber, i + 1] = values[i].ToString();
                }
            }
        }

        private bool FindColumn(string columnName, out int index)
        {
            int colNo = _workSheet.UsedRange.Columns.Count;
            int rowNo = _workSheet.UsedRange.Rows.Count;
            index = -1;

            object[,] array = _workSheet.UsedRange.Value;
            for (int j = 1; j <= colNo; j++)
            {
                for (int i = 1; i <= rowNo; i++)
                {
                    if (array[i, j] != null)
                        if (array[i, j].ToString() == columnName)
                        {
                            index = j;
                            return true;
                        }
                }
            }

            return false;
        }

        public bool FindColumns(List<string> columnNames)
        {
            return columnNames.All(columnName => _workSheet.UsedRange.Find(columnName, Type.Missing,
                Microsoft.Office.Interop.Excel.XlFindLookIn.xlValues, Microsoft.Office.Interop.Excel.XlLookAt.xlPart,
                Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows, Microsoft.Office.Interop.Excel.XlSearchDirection.xlNext, false,
                Type.Missing, Type.Missing) != null && _workSheet.UsedRange.Columns.Count == columnNames.Count);
        }
    }
}