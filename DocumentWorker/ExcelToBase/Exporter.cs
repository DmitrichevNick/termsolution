﻿using DatabaseConnector;

namespace DocumentWorker
{
    public class Exporter
    {
        protected readonly string _filePath;
        protected readonly AppEnvironment _environment;

        protected Exporter(string filePath, AppEnvironment environment)
        {
            _filePath = filePath;
            _environment = environment;
        }
        public virtual void Execute() { }
    }
}
