﻿using DatabaseConnector;
using Entity;
using Helper;
using Manager;
using System;
using System.Collections.Generic;
using System.Data;

namespace DocumentWorker
{
    public class PracticeExporter : Exporter
    {
        public PracticeExporter(string filePath, AppEnvironment environment) :base(filePath, environment) {}
        public override void Execute()
        {
            var excelReader = new ExcelWorker(_filePath, true);
            var table = excelReader.ReadTableWithColumns(new List<string>
            {
                "ID",
                "ID_PRACTICE_TYPE",
                "DATE_BEGIN",
                "DATE_END",
                "ID_EDU_PROFILE",
                "SEMESTER",
                "COURSE",
                "LEVEL",
                "ID_EDU_DIRECTION",
                "ID_GROUP"

            });
            excelReader.Quit();

            var practiceManager = new PracticeManager(_environment);

            foreach (DataRow row in table.Rows)
            {
                var ent = new PracticeEntity()
                {
                    Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                    IdPracticeType = ConvertHelper.StringToInt(row["ID_PRACTICE_TYPE"].ToString()),
                    DateBegin = Convert.ToDateTime(row["DATE_BEGIN"].ToString()),
                    DateEnd = Convert.ToDateTime(row["DATE_END"].ToString()),
                    IdEduProfile = ConvertHelper.StringToInt(row["ID_EDU_PROFILE"].ToString()),
                    Semester = ConvertHelper.StringToInt(row["SEMESTER"].ToString()),
                    Course = ConvertHelper.StringToInt(row["COURSE"].ToString()),
                };
                practiceManager.Write(ent);
            }
        }
    }
}
