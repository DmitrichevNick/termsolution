﻿using DatabaseConnector;
using Entity;
using Helper;
using Manager;
using System.Collections.Generic;
using System.Data;

namespace DocumentWorker
{
    public class GroupExporter : Exporter
    {
        public GroupExporter(string filePath, AppEnvironment environment) : base(filePath, environment) { }
        public override void Execute()
        {
            var excelReader = new ExcelWorker(_filePath, true);
            var table = excelReader.ReadTableWithColumns(new List<string>
            {
                "ID",
                "NUMBER",
                "ID_EDU_DIRECTION",
                "ID_EDU_PROFILE",
                "COURSE",
                "ID_EDU_FORM"
            });
            excelReader.Quit();

            var groupManager = new GroupManager(_environment);

            foreach (DataRow row in table.Rows)
            {
                var ent = new GroupEntity()
                {
                    Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                    Number = row["NUMBER"].ToString(),
                    IdEduProfile = ConvertHelper.StringToInt(row["ID_EDU_PROFILE"].ToString()),
                    Course = ConvertHelper.StringToInt(row["COURSE"].ToString()),
                    IdEduForm = ConvertHelper.StringToInt(row["ID_EDU_FORM"].ToString()),
                };

                groupManager.Write(ent);
            }
        }
    }
}
