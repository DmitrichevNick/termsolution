﻿using DatabaseConnector;
using Entity;
using Helper;
using Manager;
using System.Collections.Generic;
using System.Data;

namespace DocumentWorker
{
    public class PracticeTypeExporter : Exporter
    {
        public PracticeTypeExporter(string filePath, AppEnvironment environment) :base(filePath, environment) {}
        public override void Execute()
        {
            var excelReader = new ExcelWorker(_filePath, true);
            var table = excelReader.ReadTableWithColumns(new List<string>
            {
                "ID",
                "NAME",
                "NAME_R",
            });
            excelReader.Quit();

            var practiceTypeManager = new PracticeTypeManager(_environment);

            foreach (DataRow row in table.Rows)
            {
                var ent = new PracticeTypeEntity()
                {
                    Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                    Name = row["NAME"].ToString(),
                    NameR = row["NAME_R"].ToString(),
                };

                practiceTypeManager.Write(ent);
            }
        }
    }
}
