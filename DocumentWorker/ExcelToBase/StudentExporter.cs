﻿using DatabaseConnector;
using Entity;
using Helper;
using Manager;
using System.Collections.Generic;
using System.Data;

namespace DocumentWorker
{
    public class StudentExporter : Exporter
    {
        public StudentExporter(string filePath, AppEnvironment environment) : base(filePath, environment) { }
        public override void Execute()
        {
            var excelReader = new ExcelWorker(_filePath, true);
            var table = excelReader.ReadTableWithColumns(new List<string>
            {
                "ID",
                "NAME",
                "SHORT_NAME",
                "ID_GROUP",
                "ID_DEPARTMENT",
                "ID_LECTURER",
                "ID_PLACE"

            });
            excelReader.Quit();

            var studentManager = new StudentManager(_environment);

            foreach (DataRow row in table.Rows)
            {
                var ent = new StudentEntity()
                {
                    Id = ConvertHelper.StringToInt(row["ID"].ToString()),
                    Name = row["NAME"].ToString(),
                    ShortName = row["SHORT_NAME"].ToString(),
                    IdDepartment = ConvertHelper.StringToInt(row["ID_DEPARTMENT"].ToString()),
                    IdGroup = ConvertHelper.StringToInt(row["ID_GROUP"].ToString()),
                    IdLecturer = ConvertHelper.StringToInt(row["ID_LECTURER"].ToString()),
                    IdPlace = ConvertHelper.StringToInt(row["ID_PLACE"].ToString())

                };
                studentManager.Write(ent);
            }
        }
    }
}
