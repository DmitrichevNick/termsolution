﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace StorageEntity
{
    /// <summary>
    /// Интерфейс сущности хранилища.
    /// </summary>
    public interface IStorageEntity<TParameter>
    {
        /// <summary>
        /// Стандартная процедура записи.
        /// </summary>
        string Write { set; get; }

        /// <summary>
        /// Стандартная процедура чтения по ID.
        /// </summary>
        string Read { set; get; }

        /// <summary>
        /// Стандартная процедура чтения всех записей.
        /// </summary>
        string ReadAll { set; get; }

        /// <summary>
        /// Список параметров, которые использует хранилище.
        /// </summary>
        List<TParameter> Properties { get; set; }

        /// <summary>
        /// Добавить входящий параметр для базы.
        /// </summary>
        /// <param name="mySqlDbType">Тип поля базы.</param>
        /// <param name="fieldName">Имя поля в базе.</param>
        void AddProperty(MySqlDbType mySqlDbType, string fieldName);

        /// <summary>
        /// Установить значение свойства. Для добавления свойства используется <see cref="AddProperty"/>.
        /// При попытке установиться значение несуществующего свойства, ничего не произойдет.
        /// </summary>
        /// <param name="fieldName">Имя свойства.</param>
        /// <param name="fieldValue">Значение свойства.</param>
        void SetProperty(string fieldName, string fieldValue);

        /// <summary>
        /// Получить значение свойства.
        /// </summary>
        /// <param name="fieldName">Имя свойства.</param>
        /// <returns></returns>
        string GetProperty(string fieldName);
    }
}
