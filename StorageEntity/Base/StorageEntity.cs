﻿using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace StorageEntity
{
    /// <summary>
    /// Сущность хранилища. Необходима для работы с базой данных. Для MySql.
    /// </summary>
    public class StorageEntity : IStorageEntity<MySqlParameter>
    {
        private Dictionary<string, MySqlParameter> _properties;

        // Конструктор.
        protected StorageEntity()
        {
            _properties = new Dictionary<string, MySqlParameter>();
        }

        /// <summary>
        /// Стандартная процедура записи.
        /// </summary>
        public string Write { set; get; }

        /// <summary>
        /// Стандартная процедура чтения по ID.
        /// </summary>
        public string Read { set; get; }

        /// <summary>
        /// Стандартная процедура чтения всех записей.
        /// </summary>
        public string ReadAll { set; get; }

        /// <summary>
        /// Список параметров, которые использует хранилище.
        /// </summary>
        public List<MySqlParameter> Properties
        {
            get { return _properties.Values.ToList().ConvertAll(property => property.Clone()); }
            set
            {
                value.ForEach(property =>
                {
                    if (_properties.ContainsKey(property.ParameterName))
                        _properties[property.ParameterName] = property;
                    else
                        _properties.Add(property.ParameterName, property);
                });
            }
        }

        /// <summary>
        /// Добавить входящий параметр для базы.
        /// </summary>
        /// <param name="mySqlDbType">Тип поля базы.</param>
        /// <param name="fieldName">Имя поля в базе.</param>
        public void AddProperty(MySqlDbType mySqlDbType, string fieldName)
        {
            var parameter = new MySqlParameter(fieldName, mySqlDbType) { Direction = ParameterDirection.Input };

            if (!_properties.ContainsKey(fieldName))
                _properties.Add(fieldName, parameter);
        }

        /// <summary>
        /// Установить значение свойства. Для добавления свойства используется <see cref="AddProperty"/>.
        /// При попытке установиться значение несуществующего свойства, ничего не произойдет.
        /// </summary>
        /// <param name="fieldName">Имя свойства.</param>
        /// <param name="fieldValue">Значение свойства.</param>
        public void SetProperty(string fieldName, string fieldValue)
        {
            if (_properties.ContainsKey(fieldName))
                _properties[fieldName].Value = fieldValue;
        }

        /// <summary>
        /// Получить значение свойства.
        /// </summary>
        /// <param name="fieldName">Имя свойства.</param>
        /// <returns></returns>
        public string GetProperty(string fieldName)
        {
            return _properties.ContainsKey(fieldName) ? _properties[fieldName].Value?.ToString() : null;
        }
    }
}
