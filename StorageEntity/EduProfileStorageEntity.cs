﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class EduProfileStorageEntity : StorageEntity
    {
        public const string ReadByIdEduDirection = "R_EDU_PROFILE_BY_ID_EDU_DIRECTION";
        protected EduProfileStorageEntity()
        {
            Write = "PW_EDU_PROFILE";
            Read = "PR_EDU_PROFILE";
            ReadAll = "PRA_EDU_PROFILE";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.Decimal, "IN_ID_EDU_DIRECTION");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
        }
    }
}
