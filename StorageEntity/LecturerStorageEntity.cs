﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class LecturerStorageEntity : StorageEntity
    {
        protected LecturerStorageEntity()
        {
            Write = "PW_LECTURER";
            Read = "PR_LECTURER";
            ReadAll = "PRA_LECTURER";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
            AddProperty(MySqlDbType.Decimal, "IN_ID_DEPARTMENT");
            AddProperty(MySqlDbType.Decimal, "IN_ID_DEGREE");
            AddProperty(MySqlDbType.Decimal, "IN_ID_POSITION");
        }
    }
}
