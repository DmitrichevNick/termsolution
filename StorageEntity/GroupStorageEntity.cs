﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class GroupStorageEntity : StorageEntity
    {
        protected GroupStorageEntity()
        {
            Write = "PW_GROUP";
            Read = "PR_GROUP";
            ReadAll = "PRA_GROUP";


            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NUMBER");
            AddProperty(MySqlDbType.Decimal, "IN_ID_EDU_FORM");
            AddProperty(MySqlDbType.Decimal, "IN_ID_EDU_PROFILE");
            AddProperty(MySqlDbType.Decimal, "IN_COURSE");
        }
    }
}
