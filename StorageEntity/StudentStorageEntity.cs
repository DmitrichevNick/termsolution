﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class StudentStorageEntity : StorageEntity
    {
        protected StudentStorageEntity()
        {
            Write = "PW_STUDENT";
            Read = "PR_STUDENT";
            ReadAll = "PRA_STUDENT";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
            AddProperty(MySqlDbType.Decimal, "IN_ID_GROUP");
            AddProperty(MySqlDbType.Decimal, "IN_ID_DEPARTMENT");
            AddProperty(MySqlDbType.Decimal, "IN_ID_LECTURER");
            AddProperty(MySqlDbType.Decimal, "IN_ID_PLACE");
        }
    }
}
