﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class PracticeStorageEntity : StorageEntity
    {
        protected PracticeStorageEntity()
        {
            Write = "PW_PRACTICE";
            Read = "PR_PRACTICE";
            ReadAll = "PRA_PRACTICE";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.Decimal, "ID_PRACTICE_TYPE");
            AddProperty(MySqlDbType.Date, "IN_DATE_BEGIN");
            AddProperty(MySqlDbType.Date, "IN_DATE_END");
            AddProperty(MySqlDbType.Decimal, "IN_ID_EDU_PROFILE");
            AddProperty(MySqlDbType.Decimal, "IN_SEMESTER");
            AddProperty(MySqlDbType.Decimal, "IN_COURSE");
        }
    }
}
