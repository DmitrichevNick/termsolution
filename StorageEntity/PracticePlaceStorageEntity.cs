﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class PracticePlaceStorageEntity : StorageEntity
    {
        protected PracticePlaceStorageEntity()
        {
            Write = "PW_PRACTICE_PLACE";
            Read = "PR_PRACTICE_PLACE";
            ReadAll = "PRA_PRACTICE_PLACE";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
        }
    }
}
