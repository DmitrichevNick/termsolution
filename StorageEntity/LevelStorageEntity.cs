﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class LevelStorageEntity : StorageEntity
    {
        protected LevelStorageEntity()
        {
            Write = "PW_LEVEL";
            Read = "PR_LEVEL";
            ReadAll = "PRA_LEVEL";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
        }
    }
}
