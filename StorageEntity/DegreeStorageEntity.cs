﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class DegreeStorageEntity : StorageEntity
    {
        protected DegreeStorageEntity()
        {
            Write = "PW_DEGREE";
            Read = "PR_DEGREE";
            ReadAll = "PRA_DEGREE";           

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
        }
    }
}
