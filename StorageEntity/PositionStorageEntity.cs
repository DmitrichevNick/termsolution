﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class PositionStorageEntity : StorageEntity
    {
        protected PositionStorageEntity()
        {
            Write = "PW_POSITION";
            Read = "PR_POSITION";
            ReadAll = "PRA_POSITION";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
        }
    }
}
