﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class EduFormStorageEntity : StorageEntity
    {
        protected EduFormStorageEntity()
        {
            Write = "PW_EDU_FORM";
            Read = "PR_EDU_FORM";
            ReadAll = "PRA_EDU_FORM";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
        }
    }
}
