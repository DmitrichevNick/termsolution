﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class DepartmentStorageEntity : StorageEntity
    {
        protected DepartmentStorageEntity()
        {
            Write = "PW_DEPARTMENT";
            Read = "PR_DEPARTMENT";
            ReadAll = "PRA_DEPARTMENT";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_CODE");
            AddProperty(MySqlDbType.Decimal, "IN_ID_DEPARTMENT_HEAD");
        }
    }
}
