﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class PracticeTypeStorageEntity : StorageEntity
    {
        protected PracticeTypeStorageEntity()
        {
            Write = "PW_PRACTICE_TYPE";
            Read = "PR_PRACTICE_TYPE";
            ReadAll = "PRA_PRACTICE_TYPE";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_NAME_R");
            AddProperty(MySqlDbType.String, "IN_NAME_V");
            AddProperty(MySqlDbType.String, "IN_ADD_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_ADD_NAME");
        }
    }
}
