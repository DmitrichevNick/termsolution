﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class EduDirectionStorageEntity : StorageEntity
    {
        public const string ReadByIdLevel = "R_EDU_DIRECTION_BY_ID_LEVEL";
        protected EduDirectionStorageEntity()
        {
            Write = "PW_EDU_DIRECTION";
            Read = "PR_EDU_DIRECTION";
            ReadAll = "PRA_EDU_DIRECTION";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_CODE");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
            AddProperty(MySqlDbType.String, "IN_ID_LEVEL");
        }
    }
}
