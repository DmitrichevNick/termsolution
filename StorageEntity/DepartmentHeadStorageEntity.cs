﻿using MySql.Data.MySqlClient;

namespace StorageEntity
{
    public class DepartmentHeadStorageEntity : StorageEntity
    {
        protected DepartmentHeadStorageEntity()
        {
            Write = "PW_DEPARTMENT_HEAD";
            Read = "PR_DEPARTMENT_HEAD";
            ReadAll = "PRA_DEPARTMENT_HEAD";

            AddProperty(MySqlDbType.Decimal, "IN_ID");
            AddProperty(MySqlDbType.String, "IN_NAME");
            AddProperty(MySqlDbType.String, "IN_SHORT_NAME");
            AddProperty(MySqlDbType.Decimal, "IN_ID_DEGREE");
        }
    }
}
