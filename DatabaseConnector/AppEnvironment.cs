﻿using System;
using System.Configuration;
using System.IO;
using MySql.Data.MySqlClient;

namespace DatabaseConnector
{
    /// <summary>
    /// Окружение приложения.
    /// </summary>
    public class AppEnvironment
    {
        private readonly MySqlDatabase _mySqlDatabase;
        private static readonly string _tmpDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"tmp\");

        /// <summary>
        /// Конструктор.
        /// </summary>
        public AppEnvironment()
        {
            // Создание директории для временных файлов, если ее нет.
            if (!Directory.Exists(_tmpDirectory)) Directory.CreateDirectory(_tmpDirectory);
            if (_mySqlDatabase != null) return;

            var autorization = new Authorization(
                ConfigurationManager.AppSettings["DatabaseName"],
                ConfigurationManager.AppSettings["Host"],
                ConfigurationManager.AppSettings["Port"],
                ConfigurationManager.AppSettings["Username"],
                ConfigurationManager.AppSettings["Password"]);
            _mySqlDatabase = new MySqlDatabase(autorization);
        }

        /// <summary>
        /// Разрушить окружение.
        /// </summary>
        public void Destroy()
        {
            _mySqlDatabase.CloseConnection();
        }

        /// <summary>
        /// Директория для временных файлов приложения.
        /// </summary>
        public string TmpDirectory
        {
            get { return _tmpDirectory; }
        }

        /// <summary>
        /// Соединение с базой даннных.
        /// </summary>
        public MySqlConnection Connection
        {
            get { return _mySqlDatabase.Connection; }
        }
    }
}
