﻿namespace DatabaseConnector
{
    /// <summary>
    /// Авторизатор подключения к базе данных.
    /// </summary>
    public class Authorization
    {
        /// <summary>
        /// Конструктор авторизатора.
        /// </summary>
        /// <param name="databaseName">Имя базы данных.</param>
        /// <param name="host">Хост базы данных.</param>
        /// <param name="port">Порт подключения к базе данных.</param>
        /// <param name="username">Имя пользоваеля базы данных.</param>
        /// <param name="password">Пароль пользователя базы данных.</param>
        public Authorization(string databaseName, string host, string port, string username, string password)
        {
            DatabaseName = databaseName;
            Host = host;
            Port = port;
            Username = username;
            Password = password;
        }

        /// <summary>
        /// Имя базы данных.
        /// </summary>
        public string DatabaseName { get; }

        /// <summary>
        /// Хост базы данных.
        /// </summary>
        public string Host { get; }

        /// <summary>
        /// Порт подключения к базе данных.
        /// </summary>
        public string Port { get; }

        /// <summary>
        /// Имя пользоваеля базы данных.
        /// </summary>
        public string Username { get; }

        /// <summary>
        /// Пароль пользователя базы данных.
        /// </summary>
        public string Password { get; }
    }
}
