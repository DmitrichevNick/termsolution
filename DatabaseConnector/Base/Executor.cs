﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using StorageEntity;

namespace DatabaseConnector.Base
{
    /// <summary>
    /// Исполнитель. Выполнение запросов к базе данных. Для MySql
    /// </summary>
    public class Executor : IExecutor<MySqlParameter>
    {
        private readonly MySqlConnection _mySqlConnection;

        /// <summary>
        /// Конструктор исполнителя.
        /// </summary>
        /// <param name="environment">Окружение приложения</param>
        public Executor(AppEnvironment environment)
        {
            _mySqlConnection = environment.Connection;
        }

        /// <summary>
        /// Исполнитель, возвращающий список сущностей.
        /// </summary>
        /// <typeparam name="TStorage"></typeparam>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        /// <returns></returns>
        public List<TStorage> ExecuteMulty<TStorage>(string procedureName, TStorage storage) where TStorage : IStorageEntity<MySqlParameter>
        {
            var type = typeof(TStorage);
            using (var mySqlCommand = new MySqlCommand(procedureName, _mySqlConnection))
            {
                mySqlCommand.CommandType = CommandType.StoredProcedure;
                mySqlCommand.Parameters.AddRange(storage.Properties.ToArray());

                using (var mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand))
                {
                    var dataTable = new DataTable();
                    try
                    {
                        mySqlDataAdapter.Fill(dataTable);
                    }
                    catch (MySqlException mySqlException)
                    {
                        MessageBox.Show("Ошибка выполнения процедуры\r\n" + mySqlException, "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }

                    var resultList = new List<TStorage>();

                    foreach (DataRow row in dataTable.Rows)
                    {
                        var result = (TStorage)Activator.CreateInstance(type);
                        result.Properties = storage.Properties;

                        foreach (DataColumn column in row.Table.Columns)
                            result.SetProperty("IN_" + column.ColumnName, row[column.ColumnName].ToString());

                        resultList.Add(result);
                    }

                    return resultList;
                }
            }
        }

        /// <summary>
        /// Исполнитель возвращающий экземпляр сущности.
        /// </summary>
        /// <typeparam name="TStorage"></typeparam>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        /// <returns></returns>
        public TStorage ExecuteSingle<TStorage>(string procedureName, TStorage storage) where TStorage : IStorageEntity<MySqlParameter>
        {
            var type = typeof(TStorage);
            using (var mySqlCommand = new MySqlCommand(procedureName, _mySqlConnection))
            {
                mySqlCommand.CommandType = CommandType.StoredProcedure;
                mySqlCommand.Parameters.AddRange(storage.Properties.ToArray());

                using (var mySqlDataAdapter = new MySqlDataAdapter(mySqlCommand))
                {
                    var dataTable = new DataTable();
                    try
                    {
                        mySqlDataAdapter.Fill(dataTable);
                    }
                    catch (MySqlException mySqlException)
                    {
                        MessageBox.Show("Ошибка выполнения процедуры\r\n" + mySqlException, "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }

                    if (dataTable.Rows.Count == 0) return (TStorage)Activator.CreateInstance(type);

                    var row = dataTable.Rows[0];
                    var result = (TStorage)Activator.CreateInstance(type);
                    result.Properties = storage.Properties;

                    foreach (DataColumn column in row.Table.Columns)
                        result.SetProperty("IN_" + column.ColumnName, row[column.ColumnName].ToString());

                    return result;
                }
            }
        }

        /// <summary>
        /// Стандартный исполнитель для записи.
        /// </summary>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        public void ExecuteWrite<TStorage>(string procedureName, TStorage storage) where TStorage : IStorageEntity<MySqlParameter>
        {
            try
            {
                using (var mySqlCommand = new MySqlCommand(procedureName, _mySqlConnection))
                {
                    mySqlCommand.CommandType = CommandType.StoredProcedure;
                    mySqlCommand.Parameters.AddRange(storage.Properties.ToArray());
                    mySqlCommand.ExecuteNonQuery();
                }
            }
            catch (MySqlException mySqlException)
            {
                MessageBox.Show("Ошибка выполнения процедуры\r\n" + mySqlException, "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ошибка приложения\r\n" + exception, "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }
    }
}
