﻿using System.Collections.Generic;
using StorageEntity;

namespace DatabaseConnector.Base
{
    /// <summary>
    /// Интерфейс исполнителя.
    /// </summary>
    public interface IExecutor<TParameter>
    {
        /// <summary>
        /// Исполнитель, возвращающий список сущностей.
        /// </summary>
        /// <typeparam name="TStorage"></typeparam>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        /// <returns></returns>
        List<TStorage> ExecuteMulty<TStorage>(string procedureName, TStorage storage)
            where TStorage : IStorageEntity<TParameter>;

        /// <summary>
        /// Исполнитель возвращающий экземпляр сущности.
        /// </summary>
        /// <typeparam name="TStorage"></typeparam>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        /// <returns></returns>
        TStorage ExecuteSingle<TStorage>(string procedureName, TStorage storage)
            where TStorage : IStorageEntity<TParameter>;

        /// <summary>
        /// Стандартный исполнитель для записи.
        /// </summary>
        /// <param name="procedureName">Имя процедуры.</param>
        /// <param name="storage">Сущность хранилища.</param>
        void ExecuteWrite<TStorage>(string procedureName, TStorage storage)
            where TStorage : IStorageEntity<TParameter>;
    }
}
