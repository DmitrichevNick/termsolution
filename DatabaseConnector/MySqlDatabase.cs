﻿using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;

namespace DatabaseConnector
{
    /// <summary>
    /// База данных.
    /// </summary>
    public class MySqlDatabase
    {
        /// <summary>
        /// Конструктор базы данных.
        /// </summary>
        /// <param name="authorization">Авторизатор.</param>
        /// <exception cref="MySqlException">Выбрасывается, когда нет подключения к базе данных.</exception>
        /// <exception cref="System.Exception">Выбрасывается в остальных некорректных случаях.</exception>
        public MySqlDatabase(Authorization authorization)
        {
            Authorization = authorization;

            try
            {
                var connString =
                    "Server=" + Authorization.Host +
                    ";Database=" + Authorization.DatabaseName +
                    ";Port=" + Authorization.Port +
                    ";Uid=" + Authorization.Username +
                    ";Pwd=" + Authorization.Password +
                    ";persistsecurityinfo=True;SslMode=none;CharSet=utf8";

                Connection = new MySqlConnection(connString);
                Connection.Open();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Нет соединения с базой данных\r\n" + ex, "Ошибка", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("Ошибка приложения\r\n" + ex, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (Connection.State != ConnectionState.Open) Connection = null;
            }
        }

        /// <summary>
        /// Авторизатор.
        /// </summary>
        public Authorization Authorization { get; }

        /// <summary>
        /// Соединение.
        /// </summary>
        public MySqlConnection Connection { get; }

        /// <summary>
        /// Закрыть подключение.
        /// </summary>
        public void CloseConnection()
        {
            Connection?.Close();
        }
    }
}
